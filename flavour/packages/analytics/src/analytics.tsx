/**
 * analytics.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { useEffect }      from 'react';
import { AnalyticsProps } from './types';
import { init }           from './tracker';

export const Analytics = ({ sid }: AnalyticsProps) => {
  useEffect(() => init(sid), [sid]);
  return null;
};