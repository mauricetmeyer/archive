/**
 * tracker.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { AnalyticsGlobal, AnalyticsType } from './types';


const TRACKER:   string = 'https://stats.lavireo.com';
const BLACKLIST: RegExp = /bot|google|baidu|bing|msn|duckduckbot|teoma|slurp|yandex/i;


/**
 * Just a small wrapper to get the global object.
 *
 * @return {AnalyticsGlobal}
 */
function getGlobal () : Window & AnalyticsGlobal
{
  return window as Window & AnalyticsGlobal;
}

/**
 * Transform data into url parameter.
 *
 * @param  {any} data
 * @return {string}
 */
const encode = (data: any) => '?' + Object.keys(data).map(key => (
  encodeURIComponent(key) + "=" + encodeURIComponent(data[key])
)).join('&');


/**
 * Actual event collection
 *
 * @param {any}           data
 * @param {AnalyticsType} type
 */
function track (data: any = {}, type: AnalyticsType)
{
  /**
   * Check if the visitor's user agent matches out blacklist. */
  if (BLACKLIST.test(navigator.userAgent))
    return;

  /**
   * There is no body attached to the site yet,
   * we should wait for the loading of the DOM content. */
  if (null === document.body)
  {
    const fn = () => {
      document.removeEventListener('DOMContentLoaded', fn);
      track(data, type);
    };

    return document.addEventListener('DOMContentLoaded', fn);
  }


  /**
   * Pack data */
  const { location } = window;
  const { referrer } = document;
  const { search, pathname, protocol, hostname } = location;

  const path = pathname + search;
  const host = protocol + "//" + hostname;
  const ref  = referrer && referrer.indexOf(host) < 0 ? referrer : undefined;

  let evData: any = {
    sid: getGlobal().trackId,
    r:   ref,
    p:   path,
    h:   host,
    tz:  Intl.DateTimeFormat().resolvedOptions().timeZone
  };

  /**
   * Send events/errors using beacons. */
  if (type !== AnalyticsType.View)
  {
    evData = { ...evData, t: type, e: data };
    navigator.sendBeacon(`${TRACKER}/beacon` + encode(evData));
    return;
  }

  const { body } = document;
  const img = document.createElement('img');
  img.setAttribute('alt', '');
  img.setAttribute('area-hidden', 'true');
  img.style.position = 'absolute';
  img.src = `${TRACKER}/img` + encode(evData);
  img.addEventListener('load', () => body.removeChild(img));
  body.appendChild(img);
}

/**
 * Initialize the analytics tracker.
 *
 * Example:
 * ```
 * import { init } from '@flavour/analytics';
 * init('sid_123456');
 * ```
 *
 * @param {string} sid
 */
export function init (sid: string)
{
  /**
   * Make sid accessible */
  getGlobal().trackId = sid;

  /**
   * Setup pushstate tracking */
  if (!!history)
  {
    /**
     * Wrap pushState/replaceState functions. */
    const { pushState, replaceState } = history;
    history.pushState = (...args: any) => {
      pushState.apply(history, args);
      track({}, AnalyticsType.View);
    };

    history.replaceState = (...args: any) => {
      replaceState.apply(history, args);
      track({}, AnalyticsType.View);
    };

    /**
     * Listen for popstate. */
    window.addEventListener('popstate', () => {
      track({}, AnalyticsType.View);
    });
  }

  /**
   * Initial page view */
  track({}, AnalyticsType.View);
}

export const event     = (id: string) => track(id, AnalyticsType.Event);
export const exception = (ex: Error)  => track(ex, AnalyticsType.Exception);
