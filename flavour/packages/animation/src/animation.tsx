/**
 * animation.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * (c) Laviréo. All rights reserved.
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 */


import { createElement, Component, ReactNode } from 'react';
import Lottie, { AnimationItem }               from 'lottie-web';


interface AnimationSegment
{
  name?:      string;
  frameEnd:   number;
  frameStart: number;
}

interface AnimationProps
{
  data:      any;
  loop?:     boolean;
  segment?:  number;
  segments?: AnimationSegment[];
}

interface AnimationState
{
  animation?: AnimationItem;
}

export class Animation extends Component<AnimationProps, AnimationState>
{
  state: AnimationState = {};
  private el?: HTMLDivElement;

  /**
   * **INTERNAL**
   * Render the actual content that we resolved.
   */
  render () : ReactNode
  {
    return <div ref={r => r && (this.el = r)} style={{ width: '100px' }} tabIndex={0} />
  }


  /**
   * **INTERNAL**
   * We have to handle prop changes manually to
   * control lottie's web player. */
  componentDidUpdate (prevProps: AnimationProps)
  {
  }


  /**
   * **INTERNAL**
   * Start the animation once the component mounts. */
  componentDidMount = () => this.loadAnimation();

  /**
   * **INTERNAL**
   * Stop the animation when the component will unmount. */
  componentWillUnmount = () => this.stopAnimation();


  private loadAnimation () : void
  {
    const { data } = this.props;
    const animation = Lottie.loadAnimation({
      renderer:      'svg',
      container:     this.el!,
      animationData: data
    });

    this.setState({ animation });
    this.startAnimation();
  }

  /**
   * Stop the timer. */
  private stopAnimation () : void
  {
    const { animation } = this.state;
    if (animation) animation.stop();
  }

  /**
   * Start the timer. */
  private startAnimation () : void
  {
    const { segment, segments } = this.props;
    const { animation }         = this.state;
    if (animation)
    {
      /**
       * Segments */
      if (segment && segments)
      {
        const { frameEnd, frameStart } = segments[segment];
        animation.playSegments([frameStart, frameEnd], false);
        return;
      }

      /**
       * Fill animation */
      animation.play();
    }
  }
}
