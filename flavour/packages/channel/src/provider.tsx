/**
 * channel.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { createElement, useEffect, useCallback } from 'react';
import { ChannelProps }                          from './types';
import { ChannelContext }                        from './context';

export const Connection = ({ url, ...props }: ChannelProps) => {
  const handleMessage = useCallback((ev: MessageEvent) => {

  }, []);

  useEffect(() => {
    /**
     * Check for websocket support
     * before we do anything else. */
    if (!('WebSocket' in window && window.WebSocket))
    {
      /**
       * Well... seems like we can't
       * do anything for this unfortunate user. */
      console.error('WebSockets unavailable...');
    }

    const sock = new WebSocket(url);
    sock.onopen    = () => {};
    sock.onclose   = () => {};
    sock.onerror   = () => {};
    sock.onmessage = handleMessage;

    /**
     * Return disposable so that
     * the socket will close on unmount. */
    return () => sock.close();
  }, [url]);

  return <ChannelContext.Provider {...props} value={} />;
};
