/**
 * types.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * (c) Laviréo. All rights reserved.
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 */


import { ReactNode } from "react";

export enum ChannelMessageType
{
  CONNECTED  = 1,
  DISCONNECT = 2,
  ACK        = 3
}

export interface ChannelProps
{
  url:      string;
  children: ReactNode;
}

export interface ChannelMessage<T extends object>
{
  id:      string;
  type:    ChannelMessageType;
  payload: T;
}

export interface ChannelFn
{
  (message: ChannelMessage<any>) : Promise<void>;
}