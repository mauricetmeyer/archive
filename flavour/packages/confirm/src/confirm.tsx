/**
 * switch.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { createRef, createElement, useState, useEffect, MouseEvent, ReactNode } from 'react';
import { A }                                                                    from '@flavour/text';


interface ConfirmProps
{
  label:   string | ReactNode;
  confirm: string | ReactNode;

  style?:     any;
  className?: string;
  onConfirm:  (ev: MouseEvent) => void;
}

export const Confirm = ({ label, confirm, onConfirm, ...props }: ConfirmProps) => {
  const [isPreClicked, setPreClicked] = useState(false);
  const handleBlur  = () => setPreClicked(false);
  const handleClick = (ev: MouseEvent) => {
    ev.preventDefault();
    if (isPreClicked)
    {
      setPreClicked(false);
      onConfirm(ev);
    }
    else setPreClicked(true);
  };

  /**
   * Add hooks that attach event handlers on the document
   * that are used for the onBlur detection. */
  const root           = createRef<HTMLDivElement>();
  const handleDocClick = (ev: any) => {
    const { current } = root;
    if (!current || current === ev.target || current.contains(ev.target as Node))
      return;

    ev.stopPropagation();
    handleBlur();
    return false;
  };
 
  useEffect(() => {
    if (isPreClicked) document.addEventListener('click', handleDocClick);
    return () => document.removeEventListener('click', handleDocClick);
  }, [isPreClicked]);

  return (
    <A {...props}
      ref={root}
      role="button"
      color={isPreClicked ? "bombay" : "red"}
      onClick={handleClick}>
        {isPreClicked ? confirm : label}
    </A>
  );
};
