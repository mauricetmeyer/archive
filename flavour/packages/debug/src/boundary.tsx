/**
 * boundary.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { createElement, Children, ReactNode } from 'react';
import styled                                 from 'styled-components';
import { Text }                               from '@flavour/text';
import { Colors }                             from '@flavour/theme';


interface BoundaryProps
{
  name:      string;
  color:     "red" | "green" | "yellow";
  enabled?:  boolean;
  children?: ReactNode;
}

interface BoundaryDivProps
{
  color: "red" | "green" | "yellow";
}


const BoundaryName = styled.div`
  top:            -12px;
  right:          0;
  position:       absolute;
  pointer-events: none;
`;

const BoundaryContainer = styled.div<BoundaryDivProps>`
  position:     relative;
  border-width: 12px;
  border-style: solid;
  border-color: ${p => Colors[p.color]};

  & > ${BoundaryName} {
    background-color: ${p => Colors[p.color]};
  }
`;


export const Boundary = ({ name, color, enabled, children }: BoundaryProps) => (
  enabled ? (
    <BoundaryContainer color={color}>
      {children}
      <BoundaryName>
        <Text as="span" color="white" mono>{name}</Text>
      </BoundaryName>
    </BoundaryContainer>
  ) : Children.only(children)
);