/**
 * staffbar.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { createElement } from 'react';
import styled from 'styled-components';


const StaffbarContainer = styled.div`

`;


interface StaffbarProps
{
  enabled?: boolean;
}

export const Staffbar = ({ enabled }: StaffbarProps) => enabled ? (
  <StaffbarContainer>

  </StaffbarContainer>
) : null;