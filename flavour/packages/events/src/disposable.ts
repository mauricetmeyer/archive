/**
 * disposable.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


type DisposableFn = () => void | Promise<void>;
class Disposable
{
  private fn:       DisposableFn;
  private disposed: boolean;


  /**
   * Create disposable.
   *
   * @param  {DisposableFn} fn
   * @return {Disposable}
   */
  constructor (fn: DisposableFn)
  {
    this.fn       = fn;
    this.disposed = false;
  }


  /**
   * Perform the disposal action, indicating that the resource
   * linked to this disposable is no longer needed.
   * 
   * @return {Promise<void>}
   */
  async dispose () : Promise<void>
  {
    if (!this.disposed)
    {
      const res = this.fn();
      await Promise.resolve(res);
      this.disposed = true;
    }
  }
}

export { Disposable };
