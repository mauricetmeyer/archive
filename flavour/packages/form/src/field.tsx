/**
 * validation.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { useMemo, useState, useCallback, useContext } from 'react';
import { FieldProps }                                 from './types';
import { FormContext }                                from './context';

export function Field<T>({ name, children, validations }: FieldProps<T>) {
  const context = useContext(FormContext)!;
  if (!context)
  {
    throw new Error("Field needs to be a descendant of Form.");
  }

  const [ value, setValueInternal ] = useState<T>();
  const setValue = useCallback((value?: T) => {
    setValueInternal(value);
    context.onChange(name, value);
  }, [children, validations]);

  const error = useMemo(() => {
    /**
     * Loop over validations until
     * all succeed or one fails. */
    for (let i = 0; i < validations.length; i++)
    {
      const { validate, message } = validations[i];
      if (!validate(value)) return message;
    }

    return;
  }, [value, validations]);

  return children({ error, setValue });
}
