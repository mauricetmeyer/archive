/**
 * form.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { createElement, FormEvent, useCallback, useMemo } from 'react';
import { FormProps }                                      from './types';
import { FormContext }                                    from './context';

export const Form = ({ children, onSubmit }: FormProps) => {
  const onChange = useCallback(() => {}, []);
  const context = useMemo(() => ({
    onChange
  }), [children]);

  const handleSubmit = useCallback((ev: FormEvent) => {
    ev.preventDefault();
    onSubmit && onSubmit({});
  }, [onSubmit]);

  return (
    <FormContext.Provider value={context}>
      <form onSubmit={handleSubmit}>
        {children}
      </form>
    </FormContext.Provider>
  );
};
