/**
 * types.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { ReactElement, ReactNode } from 'react';

interface FormValues
{
  [key: string]: string;
}

export interface FormProps
{
  children:  ReactNode;
  onSubmit?: (values: FormValues) => void;
}

export interface FormContextProps
{
  onChange: <T>(key: string, value: T) => void;
}

interface Validation<T>
{
  message: string;
  validate: (value?: T) => boolean;
}

interface FieldChildrenProps<T>
{
  error?:   string;
  setValue: (value?: T) => void;
}

export interface FieldProps<T = string>
{
  name:        string;
  children:    (props: FieldChildrenProps<T>) => ReactElement<any>;
  validations: Validation<T>[];
}
