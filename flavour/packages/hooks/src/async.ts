/**
 * async.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { DependencyList, useState, useEffect } from 'react';
import { AsyncFn, AsyncResult }                from './types';

export function useAsync<T> (fn: AsyncFn<T>, deps: DependencyList = []) : AsyncResult<T>
{
  const [ state, setState ] = useState<AsyncResult<T>>({});

  /**
   * Only run on update. */
  useEffect (() => {
    fn().then(
      val => setState({ result: val }),
      err => setState({ error: err })
    );
  }, deps); 

  return state;
}
