/**
 * blur.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * (c) Laviréo. All rights reserved.
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 */


import { useEffect, useCallback, useRef, RefObject } from 'react';
import { BlurFn }                                       from './types'

export function useBlur<T extends HTMLElement> (fn: BlurFn, attached: boolean = true) : RefObject<T>
{
  /**
   * Ref to use for the element that should receive the
   * blur event. */
  const ref = useRef<T>(null);

  /**
   * Memoized callback */
  const handleClick = useCallback((ev: MouseEvent) => {
    const { current } = ref;
    /**
     * The event target is either the ref
     * or a child. */
    if (!current || current === ev.target || current.contains(ev.target as Node))
      return;

    /**
     * Fire the blur event! */
    ev.stopPropagation();
    fn();
    return false;
  }, []);

  /**
   * Add hooks that attach the event handlers on the document
   * that are used for the onBLur detection. */
  useEffect(() => {
    if (attached) document.addEventListener('click', handleClick)
    else document.removeEventListener('click', handleClick);
    return () => document.removeEventListener('click', handleClick);
  }, [attached]);

  return ref;
}
