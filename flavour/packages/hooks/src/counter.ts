/**
 * counter.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * (c) Laviréo. All rights reserved.
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 */


import { useMemo, useState }          from 'react';
import { CounterOpts, CounterResult } from './types';

export function useCounter (val: number, opts: CounterOpts = {}) : CounterResult
{
  if (opts.min) val = Math.max(opts.min, val);
  if (opts.max) val = Math.min(opts.max, val);

  const [ value, setValue ] = useState(val);
  const memoized = useMemo(() => ({
    reset (delta: number = 1)
    {
      setValue(delta);
    },

    increase (delta: number = 1)
    {
      setValue(value + delta);
    },

    decrease (delta: number = 1)
    {
      setValue(value - delta);
    }
  }), [opts.min, opts.max, val]);

  return { value, ...memoized };
}
