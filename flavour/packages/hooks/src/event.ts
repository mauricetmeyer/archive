/**
 * event.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { useEffect } from 'react';
import { EventFn }   from './types';

/**
 * Hook that attaches to an event.
 *
 * @param string     eventType
 * @param EventFn<T> fn
 */
export function useEvent<K extends keyof WindowEventMap> (eventType: K, fn: EventFn<WindowEventMap[K]>)
{
  useEffect(() => {
    window.addEventListener(eventType, fn);
    return () => window.removeEventListener(eventType, fn);
  }, [eventType, fn]);
}
