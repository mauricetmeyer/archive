/**
 * measure.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { useRef, useMemo, useState }  from 'react';
import { MeasureSize, MeasureResult } from './types';
import { useIsomorphic }              from './isomorphic';

export function useMeasure<T extends HTMLElement = HTMLElement> () : MeasureResult<T>
{
  const ref               = useRef<T | null>(null);
  const [ size, setSize ] = useState<MeasureSize>([0, 0]);

  /**
   * Create memoized observer */
  const observer = useMemo(() => new ResizeObserver(e => {
    if (e[0])
    {
      const { width, height } = e[0].contentRect;
      setSize([width, height]);
    }
  }), []);

  /**
   * Only run on element update. */
  useIsomorphic (() => {
    /**
     * Make sure to only run
     * when an element is present. */
    if (!ref.current) return;

    /**
     * Subscribe observer to the current ref
     * and return disconnect function. */
    observer.observe(ref.current);
    return () => observer.disconnect();
  }, [ref]); 

  return [ size, ref ];
}

