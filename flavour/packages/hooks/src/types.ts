/**
 * types.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { RefObject } from 'react';

export interface EnvResult
{
  [key: string]: string;
}

export type BlurFn = () => void;
export type AsyncFn<T> = () => Promise<T>;
export type EventFn<T> = (ev: T) => void;
export interface AsyncResult<T>
{
  result?: T;
  error?:  any;
}

export interface CounterOpts
{
  min?: number;
  max?: number;
}

export interface CounterResult
{
  value:    number;
  reset:    (value?: number) => void;
  increase: () => void;
  decrease: () => void;
}

export type MeasureSize      = [number, number];
export type MeasureResult<T> = [MeasureSize, RefObject<T>];
