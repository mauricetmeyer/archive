/**
 * http.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { is } from './is';


export async function request (method: string, action: string, body?: any)
{
  const headers: any = {};

  /**
   * This makes sure that the body will be discarded
   * when it's isn't supported. */
  let query;
  if (-1 !== ['GET', 'HEAD'].indexOf(method)) {
    if (is.object(body))
    {
      query = Object.keys(body).filter(k => is.number(body[k]) || is.string(body[k])).map(k => {
        return encodeURIComponent(k) + '=' + encodeURIComponent(body[k]);
      })
    }

    body = undefined;
  }

  /**
   * @TODO (Maurice):
   * Better handling of missing parameters,
   * so that we can display missing fields
   * by assigning the `.error` class.
   *
   * for(let i = 0; i < params.length; i++)
   * {
   *   if(data[params[i]] == undefined)
   *     if (options.fail)
   *     {
   *       options.fail("You have to fill out the required argument: " + params[i]);
   *       return;
   *     }
   * }
   */

  /**
   * This transforms the body data
   * to fetch supported values. */
  (() => {
    if (!body) return;

    /**
     * Handle the types that
     * are natively supported by fetch first. */
    if (is.formData(body)
     || is.stream(body)
     || is.buffer(body)
     || is.blob(body)
     || is.file(body))
    {
      return;
    }
  
    /**
     * Handle objects as json. */
    if (is.object(body))
    {
      headers['content-type'] = 'application/json;charset=utf-8';
      body = JSON.stringify(body);
    }
  })();

  /**
   * We wait for the response
   * and transform it the same way we transformed
   * the outgoing data. */
  try
  {
    /**
     * Await the response from the server. */
    action = query ? `${action}?${query}` : action;
    const response = await fetch (action, {
      body,
      method,
      credentials: 'include',
      headers: {
        //'X-CSRF': Config.get('csrf'),
        accept: 'application/json',
        ...headers
      },
    });

    /**
     * Try getting the json data. */
    return response.json();
  }
  catch (err) {}
}

export const GET    = request.bind(null, 'GET');
export const PUT    = request.bind(null, 'PUT');
export const POST   = request.bind(null, 'POST');
export const DELETE = request.bind(null, 'DELETE');
