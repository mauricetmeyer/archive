/**
 * is.js
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


const toString = Object.prototype.toString;
const is = {
  dom:    (val: any) => (val != null) && (val.nodeType != null),
  window: (val: any) => (val != null) && (val == val.window),

  emptyObject (val: any)
  {
    for (const _ in val) return false;
    return true;
  },


  /**
   * Determine if a value is an Array
   *
   * @param  {Object} val
   * @return {boolean}
   */
  array: (val: any) : boolean => (val !== null) && Array.isArray(val),

  /**
   * Determine if a value is a String
   *
   * @param  {Object} val
   * @return {boolean}
   */
  string: (val: any) => typeof val === 'string',

  /**
   * Determine if a value is a Number
   *
   * @param  {Object} val
   * @return {boolean}
   */
  number: (val: any) => typeof val === 'number',

  /**
   * Determine if a value is an Object
   *
   * @param  {Object} val
   * @return {boolean}
   */
  object: (val: any) => val !== null && typeof val === 'object',

  /**
   * Determine if a value is undefined
   *
   * @param  {Object} val
   * @return {boolean}
   */
  undefined: (val: any) => typeof val === 'undefined',

  /**
   * Determine if a value is a Function
   *
   * @param  {Object} val
   * @return {boolean}
   */
  function: (val: any) => typeof val === 'function',


  /**
   * Determine if a value is a Date
   *
   * @param  {Object} val
   * @return {boolean}
   */
  date: (val: any) => toString.call(val) === '[object Date]',

  /**
   * Determine if a value is a File
   *
   * @param  {Object} val
   * @return {boolean}
   */
  file: (val: any) => toString.call(val) === '[object File]',

  /**
   * Determine if a value is a Blob
   *
   * @param  {Object} val
   * @return {boolean}
   */
  blob: (val: any) => toString.call(val) === '[object Blob]',

  /**
   * Determine if a value is a Stream
   *
   * @param  {Object} val
   * @return {boolean}
   */
  stream: (val: any) => is.object(val) && is.function(val.pipe),
  buffer: (val: any) => is.object(val) && is.object(val.constructor) && val.constructor.isBuffer && val.constructor.isBuffer(val),

  /**
   * Determine if a value is a FormData
   *
   * @param  {Object} val
   * @return {boolean}
   */
  formData: (val: any) => (typeof FormData !== 'undefined') && (val instanceof FormData),
};

export { is };
