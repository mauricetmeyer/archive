/**
 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 * This file is automatically generated by the build process.
 * DO NOT CHANGE IT BY HAND or your changes will be lost.
 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 * 
 * (c) Laviréo. All rights reserved.
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 */


import { createElement } from 'react';
import { Icon }          from '../icon';
import { IconProps }     from '../types';

export const GitIcon = (props: IconProps) => (
  <Icon {...props}>
    <svg width="32" height="32" version="1.1"><path d="M31.358 14.597L17.403.642a2.058 2.058 0 00-2.91 0l-2.898 2.899 3.676 3.675a2.444 2.444 0 013.095 3.117l3.543 3.542a2.449 2.449 0 11-1.467 1.38l-3.304-3.303v8.694a2.45 2.45 0 11-2.014-.071V11.8a2.45 2.45 0 01-1.33-3.212l-3.623-3.624-9.569 9.568a2.06 2.06 0 000 2.911l13.956 13.955a2.06 2.06 0 002.91 0l13.89-13.89a2.059 2.059 0 000-2.911" fill="#DE4C36" fillRule="evenodd"/></svg>
  </Icon>
);
