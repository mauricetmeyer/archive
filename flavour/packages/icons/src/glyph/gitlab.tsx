/**
 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 * This file is automatically generated by the build process.
 * DO NOT CHANGE IT BY HAND or your changes will be lost.
 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 * 
 * (c) Laviréo. All rights reserved.
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 */


import { createElement } from 'react';
import { Icon }          from '../icon';
import { IconProps }     from '../types';

export const GitlabIcon = (props: IconProps) => (
  <Icon {...props}>
    <svg width="32" height="32" version="1.1"><g fillRule="nonzero" stroke="none" strokeWidth="1" fill="none"><path d="M31.908 17.837l-1.788-5.505-3.546-10.911c-.182-.561-.976-.561-1.159 0L21.87 12.332H10.098L6.553 1.421c-.183-.561-.977-.561-1.16 0L1.849 12.332.06 17.837A1.219 1.219 0 00.502 19.2l15.482 11.248L31.466 19.2c.427-.31.606-.86.442-1.363" fill="#FC6D26"/><path fill="#E24329" d="M15.984 30.448l5.886-18.116H10.098z"/><path fill="#FC6D26" d="M15.984 30.448l-5.886-18.116h-8.25z"/><path d="M1.849 12.332l-1.79 5.505A1.219 1.219 0 00.503 19.2l15.482 11.248L1.85 12.332z" fill="#FCA326"/><path d="M1.849 12.332h8.249L6.553 1.422c-.183-.562-.977-.562-1.16 0L1.85 12.331z" fill="#E24329"/><path fill="#FC6D26" d="M15.984 30.448l5.886-18.116h8.25z"/><path d="M30.12 12.332l1.788 5.505a1.219 1.219 0 01-.442 1.363L15.984 30.448 30.12 12.332z" fill="#FCA326"/><path d="M30.12 12.332h-8.25l3.545-10.91c.183-.562.977-.562 1.16 0l3.545 10.91z" fill="#E24329"/></g></svg>
  </Icon>
);
