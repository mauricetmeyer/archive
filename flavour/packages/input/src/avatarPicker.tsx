/**
 * avatarPicker.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { createElement, useState, MouseEvent, ReactNode } from 'react';
import styled                                             from 'styled-components';
import { Text }                                           from '@flavour/text';
import { Colors }                                         from '@flavour/theme';
import { Popover }                                        from '@flavour/popover';
import { Confirm }                                        from '@flavour/confirm';

interface AvatarPickerProps
{
  avatar?:        string;
  fallback?:      string;
  onSelectClick?: () => void;
  onDeleteClick?: () => void;

  selectLabel?:   string | ReactNode;
  deleteLabel?:   string | ReactNode;
  confirmLabel?:  string | ReactNode;
}

const Avatar = styled.div<{ src?: string }>`
  width:           22px;
  height:          22px;
  cursor:          pointer;
  border-radius:   11px;
  background:      ${p => `url(${p.src})` || Colors.tuna};
  background-size: cover;
`;

const AvatarContainer = styled.div`
  padding:  11px;
  position: relative;
`;


const AvatarMenuItem = styled.div`
  cursor:        pointer;
  padding:       5px 8px;
  border-radius: 5px;
  transition:    background-color .2s;
  
  &:hover {
    background-color: ${Colors.tuna};
  }
`;

const AvatarConfirmItem = styled(Confirm)`
  cursor:        pointer;
  padding:       5px 8px;
  display:       block;
  border-radius: 5px;
  transition:    background-color .2s;
  
  &:hover {
    background-color: ${Colors.tuna};
  }
`;

export const AvatarPicker = ({ avatar, fallback, onSelectClick, onDeleteClick, selectLabel, deleteLabel, confirmLabel }: AvatarPickerProps) => {
  const [ isOpen, setIsOpen ] = useState(false);
  const handleClick = (ev: MouseEvent) => {
    ev.preventDefault();
    avatar ? setIsOpen(!isOpen) : onSelectClick && onSelectClick();
  }

  const handleItemClick = (ev: MouseEvent, action?: () => void) => {
    ev.preventDefault();
    setIsOpen(false);
    action && action();
  }

  return (
    <AvatarContainer>
      <Avatar src={avatar || fallback} onClick={handleClick} />
      <Popover right bottom isOpen={isOpen} onBlur={() => setIsOpen(false)}>
        <AvatarMenuItem onClick={e => handleItemClick(e, onSelectClick)}>
          <Text color="white" size="small" weight="medium">{selectLabel || 'Upload image'}</Text>
        </AvatarMenuItem>
        <AvatarConfirmItem
          label={deleteLabel || 'Remove'}
          confirm={confirmLabel || 'Sure?'}
          onConfirm={e => handleItemClick(e, onDeleteClick)} />
      </Popover>
    </AvatarContainer>
  );
};

