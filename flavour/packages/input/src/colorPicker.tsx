/**
 * colorPicker.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { createElement, useState, MouseEvent } from 'react';
import styled                                  from 'styled-components';
import { Colors }                              from '@flavour/theme';
import { Popover }                             from '@flavour/popover';


interface ColorPickerProps
{
  color?:    string;
  onBlur?:   () => void;
  onChange?: (color: string) => void;
}

const Color = styled.div<{ color: string }>`
  width:            22px;
  height:           22px;
  cursor:           pointer;
  border-radius:    11px;
  background-color: ${p => p.color};
`;

const ColorItem = styled.div`
  padding: 3px;
`;

const ColorList = styled.div`
  width:     84px;
  display:   flex;
  flex-wrap: wrap;
`;

const ColorContainer = styled.div`
  padding:  11px;
  position: relative;
`;

const COLORS = [
  Colors.tuna,  Colors.bombay, Colors.white,
  Colors.red,   Colors.orange, Colors.yellow,
  //Colors.green, Colors.blue,   Colors.purple
];

export const ColorPicker = ({ color, onBlur, onChange }: ColorPickerProps) => {
  const [ isOpen, setIsOpen ]     = useState(false);
  const [ selected, setSelected ] = useState<string | undefined>(color);
  const handleBlur = () => {
    setIsOpen(false);
    onBlur && onBlur();
  }

  const handleClick = (ev: MouseEvent) => {
    ev.preventDefault();
    setIsOpen(!isOpen);
  }

  const handleChange = (ev: MouseEvent, color: string) => {
    ev.preventDefault();
    setSelected(color);
    setIsOpen(false);
    onChange && onChange(color);
  }

  return (
    <ColorContainer>
      <Color color={selected ? selected : Colors.tuna} onClick={handleClick} />
      <Popover right bottom isOpen={isOpen} onBlur={handleBlur}>
        <ColorList>
          { COLORS.map((c: string) => 
            <ColorItem key={c}>
              <Color color={c} onClick={e => handleChange(e, c)} />
            </ColorItem>
          )}
        </ColorList>
      </Popover>
    </ColorContainer>
  );
};

