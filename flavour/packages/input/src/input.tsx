/**
 * input.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { createElement, ChangeEvent, ReactNode } from 'react';
import styled                                    from 'styled-components';
import { Colors, useTheme }                      from '@flavour/theme';

export interface InputProps
{
  type:         "text" | "email" | "number" | "password";
  value?:       string;
  invalid?:     boolean;
  disabled?:    boolean;
  children?:    ReactNode;
  className?:   string;
  placeholder?: string;
  optionsSize?: number;

  onChange?:    (ev: ChangeEvent<HTMLInputElement>) => void;
}

const InputEl = styled.input<{ isDark?: boolean, hasOptions?: boolean, optionsSize?: number, invalid?: boolean }>`
  color:            ${p => p.isDark ? Colors.white : Colors.onyx};
  width:            100%;
  height:           44px;
  padding:          10px 15px;
  font-size:        16px;
  line-height:      24px;
  border-radius:    9px;
  font-weight:      500;
  transition:       box-shadow .2s;
  background-color: ${p => p.isDark ? Colors.bunker : Colors.light};
  ${p => p.hasOptions && `padding-right: ${p.optionsSize || 44}px;`}

  ${p => p.invalid ? `
    box-shadow: ${Colors.red} 0 0 0 2px inset;
  ` : `
    :focus {
      box-shadow: ${Colors.bombay} 0 0 0 2px inset;
    }

    :disabled,
    ::placeholder {
      color: ${p.isDark ? Colors.tuna : Colors.bombay}
    }
  `}
`;

const InputOption = styled.div`
  top:      0;
  right:    0;
  position: absolute;
`;

const InputContainer = styled.div`
  width:    100%;
  height:   44px;
  position: relative;
`;

export const Input = ({ children, optionsSize, ...props }: InputProps) => {
  const { darkmode } = useTheme();

  if (!children) return <InputEl {...props} isDark={darkmode} />; 
  return (
    <InputContainer>
      <InputEl {...props} hasOptions optionsSize={optionsSize} isDark={darkmode} />
      <InputOption>
        {children} 
      </InputOption>
    </InputContainer>
  );
};
