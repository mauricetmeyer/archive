/**
 * section.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * (c) Laviréo. All rights reserved.
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 */


import { createElement, ReactNode } from 'react';
import { P }                        from '@flavour/text';
import { Spaced }                   from './spaced';


interface SectionProps
{
  label:      string | ReactNode;
  children?:  ReactNode;
  className?: string;
}

/**
 * Section component - Wraps children into an element with a label above.
 * 
 * @NOTE (Maurice):
 * We require that div around the children,
 * as they would each be wrapper by the Spacing component without it.
 *
 * @param {SectionProps} props
 */
export const Section = ({ label, children }: SectionProps) => (
  <Spaced space={16}>
    <P size="xsmall" color="tuna">{label}</P>
    <div>{children}</div>
  </Spaced>
);
