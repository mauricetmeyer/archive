/**
 * spaced.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * (c) Laviréo. All rights reserved.
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 */


import { createElement, ReactNode, Children, Fragment } from 'react';
import styled                                           from 'styled-components';

interface SpacedProps {
  as?:         keyof JSX.IntrinsicElements | React.ComponentType<any>;
  space?:      number;
  header?:     number;
  trailer?:    number;
  horizontal?: boolean;
  children:    ReactNode;
}

interface SpacedChildsProps extends SpacedProps
{
  isHeader:  boolean;
  isTrailer: boolean;
}

const SpacedChild = styled.div<SpacedChildsProps>`
  ${p => !!p.horizontal && `display: inline-block; vertical-align: top;`}
  ${p => `padding-${p.horizontal ? 'left' : 'top'}: ${p.isHeader ? p.header || 0 : (p.space || 0) * .5}px`};
  ${p => `padding-${p.horizontal ? 'right' : 'bottom'}: ${p.isTrailer ? p.trailer || 0 : (p.space || 0) * .5}px`};
`;

const Spaced = ({ as, children, ...props }: SpacedProps) => {
  const len = Children.count(children);
  return (
    <Fragment>
      { Children.map(children, (c, i) =>
        <SpacedChild as={as} {...props} isHeader={i === 0} isTrailer={i === len - 1}>{c}</SpacedChild>)
      }
    </Fragment>
  );
};

export { Spaced };
