/**
 * wrap.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * (c) Laviréo. All rights reserved.
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 */


import styled from 'styled-components';

const WrapSizes = {
  "small": 360,
  "normal": 500,
  "large": 900
}

interface WrapProps
{
  size: "small" | "normal" | "large";
}


export const Wrap = styled.div<WrapProps>`
  width:       100%;
  margin:      0 auto;
  max-width:   ${p => WrapSizes[p.size]}px;
`;
