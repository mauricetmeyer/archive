/**
 * lazy.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * (c) Laviréo. All rights reserved.
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 */


import { createElement, useEffect, useMemo, useRef, useState } from 'react';
import { resolveES6 }                                          from './es6';
import { LazyProps }                                           from './types';

export const Lazy = ({ env, error, loading, resolver, ...props }: LazyProps) => {
  /**
   * Some state stuff */
  const mounted             = useRef<boolean>(false);
  const [module, setModule] = useState<undefined | any>(undefined);
  const [err, setErr]       = useState<undefined | Error>(undefined);

  /**
   * Just some small helper
   * functions that we can use
   * to render the loading and error
   * states. */
  const renderError   = () => error ? error(err) : null;
  const renderLoading = () => loading ? loading() : null;

  /**
   * Execute the resolver once
   * the component mounts. */
  useEffect(() => {
    /**
     * In case we are in the SSR environment,
     * we don't need to resolve anything. */
    env = !env ? typeof window === 'undefined' ? 'node' : 'browser' : env;
    if ("node" === env) return;

    /**
     * Execute the resolver
     * and handle any upcoming errors. */
    resolver().then(
      module => mounted && setModule({ es6: resolveES6(module) }),
      error => {
        /**
         * In case we are in the SSR environment,
         * we'll log warnings to the console. */
        if ("node" === env || !error)
        {
          console.warn("Failed to resolve asyncComponent");
          console.warn(error);
        }

        /**
         * Ohh yeah and we don't needs this either. */
        if ("node" !== env) setErr(err);
      }
    );

    return () => { mounted.current = false };
  }, [resolver]);

  /**
   * Memoized children */
  const children = useMemo(() => {
    if (module)
    {
      /**
       * We can only create our children
       * once the resolver was successful. */
      return createElement(module.es6, props);
    }

    /**
     * Just to shut up the typescript
     * transpiler. */
    return undefined;
  }, [props, module]);

  /**
   * Render different things
   * based on the current state of
   * the resolver. */
  return !!module ? children : !!err ? renderError() : renderLoading();
};