/**
 * types.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * (c) Laviréo. All rights reserved.
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 */


import { ReactNode } from "react";

export interface LazyProps
{
  env?:     "node" | "browser";
  error?:   (err: any) => ReactNode;
  loading?: () => ReactNode;
  resolver: () => Promise<ReactNode>;
  [key: string]: any;
}
