/**
 * actions.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import IntlMessageFormat                from 'intl-messageformat';
import { Dispatch }                     from '@flavour/state';
import { LocaleConfig, LocaleMessages } from './types';

export enum LocaleActions
{
  CHANGE         = 'locale.change',
  CHANGE_SUCCESS = 'locale.change_success',
  CHANGE_FAILURE = 'locale.change_failure'
}


export interface LocaleChangeAction
{
  type: LocaleActions;
}

export interface LocaleChangeFailureAction
{
  type: LocaleActions;
}

export interface LocaleChangeSuccessAction
{
  type:    LocaleActions;
  payload: {
    locale:   string;
    messages: LocaleMessages;
  };
}


export type LocaleAction = LocaleChangeAction | LocaleChangeFailureAction | LocaleChangeSuccessAction;
export const change = (config: LocaleConfig | Promise<LocaleConfig>) =>
{ 
  return (dispatch: Dispatch<LocaleAction>) => {
    /**
     * Making it easier to handle promises */
    dispatch({ type: LocaleActions.CHANGE });
    Promise.resolve(config).then(({ code, data }: any) => {
      /**
       * Parse messages */
      const messages: any = {};
      for (const key in data)
      {
        const val = data[key];
        messages[key] = new IntlMessageFormat(val, code);
      }

      /**
       * Dispatch change actions */
      dispatch({ type: LocaleActions.CHANGE_SUCCESS, payload: {
        locale: code,
        messages
      }});
    }, () => dispatch({ type: LocaleActions.CHANGE_FAILURE }));
  };
};

