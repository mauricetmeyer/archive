/**
 * hooks.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * (c) Laviréo. All rights reserved.
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 */


import { useCallback }           from 'react';
import { useStore, useDispatch } from '@flavour/state';
import { LocaleConfig }          from './types';
import { change }                from './actions';

export function useLocale ()
{
  const { locale } = useStore(s => s.locale);
  const dispatch   = useDispatch();

  /**
   * Memoized callback */
  const setLocale = useCallback((locale: LocaleConfig) => {
    dispatch(change(locale));
  }, []);

  return [ locale, setLocale ];
}

export function useMessage (key: string, args: any)
{
  const { messages } = useStore(s => s.locale);
  return messages[key] ? messages[key].format(args) : key;
}

