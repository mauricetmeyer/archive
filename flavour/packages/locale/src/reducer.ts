/**
 * reducer.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { reducer }       from '@flavour/state';
import { LocaleState }   from './types';
import { LocaleActions } from './actions';


const defaultState: LocaleState = ({
  locale:   'en-US', 
  loading:  false, 
  messages: {}
});

export const localeReducer = reducer<LocaleState>(defaultState, {
  [LocaleActions.CHANGE]:         (state)         => ({...state!, loading: true }),
  [LocaleActions.CHANGE_FAILURE]: (state)         => ({...state!, loading: false }),
  [LocaleActions.CHANGE_SUCCESS]: (state, action) => ({...state!, loading: false, ...(action.payload || {}) })
});
