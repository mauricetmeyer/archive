/**
 * types.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * (c) Laviréo. All rights reserved.
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 */


import { ReactNode } from 'react';

export interface LocaleData
{
  [key: string]: string;
}

export interface LocaleConfig
{
  code: string;
  lang: string;
  data: LocaleData;
}

export interface LocaleMessage<T>
{
  format (args: T) : string;
}

export interface LocaleMessages
{
  [key: string]: LocaleMessage<any>;
}

export interface LocaleProps
{
  children: ReactNode;
}

export interface LocaleState
{
  locale:   string;
  loading:  boolean;
  messages: LocaleMessages;
}
