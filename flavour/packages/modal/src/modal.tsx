/**
 * modal.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * (c) Laviréo. All rights reserved.
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 */


import { createElement }       from 'react';
import styled                  from 'styled-components';
import { PrevIcon, CloseIcon } from '@flavour/icons';
import { useTheme, Colors }    from '@flavour/theme';
import { Portal }              from '@flavour/portal';
import { Spaced }              from '@flavour/layout';

import {
  ModalProps,
  ModalActionProps,
  ModalContentProps,
  ModalContainerProps
} from './types';
import { useScrolllock } from './hooks';

const ModalContainer = styled.div<ModalContainerProps>`
  top:        0;
  left:       0;
  width:      100%;
  height:     100%;
  z-index:    100;
  opacity:    ${p => p.isOpen ? 1 : 0};
  position:   fixed;
  overflow-y: auto;
  transition: opacity .2s;
  background-color: ${p => p.isDark ? Colors.onyx : Colors.white};
`;


const ModalWrapper = styled.div`
  width:            100%;
  display:          flex;
  position:         relative;
  min-height:       100%;
  align-items:      center;
  justify-content:  center;
`;

const ModalActions = styled.div`
  z-index:  2;
  position: relative;
`;

const ModalContent = styled.div<ModalContentProps>`
  width:      100%;
  margin:     0 auto;
  opacity:    ${p => p.isOpen ? 1 : 0};
  position:   relative;
  transform:  translate(0, ${p => p.isOpen ? 0 : -200 }px);
  transition: opacity .2s, transform .3s cubic-bezier(0.12,0.4,0.29, 1);
`;


const ModalAction = styled.button<ModalActionProps>`
  width:            40px;
  height:           40px;
  cursor:           pointer;
  padding:          9px;
  position:         absolute;
  border-radius:    50%;
  background-color: ${p => p.isDark ? Colors.bunker : Colors.light};
`;


export const Modal = ({ isOpen, onPrev, onClose, hasPrev = false, hasClose = true, children }: ModalProps) => {
  const { darkmode } = useTheme();
  useScrolllock(isOpen);

  /**
   * Finally the actual rendering logic. */
  return (
    <Portal zIndex={100}>
      <ModalContainer isDark={darkmode} isOpen={isOpen}>
        <ModalActions>
          { hasPrev &&
            <ModalAction isDark={darkmode} aria-label="Previous modal" onClick={onPrev}>
              <PrevIcon />
            </ModalAction>
          }
          { hasClose &&
            <ModalAction isDark={darkmode} aria-label="Close modals" onClick={onClose}>
              <CloseIcon />
            </ModalAction>
          }
        </ModalActions>
        <ModalWrapper>
          <ModalContent isOpen={isOpen}>
            <Spaced header={50} trailer={50}>
              {children}
            </Spaced>
          </ModalContent>
        </ModalWrapper>
      </ModalContainer>
    </Portal>
  );
};
