/**
 * types.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { ReactNode, MouseEvent } from 'react';

export interface ModalProps
{
  children?:  ReactNode;
  className?: string;

  isOpen?:    boolean;

  hasPrev?:   boolean;
  hasClose?:  boolean;

  onPrev?:    (ev: MouseEvent) => void;
  onClose?:   (ev: MouseEvent) => void;
}

export interface ModalActionProps
{
  isDark?:   boolean;
  children?: ReactNode;
  onClick?:  (ev: MouseEvent) => void;
}

export interface ModalContentProps
{
  isOpen?: boolean;
}

export interface ModalContainerProps
{
  isOpen?: boolean;
  isDark?: boolean;
}
