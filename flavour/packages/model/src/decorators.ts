/**
 * decorators.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * (c) Laviréo. All rights reserved.
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 */


import { Info } from './info';
import {
  Constructor,
  PropertyTypes,
  ModelDecoratorFn,
  PropertyDecoratorFn
} from './types';


/**
 * Register a reference property.
 *
 * @param Object target
 * @param string propertyKey 
 * @param string ref 
 */
function reference (target: Object, propertyKey: string, ref?: string)
{
  Info.registerProperty(target.constructor.name, propertyKey, {
    ref, type: PropertyTypes.REFERENCE
  });
}


/**
 * Hook to define a basic model.
 * 
 * Example:
 * ```
 * import { model, property } from '@flavour/model';
 *
 * @model("user")
 * class User {}
 * ```
 * 
 * @param  string className
 * @return ModelDecoratorFn
 */
export function model (modelName: string) : ModelDecoratorFn
{
  /**
   * Actual decorator functionality */
  return <T extends Constructor>(constructor: T) => {
    /**
     * Make sure to only register
     * the model once. */
    if (!Info.getModel(modelName))
    {
      /**
       * Special property to reference the model
       * by name. */
      constructor.prototype.model = modelName;

      /**
       * Make properties watchable... */
      const properties = Info.getModelProperties(constructor.name);
      for (const name of Object.keys(properties))
      {
        /**
         * Depending on the type choose the
         * observer pattern for property. */
        const { type, isGetter } = properties[name];
        if (type === PropertyTypes.REFERENCE || isGetter)
        {
          /**
           * @TODO (Maurice):
           * Implement computed properties.
           */
        }

        /**
         * @TODO (Maurice):
         * Implement observed properties.
         */
      }

      /**
       * Register the model. */
      Info.registerModel(modelName, constructor);
    }
  }
}

/**
 * Basic hook that will
 * observe the property and automatically sync it.
 * 
 * Example:
 * ```
 * import { model, property } from '@flavour/model';
 *
 * @model("user")
 * class User
 * {
 *   @property()
 *   public id: string;
 * }
 * ```
 * 
 * @param {}
 * @return PropertyDecoratorFn
 */
export function property () : PropertyDecoratorFn
{
  /**
   * Actual decorator functionality */
  return (target, propertyKey) => {
    const desc     = Object.getOwnPropertyDescriptor(target, propertyKey);
    const isGetter = desc !== undefined && undefined !== desc.get;

    Info.registerProperty(target.constructor.name, propertyKey, {
      isGetter, type: PropertyTypes.PROPERTY
    });
  };
}

/**
 * Basic hook that will
 * observe the property and automatically sync it.
 * 
 * Example:
 * ```
 * import { model, oneToMany } from '@flavour/model';
 *
 * @model("country")
 * class Country
 * {
 *   @oneToOne<Capital>()
 *   public capital: Capital;
 * }
 * ```
 *
 * @param  string referenceKey
 * @return PropertyDecoratorFn
 */
export function oneToOne (referenceKey?: string) : PropertyDecoratorFn
{
  /**
   * Actual decorator functionality */
  return (target, propertyKey) => reference(target, propertyKey, referenceKey);
}

/**
 * Basic hook that will
 * observe the property and automatically sync it.
 * 
 * Example:
 * ```
 * import { model, oneToMany } from '@flavour/model';
 *
 * @model("team")
 * class Team
 * {
 *   @oneToMany<Role>()
 *   public roles: Role[];
 * }
 * ```
 *
 * @return PropertyDecoratorFn
 */
export function oneToMany () : PropertyDecoratorFn
{
  /**
   * Actual decorator functionality */
  return (target, propertyKey) => {
    const desc     = Object.getOwnPropertyDescriptor(target, propertyKey);
    const isGetter = desc !== undefined && undefined !== desc.get;
    Info.registerProperty(target.constructor.name, propertyKey, {
      isGetter, type: PropertyTypes.REFERENCE
    });
  };
}

/**
 * Basic hook that will
 * observe the property and automatically sync it.
 * 
 * Example:
 * ```
 * import { model, manyToOne } from '@flavour/model';
 *
 * @model("user")
 * class User
 * {
 *   @manyToOne<Team>()
 *   public team: Team;
 * }
 * ```
 * 
 * @param  string referenceKey
 * @return PropertyDecoratorFn
 */
export function manyToOne (referenceKey?: string) : PropertyDecoratorFn
{
  /**
   * Actual decorator functionality */
  return (target, propertyKey) => reference(target, propertyKey, referenceKey);
}

/**
 * Basic hook that will
 * observe the property and automatically sync it.
 * 
 * Example:
 * ```
 * import { model, manyToMany } from '@flavour/model';
 *
 * @model("user")
 * class User
 * {
 *   @manyToMany<Space>()
 *   public space: Space;
 * }
 * ```
 * 
 * @param  string referenceKey
 * @return PropertyDecoratorFn
 */
export function manyToMany (referenceKey?: string) : PropertyDecoratorFn
{
  /**
   * Actual decorator functionality */
  return (target, propertyKey) => {
    Info.registerProperty(target.constructor.name, propertyKey, {
      ref: referenceKey, type: PropertyTypes.PROPERTY, isGetter: false
    });
  };
}