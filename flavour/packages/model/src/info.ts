/**
 * info.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * (c) Laviréo. All rights reserved.
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 */


import {
  Model,
  Property,
  ModelMap,
  ModelPropertyMap
} from './types';

class InfoStore
{
  private _models:     ModelMap         = {};
  private _properties: ModelPropertyMap = {};

  /**
   * Get model class
   *
   * @param  string name
   * @return Model
   */
  public getModel (name: string)
  {
    return this._models[name];
  }

  /**
   * Get model properties
   *
   * @param  string name
   * @return Properties
   */
  public getModelProperties (name: string)
  {
    return this._properties[name];
  }

  /**
   * Register a model
   * 
   * @param Model model
   */
  public registerModel<T extends Model> (name: string, model: T) : void
  {
    this._models[name] = model;
  }

  /**
   * Register a model property
   * 
   * @param Model    model
   * @param Property property
   */
  public registerProperty (model: string, name: string, property: Property) : void
  {
    const properties = this._properties[model] || {};
    properties[name] = property;
    this._properties[model] = properties;
  }
}

export const Info = new InfoStore();