/**
 * types.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * (c) Laviréo. All rights reserved.
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 */


export interface Model
{

}

export interface Property
{
  ref?:      string;
  type:      PropertyTypes;
  isGetter?: boolean;
}

export interface PropertyMap
{
  [key: string]: Property;
}

export interface ModelMap
{
  [key: string]: Model;
}

export interface ModelPropertyMap
{
  [key: string]: PropertyMap;
}

export interface Constructor
{
  new (...args: any[]): any;
}


export enum PropertyTypes
{
  PROPERTY,
  REFERENCE
}


export interface ModelDecoratorFn
{
  <T extends Constructor>(Constructor: T) : void;
}

export interface PropertyDecoratorFn
{
  (target: Object, propertyKey: string) : void;
}