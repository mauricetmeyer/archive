/**
 * popover.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { createElement }                       from 'react';
import styled, { css }                         from 'styled-components';
import { useBlur }                             from '@flavour/hooks';
import { useTheme, Colors }                    from '@flavour/theme';
import { PopoverProps, PopoverContainerProps } from './types';

const PopoverOpenCss = css`
  opacity:    1;
  transform:  translateY(0);
  visibility: visible;
`;

const PopoverClosedCss = css`
  opacity:    0;
  visibility: hidden;
`;

const PopoverContainer = styled.div<PopoverContainerProps>`
  z-index:          100;
  padding:          8px;
  outline:          none;
  position:         absolute;
  background:       ${p => p.isDark ? Colors.bunker : Colors.white};
  white-space:      nowrap;
  box-shadow:       0 3px 8px 0 rgba(0,0,0,.2);
  border-radius:    9px;

  transition-duration:        .3s;
  transition-timing-function: cubic-bezier(.68, -.55, .265, 1.55);


  ${p => p.top ? `
    bottom:           100%;
    margin-bottom:    4px;
    transform:        translateY(15px);
    transform-origin: bottom;
  ` : `
    top:              100%;
    margin-top:       4px;
    transform:        translateY(-15px);
    transform-origin: top;
  `}

  ${p => p.left && `
    left:  0;
    right: auto;
  `}

  ${p => p.right && `
    left:  auto;
    right: 0;
  `}

  ${p => p.isFull && `width: 100%;`}
  ${p => p.isOpen ? PopoverOpenCss : PopoverClosedCss}
`;

export const Popover = ({ isOpen, onBlur, children, ...props }: PopoverProps) => {
  const { darkmode } = useTheme();
  const ref          = useBlur<HTMLDivElement>(() => onBlur && onBlur(), isOpen);

  const opts = { isOpen, ...props };
  return (
    <PopoverContainer ref={ref} tabIndex={-1} {...opts} isDark={darkmode}>
      {children}
    </PopoverContainer>
  );
};

