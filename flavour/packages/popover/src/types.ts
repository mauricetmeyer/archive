/**
 * types.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { ReactNode } from 'react';

export interface PopoverProps
{
  isFull?:  boolean;
  isOpen?:  boolean;

  top?:     boolean;
  left?:    boolean;
  right?:   boolean;
  bottom?:  boolean;

  children: ReactNode;
  onBlur?:  () => void;
}

export interface PopoverContainerProps
{
  isFull?:  boolean;
  isOpen?:  boolean;
  isDark?:  boolean;

  top?:     boolean;
  left?:    boolean;
  right?:   boolean;
  bottom?:  boolean;
}

