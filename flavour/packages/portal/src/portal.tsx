/**
 * portal.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * (c) Laviréo. All rights reserved.
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 */


import { Component, ReactNode } from 'react';
import { createPortal }         from 'react-dom';


const createParent = () => {
  const parentElement = document.querySelector<HTMLElement>('body > .portals');
  if (!parentElement) {
    const parent = document.createElement('div');
    parent.setAttribute('class', 'portals');
    document.body.appendChild(parent);
    return parent;
  }
  return parentElement;
};

const createContainer = (zIndex: number | string) => {
  const container = document.createElement('div');
  container.setAttribute('class', 'portal');
  container.setAttribute('style', `z-index: ${zIndex};`);
  return container;
};


interface PortalProps
{
  children: ReactNode;
  zIndex:   number | string;
}

interface PortalState
{
  parent:    HTMLElement;
  container: HTMLElement;
}

class Portal extends Component<PortalProps, PortalState>
{
  state = {
    parent:    createParent(),
    container: createContainer(this.props.zIndex)
  };


  componentDidMount ()
  {
    const { parent, container } = this.state;
    if (container) parent.appendChild(container);
  }

  componentWillUnmount ()
  {
    const { parent, container } = this.state;
    if (container) parent.removeChild(container);
  }

  componentDidUpdate (prevProps: PortalProps)
  {
    const { zIndex }            = this.props;
    const { parent, container } = this.state;
    if (container && prevProps.zIndex !== zIndex)
    {
      const newContainer = createContainer(zIndex);
      parent.replaceChild(container, newContainer);
      this.setState({ container: newContainer });
    }
  }


  /**
   * **INTERNAL**
   * Render the portal into the container. */
  render()
  {
    const { container } = this.state;
    return container && createPortal(this.props.children, container);
  }
}

export { Portal };
