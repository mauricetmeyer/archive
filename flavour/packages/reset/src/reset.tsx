/**
 * reset.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * (c) Laviréo. All rights reserved.
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 */


import { createGlobalStyle } from 'styled-components';
import { Fonts }             from '@flavour/theme';

export const Reset = createGlobalStyle`
html,
Body,
button,
div,
h1,
h2,
h3,
h4,
h5,
h6,
p,
a,
img,
small,
ol,
ul,
li,
hr,
form,
label,
article,
canvas,
footer,
header,
head,
section,
audio,
video,
input,
textarea,
pre,
code,
dt,
dd
{
  margin:  0;
  border:  0;
  padding: 0;
}

div,
section,
article,
head,
ul,
li,
p,
a,
h1,
h2,
h3,
h4,
h5,
h6,
button,
input,
textarea
{
  -webkit-font-smoothing: antialiased;
     -moz-font-smoothing: antialiased;
          font-smoothing: antialiased;
}

.unselectable
{
  -webkit-user-select: none;
     -moz-user-select: none;
      -ms-user-select: none;
       -o-user-select: none;
          user-select: none;
}

pre,
kbd,
code
{
	tab-size:    2;
  font-family: ${Fonts.mono};
}


a,
input,
button,
select,
textarea,
div[contenteditable]
{
  border:     0;
  outline:    none;
  appearance: none;
  background: transparent;
}

a {
  text-decoration: none;
}

li { list-style: none }

img,
svg,
video
{
  vertical-align: middle;
}

img,
video
{
  width:     100%;
  max-width: 100%;
}

html,
body
{
  width:      100%;
  min-height: 100%;
}

html
{
  font-size: 1em;
}

body
{
  font-family: ${Fonts.normal};
}

*, ::before, ::after
{
  box-sizing: border-box;
}
`;
