/**
 * actions.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { navigate, getLocation } from './helpers';

export enum RouterActions
{
  CHANGE = 'router.change'
}

export const change = () => ({
  type:    RouterActions.CHANGE,
  payload: getLocation()
})

export const push = (to: string, state?: any) => {
  navigate(to, state);
  return change();
};

export const replace = (to: string, state?: any) => {
  navigate(to, state, true);
  return change();
};
