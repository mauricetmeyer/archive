/**
 * helpers.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { RouterLocation } from './types';

export function getLocation () : RouterLocation
{
  const { pathname, search, hash } = window.location;
  return {
    pathname,
    search,
    hash,
    state: window.history.state,
    key: (window.history.state && window.history.state.key) || "initial"
  };
}

export function navigate (to: string, state?: any, replace: boolean = false)
{
  const transitioning = false;
  state = { ...state, key: Date.now() + "" };

  try {
    /**
     * Catch iOS Safari limits to 100 pushState calls. */
    const type = transitioning || replace ? 'replaceState' : 'pushState';
    window.history[type](state, '', to);
  } catch (e) {
    window.location[replace ? "replace" : "assign"](to);
  }
}

export const parse = ({ path, value }: any) => {
  const segments = stripSlashes(path).split('/');
  return { path, value, segments };
};

export const match = (uri: string, routes: any[]) => {
  const [ uriPathname ] = uri.split("?");
  const uriSegments = stripSlashes(uriPathname).split('/');
  
  /**
   * Loop over routes */
  for (let i = 0; i < routes.length; i++)
  {
    let params: any = {};
    let missed = false;

    const { path, value, segments } = routes[i];

    /**
     * Loop over segments */
    const max = Math.max(segments.length, uriSegments.length);
    for (let i = 0; i < max; i++)
    {
      const uriSegment   = uriSegments[i];
      const routeSegment = segments[i];

      /**
       * Found wildscard */
      if (routeSegment && routeSegment[0] === '*')
      {
        const param = routeSegment.slice(1) || "*";
        return {
          path,
          value,
          params: { [param]: uriSegments.slice(i).join('/') },
        }
      }

      /**
       * Continue to the next route. */
      if (uriSegment === undefined) {
        missed = true;
        break;
      }

      const dynamic = /^:(.+)/.exec(routeSegment);
      if (dynamic) {
        params[dynamic[1]] = uriSegment;
      }
      else if (routeSegment !== uriSegment) {
        missed = true;
        break;
      }
    }

    if (!missed)
    {
      return { path, value, params };
    }
  }

  return undefined;
};

export const stripSlashes = (str: string) => str.replace(/(^\/+|\/+$)/g, "");
