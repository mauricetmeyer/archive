/**
 * hooks.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { useStore, useDispatch } from '@flavour/state';
import { push, replace }         from './actions';

export function useLocation ()
{
  const { location } = useStore(s => s.router);
  return location;
}

export function useRouter ()
{
  const location = useLocation();
  const dispatch = useDispatch();

  return {
    push:    (to: string) => dispatch(push(to)), 
    replace: (to: string) => dispatch(replace(to)),
    location
  };
}
