/**
 * reducer.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { reducer }       from '@flavour/state';
import { RouterState }   from './types';
import { getLocation }   from './helpers';
import { RouterActions } from './actions';


const defaultState: RouterState = ({ base: '/', location: getLocation() });
export const routerReducer = reducer<RouterState>(defaultState, {
  [RouterActions.CHANGE]: (state, action) => Object.assign({}, state, { location: action.payload })
});
