/**
 * router.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { Children, useMemo, createElement, cloneElement, useContext } from 'react';
import { useEvent }                                       from '@flavour/hooks';
import { useStore, useDispatch }                          from '@flavour/state';
import { RouterProps }                                    from './types';
import { change }                                         from './actions';
import { RouterContext } from './context';
import { match, parse, stripSlashes } from './helpers';


export const Router = ({ children }: RouterProps) => {
  const { location } = useStore(s => s.router);
  const basepath     = useContext(RouterContext);
  const dispatch     = useDispatch();

  /**
   * Mount event handler for the popstate
   * window event */
  useEvent('popstate', () => {
    /**
     * @TODO (Maurice):
     * Use react deferred update API when facebook/react/issues/13306 is ready */
    dispatch(change());
  });

  /**
   * Transforms children to routes. */
  const routes = useMemo(() => Children.toArray(children).map((c: any) => {
    let { path } = c.props;
    if (!path) throw new Error("Router child is missing `path` prop!");

    path = path === '/' ? basepath : `${stripSlashes(basepath)}/${stripSlashes(path)}`
    return parse({ path, value: c });
  }), [children]);

  /**
   * Get the matching route with params. */
  const route = useMemo(() => {
    const route = match(location.pathname, routes);
    if (!route) return;

    const { path, ...props } = route;
    return { ...props, path: path.replace(/\*$/, '') };
  }, [routes, location.pathname]);

  /**
   * Render fallback view. */
  if (!route) return null;
  const { path, value, params } = route;
  return (
    <RouterContext.Provider value={path}>
      {cloneElement(value, params)}
    </RouterContext.Provider>
  );
};
