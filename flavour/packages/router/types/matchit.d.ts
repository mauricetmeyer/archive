/**
 * matchit.d.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * (c) Laviréo. All rights reserved.
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 */


declare module 'matchit' {
  export type MatchArgs = { [key: string]: string };
  export type MatchRoute = {
    old:  string,
    val:  string,
    type: number
  };

  export function exec (cmd: string, routes: MatchRoute[]) : MatchArgs;
  export function match (cmd: string, routes: MatchRoute[]) : MatchRoute[];
  export function parse (cmd: string) : MatchRoute;
}