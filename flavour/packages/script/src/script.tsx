/**
 * script.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * (c) Laviréo. All rights reserved.
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 */


import { Component } from 'react';

const ScriptsLoaded  = new Set<string>();
const ScriptsErrored = new Set<string>();
interface ScriptProps
{
  url: string;

  onLoad?:   () => void;
  onError?:  () => void;
  onCreate?: () => void;
}

class Script extends Component<ScriptProps>
{
  /**
   * **INTERNAL**
   * Checks if the script is already loaded, otherwise
   * creates a new script tag. */
  componentDidMount ()
  {
    const { url, onLoad, onError } = this.props;
    if (ScriptsLoaded.has(url)) onLoad && onLoad();
    else if (ScriptsErrored.has(url)) onError && onError();
    else this.createScript();
  }


  /**
   * **INTERNAL**
   * This won't actually render anything except adding a script
   * tag to the page. */
  render()
  {
    return null;
  }


  private createScript ()
  {
    const { url, onLoad, onError, onCreate } = this.props;

    /**
     * Call onCreate callback if
     * it is set. */
    if (onCreate) onCreate();

    /**
     * Prepare script tag */
    const el = document.createElement('script');
    el.src   = url;
    el.async = true;

    /**
     * Helper for the callbacks.
     * This will add the script url to the `Scripts` set so
     * that we can check if it was already loaded.
     *
     * @param {function} handle
     */
    const handler = (set: Set<string>, handle?: () => void) => {
      return () => {
        set.add(url);
        handle && handle();
      };
    };

    /**
     * Attach event handlers. */
    el.onload  = handler(ScriptsLoaded,  onLoad);
    el.onerror = handler(ScriptsErrored, onError);

    /**
     * Append to the body. */
    document.body.appendChild(el);
  }
}

export { Script };
