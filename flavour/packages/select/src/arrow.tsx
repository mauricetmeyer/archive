/**
 * arrow.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { createElement } from 'react';
import { Colors }        from '@flavour/theme';

export const Arrow = (props: any) => (
  <svg width="22px" height="22px" viewBox="0 0 22 22" {...props}>
    <g id="Icons" stroke="none" strokeWidth="1" fill={Colors.tuna} fillRule="evenodd">
      <path d="M6.99539757,13.3431458 C6.44565467,13.3431458 6,12.8992809 6,12.3431458 C6,11.790861 6.4556644,11.3431458 6.99539757,11.3431458 L14,11.3431458 L14,4.33854332 C14,3.82806777 14.3827202,3.40734177 14.8826674,3.34984252 L15,3.34314575 C15.5522847,3.34314575 16,3.79881015 16,4.33854332 L16,12.3477482 C16,12.8387638 15.6459031,13.2467422 15.1739825,13.3283048 C15.1189529,13.338037 15.0622573,13.3431458 15.0046024,13.3431458 L6.99539757,13.3431458 Z" id="path-1" transform="translate(11.000000, 8.343146) rotate(-315.000000) translate(-11.000000, -8.343146) "></path>
    </g>
  </svg>
);
