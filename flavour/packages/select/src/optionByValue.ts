/**
 * optionByValue.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { SelectOption } from './types';

export const optionByValue = (options: SelectOption[], value?: string) => {
  if (!value) return;
  for (let i = 0; i < options.length; i++)
    if (options[i].value === value) return options[i];
  return;
};
