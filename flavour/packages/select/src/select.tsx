/**
 * select.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { createElement, MouseEvent, FocusEvent, useMemo, useState, Fragment } from 'react';
import styled                                                       from 'styled-components';
import { Text }                                                     from '@flavour/text';
import { useTheme, Colors }                                         from '@flavour/theme';
import { Popover }                                                  from '@flavour/popover';
import { optionByValue }                                            from './optionByValue';
import { Arrow }                                                    from './arrow';
import { SelectProps, SelectOption }                                from './types';

const SelectItemContainer = styled.div<{ isDark?: boolean, isActive?: boolean }>`
  cursor:        pointer;
  padding:       5px 8px;
  border-radius: 5px;
  transition:    background-color .2s;

  ${p => p.isActive && `background-color: ${p.isDark ? Colors.tuna : Colors.light};`}
  &:hover {
    background-color: ${p => p.isDark ? Colors.tuna : Colors.light};
  }
`;

const SelectItem = ({ label, description }: SelectOption) => {
  const { darkmode } = useTheme();
  return (
    <Fragment>
      <Text color={darkmode ? Colors.white : Colors.black} size="small" weight="medium">{label}</Text>
      { description && <Text color={darkmode ? Colors.bombay : Colors.grey} size="small" weight="medium">{description}</Text> }
    </Fragment>
  );
}


const SelectInput = styled.div<{ isDark?: boolean, isOpen?: boolean, invalid?: boolean, disabled?: boolean }>`
  cursor:           pointer;
  display:          flex;
  width:            100%;
  height:           44px;
  padding:          11px 15px;
  border-radius:    9px;
  transition:       box-shadow .2s;
  background-color: ${p => p.isDark ? Colors.bunker : Colors.light};

  ${p => p.invalid ? `
    box-shadow: ${Colors.red} 0 0 0 2px inset;
  ` : p.isOpen && !p.disabled && `
    box-shadow: ${Colors.bombay} 0 0 0 2px inset;`
  }
`;

const SelectValue = styled.div<{ isDark?: boolean, isOpen?: boolean, isDisabled?: boolean, isSelected: boolean }>`
  color:       ${p => (p.isSelected && !p.isDisabled) ? p.isDark ? Colors.white : Colors.black : p.isDark ? Colors.tuna : Colors.bombay};
  width:       100%;
  font-size:   16px;
  line-height: 22px;
  font-weight: 500;
`;

const SelectHidden = styled.div`
  height:   0;
  overflow: hidden;
  position: relative;
`;

const SelectTrigger = styled.input`
  left:      -100px;
  width:     1px;
  margin:    0;
  padding:   0;
  opacity:   0;
  position:  relative;
  transform: scale(0);
`;

const SelectArrow = styled(Arrow)`
  flex-shrink: 0;
`;

const SelectContainer = styled.div`
  position: relative;
`;

const SelectOptions = styled.div`
  max-height: 200px;
  overflow-y: auto;
`;


export const Select = ({ value, options, placeholder, keepOpen, invalid, disabled, item, onBlur, onChange }: SelectProps) => {
  const { darkmode } = useTheme();
  const defaultValue = useMemo(() => optionByValue(options, value), [value]);
  const [ selected, setSelected ] = useState<SelectOption | undefined>(defaultValue);
  const [ focused, setFocused ]   = useState<SelectOption | undefined>(selected);
  const [ isOpen, setIsOpen ]     = useState(false);

  const isCurrent = ({ value }: SelectOption) => {
    const current = focused || selected;
    return current && value === current.value;
  }

  const handleBlur = () => {
    setIsOpen(false); 
    setFocused(undefined);
    onBlur && onBlur();
  }

  const handleTriggerBlur = (ev: FocusEvent) => {
    if (isOpen) return ev.preventDefault();
    //setIsOpen(true); 
    //onFocus && onFocus();
  }

  const handleTriggerFocus = () => {
    setIsOpen(true); 
    //onFocus && onFocus();
  }

  const handleKeyDown = (ev: any) => {
    switch (ev.key) {
      case 'Tab':
        setIsOpen(false);
        setFocused(undefined);
        break;

      case 'Escape':
        setIsOpen(false);
        setFocused(undefined);
        ev.preventDefault();
        break;

      case 'Enter':
        setIsOpen(false);
        setSelected(focused);
        ev.preventDefault();
        break;

      case 'ArrowUp': {
        let idx = focused ? options.indexOf(focused) : 0;
        if (idx > 0) --idx;
        setFocused(options[idx]);
        ev.preventDefault();
        break;
      }

      case 'ArrowDown': 
        const len = options.length;
        let   idx = focused ? options.indexOf(focused) : len - 1;
        if (idx < len - 1) ++idx;
        setFocused(options[idx]);
        ev.preventDefault();
        break;
    }
  }

  const handleItemClick = (ev: MouseEvent, opt: SelectOption) => {
    ev.preventDefault();

    /**
     * Check if item is already selected,
     * if it isn't fire the onChange event. */
    if (!selected || selected.value !== opt.value)
    {
      !keepOpen && setIsOpen(false);
      setFocused(undefined);
      setSelected(opt);
      onChange && onChange(opt.value);
    }
  }

  return (
    <SelectContainer onKeyDown={handleKeyDown}>
      <SelectHidden>
        <input type="hidden" />
        <SelectTrigger readOnly tabIndex={0} onBlur={handleTriggerBlur} onFocus={handleTriggerFocus} />
      </SelectHidden>
      <SelectInput
          isDark={darkmode}
          isOpen={isOpen && !disabled}
          invalid={invalid}
          disabled={disabled}
          onClick={() => setIsOpen(!isOpen)}>
        <SelectValue isSelected={!!selected}>
          {!!selected ? selected.label : placeholder}
        </SelectValue>
        <SelectArrow />
      </SelectInput>
      <Popover bottom isFull isOpen={isOpen && !disabled} onBlur={handleBlur}>
        <SelectOptions>
          { 
            options.map((o: any) => (
              <SelectItemContainer key={o.value} isDark={darkmode} onClick={e => handleItemClick(e, o)} isActive={isCurrent(o)}>
                { item ? createElement(item, o) : <SelectItem {...o} /> }
              </SelectItemContainer>
            ))
          }
        </SelectOptions>
      </Popover>
    </SelectContainer>
  );
};
