/**
 * types.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { ReactNode, ComponentType } from 'react';

export interface SelectOption
{
  value: string;
  label: string | ReactNode;
  [key: string]: any;
}

export interface SelectProps
{
  options:      SelectOption[];
  value?:       string;
  className?:   string;
  placeholder?: string;

  invalid?:     boolean;
  disabled?:    boolean;
  keepOpen?:    boolean;

  item?:        ComponentType<SelectOption>;
  onBlur?:      () => void;
  onChange?:    (key: string) => void;
}

