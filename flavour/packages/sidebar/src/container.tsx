/**
 * container.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * (c) Laviréo. All rights reserved.
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 */


import { createElement }         from 'react';
import sc                        from 'styled-components';

import { SidebarContainerProps } from './types';

const SidebarContainerDiv = sc.div`
  height:         100vh;
  display:        flex;
  flex-direction: row;
`;

export const SidebarContainer = ({ children }: SidebarContainerProps) => {
  return (
    <SidebarContainerDiv>
      {children}
    </SidebarContainerDiv>
  );
};
