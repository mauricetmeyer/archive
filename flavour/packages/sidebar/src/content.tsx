/**
 * content.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * (c) Laviréo. All rights reserved.
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 */


import sc from 'styled-components';

export const Content = sc.div`
  flex-grow:     1;  
  flex-shrink:   1;  
  padding-right: 16px;
`;
