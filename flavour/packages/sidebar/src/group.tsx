/**
 * group.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * (c) Laviréo. All rights reserved.
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 */


import { createElement }     from 'react';
import sc                    from 'styled-components';
import { Text }              from '@flavour/text';
import { Spaced }            from '@flavour/layout';
import { SidebarGroupProps } from './types';

const SidebarGroupLabel = sc(Text)`
  padding: 8px 16px;
`;

export const SidebarGroup = ({ label, color, children }: SidebarGroupProps) => (
  <Spaced trailer={24}>
    <SidebarGroupLabel size="small" color={ color || "grey" }>{label}</SidebarGroupLabel>
    {children}
  </Spaced>
);


