/**
 * item.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * (c) Laviréo. All rights reserved.
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 */


import { createElement }    from 'react';
import sc                   from 'styled-components';
import { useTheme, Colors } from '@flavour/theme';

const SidebarItemEl = sc.a<any>`
  display:       block; 
  padding:       8px 16px;
  position:      relative;
  border-radius: 9px;

  ${p => p.isActive && `background-color: ${p.isDark ? Colors.bunker : Colors.light};`}

  &:hover {
    background-color: ${p => p.isDark ? Colors.bunker : Colors.light};
  }
`;

export const SidebarItem = (props: any) => {
  const { darkmode } = useTheme();
  return <SidebarItemEl {...props} isDark={darkmode} />;
};
