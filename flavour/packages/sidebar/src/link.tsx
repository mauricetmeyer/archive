/**
 * link.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * (c) Laviréo. All rights reserved.
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 */


import { createElement, useCallback } from 'react';
import { Text }                       from '@flavour/text';
import { useTheme, Colors }           from '@flavour/theme';
import { useRouter }                  from '@flavour/router';
import { SidebarItem }                from './item';
import { SidebarLinkProps }           from './types';


export const SidebarLink = ({ to, children }: SidebarLinkProps) => {
  const { push, location } = useRouter();
  const { darkmode }       = useTheme();
  const isActive           = location.pathname === to;

  const handleClick  = useCallback ((ev: MouseEvent) => {
    ev.preventDefault();
    push(to);
  }, []);

  return (
    <SidebarItem href={to} isActive={isActive} onClick={handleClick}>
      <Text as="span" size="small" color={darkmode ? Colors.white : Colors.black}>
        {children}
      </Text>
    </SidebarItem>
  );
};

