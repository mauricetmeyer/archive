/**
 * signature.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * (c) Laviréo. All rights reserved.
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 */


import { createElement } from 'react';
import styled            from 'styled-components';
import { Text }          from '@flavour/text';
import { Colors }        from '@flavour/theme';
import { Spaced }        from '@flavour/layout';


const SignatureLink = styled.a`
  color:       ${Colors.tuna};
  font-size:   16px;
  font-weight: 500;
  line-height: 24px;

  &:hover,
  &:active
  {
    color: ${Colors.white};
  }
`;

const SignatureHeart = styled.i`
  &::before {
    content:     '\\2665';
    display:     inline-block;
    font-size:   20px;
    font-style:  normal;
    font-weight: 400;
  }
`;

export const Signature = () => (
  <Spaced header={10} trailer={10}>
    <Text align="center">
      <SignatureLink href="https://lavireo.com" target="_blank">
        Made with <SignatureHeart /> in Germany
      </SignatureLink>
    </Text>
  </Spaced>
);