/**
 * combine.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * (c) Laviréo. All rights reserved.
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 */


import {
  Reducer,
  ReducerMap,
  StateFromReducerMap,
  ActionFromReducerMap
} from './types';

export function combine<M extends ReducerMap> (reducers: M) : Reducer<
  StateFromReducerMap<M>, 
  ActionFromReducerMap<M>
>
export function combine(reducers: ReducerMap)
{
  const keys = Object.keys(reducers);
  return (
    state: StateFromReducerMap<typeof reducers> = {},
    action: ActionFromReducerMap<typeof reducers>
  ) => {
    const nextState: StateFromReducerMap<typeof reducers> = {};
    for (const key of keys)
    {
      const reducer         = reducers[key];
      const prevStateForKey = state[key];
      const nextStateForKey = reducer(prevStateForKey, action);
      nextState[key] = nextStateForKey;
    }

    return nextState;
  };
}
