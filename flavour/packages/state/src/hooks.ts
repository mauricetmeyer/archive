/**
 * hooks.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * (c) Laviréo. All rights reserved.
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 */


import { useState, useEffect, useContext } from 'react';
import { Action, Dispatch, SelectorFn }    from './types';
import { StateContext }                    from './context';

export function useStore<T> (selector: SelectorFn<T>) : T
{
  const store = useContext(StateContext);

  /**
   * State 'n' stuff */
  const rawState = store.getState();
  const [ state, setState ] = useState<T>(selector(rawState));

  /**
   * Handle subscription to the
   * store */
  useEffect(() => {
    const checkForUpdates = () => {
      const rawState = store.getState();
      const state    = selector(rawState);
      setState(state);
    };

    checkForUpdates();
    return store.subscribe(checkForUpdates);
  }, [store]);

  return state;
}

export function useDispatch<A extends Action> () : Dispatch<A>
{
  const { dispatch } = useContext(StateContext);
  return dispatch;
}

