/**
 * provider.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * (c) Laviréo. All rights reserved.
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 */


import { createElement, useMemo } from 'react';
import { Store }                  from './store';
import { StateProps }             from './types';
import { StateContext }           from './context';

export const State = ({ initial, reducer, children }: StateProps) => {
  const store = useMemo(() => new Store(reducer, initial), [reducer]);

  return (
    <StateContext.Provider value={store}>
      {children}
    </StateContext.Provider>
  );
};
