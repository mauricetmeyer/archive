/**
 * reducer.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * (c) Laviréo. All rights reserved.
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 */


import { Action, AnyAction, ReducerFnMap }  from './types';

export function reducer<S, A extends Action = AnyAction> (init: S, map: ReducerFnMap<S, A>)
{
  return (state: S = init, action: A) =>
    map[action.type] ? map[action.type](state, action) : state;
}
