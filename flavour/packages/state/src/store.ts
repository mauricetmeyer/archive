/**
 * store.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * (c) Laviréo. All rights reserved.
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 */


import { unstable_batchedUpdates } from 'react-dom'
import { INIT }                    from './const';
import { 
  Action,
  ActionFn,
  Reducer,
  Dispatch,
  StoreSubscriberFn,
  StoreUnsubscribeFn
} from './types';

export class Store<T = any>
{
  private state?:    T;
  private reducer:   Reducer<T>;
  private listeners: StoreSubscriberFn[] = [];
  private isDispatching: boolean = false;


  /**
   * Construct a new store instance.
   *
   * Example:
   * ```
   * const store = new Store(reducer);
   * ```
   *
   * @param Reducer<T> reducer function
   */
  constructor (reducer: Reducer<T>, state?: T)
  {
    /**
     * Set reducer... easy as cake! */
    this.reducer = reducer;
    this.state   = state;

    /**
     * When a store is created, an "INIT" action is dispatched so that every
     * reducer returns their initial state. */
    this.dispatch({ type: INIT });
  }


  /**
   * Get current state.
   *
   * Example:
   * ```
   * ```
   *
   * @return T state
   */
  public getState () : T
  {
    /**
     * Check if we are dispatching right now,
     * as this can will break stuff! */
    if (this.isDispatching)
    {
      throw new Error('You may not call store.getState() while dispatching.');
    }

    return this.state as T;
  }

  /**
   * Dispatch a new action.
   *
   * Example:
   * ```
   * ```
   *
   * @param Action action
   */
  public dispatch: Dispatch<any> = <A extends Action>(action: A | ActionFn<A>) => {
    /**
     * Handle async actions */
    if (typeof action === 'function')
    {
      return action(this.dispatch);
    }

    /**
     * Check if we are dispatching right now,
     * as reducers aren't allowed to do so. */
    if (this.isDispatching)
    {
      throw new Error("Reducers aren't allowed to dispatch actions.");
    }

    try {
      /**
       * We are dispatching. STAY AWAY!!!
       * Let the reducer do it's magic with the action. */
      this.isDispatching = true;
      this.state = this.reducer(this.state, action);
    } finally {
      this.isDispatching = false;
    }

    /**
     * Notify listeners that stuff has changed! */
    unstable_batchedUpdates(() => {
      this.listeners.forEach(l => l());
    });
  }

  /**
   * Subscribe to store updates
   *
   * Example:
   * ```
   * ```
   *
   * @param StoreSubscriberFn fn
   */
  public subscribe (fn: StoreSubscriberFn) : StoreUnsubscribeFn
  {
    /**
     * Check if we are dispatching right now,
     * as this can will break stuff! */
    if (this.isDispatching)
    {
      throw new Error('You may not call store.subscribe() while dispatching.');
    }

    /**
     * Add the listener to the list. */
    this.listeners.push(fn);
    return () => {
      const idx = this.listeners.indexOf(fn);
      if (idx !== -1) this.listeners.splice(idx, 1);
    }
  }
}

