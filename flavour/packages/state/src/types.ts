/**
 * types.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * (c) Laviréo. All rights reserved.
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 */


import { ReactNode } from 'react';

export interface Action<T = any>
{
  type: T;
}

export interface AnyAction<T = any> extends Action<T>
{
  [key: string]: any;
}

export interface ActionFn<A extends Action, T = void>
{
  (dispatch: Dispatch<A>) : T;
}

export interface Reducer<S = any, A extends Action = AnyAction>
{
  (state: S | undefined, action: A) : S;
}

export interface ReducerFnMap<S, A extends Action = AnyAction>
{
  [key: string]: Reducer<S, A>
}

export interface Dispatch<A extends Action = AnyAction>
{
  <T extends A>(action: T | ActionFn<T>) : void;
}

export interface StateProps
{
  initial:  any;
  reducer:  Reducer;
  children: ReactNode;
}

export interface StoreSubscriberFn {
  (): void
}

export interface StoreUnsubscribeFn {
  (): void
}

export interface SelectorFn<T> {
  (state: any): T
}


export type ReducerMap<S = any, A extends Action = Action> = {
  [K in keyof S]: Reducer<S[K], A>;
}

export type ActionFromReducer<R> = R extends Reducer<any, infer A> ? A : never
export type ActionFromReducerMap<M> = M extends ReducerMap<any, any> ? 
  ActionFromReducer<ReducerFromReducerMap<M>> : never

export type StateFromReducer<R> = R extends Reducer<infer S, any> ? S : never
export type StateFromReducerMap<M> = M extends ReducerMap<any, any> ? {
  [P in keyof M]: M[P] extends Reducer<infer S, any> ? S : never;
} : never

export type ReducerFromReducerMap<M> = M extends {
  [P in keyof M]: infer R
} ? R extends Reducer<any, any> ? R : never : never;