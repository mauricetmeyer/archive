/**
 * switch.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { createElement, ChangeEvent } from 'react';
import styled                         from 'styled-components';
import { Colors }                     from '@flavour/theme';

const SwitchInput = styled.input.attrs(_ => ({ type: 'checkbox' }))`
  top:      0;
  left:     0;
  opacity:  0;
  position: absolute;
`;

const SwitchToggle = styled.div`
  top:           3px;
  left:          3px;
  right:         11px;
  bottom:        3px;
  width:         10px;
  height:        10px;
  display:       block;
  position:      absolute;
  background:    #ffffff;
  transition:    left .1s ease-in-out, right .1s ease-in-out;
  border-radius: 5px;
`;

const SwitchContainer = styled.label`
  width:         24px;
  height:        16px;
  margin:        4px 0;
  cursor:        pointer;
  display:       block;
  position:      relative;
  background:    ${Colors.yellow};
  border-radius: 8px;

  & > ${SwitchInput}:checked ~ & > ${SwitchToggle}
  {
    left:  11px;
    right: 3px;
  }
`;


interface SwitchProps
{
  isChecked?: boolean;
  onChange?:  (ev: ChangeEvent<HTMLInputElement>) => void;
}

export const Switch = ({ isChecked, onChange }: SwitchProps) => (
  <SwitchContainer>
    <SwitchInput checked={isChecked} onChange={onChange} />
    <SwitchToggle />
  </SwitchContainer>
);
