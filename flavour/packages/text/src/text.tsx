/**
 * text.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * (c) Laviréo. All rights reserved.
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 */


import { createElement, ReactNode }       from 'react';
import styled                             from 'styled-components';
import { Colors, TextSizes, TextWeights, Fonts } from '@flavour/theme';

type TextColor = string;
interface TextProps
{
  as?:        keyof JSX.IntrinsicElements | React.ComponentType<any>;
  size?:      'xsmall' | 'small' | 'normal' | 'large' | 'xlarge' | 'xxlarge';
  align?:     'left' | 'right' | 'center';
  weight?:    'light' | 'normal' | 'medium' | 'bold';
  transform?: 'uppercase' | 'lowercase';
  id?:        string;
  className?: string;
  height?:    string;
  spacing?:   number;
  mono?:      boolean;
  inline?:    boolean;
  color?:     TextColor;
  children?:  ReactNode;
}

const TextSize = ({ size, height }: TextProps) => `
  font-size: ${TextSizes[size!].size};
  line-height: ${height || TextSizes[size!].height};
  ${ TextSizes[size!].mobile ? `
    @media (max-width: 760px) {
      font-size: ${TextSizes[size!].mobile!.size};
      line-height: ${height || TextSizes[size!].mobile!.height};
    }
  `: '' }
`;

const Text = styled.div<TextProps>`
  ${p => `color: ${p.color ? Colors[p.color] || p.color : Colors.dark};`}
  ${p => `font-weight: ${TextWeights[p.weight || 'medium']};`}
  ${p => p.mono      && `font-family: ${Fonts.mono};` }
  ${p => p.align     && `text-align: ${p.align};`}
  ${p => p.spacing   && `letter-spacing: ${p.spacing};`}
  ${p => p.inline    && `display: inline;`}
  ${p => p.transform && `text-transform: ${p.transform};`}
  ${p => p.size      && TextSize(p)}
`;

const A = ({ size = 'small', color = 'yellow', ...props }: any) =>
  <Text as="a" {...{...props, size, color}} />;

const P = ({ size = 'small', ...props }: TextProps) =>
  <Text as="p" size={size} {...props} />;

const H1 = ({ size = 'xxlarge', weight = 'bold', ...props }: TextProps) =>
  <Text as="h1" size={size} weight={weight} {...props} />;

const H2 = ({ size = 'xlarge', weight = 'bold', ...props }: TextProps) =>
  <Text as="h2" size={size} weight={weight} {...props} />;

const H3 = ({ size = 'large', weight = 'bold', ...props }: TextProps) =>
  <Text as="h3" size={size} weight={weight} {...props} />;

const H4 = ({ size = 'normal', weight = 'bold', ...props }: TextProps) =>
  <Text as="h4" size={size} weight={weight} {...props} />;

const H5 = ({ size = 'xsmall', weight = 'bold', ...props }: TextProps) =>
  <Text as="h5" size={size} weight={weight} {...props} />;

export { A, P, H1, H2, H3, H4, H5, Text, TextProps };
