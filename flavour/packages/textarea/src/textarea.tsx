/**
 * textarea.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { createElement, ChangeEvent } from 'react';
import styled                         from 'styled-components';
import { Colors, useTheme }           from '@flavour/theme';

export interface TextareaProps
{
  value?:       string;
  invalid?:     boolean;
  disabled?:    boolean;
  className?:   string;
  placeholder?: string;
  onChange?:    (ev: ChangeEvent<HTMLTextAreaElement>) => void;
}

const TextareaEl = styled.textarea<{ isDark?: boolean, invalid?: boolean }>`
  color:            ${p => p.isDark ? Colors.white : Colors.onyx};
  width:            100%;
  padding:          10px 15px;
  font-size:        16px;
  line-height:      24px;
  border-radius:    9px;
  font-weight:      500;
  transition:       box-shadow .2s;
  background-color: ${p => p.isDark ? Colors.bunker : Colors.light};

  ${p => p.invalid ? `
    box-shadow: ${Colors.red} 0 0 0 2px inset;
  ` : `
    :focus {
      box-shadow: ${Colors.bombay} 0 0 0 2px inset;
    }

    :disabled,
    ::placeholder {
      color: ${p.isDark ? Colors.tuna : Colors.bombay}
    }
  `}
`;

export const Textarea = (props: TextareaProps) => {
  const { darkmode } = useTheme();
  return <TextareaEl {...props} isDark={darkmode} />;
};
