/**
 * colors.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { Color } from "./color";

interface IColors { [key: string]: string; }
const COLORS: { [key: string]: Color; } = {
  red:    Color.fromHex('#FA3C3C')!,
  blue:   Color.fromHex('#0052CC')!,
  green:  Color.fromHex('#1EC08C')!,
  orange: Color.fromHex('#FF8B00')!,
  yellow: Color.fromHex('#FFC900')!,
};

const TINT_ALPHA = .1;

const tints: IColors  = {};
const colors: IColors = {};
for (const key in COLORS)
{
  const c = COLORS[key];
  colors[key]         = c.hex;
  tints[key + 'Tint'] = c.setAlpha(TINT_ALPHA).rgba
}

export const Colors: IColors = {
  black:     '#0D0D0D',
  onyx:      '#1E1F20',
  bunker:    '#2B2C2F',
  tuna:      '#46474D',
  grey:      '#74767C',
  bombay:    '#A4A5A9',
  light:     '#EDEDED',
  white:     '#FFFFFF',

  ...tints,
  ...colors
};
