/**
 * context.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * (c) Laviréo. All rights reserved.
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 */


import { createContext }     from 'react';
import { ThemeContextProps } from './types';

export const ThemeContext = createContext<ThemeContextProps>({
  motion:   true,
  darkmode: false
});

