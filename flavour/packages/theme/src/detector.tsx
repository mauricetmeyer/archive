/**
 * detector.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * (c) Laviréo. All rights reserved.
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 */


import { useEffect }     from 'react';
import { DetectorProps } from './types';

export const Detector = ({ query, onChange }: DetectorProps) => {
  const handleChange = (ev: any) => onChange(ev.matches);

  useEffect(() => {
    const mediaQuery = window.matchMedia(query);
    mediaQuery.addListener(handleChange);
    onChange(mediaQuery.matches);
    return () => mediaQuery.removeListener(handleChange);
  });

  return null;
};
