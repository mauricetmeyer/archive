/**
 * theme.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * (c) Laviréo. All rights reserved.
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 */


import { createElement }           from 'react';
import { useMedia }                from '@flavour/hooks';

import { ThemeProps }              from './types';
import { Style }                   from './style';
import { ThemeContext }            from './context';

export const Theme = ({ theme, children }: ThemeProps) => {
  const reduceMotions   = useMedia('(prefers-reduced-motion: reduce)');
  const prefersDarkmode = useMedia('(prefers-color-scheme: dark)');

  const dark  = (!theme || theme === 'auto') ? prefersDarkmode : theme === 'dark';
  const state = {
    motion:     !reduceMotions,
    darkmode:   dark,
    colorblind: false
  };

  return (
    <ThemeContext.Provider value={state}>
      <Style darkmode={dark} />
      {children}
    </ThemeContext.Provider>
  );
};

export const ThemeConsumer = ThemeContext.Consumer;

