/**
 * types.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * (c) Laviréo. All rights reserved.
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 */


import { ReactNode } from 'react';

export interface ThemeProps
{
  theme:    'auto' | 'dark' | 'light';
  children: ReactNode;
}

export interface ThemeContextProps
{
  motion?:     boolean;
  darkmode?:   boolean;
  colorblind?: boolean;
}

export interface StyleProps
{
  darkmode?: boolean;
}

export interface DetectorProps
{
  query:    string;
  onChange: (flag: boolean) => void;
}
