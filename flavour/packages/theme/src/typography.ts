/**
 * fonts.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


interface TextSize
{
  size:    string;
  height:  string;
  mobile?: {
    size:   string;
    height: string;
  };
}

export const TextSizes: { [key: string]: TextSize } = {
  xsmall:  { size: '0.875rem', height: '1rem' },
  small:   { size: '1rem',     height: '1.5rem' },
  normal:  { size: '1.25rem',  height: '1.875rem' },
  large:   { size: '1.875rem', height: '2.5rem' },
  xlarge:  { size: '2.5rem',   height: '3.125rem' },
  xxlarge: { size: '3.125rem', height: '3.75rem' }
};

export const TextWeights: { [key: string]: number } = {
  'light':  300,
  'normal': 400,
  'medium': 500,
  'bold':   600,
};
