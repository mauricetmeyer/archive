/**
 * build-packages.js
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


const path         = require('path');
const bolt         = require('bolt');
const concurrently = require('concurrently');
const TYPES = {
  "cjs": "commonjs",
  "esm": "esnext"
}

const CMD = (type, flag, dir) => `bolt ws exec${flag ? ' --parallel' : ''} --no-bail --excludeFromGraph devDependencies ${dir ? `--only-fs "${dir}"` : ''} -- bash -c 'tsc --outDir ./lib/${type} --module ${TYPES[type]} && echo Success'`;
const run = async (...cmds) => {
  for (const cmd of cmds)
  {
    try { await concurrently([cmd]) }
    catch (e)
    {
      if (e.constructor === Error) throw Error('Command failed');
      throw e;
    }
  }
}

const getPackages = async (cwd) => {
  const proj     = await bolt.getProject({ cwd });
  const packages = await bolt.getWorkspaces({ cwd });

  return packages.map(pkg => {
    const { dir, name, config } = pkg;
    const dirRelative = path.relative(proj.dir, dir);
    return { dir, name, config, dirRelative };
  });
};

/**
 * Start builing the packages. */
const buildPackages = async (packages) => {
  const cwd  = process.cwd();
  const info = await getPackages(cwd);

  let pkg;
  if (packages.length > 0) {
    const matches = info.filter(p => packages.indexOf(p.name) !== -1);
    if (matches.length > 1) throw Error('Matched multiple packages 😢!');
    if (matches.length < 1) throw Error('Could not find package in project 😢!');
    pkg = matches[0];
  }

  return run(
    CMD('cjs', false, pkg && pkg.dirRelative),
    CMD('esm', true, pkg && pkg.dirRelative)
  );
};

module.exports = buildPackages;
