/**
 * build.js
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */

const chalk         = require('chalk');
const buildIcons    = require('./build-icons');
const buildPackages = require('./build-packages');

const log = (...msg) => console.log(chalk.green('Flavour build:'), ...msg);
const err = (...msg) => console.log(chalk.red('Flavour build:'), ...msg);

/**
 * Start builing the packages. */
(async () => {
  const [ node, path, ...packages ] = process.argv;

  log('🔨 Updating icons...');
  await buildIcons();
  log('📦 Icons processed');

  log(packages.length < 1 ? '🔨 Building all packages...' : '🔨 Building the following packages: ' + packages.join(', '));
  await buildPackages(packages);
  log('📦 Packages build');
  log('Success');
  process.exit(0);
})().catch(e => console.error(err(e)));
