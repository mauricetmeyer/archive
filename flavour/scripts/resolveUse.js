exports.type   = 'full';
exports.active = true;
exports.params = {
};

exports.description = 'Resolved #use elements and removes #defs';

/**
 * Resolved use elements with the actual def.
 *
 * @param {Object} tree
 */
exports.fn = (tree) => {
  /**
   * Remove xmlns:links */
  const [ svg ] = tree.content;
  if (svg.hasAttr('xmlns:xlink')) {
    svg.removeAttr('xmlns:xlink');
  }

  /**
   * Make a map with all the def entries. */
  const map = {};
  svg.content = svg.content.filter(ent => {
    if (ent.elem === 'defs') {
      const defs = ent.content;
      defs.forEach(def => {
        const key = def.attr('id').value;
        def.removeAttr('id');
        map[key] = def;
      });

      return false;
    }

    return true;
  });

  svg.content = svg.content.map(e => {
    if (e.elem === 'use')
    {
      const { attrs } = e;
      const link = attrs['xlink:href'].value;
      const key  = link.substr(1);
      delete attrs['xlink:href'];

      if (!map[key]) throw new Error(`Failed to resolve <use xlink:href="${key}">!`);
      const item = map[key];
      item.attrs = Object.assign(item.attrs, attrs);
      return item;
    }

    return e;
  });

  return tree;
};