exports.type   = 'perItem';
exports.active = true;
exports.params = {
};

exports.description = 'Make attributes work with React';

/**
 * Resolved use elements with the actual def.
 *
 * @param {Object} tree
 */
exports.fn = (item) => {
  /**
   * Go over attributes and transform them. */
  Object.keys(item.attrs).forEach(k => {
    const attr  = item.attr(k);
    const parts = attr.name.split('-');
    if (parts.length === 1) return;
    const [ start, ...rest ] = parts;
    const name = [start.toLowerCase(), ...rest.map(p => `${p[0].toUpperCase()}${p.slice(1)}`)].join('');
    let local = attr.name === attr.local ? name : attr.local;
    let prefix = attr.name !== attr.local ? attr.prefix : '';

    item.attrs[k] = {
      name,
      prefix,
      local,
      value: attr.value
    }
  });

  return item;
};