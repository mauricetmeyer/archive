module.exports = function(grunt)
{
  ///
  // Javascript definitions
  const sass = require('node-sass');
  const js_out  = "public/static/js/";
  const js_libs = [
    'app/assets/js/libs/bundler.js',
    'app/assets/js/libs/bundler-*.js'
  ];

  const js_dependencies = {
    app: [
      'app/assets/js/config/**/*.js',
      'app/assets/js/locales/**/*.js',
      'app/assets/js/modules/**/*.js',

      'app/assets/js/components/actions.js',
      'app/assets/js/components/money.js', 
      'app/assets/js/components/date.js', 

      'app/assets/js/components/onboard.js',
      'app/assets/js/components/store.js', 
      'app/assets/js/components/config.js', 
      'app/assets/js/components/events.js', 
      'app/assets/js/components/stripe.js', 
      'app/assets/js/components/editor.js', 
      'app/assets/js/components/posts.js', 
      'app/assets/js/components/head.js', 
      'app/assets/js/components/shell.js',

      'app/assets/js/components/modals.js', 
      'app/assets/js/components/glass.js', 

      'app/assets/js/components/backdrop.js', 
      'app/assets/js/components/progress.js', 
      'app/assets/js/components/search.js', 
      'app/assets/js/components/routes.js', 

      'app/assets/js/modals/**/*.js',
      'app/assets/js/views/**/*.js',

      'app/assets/js/app.js',

      'app/assets/js/vendor/**/*.js', 
    ]
  };

  let js_files = {};
  for (let js_t in js_dependencies)
  {
    js_files[js_out + js_t + '.js'] = js_libs.concat(js_dependencies[js_t]);
  }


  //
  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    ///
    // MJML
    mjml: {
      prod: {
        options: {},
        files: [{
          expand: true,
          cwd: 'app/assets/mjml',
          src: ['*.mjml'],
          dest: 'app/assets/views/mailer',
          ext: '.html.erb'
        }]
      }
    },

    ///
    // Sass
    sass: {
      dev: {
        options: {
          style:     'expanded',
          implementation: sass,
          sourceMap: false,
        },

        files: [{
          expand: true,
          cwd: 'app/assets/scss',
          src: ['*.scss'],
          dest: 'public/static/css',
          ext: '.css'
        }]
      },

      prod: {
        options: {
          style:      'compact',
          noCache:    true,
          sourceMap:  false,
          bundleExec: false
        },

        files: [{
          expand: true,
          cwd: 'app/assets/scss',
          src: ['*.scss'],
          dest: 'public/static/css',
          ext: '.css'
        }]
      }
    },

    ///
    // Watch
    watch: {
      js: {
        files: ['app/assets/js/**/*.js'],
        tasks: ['uglify']
      },

      img: {
        files: ['app/assets/img/**/*.png', 'app/assets/img/**/*.svg', 'app/assets/img/**/*.jpg'],
        tasks: ['imagemin:prod']
      },

      mjml: {
        files: ['app/assets/mjml/**/*.mjml'],
        tasks: ['mjml:prod']
      },

      sass: {
        files: ['app/assets/scss/**/*.scss'],
        tasks: ['sass:dev']
      },

      configFiles: {
        files: ['Gruntfile.js'],
        options: {
          reload: true
        }
      }
    },

    ///
    // Uglify
    uglify: {
      options: {
        banner: '/*\n'
              + ' *\n'
              + ' * Un projet par LavirÃ©o\n'
              + ' *\n'
              + ' * Author: Maurice T. Meyer\n'
              + ' * E-Mail: maurice@lavireo.com\n'
              + ' *\n'
              + ' * (c) LavirÃ©o\n'
              + ' */\n\n\n'
      },
      all: {
        files: js_files
      }
    },

    ///
    // cssmin
    cssmin: {
      prod: {
        files: [{
          expand: true,
          src: ['*.css'],
          cwd: 'public/static/css/',
          dest: 'public/static/css/',
          ext: '.css'
        }]
      }
    },

    ///
    // Imagemin
    imagemin: {
      prod: {
        options: {
          optimizationLevel: 3
        },
        files: [{
           expand: true,
           cwd: 'app/assets/img/',
           src: ['**/*.{png,jpg,gif,svg}'],
           dest: 'public/static/img/'
        }]
      }
    },
  });

  //
  // Load the plugins
  grunt.loadNpmTasks('grunt-mjml');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-imagemin');
  grunt.loadNpmTasks('grunt-sass');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-jshint');

  //
  // Default task(s).
  grunt.registerTask('dev',     ['uglify', 'sass:dev', 'imagemin:prod', 'mjml:prod', 'watch']);
  grunt.registerTask('default', ['uglify', 'imagemin:prod', 'sass:prod', 'cssmin:prod']);
};
