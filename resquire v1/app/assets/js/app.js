(function (_)
{
  var sel       = _('selector');
  var win       = sel(window);
  var event     = _('event');
  var routes    = _('routes');
  var router    = _('router');
  var events    = _('gevents');
  var shortcuts = _('shortcuts');

  /**
   * Register routes */
  router.add(routes);

  win.on('load', function ()
  {
    var wrap     = sel('#wrap');
    var config   = _('config');
    var shell    = new (_('shell'))();
    var glass    = new (_('glass'))();
    var modals   = new (_('modals'))();
    var progress = new (_('progress'))();


    wrap
      .append(shell.el)
      .append(glass.el)
      .append(modals.el)
      .append(progress.el);

    shell.attach();
    glass.attach();
    modals.attach();
    progress.attach();

    /**
     * Init message */
    if (config.dat)
      console.log('Initialising Resquire as', config.dat);

    win
      .on('click', 'a', function (ev)
      {
        ev.preventDefault();
        var path = ev.currentTarget.getAttribute('href');
        if (path !== '#')
        {
          event.trigger('shell:navigate', path);
          return false; 
        }
      })
      .on('click', '.js-trigger', function (ev)
      {
        var tar  = sel(ev.currentTarget);
        var attr = tar.attr('data-action');
        var args = attr.split(' ');
        event.trigger.apply(event, args);
        return false;
      })
      .on('click', '.js-url-field', function (ev)
      {
        ev.currentTarget.select();
        return false;
      })
      .on('click', '.js-popover', function (ev)
      {
        var tar  = sel(ev.currentTarget);
        var trig = sel(ev.target).closest('.js-popover--trigger');
        if (trig.length < 1)
          return false;

        var cont = tar.find('.popover');
        if (cont.length < 1)
          return false;

        var act = cont.hasClass('active');
        cont.toggleClass('active', !act);

        return false; 
      });

    ///
    // Parallax
    var hero  = sel('.parallax');
    var speed = 0.5;
    if (hero.length > 0)
      win
        .on('scroll', function (ev)
        {
          hero.each(function ()
          {
            sel(this).css({ 
              'background-position': '50% ' + (window.pageYOffset * speed) + 'px'
            });
          });
        });

    event.on('backdrop', function ()
    {
      var drops = sel('.js-dropdown');
      drops.toggleClass('active', false);
      event.trigger('backdrop:hide');
    });

    events.init();

    /**
     * Init shortcuts */
    shortcuts.bind(window);

    /**
     * Cookie notice */
    var cookies    = _('cookie');
    var cookie_el  = sel('#cookies_notice');
    var cookie_ack = cookies.get('cookie_ack') == 1;
    if (cookie_ack) cookie_el.removeClass('active');
    event.on('cookie-acknowledge', function () {
      cookie_el.removeClass('active');

      var date = new Date();
      date.setYear(date.getFullYear() + 10);
      cookies.set('cookie_ack', 1, { expires: date.toGMTString() });
      return false;
    })
  });
})(_);
