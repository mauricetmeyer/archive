(function(_)
{
  var sel       = _('selector');
  var xhr       = _('transports').xhr;
  var ready     = {};
  var action    = null;
  var protect   = '])}for(;;);';
  var templates = {
    ///
    // Plans
    plans: {
      url:    '/ctrl/plans',
      method: 'GET'
    },
    plans_cancel: {
      url:    '/ctrl/plans/cancel',
      method: 'POST'
    },
    plans_upgrade: {
      url:    '/ctrl/plans/upgrade',
      method: 'POST'
    },


    ///
    // Users
    users: {
      url:    '/ctrl/users',
      method: 'GET'
    },
    users_posts: {
      url:    '/ctrl/users/posts',
      method: 'GET',
      params: ['user_id']
    },
    users_events: {
      url:    '/ctrl/users/events',
      method: 'GET',
      params: ['user_id']
    },
    users_delete: {
      url:    '/ctrl/users/delete',
      method: 'POST',
      params: ['user_id']
    },


    ///
    // Auth
    login: {
      url:    '/ctrl/login',
      method: 'POST',
      params: ['email', 'password']
    },
    logout: {
      url:    '/ctrl/logout',
      method: 'POST'
    },
    register: {
      url:    '/ctrl/register',
      method: 'POST',
      params: ['name', 'email', 'password']
    },


    ///
    // Settings
    settings: {
      url:    '/ctrl/settings',
      method: 'GET'
    },
    settings_check: {
      url:    '/ctrl/settings/check',
      method: 'POST'
    },
    settings_update: {
      url:    '/ctrl/settings',
      method: 'POST'
    },
    settings_password: {
      url:    '/ctrl/settings/password',
      method: 'POST'
    },


    ///
    // Posts
    posts: {
      url:    '/ctrl/posts',
      method: 'GET'
    },
    posts_create: {
      url:    '/ctrl/posts',
      method: 'POST'
    },
    posts_retry: {
      url:    '/ctrl/posts/retry',
      method: 'POST',
      params: ['post_id']
    },
    posts_cancel: {
      url:    '/ctrl/posts/cancel',
      method: 'POST',
      params: ['post_id']
    },
    posts_publish: {
      url:    '/ctrl/posts/publish',
      method: 'POST',
      params: ['post_id']
    },


    ///
    // Stories
    stories_create: {
      url:    '/ctrl/stories',
      method: 'POST'
    },
    stories_update: {
      url:    '/ctrl/stories/update',
      method: 'POST',
      params: ['story_id']
    },
    stories_delete: {
      url:    '/ctrl/stories/delete',
      method: 'POST',
      params: ['story_id']
    },
    stories_preview: {
      url:    '/ctrl/stories/preview',
      method: 'POST',
      params: ['text']
    },


    ///
    // Referrals
    referrals: {
      url:    '/ctrl/referrals',
      method: 'GET'
    },

    referrals_stats: {
      url:    '/ctrl/referrals/stats',
      method: 'GET'
    }
  };

  var dec = function (str)
  {
    str.substr(0, protect.length) == protect && (str = str.substring(protect.length));
    return str;
  };

  var add = function (body)
  {
    return function (data, options)
    {
      data     = data    || {};
      options  = options || {};

      var url    = body.url;
      var method = body.method;
      var params = body.params || [];

      for(var i = 0; i < params.length && options.process !== false; i++)
      {
        if(data[params[i]] == undefined)
          if (options.fail)
          {
            options.fail("You have to fill out the required argument: " + params[i]);
            return;
          }
      }

      //
      // Fire Callback
      if (options.init)
        options.init();

      xhr(url, {
        data: data,
        type: method,
        process: options.process,
        headers: {
          'X-CSRF-Token': (sel('meta').filter(function ()
          {
            if (this.name == 'csrf-token')
              return this;
          }).attr('content'))
        },
        fail: function(attr)
        {
          if (options.fail)
            options.fail.call(this, dec(attr));
        },
        done: function(attr)
        {
          if (options.done)
            options.done.call(this, dec(attr));
        },
        progress: function (attr)
        {
          if (options.progress)
            options.progress.call(this, attr);        
        }
      });
    }
  };

  //
  // Add them now!
  for (action in templates)
  {
    var name = action;
    var body = templates[action];

    //
    // Load
    if (body.url)
    {
      ready[name] = add(body);
    }
  }

  _('actions', ready);
})(_);
