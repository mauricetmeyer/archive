(function ()
{
  ///
  // Cache
  var event = _('event');
  var cache = function ()
  {
    return this;
  };

  ///
  // Opts
  var opts = {
    dat: {},
    get: function (key)
    {
      return this.dat[key];
    },

    del: function (key)
    {
      this.dat[key] = null;
      delete this.dat[key];
    },

    set: function (key, val)
    {
      this.dat[key] = val; 
      event.trigger('config:changed');
    },

    print: function ()
    {
      console.log(this.dat);
    }
  };

  ///
  // Setup
  for (var opt in opts) cache.prototype[opt] = opts[opt];

  ///
  // Get data
  var obj  = new cache();
  var tar  = _('selector')('#preload');

  if (tar.length)
  {
    var raw  = tar.text();
    var data = JSON.parse(raw.replace(/&quot;/g, '"'));
    for (var key in data)
      obj.set(key, data[key]);
  }

  _('config', obj);
})(_);
