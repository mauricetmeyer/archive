(function (_)
{
  var date_names = {
    monthNames: [
      "January", 
      "February", 
      "March", 
      "April", 
      "May", 
      "June", 
      "July", 
      "August", 
      "September", 
      "October", 
      "November", 
      "December"
    ],
    monthNamesShort: [
      "Jan", 
      "Feb", 
      "Mar", 
      "Apr", 
      "May", 
      "Jun", 
      "Jul", 
      "Aug", 
      "Sep", 
      "Oct", 
      "Nov", 
      "Dec"
    ],
    dayNames: [
      "Sunday", 
      "Monday", 
      "Tuesday", 
      "Wednesday", 
      "Thursday", 
      "Friday", 
      "Saturday"
    ],
    dayNamesShort: [
      "Sun", 
      "Mon", 
      "Tue", 
      "Wed", 
      "Thu", 
      "Fri", 
      "Sat"
    ],

    relativeDays: [
      'Yesterday',
      'Today',
      'Tomorrow'
    ]
  };

  ///
  // Utils
  _('date/utils', {
    __ms:    1,
    __sec:   1e3,
    __min:   6e4,
    __hour:  36e5,
    __day:   864e5,
    __week:  6048e5,
    __month: 2629743830,
    __year:  31556926000,

    __tokens: {
      sec: {
        one: [{
          value: "{0}",
          type: "placeholder"
        }, {
          value: " second",
          type: "plaintext"
        }],
        other: [{
          value: "{0}",
          type: "placeholder"
        }, {
          value: " seconds",
          type: "plaintext"
        }]
      },
      min: {
        one: [{
          value: "{0}",
          type: "placeholder"
        }, {
          value: " minute",
          type: "plaintext"
        }],
        other: [{
          value: "{0}",
          type: "placeholder"
        }, {
          value: " minutes",
          type: "plaintext"
        }]
      },
      hour: {
        one: [{
          value: "{0}",
          type: "placeholder"
        }, {
          value: " hour",
          type: "plaintext"
        }],
        other: [{
          value: "{0}",
          type: "placeholder"
        }, {
          value: " hours",
          type: "plaintext"
        }]
      },
      day: {
        one: [{
          value: "{0}",
          type: "placeholder"
        }, {
          value: " day",
          type: "plaintext"
        }],
        other: [{
          value: "{0}",
          type: "placeholder"
        }, {
          value: " days",
          type: "plaintext"
        }]
      },
      week: {
        one: [{
          value: "{0}",
          type: "placeholder"
        }, {
          value: " week",
          type: "plaintext"
        }],
        other: [{
          value: "{0}",
          type: "placeholder"
        }, {
          value: " weeks",
          type: "plaintext"
        }]
      },
      month: {
        one: [{
          value: "{0}",
          type: "placeholder"
        }, {
          value: " month",
          type: "plaintext"
        }],
        other: [{
          value: "{0}",
          type: "placeholder"
        }, {
          value: " months",
          type: "plaintext"
        }]
      },
      year: {
        one: [{
          value: "{0}",
          type: "placeholder"
        }, {
          value: " year",
          type: "plaintext"
        }],
        other: [{
          value: "{0}",
          type: "placeholder"
        }, {
          value: " years",
          type: "plaintext"
        }]
      }
    },


    off: function (date)
    {
      return -Math.round(date.getTimezoneOffset() / 15) * 15;
    },

    diff: function (lhs, rhs)
    {
      var diff     = rhs - lhs;
      var zoneDiff = (this.off(lhs) - this.off(rhs)) * this.__min;
      return Math.floor((diff - zoneDiff) / this.__day);
    },

    dist: function (lhs, rhs)
    {
      var opts, e, i;

      opts        = {};
      opts.diff   = rhs - lhs;
      opts.unit   = this.__calculate_unit(Math.abs(opts.diff), opts);
      opts.number = this.__calculate_time(Math.abs(opts.diff), opts.unit);
      opts.rule   = (opts.number == 1) ? 'one' : 'other';

      i = function()
      {
        var a, b, d;
        var keys = this.__tokens[opts.unit][opts.rule];
        d = [];
        for (a = 0, b = keys.length; a < b; a++)
          d.push(keys[a].value)
        return d;
      }.call(this);
      return i.join("").replace(/\{[0-9]\}/, opts.number.toString())
    },

    addDays: function (date, val)
    {
      var res  = new Date(date.getTime()); 
      res.setDate(date.getDate() + val);
      return res;
    },

    subDays: function (date, val)
    {
      return this.addDays(date, -val); 
    },

    addMonths: function (date, val)
    {
      var res  = new Date(date.getTime()); 
      var mon  = res.getMonth() + val;
      var temp = new Date(0);
      temp.setFullYear(res.getFullYear(), mon, 1);
      temp.setHours(0, 0, 0, 0);

      var days = this.daysInMonth(temp);
      res.setMonth(mon, Math.min(days, res.getDate()));
      return res;
    },

    subMonths: function (date, val)
    {
      return this.addMonths(date, -val);
    },

    daysInMonth: function (date)
    {
      return 32 - new Date(date.getFullYear(), date.getMonth(), 32).getDate();
    },

    lastOfMonth: function (date)
    {
      var res = new Date(date.getTime());
      res.setFullYear(date.getFullYear(), res.getMonth() + 1, 0);
      res.setHours(23, 59, 59, 999);
      return res;
    },

    firstOfMonth: function (date)
    {
      var res = new Date(date.getTime()); 
      res.setDate(1);
      res.setHours(0, 0, 0, 0);
      return res;
    },

    display: function (date)
    {
      var tmp       = new Date(date);
      var today     = new Date();

      tmp.setHours(0, 0, 0, 0);
      today.setHours(0, 0, 0, 0);

      var tomorrow  = this.addDays(today, 1);
      var yesterday = this.subDays(today, 1);

      if (today.getTime() == tmp.getTime())
        return 'Today';
      else if (tomorrow.getTime() == tmp.getTime())
        return 'Tomorrow';
      else if (yesterday.getTime() == tmp.getTime())
        return 'Yesterday';
      else
      {
        var day   = date_names.dayNames[date.getDay()];
        var month = date_names.monthNamesShort[date.getMonth()];
        return day + ', ' + date.getDate() + ' ' + month;
      }
    },


    __calculate_unit: function(value, opts)
    {
      var c, d, f;
      opts = opts || {};
      f = {};
      for (c in opts) {
        f[c] = opts[c];
      }
      f.approximate == null && (f.approximate = !1);
      multi = f.approximate ? this.approximate_multiplier : 1;

      return value < this.__minute * multi ? "sec" : value < this.__hour * multi ? "min" : value < this.__day * multi ? "hour" : value < this.__week * multi ? "day" : value < this.__month * multi ? "week" : value < this.__year * multi ? "month" : "year"
    },

    __calculate_time: function(a, b)
    {
      return Math.round(a / this['__' + b])
    }
  });

  ///
  // Names
  _('date/names', date_names);
})(_);
