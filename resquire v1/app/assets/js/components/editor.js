(function (_)
{
  var view  = _('view');
  var utils = _('utils');
  var event = _('event');

  var HASHTAG    = "hashtag";
  var MENTION    = "mention";
  var HASHTAG_AT = "#";
  var BROWSERS   = {
    CHROME:  1,
    SAFARI:  2,
    FIREFOX: 3,
    OPERA:   4,
    EDGE:    5,
    MSIE:    6
  };

  var NBSP_REGEX  = /[\xa0\n\t]/g;
  var CRLF_REGEX  = /\r\n/g;
  var LINES_REGEX = /(.*?)\n/g;
  var SP_LEADING_OR_FOLLOWING_CLOSE_TAG_OR_PRECEDING_A_SP_REGEX = /^ |(<\/[^>]+>) | (?= )/g;
  var SP_LEADING_OR_TRAILING_OR_FOLLOWING_A_SP_REGEX = /^ | $|( ) /g;

  var J  = /((?:^|[^a-zA-Z0-9_!#$%&*@]:?))(#)([a-zA-Z0-9_]{1,20})(\/[a-zA-Z][a-zA-Z0-9_-]{0,24})?/gi;
  var it = /((?:^|[^a-zA-Z0-9_!#$%&*@]:?))(@)([a-zA-Z0-9_]{1,20})(\/[a-zA-Z][a-zA-Z0-9_-]{0,24})?/g;
  var ct = {
    "&": "&amp;",
    ">": "&gt;",
    "<": "&lt;",
    '"': "&quot;",
    "'": "&#39;" 
  };

  _('editor', view.extend({
    name: 'editor',
    args: {
      role:            'textbox',
      contenteditable: 'true'
    },



    events: [
      ['paste',   'self', '__change'],
      ['input',   'self', '__change'],
      ['change',  'self', '__change'],
      ['keydown', 'self', '__change'],
    ],


    init: function ()
    {
      this.$el.attr('role', 'textbox');
      this.$el.attr('contenteditable', 'true');
      if (this.args['data-readonly']) this.$el.removeAttr('contenteditable');
      this.reformat = utils.throttle(this.__reformat.bind(this), 100);
      this.browser  = this.__detectBrowser();
      if (this.getText().length === 0) this.$el.toggleClass('empty', true);
      this.__reformat(this.$el.attr('data-value') || "");
    },

    setText: function (text)
    {
      this.__reformat(text || "");
    },

    getText: function ()
    {
      return this.__text(function () {});
    },

    getHashtags: function ()
    {
      const text = this.getText();
      return this.__hashtagIndices(text).map(function (val)
      { return val.hashtag });
    },

    getMentions: function ()
    {
      const text = this.getText();
      return this.__mentionIndices(text).map(function (val)
      { return val.mention });
    },


    getSelectionOffsets: function()
    {
      var sel = getSelection();
      if (!sel || 1 !== sel.rangeCount)
        return null;
      var range = sel.getRangeAt(0);

      /**
       * Check if we are a parent */
      var node = range.commonAncestorContainer;
      for (; node !== this.el;)
      {
        if (!node) return null;
        node = node.parentNode;
      }

      var ranges = [{
        node: range.startContainer,
        offset: range.startOffset
      }];

      range.collapsed || ranges.push({
        node: range.endContainer,
        offset: range.endOffset
      });

      var offsets = ranges.map(function() {
        return Number.MAX_VALUE;
      });

      var text = this.__text(function(el, isBr, offset, len, end)
      {
        return ranges.forEach(function(val, idx)
        {
          if (offsets[idx] === Number.MAX_VALUE && el === val.node && (isBr || offset === val.offset))
            offsets[idx] = len + (isBr ? Math.min(end - len, val.offset) : 0);
        });
      });

      return offsets.map(function(val)
      {
        return Math.min(val, text.length);
      })
    },

    setSelectionOffsets: function(offsets)
    {
      if (offsets) {
        var range = this.__range(offsets);
        if (range)
        {
          var sels = window.getSelection();
          sels.removeAllRanges(), sels.addRange(range);
        }
      }
    },


    __change: function ()
    {
      var html = this.el.innerHTML;
      var text = this.getText();
      var last = this.last;

      if (last !== html)
      {
        this.reformat(text);
        this.$el.trigger('editor:changed');
      }
    },

    __reformat: function (text)
    {
      this.$el.toggleClass('empty', text.length === 0);
      var indices = [].concat(this.__mentionIndices(text))
                      .concat(this.__hashtagIndices(text));

      var offsets = this.getSelectionOffsets();
      var content = this.__link(text, indices);

      var self = this;
      var res  = (content + "\n").replace(LINES_REGEX, function(match, p1)
      {
        var res;
        if (self.browser === BROWSERS.FIREFOX || self.browser === BROWSERS.MSIE)
        {
          p1  = p1.replace(SP_LEADING_OR_FOLLOWING_CLOSE_TAG_OR_PRECEDING_A_SP_REGEX, "$1&nbsp;")
          res = self.browser === BROWSERS.FIREFOX ? p1 + "<BR>" : "<P>" + p1 + "</P>";
        }
        else
        {
          p1  = (p1 || "<br>").replace(SP_LEADING_OR_TRAILING_OR_FOLLOWING_A_SP_REGEX, "$1&nbsp;");
          res = self.browser === BROWSERS.OPERA ? "<p>" + p1 + "</p>" : "<div>" + p1 + "</div>";
        }

        return res;
      });

      this.el.innerHTML = res;
      this.setSelectionOffsets(offsets);
      this.last = res;
    },


    __link: function (text, indices)
    {
      indices.sort(function (a, b) { return a.indices[0] - b.indices[0]; });

      var res    = "";
      var offset = 0;
      for (var i = 0, len = indices.length; i < len; i++)
      {
        var index = indices[i];
        res += this.__escape(text.substring(offset, index.indices[0])); 

        /**
         * Process hashtag */
        if (index.hashtag)
          res += this.__create(HASHTAG, index, text, index.hashtag);
        /**
         * Process mention */
        else if (index.mention)
          res += this.__create(MENTION, index, text, index.mention);
        offset = index.indices[1];
      }

      return res += this.__escape(text.substring(offset, text.length))
    },

    __create: function (type, index, text, val)
    {
      var start = text.substring(index.indices[0], index.indices[0] + 1);
      var val   = this.__escape(val);
      return "<a class='"+ type +"' target='_blank'>"+ start + val +"</a>";
    },

    __text: function (fn)
    {
      var self = this;
      function exec(el) {
        var len = res.length;
        if (el.nodeType === 3) {
          var space  = el.nodeValue.replace(NBSP_REGEX, " ");
          var offset = space.length;
          offset && (res += space, e = !0), fn(el, !0, 0, len, len + offset)
        } else if (el.nodeType === 1) {
          fn(el, !1, 0, len, len);

          /**
           * Br node */
          if (el.nodeName.toLowerCase() === "br")
            el == curr ? g = !0 : (res += "\n", e = !1);
          /**
           * Other element nodes */
          else {
            var style   = el.currentStyle || window.getComputedStyle(el, "");
            var isBlock = style.display == "block";
            var isMSIE  = self.browser === BROWSERS.MSIE;

            isBlock && isMSIE && (e = !0);
            for (var child = el.firstChild, off = 1; child; child = child.nextSibling, off++) {
              exec(child);
              if (g) return;
              len = res.length;
              fn(el, !1, off, len, len);
            }
            g || el == curr ? g = !0 : isBlock && e && (res += "\n", e = !1)
          }
        }
      }

      var res = "", e, curr, g;
      for (var el = this.el; el && el.nodeType === 1; el = el.lastChild) curr = el;
      return exec(this.el), res
    },

    __escape : function (text)
    {
      return text && text.replace(/[&"'><]/g, function(c) { return ct[c]; });
    },

    __hashtagIndices: function (text)
    {
      if (!text || !text.match(/#/))
        return [];

      var indices = [];
      return text.replace(J, function(t, e, n, match, unused, offset, full)
        {
          if (!full.slice(offset + t.length).match(/^(?:#|:\/\/)/))
          {
            var start = offset + e.length;
            var end   = start + match.length + 1;
            indices.push({
              hashtag: match,
              indices: [start, end]
            })
          }
        }), indices
    },

    __mentionIndices: function (text)
    {
      if (!text || !text.match(/@/))
        return [];

      var indices = [];
      return text.replace(it, function(t, i, n, match, unused, offset, full)
        {
          if (!full.slice(offset + t.length).match(/^(?:@|:\/\/)/))
          {
            var start = offset + i.length;
            var end   = start + match.length + 1;

            indices.push({
              mention: match,
              indices: [start, end]
            })
          }
        }), indices
    },


    __range: function (offsets)
    {
      var stop;
      var range   = null;
      var last    = offsets.length - 1;
      var entries = offsets.map(function()
      {
        return {}
      });

      this.__text(function(el, isBr, offset, len, end)
      {
        stop || entries.forEach(function(val, idx)
        {
          var curr = offsets[idx];
          if (len <= curr && el.nodeName.toLowerCase() !== "br")
          {
            val.node   = el;
            val.offset = isBr ? Math.min(curr, end) - len : offset;
            stop       = isBr && idx === last && end >= curr;
          }
        })
      });
      
      if (entries[0].node && entries[last].node)
      {
        range = document.createRange()
        range.setStart(entries[0].node, entries[0].offset)
        range.setEnd(entries[last].node, entries[last].offset);
      }

      return range;
    },

    __detectBrowser: function ()
    {
      var agent = navigator.userAgent.toLowerCase();
      return /edge/.test(agent)          ? BROWSERS.EDGE
           : /chrome|webkit/.test(agent) ? BROWSERS.CHROME
           : /opera/.test(agent)         ? BROWSERS.OPERA
           : /trident\/7/.test(agent)    ? -1
           : /msie/.test(agent)          ? BROWSERS.MSIE
           : !/compatible/.test(agent) && /mozilla/.test(agent) ? BROWSERS.FIREFOX
           : 0;
    }
  }));
})(_);
