(function (_)
{
  var sel     = _('selector');
  var win     = sel(window);
  var bind    = _('utils').bind;
  var event   = _('event');
  var config  = _('config');
  var actions = _('actions');
  var events  = {
    logout: function ()
    {
      actions.logout(null, {
        done: function ()
        {
          window.location.assign('/')
        }
      });
      return false;
    },

    create_post: function ()
    {
      if (config.dat.can_post)
        event.trigger('modal:open', 'create_post'); 
      else
        event.trigger('modal:open', 'instagram');
    }
  };

  _('gevents', {
    init: function ()
    {
      var ev;
      var event = _('event');
      for (ev in events)
        event.on(ev, events[ev]);
    }
  });
})(_);
