(function (_)
{
  var view  = _('view');
  var utils = _('utils');
  var event = _('event');

  ///
  // Create glass
  _('glass', view.extend({
    name: 'glass',
    shown: false,

    init: function()
    {
      event
        .on('glass:show', utils.bind(this.show, this))
        .on('glass:hide', utils.bind(this.hide, this));
    },

    show: function()
    {
      this.shown = true;
      this.$el.show();
      setTimeout(utils.bind(function()
      {
        this.$el.addClass('show')
      }, this), 0);
    },

    hide: function()
    {
      this.shown = false;
      this.$el.removeClass('show');
      setTimeout(utils.bind(function()
      {
        this.$el.hide();
      }, this), 200);
    }
  }));
})(_);
