(function (_)
{
  var view    = _('view');
  var bind    = _('utils').bind;
  var config  = _('config');
  var actions = _('actions');

  _('head', view.extend({
    class: 'pad--m wrap--l',
    views: {
      wrap: view.extend({
        class: 'inline',
        views: {
          primary: view.extend({
            class: 'inline--grow',
            views: {
              logo: view.extend({
                tag: 'a',
                class: 'logo center--vertical',
                args: {
                  href: '/'
                }
              })
            }
          }),

          secondary: view.extend({
            class: 'inline',
            views: {
              add_wrap: view.extend({
                views: {
                  button: view.extend({
                    tag:  'a',
                    class: 'btn btn--m clear js-trigger iblock lone',
                    args: {
                      href: '#',
                      'data-action': 'create_post'
                    },

                    views: {
                      inner: view.extend({
                        tag: 'i',
                        class: 'icon add'
                      })
                    }
                  }) 
                }
              }),

              account_wrap: view.extend({
                class: 'margin--left--xs',
                views: {
                  account: view.extend({
                    class: 'js-popover',
                    css: { position: 'relative' },
                    views: {
                      popover: view.extend({
                        class: 'popover popover--bottom popover--xxs popover--align--right popover--light',
                        views: {
                          accounts: view.extend({
                            tag: 'ul',
                          }),

                          actions: view.extend({
                            tag:   'ul',
                            class: 'pad--top--xxs pad--bottom--xxs',
                            views: {
                              settings: view.extend({
                                tag: 'li',
                                views: {
                                  inner: view.extend({
                                    tag:   'a',
                                    class: 'pad--xxs pad--left--s pad--right--s font_size--m block',
                                    args: {
                                      href: '/settings',
                                    },
                                    views: {
                                      icon: view.extend({
                                        tag: 'span',
                                      }),
                                      label: view.extend({
                                        tag: 'span',
                                        args: {
                                          text: 'Settings'
                                        }
                                      })
                                    }
                                  })
                                }
                              }),

                              logout: view.extend({
                                tag: 'li',
                                views: {
                                  inner: view.extend({
                                    tag:   'a',
                                    class: 'pad--xxs pad--left--s pad--right--s font_size--m block',
                                    args: {
                                      href: '#'
                                    },
                                    views: {
                                      icon: view.extend({
                                        tag: 'span',
                                      }),
                                      label: view.extend({
                                        tag: 'span',
                                        args: {
                                          text: 'Log out'
                                        }
                                      })
                                    },

                                    events: [
                                      ['click', 'self', function ()
                                      {
                                        actions.logout(null, {
                                          done: bind(window.location.reload, window.location)
                                        });
                                        return false;
                                      }]
                                    ]
                                  })
                                }
                              })
                            }
                          })
                        }
                      }),

                      button: view.extend({
                        tag:  'a',
                        class: 'avatar--m js-popover--trigger',
                        args: {
                          href: '#'
                        },

                        views: {
                          inner: view.extend({
                            tag:   'img',
                            class: 'border_radius',
                            args: {
                              alt: '',
                              src: function ()
                              {
                                return config.get('avatar').thumb.url
                              }
                            }
                          })
                        }
                      }) 
                    },

                    events: []
                  })
                }
              }) 
            }
          })
        }
      })
    }
  }));
})(_);
