(function (_)
{
  var sel   = _('selector');
  var view  = _('view');
  var utils = _('utils');
  var event = _('event');

  _('modals', view.extend({
    name: 'modals',
    cur: null,
    cur_key: null,
    prev: null,
    is_open: false,
    is_locked: false,
    views: {
      wrapper: view.extend({
        views: {
          acts:  view.extend({
            views: {
              back: view.extend({
                class: 'back',
                template: '<i class="icon back"></i>'
              }),

              close: view.extend({
                class: 'close',
                template: '<i class="icon close"></i>'
              })
            }
          }),

          float: view
        }
      })
    },
    queue:  [],
    events: [
      ['click', '.back',  'prev'], 
      ['click', '.close', 'clear'], 
    ],


    init: function()
    {
      //
      // Setup events
      event
        .on('alert',         utils.bind(this.alert,   this))
        .on('confirm',       utils.bind(this.confirm, this))
        .on('modal:open',    utils.bind(this.open,    this))
        .on('modal:hide',    utils.bind(this.prev,    this))
        .on('modal:prev',    utils.bind(this.prev,    this))
        .on('modal:close',   utils.bind(this.clear,   this))
        .on('modal:replace', utils.bind(this.replace, this));

      /**
       * Hide back button */
      this.wrapper.acts.back.hide();
    },


    prev: function(ev)
    {
      //
      // Small fix
      if (ev && ev.target !== ev.currentTarget)
      {
        ev.stopPropagation();
        return;
      }

      if (this.queue.length > 0)
      {
        if (this.cur == null) return;

        this.cur.$el.removeClass('active');
        setTimeout(utils.bind(this.cur.remove, this.cur), 200);
        this.cur = this.queue.pop();
        if (this.queue.length == 0)
          this.wrapper.acts.back.hide();
        setTimeout(utils.bind(this.__show, this), 400);
      }
      else
      {
        sel(document.body).toggleClass('modal_open', false);
        this.__hide();
      }
    },

    clear: function(ev)
    {
      /**
       * Small fix */
      if (ev && ev.target !== ev.currentTarget)
      {
        ev.stopPropagation();
        return;
      }

      this.wrapper.acts.back.hide();
      this.__hide();
    },

    replace: function (ev, type, data)
    {
      /**
       * Check if type exists */
      var modal = _('modals/' + type);
      if (!modal)
      {
        throw "Couldn't find modal: " + type;
        return;
      }

      var temp = new modal(data);
      this.wrapper.float.$el.append(temp.el);
      temp.attach();

      this.queue.push(temp);
      this.prev(ev);
    },


    lock: function()
    {
      this.is_locked = true;
    },

    unlock: function()
    {
      this.is_locked = false;
    },


    open: function(ev, type, data)
    {
      this.__open(type, data);
    },

    alert: function(ev, text, opts)
    {
      this.__open('dialog');
      this.cur.alert(text, opts);
    },

    confirm: function(ev, text, opts)
    {
      this.__open('dialog');
      this.cur.confirm(text, opts);
    },


    __show: function()
    {
      if (this.cur)
      {
        this.find('.modal').hide();
        this.cur.show();
        this.show();

        setTimeout(utils.bind(function()
        {
          this.$el.addClass('active');
          if (this.cur.fadeIn)
              this.cur.fadeIn();
          this.cur.$el.addClass('active');
        }, this), 0);
      }

      this.is_open = true;
      sel(document.body).toggleClass('modal_open', true);
    },

    __hide: function(ev)
    {
      if (!this.is_open)
        return;

      sel(document.body).toggleClass('modal_open', false);
      this.is_open = false;
      event.trigger('modal:changed');
      this.$el.removeClass('active');
      this.cur.$el.removeClass('active');
      setTimeout(utils.bind(function()
      {
        this.hide();
        this.cur.hide();
        this.cur.remove();
        this.cur   = null;
        this.queue = [];
      }, this), 200);
    },

    __open: function(type, data)
    {
      /**
       * Check if type exists */
      var modal = _('modals/' + type);
      if (!modal)
      {
        throw "Couldn't find modal: " + type;
        return;
      }

      var temp = new modal(data);
      this.wrapper.float.$el.append(temp.el);
      temp.attach();
      if (this.cur)
      {
        if (this.cur == temp) return;

        this.cur.$el.removeClass('active');
        setTimeout(utils.bind(this.cur.hide, this.cur), 200);
        this.queue.push(this.cur);
        if (this.queue.length > 1)
          this.wrapper.acts.back.show();
        this.cur = temp;
        setTimeout(utils.bind(this.__show, this), 200);
      }
      else
      {
        this.cur = temp;
        this.__show();
      }

      /**
       * Show back button if more than 2 modals are in the queue */
      if (this.queue.length > 0)
        this.wrapper.acts.back.show();

      event.trigger('modal:changed', this.cur);
    }    
  }));
})(_);
