(function (_)
{
  _('money', {
    format: function (val, sep)
    {
      while (/(\d+)(\d{3})/.test(val.toString()))
        val = val.toString().replace(/(\d+)(\d{3})/, '$1' + sep + '$2');
      return val;
    }
  });
})(_);
