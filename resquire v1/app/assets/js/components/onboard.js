(function (_)
{
  var sel   = _('selector');
  var view  = _('view');
  var event = _('event');

  var blur  = view.extend({ class: 'onboard--blur' });
  var wrap  = view.extend({ class: 'onboard--wrap' });
  var popup = view.extend({
    class: 'popover onboard--popover popover--s',
    views: {
      inner: view.extend({
        class: 'pad--s',
        views: {
          title: view.extend({
            class: 'font_size--s font_weight--700 c--white pad--bottom--xxs'
          }),

          text: view.extend({
            class: 'font_size--s font_weight--600 c--white pad--bottom--s'
          }),

          actions: view.extend({
            class: 'inline',
            views: {
              left: view.extend({
                views: {
                  skip: view.extend({
                    tag: 'a',
                    class: 'btn btn--s dark btn--skip',
                    args: {
                      href: '#',
                      text: 'Dismiss'
                    }
                  }),
                }
              }),

              right: view.extend({
                views: {
                  next: view.extend({
                    tag: 'a',
                    class: 'btn btn--s primary btn--next',
                    args: {
                      href: '#',
                      text: 'Next'
                    }
                  })
                }
              })
            },

            events: [
              ['click', '.btn--skip', function (ev) {
                _('onboard').end(true);
                return false;
              }],
              ['click', '.btn--next', function (ev) {
                _('onboard').next();
                return false;
              }]
            ]
          })
        }
      })
    }
  });

  _('onboard', {
    __step:   0,
    __items:  null,
    __parent: null,

    end: function (skipped)
    {
      if (this.__fn) this.__fn(skipped);
      if (this.__items)
      {
        var blur = this.__blur;
        var wrap = this.__wrap;
        blur.$el.removeClass('active');
        this.__popup.$el.removeClass('active');
        this.__curr.removeClass('onboard--focus');

        if(this.__items[this.__step].onExit)
          this.__items[this.__step].onExit();

        setTimeout(function ()
        {
          blur.remove();
          wrap.remove();
        }, 200);
      }

      this.__step  = 0;
      this.__curr  = null;
      this.__items = null;
    },

    start: function (items, fn)
    {
      if (!items) return;
      this.__fn    = fn;
      this.__blur  = new blur;
      this.__items = items;
      this.__wrap  = new wrap;
      this.__popup = new popup;
      this.__wrap.$el.append(this.__popup.$el);

      var item   = this.__items[0];
      var curr   = sel(item.selector);
      var parent = curr.get(0).parentNode;
      sel(parent).append(this.__blur.$el);
      curr.addClass('onboard--focus');
      this.__blur.$el.addClass('active');
      this.__popup.$el.addClass('popover--' + (item.location || 'bottom'))
      this.step(0);
    },

    next: function ()
    {
      this.step(this.__step + 1);
    },


    step: function (index)
    {
      /**
       * Check that we aren't out of range */
      if (index >= this.__items.length) return this.end(false);
      this.__step = index;

      /**
       * Let's display this shit!!! */
      var item   = this.__items[index];
      var ctx    = sel(item.selector);
      var parent = ctx.get(0).parentNode;

      if (this.__curr && this.__items[index - 1].onExit)
        this.__items[index - 1].onExit();

      if (this.__curr && ctx !== this.__curr)
        this.__curr.removeClass('onboard--focus');
      this.__curr = ctx;

      /**
       * Remove active class */
      this.__popup.$el.removeClass('active');

      setTimeout ((function ()
      {
        if (item.onEnter) item.onEnter();
        this.__curr.addClass('onboard--focus');
        this.__popup.$el.hide();
        if (this.__parent !== parent)
        {
          this.__parent = sel(parent);
          this.__parent
            .append(this.__blur.$el)
            .append(this.__wrap.$el);
        }
  
        /**
         * Change popup */
        var ppos = parent.getBoundingClientRect();
        var pos  = this.__curr.get(0).getBoundingClientRect();
        
        var x = pos.left - ppos.left;
        var y = pos.top - ppos.top;
  
        this.__wrap.$el.css({
          top:    y + 'px',
          left:   x + 'px',
          width:  pos.width + 'px',
          height: pos.height + 'px'
        })

        this.__popup.$el.removeClass('popover--top popover--left popover--right popover--bottom');
        this.__popup.inner.text.$el.html(item.text);
        this.__popup.inner.title.$el.text(item.title);
        this.__popup.$el.addClass('popover--' + (item.position || 'bottom'));
        this.__popup.$el.show();
        setTimeout((function ()
        {
          this.__popup.$el.addClass('active');
        }).bind(this), 1);
      }).bind(this), 300);
      this.__curr.addClass('onboard--focus');
    }
  });
})(_);