/*
 *
 * player.js
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * Date: 08-05-2015
 *
 * (c) 2015-2017 Laviréo
 */


(function(_)
{
  var view = _('view');

  _("player", view.extend({
    name: 'player',
    views: {
      //
      // Media
      media: view.extend({
        name: 'player--media',
        views: {
          //
          // video
          video: view.extend({
            tag: 'video',
            name: 'player--media--video',
            events: [
              ['play', 'self', '__played'],
              ['pause', 'self', '__paused'],
              ['ended', 'self', '__ended'],
              ['timeupdate', 'self', '__update'],
            ],

            init: function()
            {
              this.el.preload = true;
            },


            __ended: function()
            {
              this.parent.pause(); 
              this.parent.parent.control.main.$el.removeClass('playing');
            },

            __played: function()
            {

            },

            __paused: function()
            {

            },

            __update: function()
            {
              this.parent.parent.control.progress.__process(
                this.__current(),
                this.__buffer()
              );
            },

            __buffer: function()
            {
              var buffer = this.el.seekable.end(0)
              return (100 / this.__duration() * buffer);
            },

            __current: function()
            {
              var time = this.el.currentTime;
              return (100 / this.__duration() * time);
            },

            __duration: function()
            {
              return this.el.duration;
            }
          }),

          //
          // target
          target: view.extend({
            name: 'player--media--target',
          })
        },

        init: function()
        {
          this.playing = false;
        },

        play: function()
        {
          this.playing = true;
          this.video.el.play();
        },

        pause: function()
        {
          this.playing = false;
          this.video.el.pause();
        },

        buffer: function()
        {
          return this.video.__buffer();
        },

        setSource: function(src)
        {
          this.video.el.src = src;
          this.video.el.load();
          return this;
        },

        setBanner: function(src)
        {
          this.$el.css('background-image', src);
          return this;
        },


        setOffset: function (val)
        {
          this.video.el.currentTime = (this.video.el.duration / 100 * val);            
        },

        setVolume: function (val)
        {
          this.video.el.volume = val / 100; 
        }
      }),

      //
      // Controls
      control: view.extend({
        name: 'player--control',
        views: {
          //
          // Play/Pause
          main: view.extend({
            events: [
              ['click', '.playpause', 'togglePlay']
            ],

            args: {
              html: '<div class="playpause fcontrol"></div>'
            },

            togglePlay: function()
            {
              if (this.parent.parent.media.playing)
              {
                this.$el.removeClass('playing');
                this.parent.parent.media.pause();
              }
              else
              {
                this.$el.addClass('playing');
                this.parent.parent.media.play();
              }
            }
          }),

          //
          // Progress
          progress: view.extend({
            views: {
              wrap: view.extend({
                views: {
                  buffer: view.extend({}),
                  time: view.extend({})
                }
              })
            },

            __process: function(time, buffer)
            {
              if (!this.pressed)
                this.wrap.time.$el.css({ width: time + '%' });
              this.wrap.buffer.$el.css({ width: buffer + '%' });
            },

            events: [
              ['mousedown', '.player--control--progress--wrap', function (ev)
              {
                var width       = this.__getRelativePos(ev);
                this.pressed    = true;
                this.percentage = width;
                this.wrap.$el.toggleClass('no_animate', true);
              }]
            ],


            init: function () 
            {
              var sel  = _('selector');
              var self = this;
              sel(document)
                .on('mouseup', function (ev)
                {
                  if (!!self.pressed)
                  {
                    self.pressed = false;
                    self.wrap.$el.toggleClass('no_animate', false);
                    self.parent.parent.media.setOffset(self.percentage);
                  }
                })
                .on('mousemove', function (ev)
                {
                  if (self.pressed)
                  {
                    var width       = self.__getRelativePos(ev);
                    self.percentage = width;
                    self.wrap.time.$el.css({ width: width + '%' });
                  }
                });
            },

            __getRelativePos: function(ev)
            {
              var rect       = this.wrap.el.getBoundingClientRect()
              var offset     = ev.clientX - rect.left;
              var width      = rect.width;
              var percentage = 100 / width * offset;
              return percentage < 0 ? 0 : percentage > 100 ? 100 : percentage; 
            }
          }),

          //
          // Options
          options: view.extend({
            views: {
              //
              // Volume
              volume: view.extend({
                class: 'fbutton'
              }),

              //
              // Fullscreen
              fullscreen: view.extend({
                class: 'fbutton'
              })
            },

            events: [
              ['click', '.player--control--options--volume', '__toggleVolume'],
              ['click', '.player--control--options--fullscreen', '__toggleFullscreen'],
            ],

            __toggleVolume: function (ev)
            {
              var sel    = _('selector');
              var target = ev.currentTarget;
              var $el    = sel(target);
              var muted  = $el.hasClass('muted');
              $el.toggleClass('muted', !muted);
              this.parent.parent.media.setVolume(muted ? 100 : 0);
            },

            __toggleFullscreen: function (ev)
            {
              var sel        = _('selector');
              var target     = ev.currentTarget;
              var $el        = sel(target);
              var player     = this.parent.parent;
              var fullscreen = player.isFullscreen();
              $el.toggleClass('zoomed', !fullscreen);
              player[fullscreen ? 'exitFullscreen' : 'enterFullscreen']();
            }
          })
        }
      }),
    },


    init: function()
    {
      if (this.get('src'))
      {
        this.setMedia(this.get('src'));
      }


      var i      = 0;
      var pfx    = ["webkit", "moz", "ms", "o", ""];
      var self   = this;
      var target = self.$el;

      for (; i < pfx.length; i++)
      {
        var name = pfx[i] + 'fullscreenchange';
        target.on(name, function (ev)
        {
          target.toggleClass('fullscreen', self.isFullscreen());
        });
      }

      return this;
    },

    setMedia: function(src)
    {
      this.media.setSource(src);
      return this;
    },

    setBanner: function(src)
    {
      this.media.setBanner(src);
      return this;
    },


    isFullscreen: function ()
    {
      var i    = 0;
      var pfx  = ["webkit", "moz", "ms", "o", ""];
      var name;
      var type = 'FullscreenElement';
      while (i < pfx.length && !document[name])
      {
        var name = type;
        if (pfx[i] == "")
        {
          name = name.substr(0,1).toLowerCase() + name.substr(1);
        }

        name = pfx[i] + name;

        var test = typeof document[name];
        if (test != "undefined")
        {
          return document[name] != null;
        }

        i++;
      }
    },

    exitFullscreen: function ()
    {
      var i    = 0;
      var pfx  = ["webkit", "moz", "ms", "o", ""];
      var name;
      var type = 'ExitFullscreen';
      while (i < pfx.length && !document[name])
      {
        var name = type;
        if (pfx[i] == "")
        {
          name = name.substr(0,1).toLowerCase() + name.substr(1);
        }

        name = pfx[i] + name;

        var test = typeof document[name];
        if (test != "undefined")
        {
          return document[name]();
        }

        i++;
      }

      console.log ("Derp");
    },

    enterFullscreen: function ()
    {
      var i    = 0;
      var pfx  = ["webkit", "moz", "ms", "o", ""];
      var name;
      var type = 'RequestFullScreen';
      while (i < pfx.length && !this.el[name])
      {
        var name = type;
        if (pfx[i] == "")
        {
          name = name.substr(0,1).toLowerCase() + name.substr(1);
        }

        name = pfx[i] + name;

        var test = typeof this.el[name];
        if (test != "undefined")
        {
          return this.el[name]();
        }

        i++;
      }
    },


    __update: function()
    {
    }
  }));
})(_);
