(function (_)
{
  var view       = _('view');
  var bind       = _('utils').bind;
  var event      = _('event');
  var store      = _('store');
  var config     = _('config');
  var actions    = _('actions');
  var date_utils = _('date/utils');
  var date_names = _('date/names');


  var POSTS_NUM = 15;
  var POSTS_PRE = 30;


  _('posts', view.extend({
    css: { position: 'relative' },
    class: 'wrap--xs pad--top--xxl pad--bottom--xxl',
    temps: {
      nothing: view.extend({
        class: 'pad--top--xxl pad--bottom--xxl textAlign--m wrap--xxs',
        views: {
          lead: view.extend({ 
            tag: 'h1',
            class:    'margin--bottom--s font_size--xxl',
            template: 'No posts yet :('
          }),
          sub: view.extend({ 
            tag: 'p',
            class:    'c--lightgrey font_size--xl',
            template: 'You should schedule your next Instagram post now!'
          })
        }
      }),

      post: view.extend({
        css: { position: 'relative' },
        name:  'post',
        class: 'margin--bottom--l',
        views: {
          meta: view.extend({
            css: { position: 'relative' },
            class: 'cf margin--bottom--xs',
            views: {
              date: view.extend({
                views: {
                  day: view.extend({
                    tag: 'span',
                    args: {
                      text: function ()
                      {
                        var at    = this.attr.scheduled_at;
                        var date  = new Date(at);
                        return date_utils.display(date);
                      }
                    }
                  }),

                  time: view.extend({
                    tag: 'span',
                    args: {
                      text: function ()
                      {
                        var at      = this.attr.scheduled_at;
                        var date    = new Date(at);
                        var hours   = date.getHours();
                        var minutes = date.getMinutes();

                        hours   = hours < 10   ? '0' + hours   : hours;
                        minutes = minutes < 10 ? '0' + minutes : minutes;

                        return hours + ':' + minutes;
                      }
                    }
                  })
                }
              }),

              acts: view.extend({
                views: function ()
                {
                  var t = {};
                  if (this.attr.state === 'queued')
                    t.publish = view.extend({
                      tag:   'a',
                      class: 'publish_act',
                      args: {
                        href: '#',
                        html: '<i class="icon publish"></i>Publish'
                      }
                    });

                  if (['cancelled', 'pendind', 'posted'].indexOf(this.attr.state) == -1)
                  {
                    t.delete =view.extend({
                      tag:   'a',
                      class: 'cancel_act',
                      args: {
                        href: '#',
                        html: '<i class="icon cancel"></i>Discard'
                      }
                    });
                  }

                  return t;
                }
              })
            }
          }),
          wrap: view.extend({
            css: { position: 'relative' },
            views: {
              inner: view.extend({
                views: function ()
                {
                  var t = {};
                  t.image = view.extend({
                    tag: 'img',
                    args: {
                      alt: 'Post',
                      src: function ()
                      {
                        return this.get('image').thumb.url;
                      }
                    }
                  });

                  if (this.attr.state === 'failed')
                    t.overlay = view.extend({
                      views: {
                        wrap: view.extend({
                          views: {
                            msg: view.extend({
                              class: 'font_size--l font_weight--700 c--white pad--left--l  pad--right--l',
                              args: {
                                text: 'We missed this post. Please try again!'
                              } 
                            }),

                            act: view.extend({
                              class: 'pad--top--m pad--left--l pad--right--l center',
                              views: {
                                retry: view.extend({
                                  tag:   'a',
                                  class: 'btn full btn--m white retry_act',
                                  args: {
                                    href: '#',
                                    text: 'Retry Now'
                                  }
                                })
                              }
                            })
                          } 
                        })
                      } 
                    }); 

                  return t;
                } 
              })
            }
          })
        },

        events: [
          ['click', '.post--wrap--inner', function (ev)
          {
            event.trigger('modal:open', 'view_post', this.attr);
          }],
          ['click', '.edit_act', function (ev) {}],
          ['click', '.retry_act', function (ev) {
            if (config.dat.pro)
            {
              actions.posts_retry({
                post_id: this.attr.id
              }, {
                fail: function ()
                {
                  event.trigger('alert', 'Woops! Something went wrong.');
                }
              });
            }
            else event.trigger('modal:open', 'upgrade');
            return false;
          }],
          ['click', '.cancel_act', function (ev) {
            var self = this;
            event.trigger('confirm', 'You are about to discard this post!', {
              confirm: 'Discard',
              done: function ()
              {
                actions.posts_cancel({
                  post_id: self.attr.id
                }, {
                  fail: function ()
                  {
                    event.trigger('alert', 'Woops! Something went wrong.');
                  },
                  done: function ()
                  {
                    event.trigger('post:cancelled', self);
                  }
                })
              }
            });
            return false;
          }],
          ['click', '.publish_act', function (ev) {
            var self = this;
            event.trigger('confirm', 'Continue to publish this post?', {
              confirm: 'Publish',
              done: function ()
              {
                actions.posts_publish({
                  post_id: self.attr.id
                }, {
                  fail: function ()
                  {
                    event.trigger('alert', 'Woops! Something went wrong.');
                  }
                })
              }
            });
            return false;
          }]
        ]
      })
    },

    init: function ()
    {
      var self = this;
      actions.posts({}, {
        fail: bind(this.appendNothing, this),
        done: function (raw)
        {
          var blob = JSON.parse(raw); 
          var data = blob.data;
          var i    = 0;
          var len  = data.length;

          if (len < 1)
          {
            self.appendNothing();
            return;
          }

          for (; i < len; i++)
            self.appendPost(data[i]);
        }
      });

      event
        .on('post:added', bind(function (ev, data) {
          var i   = 0;
          var len = this.posts.length;

          if (this.nothings.length > 0)
          {
            this.nothings[0].remove();
            this.nothings.splice(0, 1);
          }

          for (; i < len; i++)
          {
            ///
            // Insert if date is less than
            if (this.posts[i].attr.scheduled_at > data.scheduled_at)
            {
              this.insertPost(i + 1, data);
              return;
            }
          }

          this.appendPost(data);
          this.__showTour();
        }, this))
        .on('post:cancelled', bind(function (ev, data) {
          var index = this.posts.indexOf(data); 
          this.posts[index].remove();
          this.posts.splice(index, 1);

          if (this.posts.length < 1)
            this.appendNothing();
        }, this));
    },

    __showTour: function ()
    {
      /**
       * Intro Tour */
      var self      = this;
      var completed = store.get('tour-complete:first_post');
      if (!completed)
      {
        _('onboard').start([
          {
            selector: this.posts[0].wrap.$el,
            position: 'bottom',
            title:    'Your Timeline',
            text:     'Here you can manage and view your upcoming and past posts.'
          },
          {
            selector: this.posts[0].meta.$el,
            position: 'bottom',
            title:    'Post Controls',
            text:     'Hover over a post to reveal the post controls.',
            onEnter:  function () { self.posts[0].$el.addClass('active') },
            onExit:   function () { self.posts[0].$el.removeClass('active') }
          }
        ], function (dismissed)
        {
          store.set('tour-complete:first_post', !0);
        });
      }
    }
  }));
})(_);
