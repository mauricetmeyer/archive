/**
 * progress.js
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * Date: 17-07-2017
 *
 * (c) 2017 Laviréo
 */


(function (_)
{
  var bind  = _('utils').bind;
  var view  = _('view');
  var event = _('event');

  _('progress', view.extend({
    name: 'progress',
    views: {
      inner: view.extend({})
    },

    init: function ()
    {
      ///
      // Setup events
      event
        .on('progress:set',  bind(this.__set,    this))
        .on('progress:hide', bind(this.__hide,   this))
        .on('progress:fail', bind(this.__fail,   this))
        .on('progress:show', bind(this.$el.show, this.$el));
    },


    __set: function (ev, val)
    {
      if (this._timeout) clearTimeout(this._timeout);
      this._timeout = 0;
      this.$el.toggleClass('error', false);
      val = val > 100 ? 100 : val < 0 ? 0 : val;
      if (this.$el.css('display') === 'none')
        this.$el.show();

      this.inner.$el.css({ width: val + '%' });
    },
    __hide: function (ev)
    {
      this._timeout = 0;
      this.$el.hide();
      this.inner.$el.css({ width: '0%' });
    },
    __fail: function (ev)
    {
      this.$el.addClass('error');    
      this.inner.$el.css({ width: '100%' });
      if (this._timeout) clearTimeout(this._timeout);
      this._timeout = setTimeout(this.__hide.bind(this), 10000);
    }
  }));
})(_);
