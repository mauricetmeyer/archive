(function (_)
{
  var xhr    = _('transports').xhr;
  var sel    = _('selector');
  var view   = _('view');
  var bind   = _('utils').bind;
  var event  = _('event');
  var cache  = _('cache');
  var router = _('router');

  _('shell', view.extend({
    id:         'shell',
    curr:       null,
    cache:      null,
    views:      null,
    history:    window.history,
    location:   window.location,
    shells_max: 10,

    init: function ()
    {
      this.cache = {};
      this.views = {};

      /**
       * Setup events */
      event.on('shell:navigate', bind(this.__navigate, this));
      sel(window).on('popstate', bind(this.__change,   this));

      /**
       * Initial page load */
      this.__display(this.location.pathname);
      this.history.replaceState({
        path: this.location.pathname
      }, document.title);
    },

    __make: function (path, key)
    {
      return {
        key:    key  || Date.now(),
        path:   path || this.location.pathname,
        tree:   null,
        scroll: { x: 0, y: 0 }
      }; 
    },

    __build: function (path)
    {
      /**
       * Find route */
      var route = router.find(path);
      if (route !== null)
      {
        /**
         * Go up the route tree */
        var tree = [];
        var data = route.data;
        var node = route.route;
        do
        {
          /**
           * Does the route have a view */
          if (!node.view)
            continue;

          /**
           * Let's get the view */
          var view = _(node.view);
          if (!view)
          {
            /**
             * NOTE (Maurice):
             * This should not be called, only in development.
             * If it gets called in production collect some analytics. */
            throw new Error("Undefined view: " + node.view); 
          }

          /**
           * Write to tree */
          tree.push(view);
        }
        while (node = node.parent);

        /**
         * Return new tree */
        return tree.reverse();
      }

      return null;
    },

    __change: function (ev)
    {
      /**
       * Get some stuff from state */
      var state   = ev.state;
      var newKey  = state.key;
      var currKey = this.curr.key;

      /**
       * Previous or next? */
      var prev  = currKey > newKey;

      /**
       * Close open modals */
      _('event').trigger('modal:close');

      /**
       * Transition */
      this.__display(state.path);
    },

    __display: function (path)
    {
      /**
       * Nothing will change */
      if (this.__path === path)
        return;

      var self = this;
      var tree = this.__build(path);
      if (!!tree)
      {
        /**
         * Set new path */
        this.__path = path;

        /**
         * Save scroll state */
        var data = self.__make();
        data.scroll = { 
          x: window.pageXOffset,
          y: window.pageYOffset
        };


        /**
         * Create shell from tree; only replace nodes
         * that are new */
        var i    = 0;
        var len  = tree.length;
        var map  = [];
        var prev = null;
        var view = null;
        for (; i < len; i++)
        {
          /**
           * Create view and append to tree */
          var cont = null;
          var temp = tree[i];
          var curr = this.curr;
          var inst = (curr && curr[i] && curr[i][1] instanceof temp);
          var node = inst ? curr[i][1]
                          : new temp();

          var cont = inst ? curr[i][1]
                          : node.find('.shell--view');

          if (prev && !inst)
          {
            /**
             * Has route--view class?
             *
             * Sure, let's just append it! */
            var $el = prev.find('.shell--view');
            if ($el)
            {
              node.$el.addClass('shell');
              node.$el.css({ opacity: '0', display: 'none' });
              $el.append(node.el);
              node.attach();
            }
            /**
             * NOTE (Maurice):
             * This shouldn't happen in production
             * but just to make sure we should track
             * some analytics */
            else
            {
              throw new Error("View is missing route--view!"); 
            }
          }

          /**
           * Also write it to the map */
          map.push([temp, node, cont]);
          prev = node;
        }


        /**
         * Display */
        var curr = this.curr;
        this.__transition(curr, map);
        this.curr = map;

        event.trigger('shell:changed', path);
        return true;
      }
    },

    __navigate: function (ev, path)
    {
      var self  = this;
      if (this.__display(path))
      {
        /**
         * Push new state */
        this.history.pushState({
          key:  Date.now(),
          path: path
        }, "Resquire", path);
      }
    },

    __transition: function (lhs, rhs)
    {
      /**
       * NOTE (Maurice):
       * I have to find a better place for this as
       * it's only true on the initial load. */
      if (!lhs)
      {
        var i = 0;
        var l = rhs.length;
        this.$el.append(rhs[0][1].el);
        for (; i < l; i++)
        {
          var ri = rhs[i];
          ri[1].$el.show();
          ri[1].$el.css({ opacity: 1 });
        }

        return;
      }



      /**
       * Small helper */
      var processTransition = function (lhs, rhs)
      {
        setTimeout(function ()
        {
          if (lhs) lhs.remove();
          rhs.$el.show();
          setTimeout(function ()
          {
            rhs.$el.css({ opacity: 1 });
          }, 0);
        }, 100);
      }


      /**
       * This is where the actual replacement takes place,
       * we compare the current and new tree and replace
       * as soon as a non match appears. */
      var i  = 0;
      var l  = rhs.length;
      var ll = lhs.length;
      for (; i < l; i++)
      {
        var li = lhs[i];
        var ri = rhs[i];

        /**
         * Nothing left on the left hand side */
        if (i >= ll)
        {
          processTransition(null, ri[1]);
          continue;
        }

        /**
         * Check if the elements matches
         * and return early. */
        if (ri[1] instanceof li[0])
        {
          li[1] = ri[1];
          continue;
        }


        /**
         * Ok now to the tricky part...
         * We can do this! I believe in you.
         *
         * NOTE (Maurice):
         * I have to think of a way to unbind events later on
         * lhs.unbind(); */
        li[1].$el.css({ opacity: 0 });
        if (!(lhs[i - 1] && lhs[i - 1][2] !== null))
          this.$el.append(ri[1].el);

        processTransition(li[1], ri[1]);
      }
    }
  }))
})(_);
