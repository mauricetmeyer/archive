(function(_)
{
  _('store', {
    get: function (key)
    {
      return localStorage.getItem(key);
    },

    del: function (key)
    {
      localStorage.removeItem(key);
    },

    set: function (key, val)
    {
      localStorage.setItem(key, val);
    }
  });
})(_);
