(function (_)
{
  var sel   = _('selector');
  var token = (sel('meta').filter(function () {
    if (this.name == 'stripe-token') return this;
  }).attr('content'));

  /**
   * Workaround so we don't produce an error when we can't connect to
   * stripe.js */
  _('stripe', (window.Stripe || function () {})(token));
})(_);
