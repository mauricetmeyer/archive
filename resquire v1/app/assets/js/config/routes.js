_('routes', {
  /**
   * Auth */
  login: {
    path:  '/login',
    view:  'auth',
    childs: {
      root: {
        title: 'Resquire - Welcome Back!',
        view:  'auth/login'
      }
    }
  },

  signup: {
    path:  '/register',
    view:  'auth',
    childs: {
      root: {
        title: 'Resquire - Get started for free!',
        view:  'auth/register'
      }
    }
  },


  /**
   * Root */
  home: {
    path:  '/',
    view:  'app',
    childs: {
      /**
       * Root */
      root: {
        title: 'Resquire',
        view:  'timeline/index',
      },


      /**
       * Accounts */
      accounts: {
        path:  'accounts',
        view:  'account',
        childs: {
          root: {
            title: 'Resquire - Accounts',
            view:  'teams/index',
          },

          members: {
            path:  'members',
            title: 'Resquire - Members',
            view:  'teams/members'
          }
        }
      },


      /**
       * Settings */
      settings: {
        path:  'settings',
        view:  'settings',
        childs: {
          root: {
            title: 'Resquire - Settings',
            view:  'settings/index'
          },
          billing: {
            path:  'billing',
            title: 'Resquire - Billing',
            view:  'settings/billing'
          },
          activity: {
            path:  'activity',
            title: 'Resquire - Security History',
            view:  'settings/activity'
          }
        }
      }  
    }
  }
});
