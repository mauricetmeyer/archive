/*
 *
 * cookie.js
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * Date: 27-02-2017
 *
 * (c) 2017 Laviréo
 */


(function(_)
{
  ///
  // Register cookie module
  _('cookie', {
    get: function (key)
    {
      var i       = 0;
      var cookies = document.cookie.split(/;\s*/)

      for (; i < cookies.length; i++)
      {
        var res = cookies[i].split("=");
        var val = decodeURIComponent(res[0]);
        if (val === key)
          return decodeURIComponent(res[1])
      }

      return 0;
    },

    set: function (key, val, opts)
    {
      var opts = {};
      var data = encodeURIComponent(key) + "=" + encodeURIComponent(val);
      return opts || (opts = {}), opts.expires && (data += "; expires=" + opts.expires),
             opts.path && (data += "; path=" + encodeURIComponent(opts.path)),
             document.cookie = data, data; 
    }
  });
})(_);
