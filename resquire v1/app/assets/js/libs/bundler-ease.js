/*
 *
 * ease.js
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * Date: 27-02-2017
 *
 * (c) 2017 Laviréo
 */


(function(_)
{
  var easing = {
    none: function (t)
    {
      return t;
    },

    easeInCubic: function (t)
    {
      return t * t * t
    },

    easeOutCubic: function (t)
    {
      return (--t) * t * t + 1
    },

    easeInOutCubic: function (t)
    {
      return t <.5 ? 4 * t * t * t : (t - 1) * (2 * t - 2) * (2 * t - 2) + 1
    },
  };

  ///
  // Register ease module
  _('ease', function (opts)
  {
    var conf = {
      type:   opts.type   || 'none',
      period: opts.period || 500,
      update: opts.update || function () {}
    };

    if (!easing[conf.type])
    {
      return null;
    }

    var anim = {
      curr: 0,
      tick: function (delta)
      {
        this.curr += delta;
        var val    = 1 / conf.period * this.curr;
        var eased  = easing[conf.type](val > 1 ? 1 : val);
        conf.update(eased);
      }
    };

    ///
    // Setup instance

    ///
    // Return instance
    return anim;
  });
})(_);
