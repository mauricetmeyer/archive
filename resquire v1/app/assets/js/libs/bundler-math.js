_('math', {
  lerp: function(f, g, d)
  {
    return g + (d - g) * f
  },

  map: function(i, h, f, g, d)
  {
    return this.lerp(this.norm(i, h, f), g, d)
  },

  norm: function(g, f, d)
  {
    return (g - f) / (d - f)
  },

  clamp: function(g, f, d)
  {
    return Math.max(f, Math.min(d, g))
  }
});

