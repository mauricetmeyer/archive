/*
 *
 *  Un projet par Laviréo
 *
 *  transport.js
 *
 *  Author: Maurice T. Meyer
 *  E-Mail: maurice@lavireo.com
 *
 *  Date: 18-03-2016
 *
 *  (c) 2016 Laviréo
 */


_('transports', {
  xhr: function(path, options)
  {
    //
    // Force options to be an object
    options = options || {};

    //
    // Load dependencies
    var utils = _('utils');

    //
    // Options
    var opts = {
      path:     path,
      data:     options.data     || null,
      type:     options.type     || "GET",
      done:     options.done     || null,
      fail:     options.fail     || null,
      progress: options.progress || null,
      process:  options.process,
      headers:  options.headers || {}
    };

    //
    // timeout handle
    var timeoutTimer;

    //
    // Request state
    var completed;

    //
    // Headers
    var headers = {};
    var headers_res;
    var headers_str;

    //
    // Fake xhr
    var xhr = {
      tXHR: null,
      readyState: 0,

      //
      // Caches the header
      setRequestHeader: function(name, value)
      {
        if (completed == null)
        {
          headers[name] = value;
        }

        return this;
      },

      getResponseHeader: function(key)
      {
        var match;
        if (completed)
        {
          if (!headers_res)
          {
            headers_res = {};
            var pairs = headers_str.split('\r\n');
            for (var i = 0; i < pairs.length; i++)
            {
              var pair  = pairs[i];
              var index = pair.indexOf(': ');
              if (index > 0)
              {
                var key = pair.substring(0, index).toLowerCase();
                var val = pair.substring(index + 2);
                headers_res[key] = val;
              }
            }
          }

          match = headers_res[key.toLowerCase()];
        }

        return match == null ? null : match;
      },

      //
      // Send the request
      send: function()
      {
        this.tXHR = new window.XMLHttpRequest();
        var tXHR  = this.tXHR;

        //
        // Listen to events
        tXHR.onreadystatechange = function()
        {
          if (tXHR.readyState === 4)
          {
            headers = tXHR.getAllResponseHeaders();
            done(
              tXHR.status,
              tXHR.statusText,
              tXHR.responseText,
              headers
            );
          }
        };

        tXHR.onabort = tXHR.onerror = function()
        {
          done(
            tXHR.status,
            tHXR.statusText
          );
        };

        tXHR.upload.addEventListener('progress', function(ev)
        {
          var perc = (ev.loaded / ev.total) * 100;
          if (opts.progress)
          {
            (utils.bind(opts.progress, tXHR))(perc);
          }
        });

        //
        // Open request
        tXHR.open(
          opts.type,
          opts.path,
          true
        );

        //
        // Set headers
        for (var i in headers)
        {
          tXHR.setRequestHeader(i, headers[i]);
        }

        //
        // Send the request
        try
        {
          tXHR.send(opts.data || null);
        }
        //
        // Throw an error
        catch (e)
        {
          throw e;
        }

        //
        // Return
        return this;
      },

      //
      // Cancel the request
      cancel: function()
      {
        if (this.tXHR)
          this.tXHR.abort();

        //
        // Return
        return this;
      }
    };

    //
    // Add protocol
    opts.path = (path).replace(/^\/\//, location.protocol + "//");

    //
    // Convert data if not already a string
    var flag = opts.process !== false;
    if (flag && options.data && typeof options.data !== "string")
    {
      opts.data = utils.serialize(opts.data);
    }

    //
    // Uppercase the type
    opts.type = opts.type.toUpperCase();

    //
    // More options handling for requests with no content
    if (opts.type === "GET")
    {
      //
      // If data is available, append data to url
      if (opts.data)
      {
        opts.path += (/\?/.test(opts.path) ? "&" : "?") + opts.data
      }
    }
    else
    {
      //
      // Set the correct header, if data is being sent
      if (opts.data && flag)
      {
        xhr.setRequestHeader(
          "Content-Type",
          "application/x-www-form-urlencoded; charset=UTF-8"
        )
      }
    }

    //
    // Set the Accepts header
    xhr.setRequestHeader(
      "Accept",
      "application/json, text/javascript; q=0.01"
    );

    ///
    // Set XHR header
    xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');

    //
    // Check for headers option
    for (var i in opts.headers)
    {
      xhr.setRequestHeader(i, opts.headers[i]);
    }

    //
    // Set state
    xhr.readyState = 1;

    if (completed)
    {
      return xhr;
    }

    //
    // Timeout
    var timeout = opts.timeout || 0;
    if (timeout > 0)
    {
      timeoutTimer = window.setTimeout(function()
      {
        xhr.cancel();
      }, timeout);
    }

    try
    {
      completed = false;
      xhr.send(headers, done);
    }
    catch (e)
    {
      //
      // Return exception
      done(-1, e);
    }

    //
    // Callback for when everything is done
    function done(status, nativeStatusText, response, resheaders)
    {
      var error;
      var success;
      var statusText = nativeStatusText;

      //
      // Ignore repeat invocations
      if (completed)
      {
        return;
      }

      completed = true;

      //
      // Clear timeout
      if (timeoutTimer)
      {
        window.clearTimeout(timeoutTimer);
      }

      //
      // Cache response headers
      headers_str = resheaders || "";

      //
      // Set readyState
      xhr.readyState = status > 0 ? 4 : 0;

      //
      // Determine if successful
      success = status >= 200 && status < 300 || status === 304;

      //
      // If successful, handle type chaining
      if (success)
      {
        //
        // No content
        if (status === 204 || opts.type === "HEAD")
        {
          statusText = "nocontent";
        }
        //
        // Not modified
        else if (status === 304)
        {
          statusText = "notmodified";
        }
        else
        {
          success = !error;
        }
      }
      else
      {
        error      = statusText;
        status     = status < 0 ? 0 : status;
        statusText = !statusText ? "error" : statusText;
      }

      //
      // Set data for the fake xhr object
      xhr.status = status;
      xhr.statusText = (nativeStatusText || statusText) + "";

      //
      // Success/Error
      var flag = success ? opts.done != null : opts.fail != null;
      if (flag)
      {
        var call = utils.bind(
          success ? opts.done : opts.fail,
          xhr
        );

        //
        // Success and error callback
        call(response);
      }
    }

    return xhr;
  }
});
