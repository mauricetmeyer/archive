/*
 *
 * view.js
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * Date: 01-01-2015
 *
 * (c) 2015-2016 Laviréo
 */


(function(_)
{
  var utils = _('utils');
  var view  = function(args, name)
  {
    var sel = _('selector');

    //
    // Name
    this.name = this.name || name;

    if (args && args.el)
    {
      this.el = args.el; 
      delete args.el;
    }

    //
    // Construct or attach to an element
    if (!this.el) {
      var opts = {
        id:    this.id   ? this.id   : null,
        class: this.name ? this.name : null
      };

      var el = sel('<' + this.tag + '>');
      el.attr(opts);
      this.__setEl(el);
    } else {
      this.__setEl(this.el);
    }

    //
    // Setup args
    this.attr = args || {};

    //
    // Setup
    this.__setCss();
    this.__setArgs();
    this.__setTemps();
    this.__setClass();
    this.__setViews();
    this.__setEvents();
    this.__setDefaults();
    this.__setTemplate();

    //
    // Init
    this.el.view = this;
    this.init();
    this.update();

    return this;
  };


  var tip  = utils.tip;
  var spec = {
    text: function(value)
    {
      var val = tip(this.attr, value);
      this.$el.text(val);
    },

    html: function(value)
    {
      var val = tip(this.attr, value);
      this.$el.html(val);
    }
  };

  var opts = {
    t: function(key)
    {
      return key; 
    },

    d: function(timestamp, shorten)
    {
      ///
      // NOTE (Maurice):
      // Remember dividing Date.now() by 1000!
      return _('formatter').date(timestamp, shorten);
    },

    find: function(sel)
    {
      return this.$el.find(sel);
    },

    tag: 'div',
    init: function()
    {
      return this;
    },

    get: function(key)
    {
      return (this.attr[key] !== undefined) ? this.attr[key] : false;
    },

    set: function(args, value)
    {
      if (typeof args === 'string')
      {
        var tmp = {};
        tmp[args] = value;
        args = tmp;
      }

      //
      // Update args
      for (var arg in args)
      {
        this.attr[arg] = args[arg];
      }

      this.__setCss();
      this.__setArgs();
      this.__setTemplate();
      this.onChange();

      //
      // Update subviews
      if (this.views) {
        var view;
        for (view in this.views) {
          if (this[view] && typeof this[view] !== 'string')
          {
            this[view].set(args);
          }
        }
      }

      //
      // Update temps
      if (this.temps)
      {
        var temp;
        for (temp in this.temps) {
          var name = temp + 's';
          if (this[name])
          {
            utils.each(this[name], function(obj, it) {
              obj.__setCss();
              obj.__setArgs();
            });
          }
        }
      }
    },


    hide: function() {
      this.$el.hide();
      if (this.onHide) this.onHide();
      return this
    },
    show: function() {
      this.$el.show();
      if (this.onShow) this.onShow();
      return this
    },

    update: function()
    {
      this.__setCss();
      this.__setArgs();

      if (typeof this.views === 'function' && this.__viewCache) {
        this.$el.html("");
        for (var view in this.__viewCache) {
          if (this[view] && typeof this[view] !== 'string')
            this[view] = undefined;
        }

        this.__setViews();
      }

      this.__setTemplate();
      this.onChange();

      ///
      // Update subviews
      if (this.views) {
        var views = utils.value(this.views, this);
        for (var view in views) {
          if (this[view] && typeof this[view] !== 'string')
            this[view].update();
        }
      }

      ///
      // Update temps
      if (this.temps)
      {
        var temp;
        for (temp in this.temps) {
          var name = temp + 's';
          if (this[name])
          {
            utils.each(this[name], function(obj, it) {
              obj.update();
            });
          }
        }
      }
    },

    attach: function ()
    {
      this.isAttached = true;
      for (var key in this.__viewCache)
      {
        var view = this.__viewCache[key];
        if (!view.isAttached) view.attach();
      }

      if (this.onAttach)
        this.onAttach();
    },

    remove: function()
    {
      this.$el.remove();
    },

    onChange: function()
    {
      return this; 
    },

    __setEl: function(element)
    {
      var sel = _('selector');
      this.$el = element instanceof sel ? element : sel(element);
      this.el = this.$el.get(0);
      return this;
    },

    __setCss: function()
    {
      if (!this.css) return this;

      for (arg in this.css)
      {
        var val = (typeof this.css[arg] === 'function')
                ? this.css[arg].call(this)
                : this.css[arg];

        this.$el.css(arg, val);
      }
    },

    __setArgs: function()
    {
      if (!this.args) return this;

      for (arg in this.args)
      {
        var val = (typeof this.args[arg] === 'function')
                ? this.args[arg].call(this)
                : this.args[arg];

        //
        // Special argument
        if (spec[arg])
          spec[arg].call(this, val);

        //
        // Normal
        else
          this.$el.attr(arg, val);
      }
    },

    __setClass: function()
    {
      if (!this.class) return this;

      this.$el.addClass((typeof this.class === 'function')
        ? this.class.apply(this)
        : this.class
      );
    },

    __setTemps: function() {
      if (!this.temps) return this;

      var add = function(method, collect, view)
      {
        if (method === 'insert')
          return function(i, data) {
            var tv = new view(data);
            if (tv)
            {
              this[collect].splice(i, 0, tv);
              this.$el.insert(i, tv.el);
            }
            return this;
          }
        else
          return function(data) {
            var tv = new view(data);
            if (tv)
            {
              this[collect][method === 'prepend' ? 'unshift' : 'push'](tv);
              this.$el[method](tv.el);
              tv.attach();
            }
            return this;
          }
      };

      for (temp in this.temps) {
        var name = temp;
        var view = this.temps[temp];
        var method = name.charAt(0).toUpperCase() + name.slice(1);
        var collect = name + 's';

        //
        // Add Collection
        if (!this[collect]) {
          this[collect] = [];
        }

        //
        // Add Method
        var methods = ['append', 'prepend', 'insert'];
        for (key in methods)
        {
          t_method = methods[key];
          if (!this[t_method + method])
            this[t_method + method] = add(t_method, collect, view);
        }
      }
    },

    __setViews: function() {
      if (!this.views) return this;

      //
      // Setup views
      var key;
      var view;
      var temp;
      var views = utils.value(this.views, this);
      var cache = {};
      for (key in views)
      {
        //
        // View
        if (!this[key]) {
          view = views[key];
          if (typeof view === 'string')
            this.$el.append(view);
          else
          {
            var name;
            if (this.name) name = (this.name + '--' + key);
            temp = new view(this.attr, name);
            this[key] = cache[key] = temp;
            this.$el.append(temp.el);
            temp.parent = this;
            if (this.isAttached) temp.attach();
          }
        } else {
          throw "Cannot overwrite the value " + key;
        }
      }

      this.__viewCache = cache;
    },

    __setEvents: function() {
      if (!this.events) return this;

      //
      // Setup events
      for (var i = 0; i < this.events.length; i++) {
        var event  = this.events[i];
        var ev     = event[0];
        var sel    = event[1];
        var call   = event[2];
        var method = utils.bind((typeof event[2] === 'function')
                    ? event[2]
                    : this[call], this);

        if (!method) return;

        //
        // Without selector
        if (sel == 'self') {
          this.$el.on(ev, method);
        } else {
          this.$el.on(ev, sel, method);
        }
      }

      return this;
    },

    __setDefaults: function()
    {
      if (!this.defaults) return this;

      var utils = _('utils');
      for (var x in this.defaults)
      {
        var def = this.defaults[x];

        //
        // templates
        if (this[x] && utils.isObject(this[x]) && !!def.length)
        {
          var y    = 0;
          var l    = def.length;
          var name = x.slice(0, -1);
          for (; y < l; y++)
          {
            this['append' + name.charAt(0).toUpperCase() + name.slice(1)](def[y]);
          }

          continue;
        }

        //
        // arguments
        this.set(x, def);
      }
    },

    __setTemplate: function ()
    {
      if (!this.template) return this;
      this.$el.html(tip(this.attr, this.template));
    },

    __storage: {}
  }

  //
  // Construct
  for (var opt in opts) view.prototype[opt] = opts[opt];

  view.extend = function(attr)
  {
    var key;
    var res;
    var parent = this;
    var ctor   = function() {};
    var child  = function()
    {
      return parent.apply(this, arguments);
    };

    //
    // Inherit
    for (key in parent)
      child[key] = parent[key];

    ctor.prototype              = parent.prototype;
    res                         = new ctor;

    //
    // Extend object
    for (key in attr)
      res[key] = attr[key];

    child.prototype             = res;
    child.prototype.constructor = child;
    child.__parent__            = parent;

    return child;
  }

  //
  // Register view module
  _('view', view);
})(_);
