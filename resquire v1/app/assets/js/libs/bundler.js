/*
 *
 * bundler.js
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * Date: 01-01-2015
 *
 * (c) 2015-2017 Laviréo
 */


(function(blob)
{
  //
  // Core functionality
  var svgs = ['svg', 'g', 'path', 'circle'];
  var Core = function(key, val)
  {
    if (!Core.modules[key] && val)
      Core.modules[key] = val;
    return Core.modules[key] || false;
  };

  Core.uuid    = 0;
  Core.modules = {};


  /* -------------------------------------------------------------------------- */
  /* - Bundler -> Events ------------------------------------------------------ */
  /* -------------------------------------------------------------------------- */
  function returnTrue() { return true; }
  function returnFalse() { return false; }
  Core.event = function(src, data)
  {
    //
    // Save original event
    if (src && src.type)
    {
      this.type          = src.type;
      this.originalEvent = src;
      this.isDefaultPrevented = src.defaultPrevented || (src.defaultPrevented === undefined && src.returnValue === false) ? returnTrue : returnFalse;
    }
    else
    {
      this.type = src;
    }

    //
    // Apply data
    this.data      = data;
    this.timeStamp = src && src.timeStamp || Date.now();
  };
  Core.event.prototype = {
    constructor: Core.event,
    isDefaultPrevented: returnFalse,
    isPropagationStopped: returnFalse,
    isImmediatePropagationStopped: returnFalse,

    preventDefault: function()
    {
      var ev = this.originalEvent;
      this.isDefaultPrevented = returnTrue;

      if (ev)
      {
        ev.preventDefault();
      }
    },

    stopPropagation: function()
    {
      var ev = this.originalEvent;
      this.isPropagationStopped = returnTrue;

      if (ev)
      {
        ev.stopPropagation();
      }
    },

    stopImmediatePropagation: function()
    {
      var ev = this.originalEvent;
      this.isImmediatePropagationStopped = returnTrue;

      if (ev)
      {
        ev.stopImmediatePropagation();
      }

      this.stopPropagation();
    }
  };

  Core('events', {
    __evData: {},
    __evHooks: {
      key: [
        'char',
        'charCode',
        'key',
        'keyCode'
      ],

      mouse: [
        'button',
        'buttons',
        'clientX',
        'clientY',
        'offsetX',
        'offsetY',
        'pageX',
        'pageY',
        'screenX',
        'screenY',
        'toElement'
      ],

      default: [
        'state',
        'altKey',
        'bubbles',
        'cancelable',
        'ctrlKey',
        'currentTarget',
        'detail',
        'eventPhase',
        'metaKey',
        'relatedTarget',
        'shiftKey',
        'target',
        'timeStamp',
        'view',
        'which'
      ]
    },


    on: function(ctx, type, fn, sel)
    {
      var uuid;
      var events;
      var handler;
      var handlers;

      //
      // Receive data
      if (!ctx.__uuid)
      {
        ctx.__uuid = Core.uuid++;
        this.__evData[ctx.__uuid] = {};
      }

      //
      // Reference uuid
      uuid = ctx.__uuid;


      //
      // Events
      if (!(events = this.__evData[uuid].events))
      {
        events = this.__evData[uuid].events = {};
      }


      //
      // Handler
      var self = this;
      if (!(handler = this.__evData[uuid].handler))
      {
        handler = this.__evData[uuid].handler = function(ev)
        {
          return self.dispatch.apply(ctx, arguments);
        };
      }


      //
      // Object
      //
      var handleObj = {
        type: type,
        handler: fn,
        selector: sel
      };


      //
      // Queue
      if (!(handlers = this.__evData[uuid].events[type]))
      {
        handlers = this.__evData[uuid].events[type] = [];
        handlers.selectorCount = 0;
        if (ctx.addEventListener)
        {
          ctx.addEventListener(type, handler);
        }
      }

      //
      // Enqueue
      if (sel)
      {
        this.__evData[uuid].events[type].splice(handlers.selectorCount++, 0, handleObj);
      }
      else
      {
        this.__evData[uuid].events[type].push(handleObj);
      }

      return this;
    },

    off: function(ctx, type, sel)
    {
      var evs;
      var events   = Core('events');
      var selector = Core('selector');
      var handlers = events.__evData[ctx.__uuid];

      if (!handlers || !(evs = handlers.events))
        return; 

      if (evs[type])
      {
        if (sel && evs[type].selector && evs[type].selector !== sel)
          return

        delete evs[type];
      }
    },

    build: function(ev)
    {
      var attr;
      var type  = ev.type;
      var event = new Core.event(ev);
      var hook  = /^key/.test(type) ? this.__evHooks.key
        : /^(?:mouse|pointer|contextmenu|drag|drop)|click/.test(type) ? this.__evHooks.mouse : [];


      hook = hook.concat(this.__evHooks.default);
      var i = hook.length;
      while (i--)
      {
        attr        = hook[i];
        event[attr] = ev[attr];
      }

      return event;
    },


    dispatch: function(ev)
    {
      var events   = Core('events');
      var selector = Core('selector');

      ev = events.build(ev);
      var i, x, y;
      var matches;
      var matched;
      var response;
      var args     = [].slice.call(arguments);
      var queue    = [];
      var target   = ev.target;
      var handlers = events.__evData[this.__uuid].events[ev.type] || [];

      //
      // Pass custom event
      args[0] = ev;

      //
      // Find selector
      if (handlers.selectorCount && target.nodeType)
      {
        for (; target !== this; target = target.parentNode || this)
        {
          if (target.nodeType === 1 && (target.disabled !== true || ev.type !== "click" ))
          {
            matches = [];
            for (i = 0; i < handlers.selectorCount; i++)
            {
              handleObj = handlers[i];
              sel = handleObj.selector + " ";
              if (matches[sel] === undefined)
              {
                matches[sel] = selector(this).find(sel, target).length;
              }

              if (matches[sel])
              {
                matches.push(handleObj);
              }
            }

            //
            // Insert into queue
            if (matches.length)
            {
              queue.push({
                elem: target,
                handlers: matches
              });
            }
          }
        }
      }

      //
      // Insert into queue (directly)
      if (handlers.selectorCount < handlers.length)
      {
        queue.push({
          elem: this,
          handlers: handlers.slice(handlers.selectorCount)
        });
      }

      x = 0;
      while ((matched = queue[x++]) && !ev.isPropagationStopped())
      {
        ev.currentTarget = matched.elem;

        y = 0;
        while ((handleObj = matched.handlers[y++]) && !ev.isImmediatePropagationStopped())
        {
          ev.handleObj = handleObj;
          response = handleObj.handler.apply(matched.elem, args);
          if (response !== undefined)
          {
            if ((ev.result = response) === false)
            {
              ev.preventDefault();
              ev.stopPropagation();
            }
          }
        }
      }

      return ev.result;
    }
  });

  Core('event', {
    on: function(event, call)
    {
      var sel  = Core('selector');
      var temp = sel(document);
      temp.on.call(temp, event, call);

      return this;
    },

    trigger: function(event)
    {
      var sel  = Core('selector');
      var args = arguments;
      var temp = sel(document);
      temp.trigger.apply(temp, args);

      return this;
    }
  });


  /* -------------------------------------------------------------------------- */
  /* - Bundler -> Utils ------------------------------------------------------- */
  /* -------------------------------------------------------------------------- */
  var eval = /\{\{([\s\S]+?)\}\}/g;
  Core('utils', {
    tip: function(obj, str)
    {
      var match = null;
      while (match = eval.exec(str))
      {
        var key   = match[1];
        var has   = key.indexOf('|') > -1;
        key       = has ? key.split('|') : [key];
        var def   = key[1];
        var path  = key[0];
        var parts = path.split('.');

        var i    = 0;
        var len  = parts.length;
        while (obj !== undefined && i < len)
          obj = obj[parts[i++]];

        if (!(i && i == len && obj !== undefined))
        {
          if (has) str = str.replace(match[0], def);
          continue;
        }

        str = str.replace(match[0], obj);
      }

      return str;
    },

    map: function(array, call)
    {
      var out = [];
      for (var i = 0; i < array.length; i++) out[i] = call(array[i], i);
      return out;
    },

    each: function(arr, call, ctx)
    {
      var utils = Core('utils');

      //
      // Array
      if (utils.isArray(arr))
      {
        var i = 0;
        var l = arr.length;
        for (; i < l; i++)
          call(arr[i], i, arr);
      }
      //
      // Object
      else
      {
        var key;
        for (key in arr)
          call(arr[key], key, arr);
      }

      return arr;
    },

    copy: function(arr)
    {
      var utils  = Core('utils');
      var result = utils.isArray(arr) ? [] : {};
      utils.each(arr, function(obj, it)
        {
        result[it] = obj;
        });

      return result;
    },

    bind: function(func, ctx)
    {
      return function() {
        return func.apply(ctx, arguments);
      }
    },

    trim: function(text)
    {
      return (text + "").replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, "");
    },

    merge: function(lhs, rhs)
    {
      var utils = Core('utils');
      utils.each(rhs, function(obj, it)
        {
        lhs[it] = obj;
        });

      return lhs;
    },

    filter: function (arr, fn)
    {
      var i   = -1;
      var len = arr.length;

      while (++i < len)
        if (fn(arr[i], i, arr)) 
          return true;

      return false;
    },

    throttle: function(func, delay)
    {
      var now;
      var ctx;
      var args;
      var result;
      var timeout;
      var previous = 0;

      return function()
      {
        now  = Date.now();
        ctx  = this;
        args = arguments;

        //
        // Fire function
        if ((delay - (now - previous)) <= 0)
        {
          if (timeout)
          {
            clearTimeout(timeout);
            timeout = null;
          }

          result   = func.apply(ctx, args);
        }
        //
        // Setup throttle
        else if (!timeout)
        {
          timeout = setTimeout(function()
          {
            timeout = null;
            result  = func.apply(ctx, args);
          }, (delay - (now - previous)));
        }

        //
        // Set new time
        previous = now;

        return result;
      };
    },

    value: function(data, ctx)
    {
      ctx = ctx || window;
      var utils = Core('utils');
      return (typeof data === 'function') ? utils.bind(data, ctx)() : data;
    },

    parse: JSON.parse,
    serialize: function(obj)
    {
      var data  = [];
      var add   = function(key, value)
      {
        data[data.length] = encodeURIComponent(key) + "=" + encodeURIComponent(value);
      };

      var build = function(prefix, obj)
      {
        var array = (typeof obj === 'object')
        if (array)
        {
          for (name in obj)
            build(prefix + "[" + name + "]", obj[name]);
        }
        //
        // Add
        else
        {
          add(prefix, obj);
        }
      };

      for (name in obj)
      {
        build(name, obj[name]);
      }

      //
      // Return
      return data.join("&");
    },


    isDom: function(obj)
    {
      return (obj != null) && (obj.nodeType != null);
    },

    isArray: function(obj)
    {
      return (obj != null) && (typeof obj === "array");
    },

    isString: function(obj)
    {
      return (obj != null) && (typeof obj === "string");
    },

    isWindow: function(obj)
    {
      return (obj != null) && (obj == obj.window);
    },

    isObject: function(obj)
    {
      return (obj != null) && (typeof obj === "object");
    },

    isFunction: function(obj)
    {
      return (obj != null) && (typeof obj === "function");
    },


    isEmptyObject: function(obj)
    {
      var name;
      for (name in obj)
        return false;

      return true;
    }
  });


  /* -------------------------------------------------------------------------- */
  /* - Bundler -> Selector ---------------------------------------------------- */
  /* -------------------------------------------------------------------------- */
  Core('selector', function(sel)
    {
    var instance = function(sel)
    {
      if(!sel)
      {
        return this;
      }

      this.__setEl(sel);
    }

    var opts = {
      on: function(type, sel, fn)
      {
        if (fn == null)
        {
          fn  = sel;
          sel = undefined;
        }

        //
        // Callback is required
        if (!fn)
          return this;

        //
        // Enqueue event.
        var events = Core('events');
        return this.each(function()
          {
          events.on(
            this,
            type,
            fn,
            sel
          );
          });
      },

      off: function(type, sel, fn)
      {
        var events = Core('events');
        if (typeof sel === 'function')
        {
          fn  = sel;
          sel = undefined;
        }

        //
        // Callback is required
        if (fn === null)
          fn = returnFalse;

        //
        // Remove event
        return this.each(function()
        {
          events.off(this, type, fn, sel);
        });
      },

      trigger: function(type)
      {
        var args   = arguments;
        var events = Core('events');
        return this.each(function()
          {
          var i;
          var data;
          var calls;
          if ((data  = events.__evData[this.__uuid])
            && (calls = data.events[type]))
          {
            i = calls.length;
            while (i--)
            {
              var ev = new Core.event(type);

              args[0] = ev;
              calls[i].handler.apply(this, args);
            }
          }
          });
      },


      get: function(num)
      {
        var utils = Core('utils');
        var data  = utils.isDom(this.element) ? [this.element] : this.element;
        return num != null ? data[num] : data;
      },


      css: function()
      {
        if(!arguments)
          return;

        var utils   = Core('utils');
        var result  = this;
        var args    = arguments;
        var alength = arguments.length;

        this.each(function()
          {
          if(utils.isDom(this))
          {
            if(alength == 1 && utils.isString(args[0]))
            {
              if(window.getComputedStyle)
                result = window.getComputedStyle(this)[args[0]];
              else if(document.documentElement.currentStyle)
                result = this.currentStyle[args[0]];
            }
            else
            {
              if(alength == 2 && !utils.isObject(args[0]))
              {
                this.style[args[0]] = args[1];
              }
              else
              {
                for(var i in args[0])
                  this.style[i] = args[0][i];
              }
            }
          }
          });

        return result;
      },


      hide: function()
      {
        return this.css({ display: 'none' });
      },

      show: function()
      {
        return this.css({ display: 'block' });
      },


      each: function(call)
      {
        var i      = 0;
        var value  = false;

        //
        // Loop through data
        for (; i < this.length; i++)
        {
          value = call.apply(this.element[i]);
          if(value === false)
          {
            break;
          }
        }

        return this;
      },

      find: function(sel, seed)
      {
        var x        = 0;
        var self     = this;
        var stack    = [];
        var length   = this.length;
        var selector = Core('selector');

        for (; x < length; x++)
        {
          var y  = 0;
          var el = this.__searchEl(sel, this.element[x], seed);
          for (; y < el.length; y++)
            stack.push(el[y]);
        }

        var temp = selector(stack);
        return temp;
      },


      attr: function(name, value)
      {
        var safe;
        var utils  = Core('utils');
        var sname  = name;
        var svalue = value;
        this.each(function()
          {
          var i     = 0;
          var attr  = {};
          var nType = this.nodeType;
          if (nType === 3 || nType === 8 || nType === 2)
          {
            return;
          }

          if (utils.isObject(sname))
            attr = sname;
          else
            attr[sname] = svalue;

          for(i in attr)
          {
            var name  = i;
            var value = attr[i];
            if (value === undefined)
            {
              safe = this.getAttribute(name);
              return;
            }

            if (value === null)
            {
              continue;
            }

            this.setAttribute(name, value + "");
          }
          });

        return safe ? safe : false;
      },

      removeAttr: function(sel)
      {
        this.each(function() {
          var nType = this.nodeType;
          if (nType === 3 || nType === 8 || nType === 2)
            return;
          this.removeAttribute(sel);
        });

        return this;
      },


      text: function(value)
      {
        if (value == null)
          return this.element[0].textContent;

        this.each(function()
        {
          this.textContent = value;
        });

        return this;
      },

      html: function(value)
      {
        if (value == null)
          return this.element[0].innerHTML;

        this.each(function()
        {
          this.innerHTML = value;
        });

        return this;
      },

      value: function(value)
      {
        if (value == null)
          return this.element[0].value;

        return this.each(function()
        {
          this.value = value;
        });
      },

      scrollX: function(val)
      {
        var i = 0;
        for (; i < this.length; i++)
        {
          if (val === undefined)
          {
            return this.element[i].pageXOffset;
          }

          this.element[i].scrollLeft = val;
        }
      },

      scrollY: function(val)
      {
        var i = 0;
        for (; i < this.length; i++)
        {
          if (val === undefined)
          {
            return this.element[i].pageYOffset;
          }

          this.element[i].scrollTop = val;
        }
      },

      remove: function()
      {
        this.each(function()
        {
          this.parentNode.removeChild(this);
        });
      },

      insert: function (index, el)
      {
        var self  = this;
        var utils = Core('utils');
        this.each(function()
        {
          var value;
          if (utils.isDom(el))
          {
            value = [el];
          }
          else if (typeof el === 'string')
          {
            value = [self.__buildEl(el)];
          }
          else
          {
            value = el.element;
          }

          for (var i = 0; i < value.length; i++)
          {
            this.insertBefore(value[i], this.childNodes[index + i]);
          }
        });

        return this;
      },

      append: function(el)
      {
        var self   = this;
        var utils  = Core('utils');
        var target = this.element[0];

        var value;
        if (utils.isDom(el))
        {
          value = [el];
        }
        else if (typeof el === 'string')
        {
          value = [self.__buildEl(el)];
        }
        else
        {
          value = el.element;
        }

        for (var i = 0; i < value.length; i++)
          target.appendChild(value[i]);

        return this;
      },

      prepend: function(el)
      {
        var self  = this;
        var utils = Core('utils')
        this.each(function()
          {
          var value;
          if (utils.isDom(el))
            value = [el];
          else if (typeof el === 'string')
            value = [self.__buildEl(el)];
          else
            value = el.element;

          for (var i = 0; i < value.length; i++)
            this.insertBefore(value[i], this.firstChild);
          });

        return this;
      },


      closest: function (sel, ctx)
      {
        var i   = 0;
        var pos = Core('selector')(sel); 
        for (; i < this.length; i++)
        {
          var cur = this.element[i];
          while (cur && cur.ownerDocument && cur !== ctx && cur.nodeType !== 11)
          {
            var index = [].indexOf.call(pos.element, cur);
            if (index > -1)
            {
              return Core('selector')(pos.element[index]);
              break;
            }

            cur = cur.parentNode;
          }
        }

        return null;
      },


      hasClass: function(value)
      {
        var i = 0;
        for (; i < this.length; i++)
        {
          const el = this.element[i];
          if (el.classList.contains(value))
          {
            return true;
          }
        }

        return false;
      },

      addClass: function(value)
      {
        var i = 0;
        var classes;

        if (typeof value === "string")
        {
          classes = (value || "").match(/\S+/g) || [];
          for (; i < this.length; i++)
          {
            const el = this.element[i];
            el.classList.add.apply(el.classList, classes);
          }
        }

        return this;
      },

      removeClass: function(value)
      {
        var i = 0;
        var classes;

        if (typeof value === "string")
        {
          classes = (value || "").match(/\S+/g) || [];
          for (; i < this.length; i++)
          {
            const el = this.element[i];
            el.classList.remove.apply(el.classList, classes);
          }
        }

        return this;
      },

      toggleClass: function(opts, flag)
      {
        if (typeof flag === 'boolean')
          return this[flag ? 'addClass' : 'removeClass'](opts);

        return this[(this.hasClass(opts))
          ? 'removeClass'
          : 'addClass'](opts);
      },

      filter: function(fn)
      {
        var els = [];
        this.each(function()
          {
          var ret = fn.apply(this);
          if (ret)
            els.push(ret);
          });

        return Core('selector')(
          [].concat.apply([], els)
        );
      },


      data: function()
      {
        var data  = new FormData();

        //
        // Filter elements
        this
          .filter(function()
            {
            var ret      = [];
            var elements = this.elements;

            if (elements)
            {
              var i   = 0;
              var len = elements.length;
              for (; i < len; i++)
              {
                ret.push(elements[i]);
              }
            }

            return ret;
            })
        //
        // Map values
          .each(function()
            {
            var name     = this.name;
            var value    = this.value;
            var response = name && /^(?:input|select|textarea)/i.test(this.nodeName);
            if (response && value !== undefined && value !== '')
            {
              if (this.type === 'file')
              {
                value = this.files[0];
                data.append(name, value);
              }
              else
                data.append(name, value);
            }
            });

        return data;
      },

      serialize: function()
      {
        var data  = {};

        //
        // Filter elements
        this
          .filter(function()
            {
            var ret      = [];
            var elements = this.elements;

            if (elements)
            {
              var i   = 0;
              var len = elements.length;
              for (; i < len; i++)
              {
                ret.push(elements[i]);
              }
            }

            return ret;
            })
        //
        // Map values
          .each(function()
            {
            var type     = this.type;
            var name     = this.name;
            var value    = this.value;
            var response = name
              && /^(?:input|select|textarea)/i.test(this.nodeName)
              && (this.checked || !/^(?:checkbox|radio)$/i.test(type));

            if (response && value !== '')
            {
              data[name] = value;
            }
            else if (/^(?:checkbox)$/i.test(type))
            {
              data[name] = this.checked; 
            }
            });

        return data;
      },


      __setEl: function(sel)
      {
        //
        // String
        if(typeof sel === "string")
        {
          var el;
          if (sel[0] === "<"
            && sel[sel.length - 1] === ">"
            && sel.length >= 3)
          {
            el = [this.__createEl(sel)];
          }
          else
          {
            el = this.__searchEl(sel);
          }

          this.length  = el.length;
          this.element = el;
        }
        //
        // html
        else if(sel.nodeType)
        {
          this.length  = 1;
          this.element = [sel];
        }
        //
        // el
        else if(sel.element !== undefined)
        {
          this.length  = sel.length;
          this.element = sel.element;
        }
        //
        // Array
        else if ((sel.length !== undefined) && !sel.element && sel !== window)
        {
          this.length  = sel.length;
          this.element = sel;
        }
        else
        {
          this.length  = 1;
          this.element = [sel];
        }
      },

      __buildEl: function(sel)
      {
        var utils = Core('utils');
        if (!sel || !utils.isString(sel))
          return;

        //
        // Convert to p
        if (!/<|&#?\w+;/.test(sel))
          return document.createTextNode(sel);
        //
        // HTML -> DOM
        else
        {
          var frag = document.createDocumentFragment();
          var tmp  = frag.appendChild(this.__createEl('<div>'));
          tmp.innerHTML = sel.replace(
            /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([a-z][^\/\0>\x20\t\r\n\f]*)[^>]*)\/>/gi,
            "<$1><$2>"
          );

          return tmp.firstChild;
        }
      },

      __createEl: function(sel)
      {
        var parsed = /^<(\w+)\s*\/?>(?:<\/\1>|)$/.exec(sel);
        return parsed ? svgs.indexOf(parsed[1]) === -1
                      ? document.createElement(parsed[1])
                      : document.createElementNS("http://www.w3.org/2000/svg", parsed[1])
                      : null;
      },

      __searchEl: function(sel, context, seed)
      {
        var utils = Core('utils');

        //
        // Fixes it!!!
        sel      = utils.trim(sel);

        var el;
        var ctx   = (context && context != window) ? context : document;
        var first = sel[0];
        switch(first)
        {
            //
            // ID
          case '#':
            el = document.getElementById(sel.substr(1));
            break;

            //
            // Class
          case '.':
            el = ctx.getElementsByClassName(sel.substr(1));
            break;

            //
            // Tag
          default:
            el = ctx.getElementsByTagName(sel);
        }

        if (seed)
        {
          //
          // Just a dom
          if (utils.isDom(el))
            return el === seed ? [seed] : [];

          //
          // Check list of doms
          var i = el.length;
          while (i--)
          {
            if (el[i] === seed)
            {
              return [seed];
            }
          }

          return [];
        }

        return el !== null ? utils.isDom(el) ? [el] : el : [];
      }
    };

    instance.prototype = opts;
    return (new instance(sel));
    });


  /* ------------------------------------------------------------------------ */
  /* - Bundler -> Global ---------------------------------------------------- */
  /* ------------------------------------------------------------------------ */
  blob._ = blob.bundle = Core;
  })(window);
