(function (_)
{
  var view  = _('view');
  var utils = _('utils');
	var event = _('event');

  _('modals/change_password', view.extend({
    class: 'modal modal--xs',
    views: {
      form: view.extend({
        tag: 'form',
        views: {
          head: view.extend({
            class: 'margin--bottom--m textAlign--m',
            views: {
              main: '<h1 class="font_size--xxl">Change password</h1>'
            }            
          }),

          old_password: view.extend({
            class: 'input--icon input--icon--xl margin--bottom--s',
            views: {
              icon: view.extend({
                class: 'icon lock'
              }),

              input: view.extend({
                tag: 'input',
                class: 'input input--xl',
                args: {
                  name:        'password',
                  type:        'password',
                  placeholder: 'Old Password'
                }
              })
            }
          }),

          new_password: view.extend({
            class: 'input--icon input--icon--xl margin--bottom--m',
            views: {
              icon: view.extend({
                class: 'icon lock'
              }),

              input: view.extend({
                tag: 'input',
                class: 'input input--xl',
                args: {
                  name:        'password_new',
                  type:        'password',
                  placeholder: 'New Password'
                }
              })
            }
          }),

          foot: view.extend({
            views: {
              submit: view.extend({
                tag:   'button',
                class: 'btn primary btn--l width--100',
                args:  {
                  type: 'submit',
                  text: 'Change password'
                }
              })
            }
          })
        },

        events: [['click', '.close', function (ev)
        {
          event.trigger('modal:prev');
          return false;
        }]]
      })
    },

    events: [['submit', 'form', function (ev)
    {
      var sel  = _('selector');
      var data = this.form.$el.serialize();

      _('actions').settings_password(data, {
        progress: function (perc)
        {
          event.trigger('progress:set', perc);
        },
        done: function ()
        {
          event.trigger('progress:hide');
          event.trigger('modal:prev');
        },
        fail: function ()
        {
          event.trigger('progress:fail');
        }
      });

      return false; 
    }]]
  }));
})(_);
