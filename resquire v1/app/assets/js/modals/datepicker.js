/**
 *
 * datepicker.js
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * Date: 10-03-2017
 *
 * (c) 2017 Laviréo
 */


(function (_)
{
  var sel   = _('selector');
  var view  = _('view');
  var event = _('event');

  _('modals/datepicker', view.extend({
    name:   'datepicker',
    class:  'modal',
    views: {
      wrap: view.extend({
        views: {
          date: view.extend({
            closeText:   "Done",
            prevText:    "Prev",
            nextText:    "Next",
            year:        null,
            month:       null,
            endDate:     null,
            startDate:   null,
            monthNames: [
              "January", 
              "February", 
              "March", 
              "April", 
              "May", 
              "June", 
              "July", 
              "August", 
              "September", 
              "October", 
              "November", 
              "December"
            ],
            monthNamesShort: [
              "Jan", 
              "Feb", 
              "Mar", 
              "Apr", 
              "May", 
              "Jun", 
              "Jul", 
              "Aug", 
              "Sep", 
              "Oct", 
              "Nov", 
              "Dec"
            ],
            dayNames: [
              "Sunday", 
              "Monday", 
              "Tuesday", 
              "Wednesday", 
              "Thursday", 
              "Friday", 
              "Saturday"
            ],
            dayNamesShort: [
              "Sun", 
              "Mon", 
              "Tue", 
              "Wed", 
              "Thu", 
              "Fri", 
              "Sat"
            ],
            views: {
              nav: view.extend({
                views: {
                  prev: '<a href="#" class="arrowPrev"><i class="icon prev"></i></a>',
                  next: '<a href="#" class="arrowNext"><i class="icon next"></i></a>'
                }
              }),

              header: view.extend({
                views: {
                  wrap: view.extend({
                    tag: 'ul',
                    temps: {
                      day: view.extend({
                        tag: 'li',
                        args: {
                          text: function ()
                          {
                            return this.get('day');
                          }
                        }
                      })
                    },

                    defaults: {
                      days: [
                        { day: 'Mon' },
                        { day: 'Tue' },
                        { day: 'Wen' },
                        { day: 'Thu' },
                        { day: 'Fri' },
                        { day: 'Sat' },
                        { day: 'Sun' }
                      ]
                    }
                  })
                } 
              }),

              container: view.extend({
                views: {
                  wrap: view.extend({
                    temps: {
                      month: view.extend({
                        name: 'datepicker--wrap--date--month',
                        views: {
                          table: view.extend({
                            tag: 'table',
                            views: {
                              caption: view.extend({
                                tag: 'caption',
                                args: {
                                  text: function ()
                                  {
                                    return this.get('name') + ' ' + this.get('year');
                                  }
                                }
                              }),

                              body: view.extend({
                                tag: 'tbody',
                                temps: {
                                  row: view.extend({
                                    name: 'datepicker--wrap--date--row',
                                    tag: 'tr',
                                    temps: {
                                      day: view.extend({
                                        name: 'datepicker--wrap--date--day',
                                        tag: 'td', 
                                        args: {
                                          html: function ()
                                          {
                                            return this.get('day')
                                                 ? '<span>'+ this.get('day') +'</span>'
                                                 : '';
                                          }
                                        }
                                      })
                                    }
                                  })
                                }
                              })
                            }
                          })
                        },

                        init: function ()
                        {
                          var x           = 0;
                          var cnt         = 1;
                          var year        = this.get('year');
                          var month       = this.get('month');
                          var first       = this.get('first');
                          var length      = this.get('days');
                          var daysInMonth = this.days(first, length);
                          var isBlocked   = this.get('isBlocked');

                          for (; x < daysInMonth.length; x++)
                          {
                            ///
                            // Add new row.
                            this.table.body.appendRow();

                            var y    = 0;
                            var row  = this.table.body.rows[x];
                            var days = daysInMonth[x];

                            for (; y < days.length; y++)
                            {
                              var date  = new Date(year, month, cnt);
                              var shown = days[y] === null ? null : cnt;
                              row.appendDay({ day: shown });
                              if (days[y] === null)
                              {
                                row.days[y].$el.addClass('outside');
                                continue;
                              }
                              
                              var valid = isBlocked       === false
                                       || isBlocked(date) === false;

                              row.days[y].set('date', date);
                              row.days[y].$el.attr('id', month + '_' + cnt + '_' + year);
                              row.days[y].$el.addClass(valid ? 'valid' : 'blocked');
                              cnt++;
                            }
                          }
                        },

                        days: function (first, length)
                        {
                          var currDay   = first;
                          var currWeek  = [];
                          var currMonth = [];

                          ///
                          // Days of previous months
                          var i = 0;
                          for (; i < currDay; i++)
                          {
                            currWeek.unshift(null);
                          }

                          ///
                          // Current months
                          var curr = first;
                          var last = curr + length;
                          while (curr < last)
                          {
                            currWeek.push(currDay);
                            currDay = currDay + 1 > 6 ? 0 : currDay + 1;
                            curr++;

                            ///
                            // Week is done... onto the next
                            if (currDay === 0)
                            {
                              currMonth.push(currWeek);
                              currWeek = [];
                            }
                          }

                          ///
                          // Days belonging to the next month
                          if (currDay != 0)
                          {
                            for (; currDay < 7; currDay++)
                              currWeek.push(null);

                            currMonth.push(currWeek);
                          }

                          return currMonth;
                        }
                      })
                    } 
                  })
                }
              })
            },

            events: [
              ['click', '.arrowPrev', function (ev)
              {
                var self      = this;
                var old_month = this.container.wrap.months[1];
                var new_month = this.container.wrap.months[0];

                ///
                // Hide old month
                old_month.$el
                  .toggleClass("next", false)
                  .removeClass("active"),
                  setTimeout(function()
                  {
                    self.container.wrap.months[2].remove();
                    self.container.wrap.months.splice(2, 1);

                    ///
                    // Prepend new month
                    var year   = new_month.get('year');
                    var month  = new_month.get('month') - 1;
                    if (month < 0)
                    {
                      month = 11;
                      --year;
                    }

                    var length = self.daysInMonth(year,  month);
                    var first  = self.firstOfMonth(year, month);

                    self.container.wrap.prependMonth({ 
                      days:      length,
                      first:     first,
                      year:      year, 
                      name:      self.monthNames[month],
                      month:     month,
                      isBlocked: self.get('isBlocked')
                    });

                    self.changeSelected();
                  }, 200);


                ///
                // Show new month
                new_month.$el
                  .show().addClass('no-anim next');
                setTimeout(function()
                {
                  new_month.$el.removeClass('no-anim').addClass("active");
                }, 0);

                ///
                // Set new height
                this.container.$el.css({
                  height: new_month.el.offsetHeight
                })

                return false; 
              }],
              ['click', '.arrowNext', function (ev)
              {
                var self      = this;
                var old_month = this.container.wrap.months[1];
                var new_month = this.container.wrap.months[2];

                ///
                // Hide old month
                old_month.$el
                  .toggleClass("next", true)
                  .removeClass("active"),
                  setTimeout(function()
                  {
                    self.container.wrap.months[0].remove();
                    self.container.wrap.months.splice(0, 1);

                    ///
                    // Append new month
                    var year   = new_month.get('year');
                    var month  = new_month.get('month') + 1;
                    if (month > 11)
                    {
                      month = 0;
                      ++year;
                    }

                    var length = self.daysInMonth(year,  month);
                    var first  = self.firstOfMonth(year, month);

                    self.container.wrap.appendMonth({ 
                      days:      length,
                      first:     first,
                      year:      year, 
                      name:      self.monthNames[month],
                      month:     month,
                      isBlocked: self.get('isBlocked')
                    });

                    self.changeSelected();
                  }, 200)


                ///
                // Show new month
                new_month.$el.show(),
                setTimeout(function()
                {
                  new_month.$el.addClass("next active");
                }, 0);

                ///
                // Set new height
                this.container.$el.css({
                  height: new_month.el.offsetHeight
                })

                return false; 
              }],

              ['click', '.datepicker--wrap--date--day', function (ev)
              {
                var sel = _('selector');
                var tar = sel(ev.currentTarget);
                if (!tar.hasClass('valid'))
                {
                  return false; 
                }

                var part = tar.attr('id').split('_');
                var date = new Date(part[2], part[0], part[1]);
                this.setDate(date);
              }],
              ['mouseover', '.datepicker--wrap--date--day', function (ev)
              {
                var sel = _('selector');
                var tar = sel(ev.currentTarget);
                if (!tar.hasClass('valid'))
                {
                  return false; 
                }

                var part = tar.attr('id').split('_');
                var date = new Date(part[2], part[0], part[1]);
              }]
            ],


            ///
            // Methods
            init: function ()
            {
              var date   = this.get('date');
              this.year  = date
                         ? date.getFullYear()
                         : (new Date).getFullYear();
              this.month = date
                         ? date.getMonth()
                         : (new Date).getMonth();

              var x = -1;
              for (; x < 2; x++)
              {
                var days   = [];
                var year   = this.year;
                var month  = this.month + x;

                if (month < 0)
                {
                  month  = 11;
                  --year;
                }

                if (month > 11)
                {
                  month = 0;
                  ++year;
                }

                var length = this.daysInMonth(year,  month);
                var first  = this.firstOfMonth(year, month);

                this.container.wrap.appendMonth({ 
                  days:      length,
                  first:     first,
                  year:      year, 
                  name:      this.monthNames[month],
                  month:     month,
                  isBlocked: this.get('isBlocked')
                });
              }

              ///
              // Activate months
              this.container.wrap.months[1].$el.addClass("next active");

              ///
              // Apply range
              if (date)
              {
                this.date   = date;
                this.changeSelected();
              }
            },

            parse: function (date)
            {
              var arr = date.split('/');
              console.log(arr);
              return new Date(arr[2], arr[0] - 1, arr[1]); 
            },

            daysInMonth: function(year, month)
            {
              return 32 - new Date(year, month, 32).getDate();
            },

            firstOfMonth: function(year, month)
            {
              var day = (new Date(year, month, 1).getDay()) - 1;
              return day < 0 ? 6 : day;
            },


            setDate: function (date)
            {
              ///
              // Apply hours/minutes
              var hours   = this.date.getHours();
              var minutes = this.date.getMinutes();
              date.setHours(hours);
              date.setMinutes(minutes);

              ///
              // Commit
              this.date   = date;
              this.changeSelected();
            },

            changeSelected: function ()
            {
              var date  = this.date;
              var day   = date.getDate();
              var year  = date.getFullYear();
              var month = date.getMonth();

              var x = 0;
              var x_len = this.container.wrap.months.length;
              for (; x < x_len; x++)
              {
                if (this.container.wrap.months[x].get('year')  == year 
                 && this.container.wrap.months[x].get('month') == month)
                {
                  var m     = this.container.wrap.months[x];
                  var y     = 0;
                  var y_len = m.table.body.rows.length;
                  for (; y < y_len; y++)
                  {
                    var z     = 0;
                    var z_len = 7;
                    for (; z < z_len; z++)
                    {
                      var d   = m.table.body.rows[y].days[z];
                      var out = d.get('day');
                      if (out == day)
                      {
                        this.find('.datepicker--wrap--date--day').removeClass('selected');
                        d.$el.addClass('selected');
                      }
                    }
                  }

                  ///
                  // Done here
                  break;
                }
              }   
            }
          }),

          time: view.extend({
            class: 'cf',
            temps: {
              item: view.extend({
                curr: null,
                name: 'item', 
                args: {
                  'data-type': function ()
                  {
                    return this.get('name');
                  }
                },
                views: {
                  up: view.extend({
                    template: '<i></i>' 
                  }),

                  val: view.extend({
                    views: {
                      list: view.extend({
                        tag: 'ul',
                        temps: {
                          item: view.extend({
                            tag:      'li',
                            name:     'item--val--item',
                            args: {
                              text: function ()
                              {
                                var val = this.attr.value;
                                return val < 10 ? '0' + val : val;
                              }
                            }
                          })
                        }
                      })
                    } 
                  }),

                  down: view.extend({
                    template: '<i></i>' 
                  })
                },

                init: function ()
                {
                  ///
                  // Append values
                  var i   = 0;
                  var val = this.get('values');
                  var len = val.length;

                  for (; i < len; i++)
                    this.val.list.appendItem({ value: val[i] });
                },

                prev: function ()
                {
                  this.curr > 0 ? --this.curr : 0;
                  this.val.list.$el.css({
                    'transform': 'translate(0, -'+ (this.curr * 24) +'px)'
                  });
                },

                next: function ()
                {
                  this.curr < this.val.list.items.length - 1 ? ++this.curr : this.curr;
                  this.val.list.$el.css({
                    'transform': 'translate(0, -'+ (this.curr * 24) +'px)'
                  });
                },

                goto: function (val)
                {
                  this.curr = val;
                  this.val.list.$el.css({
                    'transform': 'translate(0, -'+ (this.curr * 24) +'px)'
                  });
                }
              })
            },

            events: [
              ['click', '.item--up', function (ev)
              {
                var $tar  = sel(ev.currentTarget); 
                var $item = $tar.closest('.item');
                var type  = $item.attr('data-type');

                ///
                // Check on what type we are operating
                switch (type)
                {
                  case 'minutes':
                    var down_hours = this.items[1].curr == 0;
                    this.items[1].prev();
                    if (!down_hours)
                    {
                      this.changeTime();
                      break;
                    }
                    this.items[1].goto(1);

                  case 'hours':
                    var down = this.items[0].curr == 0;
                    this.items[0].prev();
                    if (!down)
                    {
                      this.changeTime();
                      break;
                    }
                    this.items[0].goto(23);
                    this.changeTime();
                }
              }],
              ['click', '.item--down', function (ev)
              {
                var $tar  = sel(ev.currentTarget); 
                var $item = $tar.closest('.item');
                var type  = $item.attr('data-type');

                ///
                // Check on what type we are operating
                switch (type)
                {
                  case 'minutes':
                    var up_hours = this.items[1].curr == 1;
                    this.items[1].next();
                    if (!up_hours)
                    {
                      this.changeTime();
                      break;
                    }
                    this.items[1].goto(0);

                  case 'hours':
                    var up = this.items[0].curr == 23;
                    this.items[0].next();
                    if (!up)
                    {
                      this.changeTime();
                      break;
                    }
                    this.items[0].goto(0);
                    this.changeTime();
                }
              }]
            ],


            init: function ()
            {
              ///
              // Items
              this.appendItem({ name: 'hours',   values: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9 ,10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24] });
              this.appendItem({ name: 'minutes', values: [0,    30] });

              ///
              // Parse
              var date  = this.get('date') ? this.get('date') : new Date();
              var hours = date.getHours();
              var minutes = date.getMinutes();

              ///
              // Set stuff
              this.items[0].goto(hours);
              this.items[1].goto(minutes == 0 ? 0 : 1);
              this.date = date;
            },

            changeTime: function()
            {
              var date    = this.get('date');
              var hours   = this.items[0].curr;
              var minutes = this.items[1].curr * 30;
              date.setHours(hours);
              date.setMinutes(minutes);

              this.date = date;
              this.set('date', date);
            }
          })
        }
      }),

      actions: view.extend({
        views: {
          submit: view.extend({
            tag:   'button',
            class: 'btn btn--m full primary',
            args: {
              text: 'Save Selected'
            }
          })
        } 
      })
    },

    events: [['click', 'button', function (ev)
    {
      var act  = this.get('act');
      var date = this.wrap.date.date;
      var time = this.wrap.time.date;
      var real = new Date(date.getTime());
      real.setHours(time.getHours());
      real.setMinutes(time.getMinutes());

      if (act) act(real);
      event.trigger('modal:prev');
      return false;
    }]]
  }));
})(_);
