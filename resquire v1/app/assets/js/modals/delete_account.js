(function (_)
{
  var view  = _('view');
  var utils = _('utils');
	var event = _('event');

  _('modals/delete_account', view.extend({
    class: 'modal modal--xs',
    views: {
      head: view.extend({
        class: 'margin--bottom--m',
        views: {
          main: '<h1 class="font_size--xxl margin--bottom--s">Delete account?</h1>',
          sub:  '<h3 class="font_size--m c--grey">This will instantly delete your Resquire account and all associated data. Please export any files you wish to keep.<br><br>'
              + 'If you have any active subscription it will be cancelled immediately and you will not be billed going forward.<br><br>'
              + '<strong>This cannot be undone.</strong></h3>'
        }
      }),

      input: view.extend({
        tag:   'input',
        class: 'input input--xl',
        args: {
          type:        'text',
          placeholder: 'Type \'delete\' to confirm'
        }
      }),

      foot: view.extend({
        class: 'margin--top--m',
        views: {
          submit: view.extend({
            tag:   'button',
            class: 'btn bg--red btn--l width--100',
            args:  {
              type:     'submit',
              text:     'Yes, delete my account',
              disabled: 'disabled'
            }
          })
        }
      })
    },

    events: [
      ['change',  'input', '__change'],
      ['keyup',   'input', '__change'],
      ['keydown', 'input', '__change'],
      ['click', '.btn', function (ev)
      {
        if (this.input.$el.value() !== 'delete')
          return false;

        _('actions').account_delete({}, {
          done: utils.bind(window.location.reload, window.location),
          fail: function ()
          {
          
          }
        });

        return false; 
      }]
    ],

    __change: function ()
    {
      (this.input.$el.value() !== 'delete')
      ? this.foot.submit.$el.attr('disabled', 'disabled')
      : this.foot.submit.$el.removeAttr('disabled');
    }
  }));
})(_);
