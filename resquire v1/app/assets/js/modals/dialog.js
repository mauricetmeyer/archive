(function (_)
{
  var view  = _('view');

  ///
  // Create purchase modal
  _('modals/dialog', view.extend({
    name: 'dialog',
    class: 'modal',
    events: [['click', '.btn', function(ev)
    {
      var event = _('event');
      var sel   = _('selector');
      var el    = sel(ev.currentTarget);
      var i     = el.attr('btn-id');

      if (!this.cur.buttons[i] || el.hasClass('disabled'))
        return false;

      //
      // Close
      event.trigger('modal:prev');
      this.cur.buttons[i].callback(this);
    }]],
    templates: {
      //
      // Dialog
      dialog: function(text)
      {
        return '<div class="dialog--text">'+ text +'</div><div class="dialog--buttons"></div>';
      },

      //
      // Button
      button: function(id, name, type)
      {
        type = type || 'primary';
        return '<button class="btn btn--l '+ type +'" btn-id="'+ id +'">'+ name +'</button>';
      },
    },

    alert: function(text, opts)
    {
      opts = opts || {};
      var def = {
        text: text,
        type: 'alert',
        buttons: [
          {
            name: this.t(opts.confirm ? opts.confirm : 'Ok'),
            callback: (typeof opts.done == 'function') ? opts.done : function() {}
          }
        ]
      };

      return this.__dialog(def);
    },

    confirm: function(text, opts)
    {
      opts = opts || {};
      var def = {
        text: text,
        type: 'confirm',
        buttons: [
          {
            name:     this.t(opts.cancel ? opts.cancel : 'Cancel'),
            type:     'clear',
            callback: (typeof opts.fail == 'function') ? opts.fail : function() {}
          },
          {
            name:     this.t(opts.confirm ? opts.confirm : 'Ok'),
            type:     'primary',
            callback: (typeof opts.done == 'function') ? opts.done : function() {}
          },
        ]
      };

      return this.__dialog(def);
    },


    __dialog: function(def)
    {
      this.cur = def;
      this.$el.html(this.templates.dialog(def.text));

      //
      // Add buttons
      for (var i = 0; i < def.buttons.length; i++)
        this.find('.dialog--buttons').append(this.__button(i, def.buttons[i]));
    },

    __button: function(id, def)
    {
      return this.templates.button(id, def.name, def.type);
    }    
  }));
})(_);
