(function (_)
{
  var MB         = 1048576;
  var sel        = _('selector');
  var view       = _('view');
  var utils      = _('utils');
	var event      = _('event');
  var config     = _('config');
  var editor     = _('editor');
  var date_utils = _('date/utils');
  var date_names = _('date/names');

  var IDEAL_RANGE_CAPTION_DEC  = 15;
  var IDEAL_RANGE_CAPTION      = [138, 150];
  var IDEAL_RANGE_HASHTAG      = [[5, 50], [6, 45], [7, 65], [8, 75], [9, 100], [10, 90]];
  var IDEAL_RANGE_HASHTAG_DEC  = 15;
  var IDEAL_RANGE_HASHTAG_LOW  = 5;
  var IDEAL_RANGE_HASHTAG_HIGH = 10;
  var IDEAL_RANGE_HASHTAG_LEN  = [0];

  var CAPTION_MAX         = 2200;
  var HASHTAGS_MAX        = 30;
  var TRUNCATION_MAX      = 125;

  var HASHTAGS_WEIGHT     = 2;
  var CAPTION_WEIGHT      = 1;

  var STEPS               = 5;
  var MULTIBLE            = 100 / STEPS;

  _('modals/edit_post', view.extend({
    name:  'post_form',
    class: 'modal modal--xs',
    views: {
      form: view.extend({
        tag: 'form',
        views: {
          drop: view.extend({
            name:  'drop',
            class: 'margin--bottom--m',
            views: {
              wrap: view.extend({
                views: {
                  post: view.extend({
                    css: { display: 'block' },
                    views: {
                      inner: view.extend({
                        tag: 'img',
                        args: {
                          alt: 'Post',
                          src: function ()
                          {
                            return this.attr.image.thumb.url;
                          }
                        }
                      })
                    }
                  })
                } 
              })
            }
          }),

          inputs: view.extend({
            class: 'pad--top--m pad--bottom--m',
            views: function () 
            {
              var t = {};
              t.caption = view.extend({
                class: 'margin--bottom--xs',
                views: {
                  input: editor.extend({
                    class: 'post_form--form--inputs--caption--input pad--right--l',
                    args: {
                      'data-name':        'caption',
                      'data-placeholder': 'Caption'
                    },

                    score: function ()
                    {
                      var caption  = this.getText();
                      var mentions = this.getMentions();
                      var hashtags = this.getHashtags();
    
                      /**
                       * Remove duplicates */
                      hashtags = hashtags.filter(function (val, idx)
                      { return hashtags.indexOf(val) === idx });
    
                      /**
                       * Calculate score */
                      var tips          = [];
                      var score         = 0;
                      var hashtag_score = 0;
                      var hashtag_count = hashtags.length;
                      if (hashtag_count > HASHTAGS_MAX)
                      {
                        /**
                         * NOTE (Maurice):
                         * Caption will be discarded when we have more tags
                         * than the allowed max of 30. */
                        return 0;
                      }
    
                      for (var i = 0, len = IDEAL_RANGE_HASHTAG.length; i < len; i++)
                      {
                        var hashtag_item = IDEAL_RANGE_HASHTAG[i];
                        if (hashtag_item[0] !== hashtag_count)
                          continue;
                        hashtag_score += hashtag_item[1];
                      }
    
                      if (hashtag_score === 0)
                      {
                        /**
                         * Hashtag score of 0 means that we are out of range
                         * of the optimal value of tags. */
                        if (hashtag_count < IDEAL_RANGE_HASHTAG_LOW)
                        {
                          if (hashtag_count > 0) hashtag_score = 30;
                          tips.push("Try using 5 or more hashtags");
                        }

                        if (hashtag_count > IDEAL_RANGE_HASHTAG_HIGH)
                        {
                          tips.push("Try using 10 or less hashtags");
                          tips.push("Using too many hashtags can get your post excluded from them");
                          var diff      = caption_len - IDEAL_RANGE_HASHTAG_HIGH;
                          var part      = IDEAL_RANGE_HASHTAG_DEC * diff;
                          part          = part > 90 ? 90 : part < 0 ? 0 : part;
                          hashtag_score = 90 - percentage;
                        }
                      }
    
                      /**
                       * NOTE (Maurice):
                       * We won't rate based on average hashtag length just yet,
                       * as I want to explore this topic a bit more in depth.
                       *
                       * var hashtag_score_len = 0;
                       * for (var i = 0; i < hashtag_count; i++)
                       * {
                       *   var hashtag_len = hashtags[i].length;
                       *   if (IDEAL_RANGE_HASHTAG_LEN[hashtag_len])
                       *     hashtag_score_len += IDEAL_RANGE_HASHTAG_LEN[hashtag_len];
                       * }
                       * hashtag_score_len /= hashtag_count;
                       * hashtag_score     += hashtag_score_len;
                       * hashtag_score     /= 2;
                       */
    
                      var caption_score = 0;
                      var caption_len   = caption.length;
                      if (caption_len > CAPTION_MAX)
                      {
                        /**
                         * NOTE (Maurice):
                         * Caption will be discarded when we have more
                         * than the allowed 2200 chars. */
                        return 0;
                      }
    
                      if (caption_len >= IDEAL_RANGE_CAPTION[0] && caption_len <= IDEAL_RANGE_CAPTION[1])
                        caption_score = 100;
                      else
                      {
                        /**
                         * Calculate caption score and add tips
                         * regarding the caption length. */
                        var diff = 0;
                        if (caption_len < IDEAL_RANGE_CAPTION[0])
                        {
                          diff = IDEAL_RANGE_CAPTION[0] - caption_len;
                          tips.push("Using less than "+ IDEAL_RANGE_CAPTION[0] +" characters can decrease your engagement");
                        }
    
                        if (caption_len > IDEAL_RANGE_CAPTION[1])
                        {
                          diff = caption_len - IDEAL_RANGE_CAPTION[1];
                          tips.push("Using more than "+ IDEAL_RANGE_CAPTION[1] +" characters can decrease your engagement");
                        }

                        var percentage = 100 / IDEAL_RANGE_CAPTION_DEC * diff;
                        percentage = percentage > 100 ? 100 : percentage < 0 ? 0 : percentage;
                        caption_score  = 100 - percentage;
                      }

                      /**
                       * Weight together */
                      score = this.__weight(hashtag_score, caption_score, HASHTAGS_WEIGHT, CAPTION_WEIGHT);
                      if (mentions.length > 0)
                      {
                        /**
                         * NOTE (Maurice):
                         * Think about also including mentions in the
                         * score calculation. Probably a one time boost or
                         * something like that would be good.
                         * score = this.__weight(score, 100, 2, 1); */
                      }

                      /**
                       * Clamp score... just to make sure */
                      score = score < 0 ? 0 : score > 100 ? 100 : score;
                      return { score: score, tips: tips };
                    },

                    __weight: function (lhs, rhs, lhs_weight, rhs_weight)
                    {
                      lhs *= lhs_weight;
                      rhs *= rhs_weight;
                      return (lhs + rhs) / (lhs_weight + rhs_weight);
                    }
                  }),

                  indicator: view.extend({
                    views: {
                      tips: view.extend({
                        class: 'popover popover--left',
                        views: {
                          inner: view.extend({
                            class: 'pad--xxs pad--left--xs pad--right--xs'
                          })
                        },


                        clear: function (tip)
                        {
                          this.inner.$el.html("");
                        },

                        addTip: function (tip)
                        {
                          var res = new (view.extend({
                            class: 'pad--xxs font_size--s c--white',
                            args: { text: tip }
                          }));

                          this.inner.$el.append(res.$el);
                        }
                      }),

                      ctx: view.extend({
                        tag: 'svg',
                        views: {
                          underlay: view.extend({ tag: 'circle', args: { r: 6, cx: "50%", cy: "50%" } }),
                          content:  view.extend({ tag: 'circle', args: { r: 6, cx: "50%", cy: "50%" } })
                        }
                      })
                    },


                    init: function ()
                    {
                      this.setValue(0);
                    },

                    setValue: function (val)
                    {
                      var circumference = 2 * Math.PI * 6;
                      var progress      = (1 - Math.ceil(val) / 100) * circumference;

                      this.ctx.content.$el.css({
                        'stroke-dasharray':  circumference,
                        'stroke-dashoffset': progress
                      });
                    }
                  })
                },

                init: function ()
                {
                  var self = this;
                  this.input.$el.on('editor:changed', function ()
                  {
                    /**
                     * Calculate opacity */
                    var score = self.input.score();
                    //var perc  = Math.ceil(score.score / MULTIBLE) * MULTIBLE;
                    self.indicator.setValue(score.score);

                    /**
                     * Set popover values */
                    self.indicator.$el.toggleClass('popover--trigger', score.tips.length > 0);
                    self.indicator.tips.clear();
                    for (var i = 0, len = score.tips.length; i < len; i++)
                      self.indicator.tips.addTip(score.tips[i]);
                  });
                }
              });

              if (config.dat.pro)
              {
                t.tags = editor.extend({
                  class: 'post_form--form--inputs--tags pad--right--l',
                  args: {
                    'data-name':        'tags',
                    'data-value':       function ()
                    {
                      return config.dat.tags;
                    },
                    'data-placeholder': 'First comment'
                  }
                })
              }

              t.date = view.extend({
                class: 'cf',
                views: {
                  input: view.extend({
                    tag: 'input',
                    class: 'input input--m',
                    args: {
                      name: 'scheduled_at',
                      type: 'hidden'
                    }
                  }),

                  wrap: view.extend({
                    views: {
                      date: view.extend({
                        class: 'col--6 font_weight--500',
                        views: { inner: view.extend({ tag: 'span' }) }
                      }),

                      time: view.extend({
                        class: 'col--6 font_weight--500',
                        views: { inner: view.extend({ tag: 'span' }) }
                      }) 
                    }
                  })
                },

                init: function ()
                {
                  var date  = new Date();
                  var mins  = date.getMinutes();
                  var hours = date.getHours();

                  if (hours == 23 && mins >= 30)
                    hours = 0;

                  if (mins >= 30)
                    date.setHours(hours + 1);

                  date.setMinutes(mins >= 30 ? 0 : 30);
                  this.setDate(date_utils.addDays(date, 1));
                },

                setDate: function (val)
                {
                  var date  = val;
                  var str   = date.toISOString();
                  var min   = date.getMinutes();
                  var hour  = date.getHours();

                  min  = min  < 10 ? '0' + min  : min;
                  hour = hour < 10 ? '0' + hour : hour;
                  
                  this.input.$el.value(str);
                  this.wrap.date.$el.find('span').text(date_utils.display(date));
                  this.wrap.time.$el.find('span').text(hour + ':' + min);
                  this.set('date', date);
                }
              });

              return t;
            }
          }),

          actions: view.extend({
            class: 'textAlign--r',
            views: {
              cancel: view.extend({
                tag:   'button',
                class: 'btn btn--m clear',
                args:  {
                  type: 'cancel',
                  text: 'Cancel'
                }
              }),
              submit: view.extend({
                tag:   'button',
                class: 'btn btn--m primary margin--left--xs',
                args:  {
                  type: 'submit',
                  text: 'Update'
                }
              })
            }
          })
        }
      })
    },

    events: [
      ['click', '.post_form--form--actions--cancel', function (ev)
      {
        event.trigger('modal:prev');
        return false;
      }],
      ['submit', 'form', function (ev)
      {
        var sel  = _('selector');
        var data = this.form.$el.serialize();

        data['caption', this.form.inputs.caption.input.getText()];
        _('actions').posts_update(data, {
          progress: function (perc)
          {
            event.trigger('progress:set', perc);
          },
          done: function (raw)
          {
            var blob = JSON.parse(raw);
            event
              .trigger('post:added', blob.data)
              .trigger('progress:hide')
              .trigger('modal:prev');
          },
          fail: function (data)
          {
            event.trigger('progress:fail');
            try
            {
              ///
              // Display real error message
              var blob = JSON.parse(data);
              event.trigger('alert', blob.data[0]);
            }
            catch (e)
            {
              event.trigger('alert', 'Woops! Something went wrong.');
            }
          }
        });

        return false; 
      }]
    ]
  }));
})(_);
