(function (_)
{
  var sel        = _('selector');
  var view       = _('view');
  var bind       = _('utils').bind;
	var event      = _('event');
  var config     = _('config');

  ///
  // Create instagram modal
  _('modals/instagram', view.extend({
    name:  'instagram',
    class: 'modal modal--xxs',
    titles: {
      insta_active:  ['Save',      'Update your Credentials'],
      insta_failed:  ['Try again', 'Check your credentials'],
      insta_pending: ['Save',      'Instagram Setup']
    },
    views: {
      form: view.extend({
        tag: 'form',
        views: {
          header: view.extend({
            tag: 'h1',
            args: {
              text: 'Instagram Setup'
            }
          }),

          inputs: view.extend({
            views: {
              login: view.extend({
                class: 'input--icon input--icon--xl margin--bottom--s',
                views: {
                  icon: view.extend({
                    class: 'icon person'
                  }),

                  input: view.extend({
                    tag: 'input',
                    class: 'input input--xl',
                    args: {
                      name:        'insta_login',
                      type:        'text',
                      placeholder: 'Instagram Username'
                    }
                  })
                }
              }),

              password: view.extend({
                class: 'input--icon input--icon--xl margin--bottom--m',
                views: {
                  icon: view.extend({
                    class: 'icon lock'
                  }),

                  input: view.extend({
                    tag: 'input',
                    class: 'input input--xl',
                    args: {
                      name:        'insta_password',
                      type:        'password',
                      placeholder: 'Instagram Password'
                    }
                  })
                }
              }),
            }
          }),

          actions: view.extend({
            class: 'textAlign--r',
            views: {
              cancel: view.extend({
                tag:   'button',
                class: 'btn btn--m clear',
                args:  {
                  type: 'cancel',
                  text: 'Cancel'
                }
              }),
              submit: view.extend({
                tag:   'button',
                class: 'btn btn--m primary margin--left--xs',
                args:  {
                  type: 'submit',
                  text: 'Save'
                }
              })
            }
          })
        }
      })
    },

    events: [
      ['click', '.instagram--form--actions--cancel', function (ev)
      {
        event.trigger('modal:prev');
        return false;
      }],
      ['submit', 'form', function (ev)
      {
        var sel  = _('selector');
        var btn  = this.form.actions.submit;
        btn.$el.attr('disabled', '');
        btn.$el.addClass('loading');

        var self = this;
        var data = this.form.$el.serialize();
        ev.preventDefault();

        /**
         * No data */
        if (!data['insta_login'] || !data['insta_password'])
        {
          event.trigger('alert', 'You have to fill this stuff out, you know?');
          btn.$el.removeAttr('disabled').removeClass('loading');
          return false;
        }

        this.form.header.$el.text("This can take a while");
        _('actions').settings_check(data, {
          progress: function (perc)
          {
            event.trigger('progress:set', perc);
          },
          done: function (raw)
          {
            var blob = JSON.parse(raw);

            switch (blob.data)
            {
            case 'insta_2fa_active':
            case 'insta_suspicious_login':
                event
                  .trigger('progress:fail')
                  .trigger('modal:open', 'instagram_error', {
                    status: blob.data
                  });
                break;
            case 'insta_failed':
            case 'insta_pending':
              event.trigger('progress:fail');
              self.display(blob.data);
              break;
            case 'insta_active':
                config.set('can_post', true);
                config.set('insta_login', data['insta_login']);
                event
                  //.trigger('modal:replace', 'instagram_success')
                  .trigger('modal:prev')
                  .trigger('progress:hide');
                break;
            }

            btn.$el.removeAttr('disabled').removeClass('loading');
          },
          fail: function (data)
          {
            btn.$el.removeAttr('disabled').removeClass('loading');
            event.trigger('progress:fail');
            try
            {
              ///
              // Display real error message
              var blob = JSON.parse(data);
              event.trigger('alert', blob.data[0]);
            }
            catch (e)
            { event.trigger('alert', 'Woops! Something went wrong.'); }
          }
        });

        return false; 
      }]
    ],


    init: function ()
    {
      this.display(config.get('insta_login') ? 'insta_active' : 'insta_pending');
    },

    display: function (type)
    {
      var text = this.titles[type];
      this.form.header.$el.text(text[1]);
      this.form.actions.submit.$el.text(text[0]);
    }
  }));
})(_);
