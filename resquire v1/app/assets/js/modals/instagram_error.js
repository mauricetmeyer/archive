(function (_)
{
  var sel        = _('selector');
  var view       = _('view');
  var bind       = _('utils').bind;
	var event      = _('event');
  var config     = _('config');

  _('modals/instagram_error', view.extend({
    name:  'instagram',
    class: 'modal modal--xs',
    delay: 2000,
    titles: {
      insta_2fa_active: ['2FA is Active', [
        { xOff: 0, yOff: 350 },
        { xOff: 220, yOff: 350 },
        { xOff: 440, yOff: 350 },
        { xOff: 660, yOff: 350 },
      ]],
      insta_suspicious_login: ['Flagged as Suspicious', [
        { xOff: 0, yOff: 0 }
      ]]
    },
    views: {
      header: view.extend({ tag: 'h1' }),
      phone: view.extend({
        class: 'margin--bottom--xl center cf',
        views: {
          speaker: view,
          screen: view.extend({
            views: {
            }
          }),

          home: view
        }
      }),

      actions: view.extend({
        class: 'textAlign--m',
        views: {
          submit: view.extend({
            tag:   'a',
            class: 'btn btn--m primary',
            args:  {
              href: '#',
              text: 'Try Again'
            }
          })
        }
      })
    },

    events: [['click', '.btn', function ()
    {
      event.trigger('modal:prev');
      return false;
    }]],

    init: function ()
    {
      this.display(this.attr.status);
    },

    loop: function ()
    {
      var now     = new Date().getTime();
      var delta   = now - this.last;
      this.timer += delta;

      ///
      // Update offset
      var xOff = this.anim[this.frame].xOff;
      var yOff = this.anim[this.frame].yOff;
      this.phone.screen.$el.css({
        'background-position': '-' + xOff + 'px -' + yOff + 'px'
      });

      ///
      // Update frame after delay
      if (this.timer >= this.delay)
      {
        this.frame = this.frame == this.anim.length - 1 ? 0 : this.frame + 1;
        this.timer -= this.delay;
      }

      this.last = now;
      if (this.rendering)
        window.requestAnimationFrame(bind(this.loop, this));
    },

    display: function (type)
    {
      var text = this.titles[type];
      this.header.$el.text(text[0]); 

      ///
      // Display phone and frames
      this.last      = new Date().getTime();
      this.anim      = text[1];
      this.frame     = 0;
      this.timer     = 0;
      this.rendering = true;
      this.loop();
    }
  }));
})(_);
