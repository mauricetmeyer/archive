(function (_)
{
  var view       = _('view');
	var event      = _('event');

  _('modals/instagram_success', view.extend({
    name:  'instagram',
    class: 'modal modal--xxs',
    views: {
      form: view.extend({
        tag: 'form',
        views: {
          header: view.extend({
            tag: 'h1',
            args: {
              text: 'Setup Complete'
            }
          }),

          actions: view.extend({
            class: 'textAlign--m',
            views: {
              submit: view.extend({
                tag:   'a',
                class: 'btn btn--m primary margin--left--xs',
                args:  {
                  href: '#',
                  text: 'Close'
                }
              })
            }
          })
        }
      })
    },

    events: [
      ['click', '.btn', function (ev)
      {
        event.trigger('modal:prev');
        return false;
      }]
    ]
  }));
})(_);
