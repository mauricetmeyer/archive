(function (_)
{
  var view   = _('view');
	var event  = _('event');
  var player = _('player');

  _('modals/intro', view.extend({
    name:  'intro',
    class: 'modal modal--xl',
    views: {
      video: player
    },

    init: function ()
    {
      this.video.setMedia('/static/promo.mp4');
    }
  }));
})(_);
