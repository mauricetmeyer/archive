(function (_) {
  var view = _('view');
  var bind = _('utils').bind;
  var event = _('event');
  var config = _('config');
  var editor = _('editor');
  var actions = _('actions');
  _('modals/login', view.extend({
    name: 'login',
    class: 'modal modal--xxs',
    views: {
      head: view.extend({
        class: 'margin--bottom--l textAlign--m',
        views: {
          main: '<h1 class="margin--bottom--xs">Welcome Back</h1>',
          sub: '<h3>Sign into your account</h3>'
        }
      }),
      form: view.extend({
        tag: 'form',
        class: 'margin--bottom--s',
        views: {
          mail: view.extend({
            class: 'margin--bottom--s',
            views: {
              wrap: view.extend({
                class: 'input--icon input--icon--xl',
                views: {
                  icon: view.extend({
                    class: 'icon mail'
                  }),

                  input: view.extend({
                    tag: 'input',
                    class: 'input input--xl',
                    args: {
                      type: 'text',
                      placeholder: 'Email',
                      name: 'email'
                    }
                  })
                }
              })
            }
          }),

          password: view.extend({
            class: 'margin--bottom--m',
            views: {
              wrap: view.extend({
                class: 'input--icon input--icon--xl',
                views: {
                  icon: view.extend({
                    class: 'icon lock'
                  }),

                  input: view.extend({
                    tag: 'input',
                    class: 'input input--xl',
                    args: {
                      type: 'password',
                      placeholder: 'Password',
                      name: 'password'
                    }
                  })
                }
              })
            }
          }),

          submit: view.extend({
            tag: 'button',
            class: 'btn btn--l primary full',
            args: {
              type: 'submit',
              text: 'Log In'
            }
          })
        }
      }),

      switch: view.extend({
        class: 'textAlign--m',
        views: {
          inner: "<a href=\"#\" class=\"switch_btn font_size--s\">Don't have an account? Register</a>"
        }
      })
    },

    events: [
      ['click', '.switch_btn', function (ev)
      {
        event.trigger('modal:replace', 'register');
        return false;
      }],

      ['submit', 'form', function (ev) {
        var data = this.form.$el.serialize();
        _('actions').login(data, {
          progress: function (perc)
          {
            event.trigger('progress:set', perc);
          },
          done: function ()
          {
            event.trigger('progress:hide');
            window.location = "/control";
          },
          fail: function (raw)
          {
            event.trigger('progress:fail');
            try {
              var blob = JSON.parse(raw);
              event.trigger('alert', 'Check your credentials, please.');
            }
            catch (e)
            {
              event.trigger('alert', 'You have to fill this stuff out, you know?');
            }
          }
        });

        return false;
      }]
    ]
  }));
})(_);