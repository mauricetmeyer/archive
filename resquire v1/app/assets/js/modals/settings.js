(function (_)
  {
    var view    = _('view');
    var bind    = _('utils').bind;
    var event   = _('event');
    var config  = _('config');
    var editor  = _('editor');
    var actions = _('actions');
    _('modals/settings', view.extend({
      name:  'settings',
      class: 'modal modal--xs',
      views: {
        head: view.extend({
          class: 'margin--bottom--l textAlign--m',
          views: {
            main: '<h1 class="font_size--xxl">Account Settings</h1>',
          }
        }),
        form: view.extend({
          tag: 'form',
          views: {
            inputs: view.extend({
              css: { position: 'relative' },
              views: {
                user: view.extend({
                  views: {
                    full_name: view.extend({
                      class: 'margin--bottom--l',
                      views: {
                        label: '<h3 class="margin--bottom--xs">Name</h3>',
                        wrap: view.extend({
                          class: 'input--icon input--icon--l',
                          views: {
                            icon: view.extend({
                              class: 'icon person'
                            }),

                            input: view.extend({
                              tag: 'input',
                              class: 'input input--l',
                              args: {
                                type:        'text',
                                placeholder: 'Your name',
                                name:        'name',
                                value:       function ()
                                {
                                  return config.dat.name;
                                },
                              }
                            })
                          }
                        })
                      }
                    }),

                    mail: view.extend({
                      class: 'margin--bottom--l',
                      views: {
                        label: '<h3 class="margin--bottom--xs">Email</h3>',
                        wrap: view.extend({
                          class: 'input--icon input--icon--l',
                          views: {
                            icon: view.extend({
                              class: 'icon mail'
                            }),

                            input: view.extend({
                              tag: 'input',
                              class: 'input input--l',
                              args: {
                                type:        'text',
                                placeholder: 'Your email',
                                name:        'email',
                                value:       function ()
                                {
                                  return config.dat.email;
                                },
                              }
                            })
                          }
                        })
                      }
                    }),

                    password: view.extend({
                      class: 'margin--bottom--l',
                      views: {
                        label: '<h3 class="margin--bottom--xs">Password</h3>',
                        inline: view.extend({
                          class: 'inline',
                          views: {
                            wrap: view.extend({
                              class: 'input--icon input--icon--l inline--flex',
                              views: {
                                icon: view.extend({
                                  class: 'icon lock'
                                }),

                                input: view.extend({
                                  tag: 'input',
                                  class: 'input input--l',
                                  args: {
                                    type:        'password',
                                    value:       'password1',
                                    disabled:    'disabled',
                                    placeholder: 'Old Password'
                                  }
                                })
                              }
                            }),

                            change: view.extend({
                              tag: 'a',
                              class: 'btn btn--l clear margin--left--xs',
                              args: {
                                text: 'Change',
                              }
                            })
                          }
                        })
                      },

                      events: [
                        ['click', '.btn', function ()
                        {
                          event.trigger('modal:open', 'change_password');
                          return false;
                        }]
                      ]
                    })
                  }
                }),

                insta: view.extend({
                  class: 'margin--bottom--l',
                  views: function ()
                  {
                    var t    = {};
                    var name = config.dat.insta_login;

                    t.label = '<h3 class="margin--bottom--xs">Instagram</h3>';

                    if (name)
                    {
                      t.inline = view.extend({
                        class: 'inline',
                        views: {
                          wrap: view.extend({
                            class: 'input--icon input--icon--l inline--flex',
                            views: {
                              icon: view.extend({
                                class: 'icon at'
                              }),

                              input: view.extend({
                                tag: 'input',
                                class: 'input input--l',
                                args: {
                                  value:    name,
                                  disabled: 'disabled'
                                }
                              })
                            }
                          }),

                          change: view.extend({
                            class: 'margin--left--xs',
                            views: {
                              btn: view.extend({
                                class: 'btn btn--l primary js-trigger',
                                args: {
                                  'text':        'Edit',
                                  'href':        '#',
                                  'data-action': 'modal:open instagram'
                                }
                              })
                            }
                          })
                        }
                      })
                    }
                    else
                    {
                      t.setup = view.extend({
                        css: { position: 'relative' },
                        views: {
                          btn: view.extend({
                            css: { position: 'relative' },
                            class: 'btn btn--m primary js-trigger',
                            args: {
                              'text':        'Setup Instagram',
                              'href':        '#',
                              'data-action': 'modal:open instagram'
                            }
                          })
                        }
                      });
                    }

                    return t;
                  }
                }),


                // tags: view.extend({
                //   class: 'margin--bottom--l',
                //   views: {
                //     label: '<h3 class="margin--bottom--xs">Tags</h3>',
                //     wrap: view.extend({
                //       class: 'input--icon input--icon--l',
                //       views: {
                //         icon: view.extend({
                //           class: 'icon hashtag'
                //         }),

                //         tags: editor.extend({
                //           class: 'textarea textarea--l',
                //           args: {
                //             'data-placeholder': 'Your tags',
                //             'data-value': function ()
                //             {
                //               return config.dat.tags;
                //             }
                //           }
                //         })
                //       }
                //     })
                //   }
                // })
              }
            }),


            plan: view.extend({
              class: 'inline margin--bottom--l',
              views: { 
                left: view.extend({
                  class: 'inline--grow',
                  views: {
                    title: view.extend({
                      class: 'font_size--l c--purple',
                      args: {
                        text: function ()
                        {
                          return config.dat.pro ? 'Pro' : 'Lite';
                        }
                      }
                    }),

                    sub: view.extend({
                      class: 'font_size--xs',
                      args: {
                        text: function ()
                        {
                          var cancelled = config.dat.pro_cancelled;
                          var ends_at   = new Date(config.dat.subscribed_ends_at);
                          var now       = new Date();

                          if (cancelled && ends_at > now)
                          {
                            var date_utils = _('date/utils');
                            var diff       = Math.abs(date_utils.diff(ends_at, now));
                            
                            return diff + ' days left';
                          }
                          else return config.dat.pro
                            ? '$' + (config.dat.plan.price / 100).toFixed() + ' per month'
                            : 'Free forever';
                        }
                      } 
                    })
                  }
                }),

                right: view.extend({
                  views: {
                    select: view.extend({
                      tag:   'a',
                      class: 'btn btn--m primary',
                      args: {
                        href:          '#',
                        text:          function ()
                        {
                          var cancelled = config.dat.pro_cancelled;
                          var ends_at   = new Date(config.dat.subscribed_ends_at);
                          var now       = new Date();
                          if (cancelled && ends_at > now) return 'Renew';
                          else return config.dat.pro ? 'Cancel' : 'Upgrade';
                        }
                      }
                    })
                  }
                })
              } 
            }),

            actions: view.extend({
              class: 'inline',
              views: {
                left: view.extend({
                  class: 'inline--grow',
                  views: {
                    logout: view.extend({
                      class: 'font_size--s iblock center--vertical',
                      tag:   'a',
                      args:  {
                        href: '#',
                        text: 'Log out'
                      }
                    })
                  }
                }),

                right: view.extend({
                  views: {
                    cancel: view.extend({
                      tag:   'button',
                      class: 'btn btn--m clear',
                      args:  {
                        type: 'cancel',
                        text: 'Cancel'
                      }
                    }),
                    submit: view.extend({
                      tag:   'button',
                      class: 'btn btn--m primary margin--left--xs',
                      args:  {
                        type: 'submit',
                        text: 'Save'
                      }
                    })
                  }
                })
              }
            })
          }
        })
      },

      events: [
        ['click', '.settings--form--plan--right--select', function (ev)
          {
            var cancelled = config.dat.pro_cancelled;
            if (config.dat.pro && !cancelled)
            {
              event.trigger('confirm', 'You are about to cancel your pro subscription!', {
                ok: 'Continue',
                done: function ()
                {
                  actions.plans_cancel(null, {
                    fail: function (raw)
                    {
                      try
                      {
                        var blob = JSON.parse(raw);
                        event.trigger('alert', blob.data[0]);
                      }
                      catch (e)
                      {
                        event.trigger('alert', 'Woops! Something went wrong.');
                      }
                    },

                    done: function (raw)
                    {
                      var blob = JSON.parse(raw);
                      event.trigger('plan:changed', blob.data);
                    }
                  }) 
                }
              });
            }
            /**
             * Upgrade or renew */
            else event.trigger('modal:open', 'upgrade');
            return false;
          }],
        ['click', '.settings--form--actions--left--delete', function (ev)
          {
            return false;
          }],
        ['click', '.settings--form--actions--right--cancel', function (ev)
          {
            event.trigger('modal:prev');
            return false;
          }],
        ['submit', 'form', function (ev)
          {
            
            var sel  = _('selector');
            var btn  = this.form.actions.right.submit;
            btn.$el.attr('disabled', '');
            btn.$el.addClass('loading');

            var data = this.form.$el.serialize();
            _('actions').settings_update(data, {
              progress: function (perc)
              {
                event.trigger('progress:set', perc);
              },
              done: function ()
              {
                ///
                // Update settings
                for (var key in data)
                  if (config.dat[key] !== undefined)
                    config.set(key, data[key]);

                ///
                // Update can_post
                if (config.dat.can_post === false
                  && data.insta_login
                  && data.insta_password)
                  config.set('can_post', true);

                event.trigger('progress:hide');
                event.trigger('modal:prev');
                btn.$el.removeAttr('disabled').removeClass('loading');
              },
              fail: function ()
              {
                event.trigger('progress:fail');
                event.trigger('alert', 'We could not save your settings!'); 
                btn.$el.removeAttr('disabled').removeClass('loading');
              }
            });

            return false; 
          }]
      ],

      init: function ()
      {
        event.on('config:changed', bind(this.update, this));
        event.on('plan:changed', bind(this.update, this));
      },

      onShow: function ()
      {
        if (!config.dat.insta_login)
        {
          _('onboard').start([
            {
              selector: this.form.inputs.insta.setup.btn.$el,
              position: 'bottom',
              title:    'First steps',
              text:     'You may start by setting up your Instagram account.'
            }
          ]);
        }
      }
    }));
  })(_);
