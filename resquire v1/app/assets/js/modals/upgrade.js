(function (_)
{
  var sel      = _('selector');
  var view     = _('view');
  var event    = _('event');
  var config   = _('config');
  var stripe   = _('stripe');
  var actions  = _('actions');
  var defaults = _('defaults');

  _('modals/upgrade', view.extend({
    name:  'upgrade',
    class: 'modal modal--s',
    views: {
      head: view.extend({
        class: 'margin--bottom--l textAlign--m',
        views: {
          main: view.extend({
            tag: 'h1',
            class: 'font_size--xxl',
            args: {
              text: function ()
              {
                return 'Upgrade for $' + (defaults.subscription.price / 100) + '/month';
              }
            }
          })
        }
      }),

      plans: view.extend({
        class: 'cf margin--bottom--l',
        temps: {
          plan: view.extend({
            name:  'plan',
            class: 'col--6',
            args: {
              'data-key': function ()
              {
                return this.get('key');
              }
            },
            views: {
              wrap: view.extend({
                views: {
                  title: view.extend({
                    tag:   'h1',
                    class: 'margin--bottom--xs',
                    args: {
                      text: function ()
                      {
                        return this.get('name')
                      }
                    }
                  }),

                  desc: view.extend({
                    tag: 'h3',
                    args: {
                      text: function ()
                      {
                        return this.get('description')
                      }
                    }
                  }),

                  meta: view.extend({
                    tag: 'h3',
                    views: {
                      list: view.extend({
                        tag: 'ul',
                        temps: {
                          item: view.extend({
                            tag:  'li',
                            name: 'plan--item',
                            class: function ()
                            {
                              if (this.attr.value === false) return 'false';
                            }, 
                            views: {
                              val: view,
                              title: view.extend({
                                tag:      'span',
                                template: '{{name}}'
                              })
                            }
                          })
                        }
                      })
                    },

                    init: function ()
                    {
                      var key;
                      var items = this.get('items');

                      for (key in items)
                        this.list.appendItem(items[key]);
                    }
                  })
                }
              })
            }
          })
        }
      }),

      form: view.extend({
        tag: 'form',
        views: {
          card: view.extend({
            css: {
              padding: '7px 16px'
            },
            class: 'input input--l wrap--xs',
          }),

          actions: view.extend({
            class: 'textAlign--m margin--top--l',
            views: {
              submit: view.extend({
                tag:   'button',
                class: 'btn primary btn--m',
                args:  {
                  type: 'submit',
                  text: 'Upgrade'
                }
              })
            }
          })
        }
      })
    },

    events: [
      ['click', '.upgrade--form--actions--cancel', function (ev)
      {
        event.trigger('modal:prev');
        return false;
      }],
      ['submit', 'form', function (ev)
      {
        var tar  = sel(ev.currentTarget);
        var btn  = this.form.actions.submit;
        btn.$el.attr('disabled', '');
        btn.$el.addClass('loading');

        var self = this;
        ev.preventDefault();

        stripe.createToken(this.card).then(function (result)
        {
          ///
          // Success!
          if (result.token)
          {
            actions.plans_upgrade({ token: result.token.id }, {
              progress: function (perc)
              {
                event.trigger('progress:set', perc);
              },
              fail: function (raw)
              {
                event.trigger('progress:fail');
                btn.$el.removeAttr('disabled').removeClass('loading');
                try
                {
                  ///
                  // Display real error message
                  var blob = JSON.parse(raw);
                  event.trigger('alert', blob.data[0]);
                }
                catch (e)
                {
                  event.trigger('alert', 'Woops! Something went wrong.');
                }
              },
              done: function (raw)
              {
                event.trigger('progress:hide');
                var blob = JSON.parse(raw);
                if (blob.data === null)
                {
                  //config.set('plan', self.plans[key]);
                  event
                    .trigger('plan:changed')
                    .trigger('modal:prev');
                  return;
                }

                btn.$el.removeAttr('disabled').removeClass('loading');
                window.location = decodeURI(blob.data);
              }
            });
          }
          ///
          // Error
          else if (result.error)
            event.trigger('alert', result.error.message); 
        });

        return false;
      }]
    ],

    init: function ()
    {
      this.elements = stripe.elements();
      this.card     = this.elements.create('card', {
        hidePostalCode: !0,
        style: {
          base: {
            color:      '#5d6977',
            height:     '30px',
            iconColor:  '#6772e5', 
            lineHeight: '30px',
            fontSize:   '16px',
            fontFamily: '-apple-system, San Francisco, Helvetica Neue, Helvetica, Arial, sans-serif',
          }
        }
      });
      this.card.mount(this.form.card.el);
      this.card.on('change', function (ev)
      {
        ///
        // Error
        if (ev.error);
        ///
        // Success
        else;
      });

      var self = this;
      actions.plans(null, {
        progress: function (perc)
        {
          event.trigger('progress:set', perc);
        },
        fail: function ()
        {
          event.trigger('progress:fail');
          event.trigger('alert', 'Woops! Something went wrong.');
        },

        done: function (raw)
        {
          event.trigger('progress:hide');

          var blob     = JSON.parse(raw);
          var data     = blob.data;
          self.options = blob.data;

          var key;
          for (key in data)
          {
            var blob = data[key];
            blob.key = key;
            self.plans.appendPlan(blob);
          }
        }
      })
    }
  }));
})(_);
