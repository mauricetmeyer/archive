(function (_)
{
  var MB         = 1048576;
  var sel        = _('selector');
  var view       = _('view');
  var utils      = _('utils');
	var event      = _('event');
  var config     = _('config');
  var editor     = _('editor');
  var date_utils = _('date/utils');
  var date_names = _('date/names');

  var IDEAL_RANGE_CAPTION_DEC  = 15;
  var IDEAL_RANGE_CAPTION      = [138, 150];
  var IDEAL_RANGE_HASHTAG      = [[5, 50], [6, 45], [7, 65], [8, 75], [9, 100], [10, 90]];
  var IDEAL_RANGE_HASHTAG_DEC  = 15;
  var IDEAL_RANGE_HASHTAG_LOW  = 5;
  var IDEAL_RANGE_HASHTAG_HIGH = 10;
  var IDEAL_RANGE_HASHTAG_LEN  = [0];

  var CAPTION_MAX         = 2200;
  var HASHTAGS_MAX        = 30;
  var TRUNCATION_MAX      = 125;

  var HASHTAGS_WEIGHT     = 2;
  var CAPTION_WEIGHT      = 1;

  var STEPS               = 5;
  var MULTIBLE            = 100 / STEPS;

  _('modals/view_post', view.extend({
    name:  'post_form',
    class: 'modal modal--xs',
    views: {
      form: view.extend({
        views: {
          drop: view.extend({
            name:  'drop',
            class: 'margin--bottom--m',
            views: {
              wrap: view.extend({
                views: {
                  post: view.extend({
                    css: { display: 'block' },
                    views: {
                      inner: view.extend({
                        tag: 'img',
                        args: {
                          alt: 'Post',
                          src: function ()
                          {
                            return this.attr.image.thumb.url;
                          }
                        }
                      })
                    }
                  })
                } 
              })
            }
          }),

          inputs: view.extend({
            class: 'pad--top--m pad--bottom--m',
            views: function () 
            {
              var t = {};
              t.caption = view.extend({
                class: 'margin--bottom--xs',
                views: {
                  input: editor.extend({
                    class: 'post_form--form--inputs--caption--input pad--right--l',
                    args: {
                      'data-readonly':    'true',
                      'data-name':        'caption',
                      'data-value':       this.attr.caption,
                      'data-placeholder': 'No Caption'
                    }
                  })
                }
              });

              if (this.attr.tags)
              {
                t.tags = editor.extend({
                  class: 'post_form--form--inputs--tags pad--right--l',
                  args: {
                    'data-name':        'tags',
                    'data-value':       this.attr.tags,
                    'data-placeholder': 'First comment'
                  }
                })
              }

              t.date = view.extend({
                class: 'cf',
                views: {
                  input: view.extend({
                    tag: 'input',
                    class: 'input input--m',
                    args: {
                      name: 'scheduled_at',
                      type: 'hidden'
                    }
                  }),

                  wrap: view.extend({
                    views: {
                      date: view.extend({
                        class: 'col--6 font_weight--500',
                        views: { inner: view.extend({ tag: 'span' }) }
                      }),

                      time: view.extend({
                        class: 'col--6 font_weight--500',
                        views: { inner: view.extend({ tag: 'span' }) }
                      }) 
                    }
                  })
                },

                init: function ()
                {
                  var at   = this.attr.scheduled_at;
                  var date = new Date(at);
                  this.setDate(date);
                },

                setDate: function (val)
                {
                  var date  = val;
                  var str   = date.toISOString();
                  var min   = date.getMinutes();
                  var hour  = date.getHours();

                  min  = min  < 10 ? '0' + min  : min;
                  hour = hour < 10 ? '0' + hour : hour;
                  
                  this.input.$el.value(str);
                  this.wrap.date.$el.find('span').text(date_utils.display(date));
                  this.wrap.time.$el.find('span').text(hour + ':' + min);
                  this.set('date', date);
                }
              });

              return t;
            }
          })
        }
      })
    }
  }));
})(_);
