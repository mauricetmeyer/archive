/**
 * router.js
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * Date: 18-03-2016
 *
 * (c) Laviréo
 */


(function(_)
{
  _('router', {
    __list:     [],
    __named:    {},
    __routes:   {},
    __fragment: null,

    params:     {},
    modules:    {},
    location:   window.location,


    url: function(key, params)
    {
      var copy;
      var utils = _('utils');

      copy   = utils.copy(this.params);
      params = params || {};
      params = utils.merge(copy, params);

      if (!this.__named[key])
      {
        throw new Error("No route named " + key + " was found.");
      }

      var temp  = this.__named[key];
      var named = temp.path;

      utils.each(params, function(obj, it)
      {
        obj = encodeURIComponent(obj);
        named = named.replace("*" + it, obj.replace(/%2F/g, "/"));
        named = named.replace(":" + it + "?", obj);
        named = named.replace(":" + it, obj)
      });

      var s = named.match(/:([^\/\?]+)\?/);
      if (!!s)
      {
        named = named.replace("/" + s[0], "")
      }

      s = named.match(/\*([a-zA-Z]*)/);
      if (!!s)
      {
        named = named.replace("/" + s[0], "")
      }

      named = named.replace(/\/\//, "/");
      named = named.replace(/\/$/, "");

      if (!!params&&!!params["?"])
      {
          var key;
          var param;
          var result = "";
          for (key in params["?"])
          {
            param = params["?"][o];
            if (!utils.isNull(a))
            {
              if (result === "")
                result += "?"
              else
                result += "&"

              result += key + "=" + encodeURIComponent(param);
            }
          }
          named += result;
      }
      return named;
    },

    add: function(routes)
    {
      var utils = _('utils');
      var route;
      for (route in routes)
        this.__add(route, routes[route]);

      /**
       * ensure wildcard routes are always at the end */
      var i = 0;
      var l = this.__list.length;
      for (; i < l; i++)
        if (this.__list[i] === '*')
        {
          this.__list.push(this.__list.splice(i, 1)[0]);
          l--;
          i--;
        }

      /**
       * Return for chaining */
      return this;
    },

    find: function (path)
    {
      var i        = 0;
      var len      = this.__list.length;
      var fragment = this.__fragment = path;
      for (; i < len; i++)
      {
        var path   = this.__list[i];
        var record = this.__routes[path];
        if (record.regex.test(fragment))
        {
          var args  = this.__toParams(record, fragment);
          args.path = this.location.pathname;
          args.view = record.name;

          return {
            data:  args,
            route: record
          };
        }
      }

      return null;
    },


    __add: function (name, route, parent)
    {
      /**
       * Load */
      //var tnames = temp.path.match(/(\(\?)?[:\*]\w+/g);
      //if (!!tnames)
      //  utils.each(tnames, function(obj, it)
      //  {
      //    tnames[it] = obj.substring(1);
      //  });
      //
      //var tpath = this.__toRegex(temp.path);
      //this.__named[route] = routes[route];
      //this.__routes.push({
      //  name:   route,
      //  path:   tpath,
      //  shell:  temp.shell,
      //  title:  temp.title,
      //  params: tnames,
      //});


      /**
       * Create record */
      var path   = route.path || '';
      path       = this.__toPath(path, parent);
      var record = {
        path:   path,
        view:   route.view,
        title:  route.title,
        regex:  this.__toRegex(path),
        parent: parent,
      };


      /**
       * Process children */
      if (route.childs !== undefined)
      {
        /**
         * Loop over childs */
        for (idx in route.childs)
        {
          var tmp = route.childs[idx].path ? name + '_' + idx : name;
          this.__add(tmp, route.childs[idx], record);
        }
      }


      /**
       * Insert */
      if(!this.__routes[record.path])
      {
        this.__list.push(record.path);
        this.__named[name]         = record;
        this.__routes[record.path] = record;
      }
    },

    __check: function(ev, path)
    {
      path = path ? path : this.__path();
      if (path !== this.__fragment) this.__route(path);
    },

    __path: function()
    {
      var path = this.location.pathname;
      var args = this.__params();
      return decodeURI(((path !== '/' && path.charAt(path.length - 1) === '/')
            ? path.slice(0, -1)
            : path) + args
      );
    },

    __params: function()
    {
      var match = this.location.href.replace(/#.*/, '').match(/\?.+/);
      return match ? match[0] : '';
    },

    __toPath: function (path, parent)
    {
      if (parent && parent.path === '/') return '/' + path;
      if (!parent || parent.path === '') return path;
      if (!path   || path        === '') return parent.path;
      return parent.path + '/' + path;
    },

    __toRegex: function(route)
    {
      route = route
        .replace(/[\-{}\[\]+?.,\\\^$|#\s]/g, '\\$&')
        .replace(/\((.*?)\)/g, '(?:$1)?')
        .replace(/(\(\?)?:\w+/g, function(match, optional)
      {
        return optional ? match : '([^/?]+)';
      }).replace(/\*\w+/g, '([^?]*?)');

      return new RegExp('^' + route + '(?:\\?([\\s\\S]*))?$');
    },

    __toParams: function(route, fragment)
    {
      var i      = 0;
      var raw    = route.regex.exec(fragment).slice(1);
      var params = {};
      for (; i < raw.length; i++)
      {
        var param = raw[i] ? decodeURIComponent(raw[i]) : null;
        if (param)
        {
          if (param.indexOf('=') > -1)
          {
            var values = param.split('=');
            params[values[0]] = values[1];
          }
          else
          {
            if (!!route.params)
            {
              var name  = route.params[i];
              var value = param;
              params[name] = value;
            }
          }
        }
      }

      return params;
    }
  });
})(_);
