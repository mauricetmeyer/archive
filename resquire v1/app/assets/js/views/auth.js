(function (_)
{
  var view    = _('view');
  var bind    = _('utils').bind;
  var actions = _('actions');

  _('auth', view.extend({
    name: 'auth',
    class: 'height--100',
    views: {
      wrap: view.extend({
        class: 'cf height--100',
        views: {
          container: view.extend({
            class: 'col--6 height--100',
            views: {
              wrap: view.extend({
                class: 'center center--vertical wrap--xxs shell--view'
              })
            }
          }),

          promo: view.extend({
            class: 'col--6 bg--purple height--100',
            views: {

            }
          })
        }
      })
    }
  }));
})(_);
