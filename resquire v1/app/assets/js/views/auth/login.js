(function (_)
{
  var view    = _('view');
  var bind    = _('utils').bind;
  var actions = _('actions');

  _('auth/login', view.extend({
    views: {
      form: view.extend({
        tag:   'form',
        class: 'margin--bottom--s',
        views: {
          head: view.extend({
            class: 'textAlign--m',
            views: {
              main: "<h1 class=\"margin--bottom--xs\">Welcome Back!</h1>",
              sub:  "<h3 class=\"margin--bottom--l\">Sign into your account</h3>"
            }
          }),

          inputs: view.extend({
            views: {
              mail: view.extend({
                tag:   'input',
                class: 'input input--l width--100 margin--bottom--s',
                args: {
                  type:        'text',
                  name:        'email',
                  placeholder: 'Email'
                }
              }),

              password: view.extend({
                tag: 'input',
                class: 'input input--l width--100 margin--bottom--s',
                args: {
                  type:        'password',
                  name:        'password',
                  placeholder: 'Password'
                }
              }),

              submit: view.extend({
                tag: 'button',
                class: 'btn btn--l width--100 primary margin--bottom--s',
                args: {
                  type: 'submit',
                  text: 'Log in'
                }
              })
            }
          })
        },

        events: [
          ['submit', 'self', function (ev)
          {
            /**
             * Prepare data */
            var self = this;
            var data = this.$el.serialize();

            /**
             * Disable Button */
            this.inputs.submit.$el.addClass('loading');

            /**
             * Send it off to the server */
            actions.login(data, {
              fail: function (err)
              {
                self.inputs.submit.$el.removeClass('loading');
                console.log(err);
              },

              done: function (data)
              {
                self.inputs.submit.$el.removeClass('loading');
                window.location.reload();
              }
            });

            /**
             * Prevent default event */
            return false;
          }]
        ]
      }),

      switch: view.extend({
        class: 'textAlign--m',
        views: {
          inner: "<a href=\"/register\" class=\"switch_btn font_size--s\">Don't have an account? Register</a>"
        }
      })
    }
  }));
})(_);
