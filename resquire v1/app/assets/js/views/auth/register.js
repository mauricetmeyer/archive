(function (_)
{
  var view    = _('view');
  var bind    = _('utils').bind;
  var actions = _('actions');

  _('auth/register', view.extend({
    views: {
      form: view.extend({
        tag:   'form',
        class: 'margin--bottom--s',
        views: {
          head: view.extend({
            class: 'textAlign--m',
            views: {
              main: "<h1 class=\"margin--bottom--xs\">Sign up for free</h1>",
              sub:  "<h3 class=\"margin--bottom--l\">You will love it!</h3>"
            }
          }),

          inputs: view.extend({
            views: {
              full_name: view.extend({
                tag: 'input',
                class: 'input input--l width--100 margin--bottom--s',
                args: {
                  type:        'text',
                  name:        'name',
                  placeholder: 'Full Name'
                }
              }),

              mail: view.extend({
                tag: 'input',
                class: 'input input--l width--100 margin--bottom--s',
                args: {
                  type:        'text',
                  name:        'email',
                  placeholder: 'Email'
                }
              }),

              password: view.extend({
                tag:   'input',
                class: 'input input--l width--100 margin--bottom--s',
                args: {
                  type:        'password',
                  name:        'password',
                  placeholder: 'Password'
                }
              }),

              notice: view.extend({
                class: 'margin--bottom--m font_size--s',
                args: {
                  html: 'By signing up, you agree to our <a href="/terms" class="c--purple">Terms of Service</a>. Learn how we collect, use and share your data in our <a href="/privacy" class="c--purple">Privacy Policy</a>'
                }
              }),

              submit: view.extend({
                tag: 'button',
                class: 'btn btn--l width--100 primary margin--bottom--s',
                args: {
                  type: 'submit',
                  text: 'Continue'
                }
              })
            }
          })
        },

        events: [
          ['submit', 'self', function (ev)
          {
            /**
             * Prepare data */
            var self = this;
            var data = this.$el.serialize();

            /**
             * Disable Button */
            self.inputs.submit.$el.addClass('loading');

            /**
             * Send it off to the server */
            actions.register(data, {
              fail: function (err)
              {
                self.inputs.submit.$el.removeClass('loading');
                console.log(err);
              },

              done: function (data)
              {
                self.inputs.submit.$el.removeClass('loading');
                window.location.reload();
              }
            });

            /**
             * Prevent default event */
            return false;
          }]
        ]
      }),

      switch: view.extend({
        class: 'textAlign--m',
        views: {
          inner: "<a href=\"/login\" class=\"switch_btn font_size--s\">Already have an account? Login</a>"
        }
      })
    }
  }));
})(_);
