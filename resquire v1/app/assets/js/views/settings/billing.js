(function (_)
{
  "use strict";

  var bind    = _('utils').bind;
  var view    = _("view");
  var event   = _('event');
  var store   = _('store');
  var config  = _("config");
  var actions = _('actions');


  _("settings/billing", view.extend({
    views: {
      plan: view.extend({
        class: 'inline margin--bottom--l',
        views: { 
          left: view.extend({
            class: 'inline--grow',
            views: {
              title: view.extend({
                class: 'font_size--l c--purple',
                args: {
                  text: function ()
                  {
                    return config.dat.pro ? 'Pro' : 'Lite';
                  }
                }
              }),

              sub: view.extend({
                class: 'font_size--xs',
                args: {
                  text: function ()
                  {
                    var cancelled = config.dat.pro_cancelled;
                    var ends_at   = new Date(config.dat.subscribed_ends_at);
                    var now       = new Date();

                    if (cancelled && ends_at > now)
                    {
                      var date_utils = _('date/utils');
                      var diff       = Math.abs(date_utils.diff(ends_at, now));
                      
                      return diff + ' days left';
                    }
                    else return config.dat.pro
                      ? '$' + (config.dat.plan.price / 100).toFixed() + ' per month'
                      : 'Free forever';
                  }
                } 
              })
            }
          }),

          right: view.extend({
            views: {
              select: view.extend({
                tag:   'a',
                class: 'btn btn--m primary btn--plan',
                args: {
                  href:          '#',
                  text:          function ()
                  {
                    var cancelled = config.dat.pro_cancelled;
                    var ends_at   = new Date(config.dat.subscribed_ends_at);
                    var now       = new Date();
                    if (cancelled && ends_at > now) return 'Renew';
                    else return config.dat.pro ? 'Cancel' : 'Upgrade';
                  }
                }
              })
            }
          })
        } 
      }),

      wrap: view.extend({
        class: 'box',
        views: {
          inner: view.extend({
            class: 'pad--xl wrap--xs',
            views: {
              cont: view.extend({
                views: {
                  info: view.extend({
                    class: 'textAlign--m',
                    views: {
                      head: view.extend({
                        tag: 'h3',
                        class: 'margin--bottom--l font_size--xxl',
                        args: {
                          text: 'Invite friends to Resquire!'
                        }
                      }),

                      link: view.extend({
                        tag:   'input',
                        class: 'input input--l margin--bottom--l js-url-field',
                        args: {
                          readonly: '',
                          value: function ()
                          {
                            return 'https://resquire.com/?refcode=' + config.dat.referral_id;
                          }
                        }
                      }),

                      meta: view.extend({
                        tag:   'p',
                        args: {
                          text: 'Simply share the link above and when someone signs up for a pro subscription, you will get 5 days in return!'
                        }
                      }),
                    }
                  }),
                }
              })
            }
          }),

          meta: view.extend({
            class: 'cf border--top pad--s font_size--l textAlign--m',
            views: {
              days: view.extend({
                tag: 'strong',
                class: 'c--purple',
                args: {
                  text: function ()
                  {
                    return this.attr.total_days;
                  }
                }
              }),

              text: view.extend({
                tag:  'span',
                args: {
                  text: 'days earned'
                },
                class: 'margin--left--xxs'
              })
            },

            onAttach: function ()
            {
              var self = this;
              actions.referrals_stats(null, {
                done: function (data)
                {
                  var blob = JSON.parse(data);
                  self.set(blob.data);
                },

                fail: function (err) {}
              }) 
            }
          }),

          list: view.extend({
            temps: {
              item: view.extend({
                class: 'inline pad--s pad--left--m pad--right--m border--top',
                views: {
                  mail: view.extend({
                    class: 'inline--grow',
                    views: {
                      inner: view.extend({
                        class: '',
                        args: {
                          text: function ()
                          {
                            return this.attr.email;
                          }
                        }
                      })
                    }
                  }),
                  
                  state: view.extend({
                    views: {
                      inner: view.extend({
                        args: {
                          text: function ()
                          {
                            var text;
                            switch (this.attr.state)
                            {
                              case 'completed':
                                return 'Completed';

                              default:
                                return 'Not Complete'
                            }

                            return text;
                          }
                        }
                      })
                    }
                  })
                }
              })
            },

            onAttach: function ()
            {
              var self = this;
              actions.referrals(null, {
                done: function (data)
                {
                  var blob     = JSON.parse(data);
                  var referred = blob.data;
                  console.log(referred);
                  for (var i = 0; i < referred.length; i++)
                    self.appendItem(referred[i]);
                },

                fail: function (err) {}
              }) 
            }
          })
        }
      })
    },


    events: [
      ['click', '.btn--plan', function (ev)
      {
        var cancelled = config.dat.pro_cancelled;
        if (config.dat.pro && !cancelled)
        {
          event.trigger('confirm', 'You are about to cancel your pro subscription!', {
            ok: 'Continue',
            done: function ()
            {
              actions.plans_cancel(null, {
                fail: function (raw)
                {
                  try
                  {
                    var blob = JSON.parse(raw);
                    event.trigger('alert', blob.data[0]);
                  }
                  catch (e)
                  {
                    event.trigger('alert', 'Woops! Something went wrong.');
                  }
                },

                done: function (raw)
                {
                  var blob = JSON.parse(raw);
                  event.trigger('plan:changed', blob.data);
                }
              }) 
            }
          });
        }
        /**
         * Upgrade or renew */
        else event.trigger('modal:open', 'upgrade');
        return false;
      }],
    ],



    onAttach: function ()
    {
      //_('onboard').start([
      //  {
      //    selector: this.form.inputs.insta.setup.btn.$el,
      //    position: 'bottom',
      //    title:    'First steps',
      //    text:     'You may start by setting up your Instagram account.'
      //  }
      //]);
    }
  }));
})(_);

