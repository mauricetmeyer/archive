
(function (_)
{
  var sel   = _('selector');
  var view  = _('view');
  var event = _('event');

  _('settings', view.extend({
    views: {
      wrap: view.extend({
        class: 'wrap--s',
        views: {
          head: view.extend({
            class: 'margin--bottom--s',
            views: {
              main: '<h1 class="font_size--xxxl">Settings</h1>',
            }
          }),

          tabs: view.extend({
            tag:   'ul',
            class: 'tabs margin--bottom--l',

            temps: {
              tab: view.extend({
                tag: 'li',
                views: {
                  inner: view.extend({
                    tag: 'a',
                    class: 'font_size--m',
                    args: {
                      href: function ()
                      {
                        return this.attr.url;
                      },

                      text: function ()
                      {
                        return this.attr.title;
                      }
                    }
                  })
                }
              })
            },

            init: function ()
            {
              var self = this;
              event.on('shell:changed', function (ev, path)
              {
                var $tabs = self.find('li');
                $tabs.removeClass('active');

                var tabs = self.tabs;
                for (var i = 0, len = tabs.length; i < len; i++)
                {
                  var tab = tabs[i];
                  if (tab.attr.url === path) tab.$el.addClass('active');
                }
              }) 
            },


            defaults: {
              tabs: [
                { url: '/settings',          title: 'Account' },
                { url: '/settings/billing',  title: 'Billing' },
                { url: '/settings/activity', title: 'Activity' }
              ]
            }
          }),

          content: view.extend({
            class: 'shell--view'
          })
        }
      })
    }
  }));
})(_);
