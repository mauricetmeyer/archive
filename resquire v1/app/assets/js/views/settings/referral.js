(function (_)
{
  "use strict";

  var bind    = _('utils').bind;
  var view    = _("view");
  var event   = _('event');
  var store   = _('store');
  var config  = _("config");
  var actions = _('actions');


  _("settings/referral", view.extend({
    class: 'wrap--s',
    views: {
      wrap: view.extend({
        class: 'box',
        views: {
          inner: view.extend({
            class: 'pad--xl wrap--xs',
            views: {
              head: view.extend({
                class: 'margin--bottom--l textAlign--m',
                views: {
                  main: '<h1 class="font_size--xxl">Your referral link</h1>',
                }
              }),
              cont: view.extend({
                views: {
                  info: view.extend({
                    class: 'textAlign--m',
                    views: {
                      head: view.extend({
                        tag: 'h3',
                        class: 'margin--bottom--l',
                        args: {
                          text: 'Invite friends to Resquire and get free Pro!'
                        }
                      }),

                      link: view.extend({
                        tag:   'input',
                        class: 'input input--l margin--bottom--l',
                        args: {
                          readonly: '',
                          value:    'https://resquire.com/?refcode=1b2b3h3hkhbXhx'
                        }
                      }),

                      meta: view.extend({
                        tag:   'p',
                        args: {
                          text: 'Simply share the link above and when someone signs up for a pro subscription, you will get 5 days in return!'
                        }
                      }),
                    }
                  }),
                }
              })
            }
          }),

          meta: view.extend({
            class: 'cf border--top pad--s font_size--l textAlign--m',
            views: {
              days: view.extend({
                tag: 'strong',
                class: 'c--purple',
                args: {
                  text: function ()
                  {
                    return 5 //this.attr.total_days;
                  }
                }
              }),

              text: view.extend({
                tag:  'span',
                args: {
                  text: 'days earned'
                },
                class: 'margin--left--xxs'
              })
            }
          }),

          list: view.extend({
            temps: {
              item: view.extend({
                class: 'inline pad--s pad--left--m pad--right--m border--top',
                views: {
                  mail: view.extend({
                    class: 'inline--grow',
                    views: {
                      inner: view.extend({
                        class: '',
                        args: {
                          text: function ()
                          {
                            return this.attr.mail;
                          }
                        }
                      })
                    }
                  }),
                  
                  state: view.extend({
                    views: {
                      inner: view.extend({
                        args: {
                          text: function ()
                          {
                            return this.attr.state;
                          }
                        }
                      })
                    }
                  })
                }
              })
            },

            defaults: {
              items: [
                { mail: 'hello@mauricetmeyer.com', state: 'Not finished' },
                { mail: 'maurice@lavireo.com', state: 'Completed' },
                { mail: 'maurice@lavireo.com', state: 'Completed' }
              ] 
            }
          })
        }
      })
    },

    onAttach: function ()
    {
      //_('onboard').start([
      //  {
      //    selector: this.form.inputs.insta.setup.btn.$el,
      //    position: 'bottom',
      //    title:    'First steps',
      //    text:     'You may start by setting up your Instagram account.'
      //  }
      //]);
    }
  }));
})(_);

