class ApplicationController < ActionController::Base
  include ErrorHelper
  include LayoutHelper
  include CurrentHelper
  include ReferralHelper

  before_action :set_referral_cookie

  protect_from_forgery


  def index
    render 'layouts/app'
  end


  ##
  # Methods
  def peek_enabled?
    current_user && current_user.staff?
  end


  def require_user!
    redirect_to login_path unless current_user
  end

  def require_staff!
    redirect_to login_path unless current_user && current_user.staff?
  end
end
