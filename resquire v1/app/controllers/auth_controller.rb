class AuthController < ApplicationController
  before_action :check_no_user!

  private
    def check_no_user!
      redirect_to root_path if current_user
    end
end
