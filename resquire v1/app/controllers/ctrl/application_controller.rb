module Ctrl
  class ApplicationController < ::ApplicationController
    include RequestHelper

    def require_user!
      restricted! unless current_user
    end

    def require_staff!
      not_found! unless current_user && current_user.staff?
    end
  end
end
