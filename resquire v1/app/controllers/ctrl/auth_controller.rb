module Ctrl
  class AuthController < Ctrl::ApplicationController
    include CoinhiveHelper

    before_action :require_user!, only: [:check, :logout]


    ##
    # Methods
    def check
      ok!
    end

    def login
      #unless verify(Settings.miner.hashes, params['coinhive-captcha-token'])
      #  restricted!(['Bad Captcha!']) and return
      #end

      user = User.find_by(:email => params[:email])
      if user && user.authenticate(params[:password])
        ses = user.sessions.create

        #
        # Redirect
        if ses.valid?
          cookies[:user_session] = {
            :value     => ses.id,
            :expires   => Time.now + (31 * 24 * 3600),
            :http_only => true
          }
 
          user.events << Events::UserLogin.new(session: ses)
          ok! and return
        end

        bad_request! and return
      end

      restricted!
    end

    def logout
      cookies.delete(:user_session)
      current_session.destroy
      ok!
    end

    def register
      if Settings.auth['restrict_signup']
        restricted!(['Invalid access key!']) and return unless params[:access_key] == Settings.auth['restrict_signup_key']
      end

      #unless verify(Settings.miner.hashes, params['coinhive-captcha-token'])
      #  restricted!(['Bad Captcha!']) and return
      #end

      user = User.create(user_params)
      if user.valid?
        ##
        # User was referred so setup the referral thing!
        if referral_id && referrer = User.find_by(referral_id: referral_id)
          referrer.referrals << User::Referral.new(user: user)
          referrer.events    << Events::UserReferred.new(subject: user)
        end

        ses = user.sessions.create
        if ses
          cookies[:user_session] = {
            :value     => ses.id,
            :expires   => Time.now + (31 * 24 * 3600),
            :http_only => true
          }

          user.events << Events::UserCreate.new(session: ses)
          ok! and return
        end
      end

      bad_request!(user.errors.full_messages)
    end

    private
      def user_params
        params.permit(:name, :email, :password)
      end
  end
end
