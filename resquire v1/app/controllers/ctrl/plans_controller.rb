module Ctrl
  class PlansController < Ctrl::ApplicationController
    before_action :require_user!

    def index
      ok!(Settings.plans)
    end

    def cancel
      error!(['Already on the free plan!']) and return unless current_user.pro
      error!(["Subscription already cancelled!"]) and return if current_user.pro_cancelled?
      if current_user.stripe_id.nil?
        current_user.cancel_pro
        current_user.subscribed_ends_at = DateTime.now
        current_user.save!
      else
        customer = Stripe::Customer.retrieve(current_user.stripe_id)
        customer.cancel_subscription
      end

      ok!
    rescue Stripe::CardError => e
      error!([e.message])
    rescue Stripe::InvalidRequestError => e
      error!
    end

    def upgrade
      error!(['Already a Pro member!']) and return if current_user.pro_active?

      ##
      # Check token first
      token    = params[:token]
      customer = nil

      bad_resquest!(['No credit card provided!']) and return unless token

      ##
      # Create customer
      if current_user.stripe_id.nil? && token
        customer = Stripe::Customer.create(
          plan:   Settings.integrations.stripe.plan_id,
          email:  current_user.email, 
          source: token 
        )
        current_user.stripe_id = customer.id
        current_user.save!
      elsif token
        customer = Stripe::Customer.retrieve(current_user.stripe_id)
        customer.source = token
        customer.save
        customer.create_subscription({
          plan: Settings.integrations.stripe.plan_id,
          billing_cycle_anchor: current_user.subscribed_ends_at
        })
      end

      current_user.events << Events::PlanUpgradeStarted.new(session: current_session)
      ok!
    rescue Stripe::CardError => e
      current_user.events << Events::PlanUpgradeFailed.new(session: current_session)
      error!(['There was an issue with your card'])
    rescue Stripe::InvalidRequestError => e
      error!(['We couldn\'t complete your request'])
    end
  end
end
