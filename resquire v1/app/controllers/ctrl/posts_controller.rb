module Ctrl
  class PostsController < Ctrl::ApplicationController
    before_action :require_user!
    before_action :set_post, except: [:create, :index]

    def index
      ok!(current_user.posts.order(posted_at: :desc))
    end

    def create
      restricted! and return unless current_user.can?(:create_post)
      restricted!(["Sorry you've reached the limit"]) and return if current_user.is?(:at_post_limit)

      current_user.counter.expires_at('posts', Time.now.utc.end_of_month.to_i)
      post = current_user.posts.create(post_params)
      if post.valid?
        Posts::UploadJob.set(wait_until: post.scheduled_at).perform_later(post)
        #Posts::UploadJob.perform_later(post)
        current_user.counter.increment('posts')
        current_user.events << Events::PostSchedule.new(session: current_session, subject: post)
        ok!(post) and return
      end

      error!(post.errors.full_messages)
    end

    def update
      restricted! and return unless @post.pending?
      ok!
    end

    def retry
      restricted! and return unless current_user.can?(:use_retry)
      @post.retry
      Posts::UploadJob.perform_later(@post)
      ok!
    end

    def cancel
      restricted! and return unless @post.queued? || @post.failed?
      current_user.events << Events::PostCancel.new(session: current_session, subject: @post)
      @post.cancel
      current_user.counter.decrement('posts')
      Posts::CleanJob.perform_later(@post)
      ok!
    end

    def publish
      restricted! and return unless @post.queued?
      Posts::UploadJob.perform_later(@post)
      ok!
    end


    private
    def set_post
      not_found! unless @post ||= current_user.posts.find_by_id(params[:post_id])
    end

    def post_params
      params.permit(:image, :caption, :tags, :scheduled_at)
    end
  end
end
