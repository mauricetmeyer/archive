module Ctrl
  class ReferralsController < Ctrl::ApplicationController
    before_action :require_user!

    ##
    # Methods
    def index
      data = ActiveModel::Serializer::CollectionSerializer.new(current_user.referrals,
        each_serializer: User::ReferralSerializer
      )
      ok!(data)
    end

    def stats
      total = current_user.referrals.where(state: :completed).count

      ok!({
        total:      total,
        total_days: total * Settings.referral.days
      })
    end
  end
end
