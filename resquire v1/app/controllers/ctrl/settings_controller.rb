module Ctrl
  class SettingsController < Ctrl::ApplicationController
    before_action :require_user!

    def index
      ok!(current_user)
    end

    def check
      ##
      # Run check job and check the state after that
      Users::CheckJob.perform_now(current_user, instagram_params)
      current_user.reload
      ok!(current_user.insta_state)
    end


    def update
      ok! and return if current_user.update(user_params)
      bad_request!(current_user.errors.full_messages)
    end

    def password
      if current_user.authenticate(params[:password])
        current_user.update(:password => params[:password_new])
        ok! and return
      end

      restricted!
    end

    private
    def user_params
      params.permit(:name, :email, :tags)
    end

    def instagram_params
      params.permit(:insta_login, :insta_password)
    end
  end
end
