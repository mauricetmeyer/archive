module Ctrl
  class UserController < Ctrl::ApplicationController
    before_action :require_staff!
  end
end
