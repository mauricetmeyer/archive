module Webhook
  class ApplicationController < ::ApplicationController
    include RequestHelper

    skip_before_action :verify_authenticity_token
  end
end
