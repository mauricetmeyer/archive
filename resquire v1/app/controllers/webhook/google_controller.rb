module Webhook
  class GoogleController < Webhook::ApplicationController
    def index
      head :created
    rescue JSON::ParserError => e
      head :bad_request
    end
  end
end
