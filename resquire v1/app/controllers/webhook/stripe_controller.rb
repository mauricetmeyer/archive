module Webhook
  class StripeController < Webhook::ApplicationController
    def index
      ##
      # Process events
      payload = request.body.read
      header  = request.env['HTTP_STRIPE_SIGNATURE']
      event   = Stripe::Webhook.construct_event(
        payload, header, Settings.integrations.stripe.signature_key
      )

      case event.type
      when 'customer.subscription.created'
        Users::ActivateSubscriptionJob.perform_later(event.data.object.id)
      when 'customer.subscription.deleted'
        Users::CancelSubscriptionJob.perform_later(event.data.object.id)
      end

      head :created
    rescue JSON::ParserError => e
      head :bad_request
    rescue Stripe::APIConnectionError, Stripe::SignatureVerificationError => e
      head :bad_request
    end
  end
end
