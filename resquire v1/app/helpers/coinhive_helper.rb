module CoinhiveHelper
  def verify(hashes, token)
    conn = Faraday.new(:url => 'https://api.coinhive.com')

    resp = conn.post '/token/verify', { 
      'secret': Settings.miner.private_key,
      'hashes': hashes,
      'token':  token
    }

    JSON.parse(resp.body, symbolize_names: true)[:success]
  end
end
