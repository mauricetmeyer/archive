module CurrentHelper
  def current_user
    @current_user ||= current_session.user if current_session
  end

  def current_session
    key = cookies[:user_session]
    @current_session ||= User::Session.authenticate(key)
  end
end
