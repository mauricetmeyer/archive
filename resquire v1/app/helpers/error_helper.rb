module ErrorHelper
  def ok!(data = nil, errors = nil)
    response!(200, data, errors)
  end

  def restricted!(data = nil, errors = nil)
    response!(404, data, errors)
  end

  def bad_request!(data = nil, errors = nil)
    response!(402, data, errors)
  end

  def not_found!(data = nil, errors = nil)
    response!(404, data, errors)
  end


  private
    def response!(code, data = nil, errors = nil)
      render file: Rails.root.join('public', code.to_s), layout: false, status: code
    end
end
