module InstagramHelper
  AGENT               = "Mozilla/5.0 (iPhone; CPU iPhone OS 10_3 like Mac OS X) AppleWebKit/603.1.30 (KHTML, like Gecko) Version/10.3 Mobile/14E277 Safari/603.1.30"
  SWITCHES            = ["--incognito", "--user-agent=#{AGENT}", "--disable-extensions", "--headless"]

  CAMERA_PATH         = "//section/nav[2]/div/div/div[2]/div/div/div[@role='menuitem']"
  EXPANDER_PATH       = "//button/span[@text='Expand']//parent::button"
  POSTS_PATH          = "//main/div/article/div[1]/div"
  FILE_INPUT_PATH     = "//section/nav[2]/div/div/form/input[@type='file']"
  CAPTION_INPUT_PATH  = "//textarea[@placeholder='Write a caption…']"
  COMMENT_INPUT_PATH  = "//textarea[@placeholder='Add a comment…']"
  COMMENT_INPUT2_PATH = "//input[@placeholder='Add a comment…']"

  STORY_SUBMIT_PATH   = "//div[@role='button']/span[@text='Add to your story']//parent::div"
  STORY_INPUT_PATH    = "//button/form/input[@type='file']"
  STORY_BUTTON_PATH   = "//button/span[@text='Your Story']//parent::button"

  class InstagramNextError < StandardError; end
  class InstagramPostError < StandardError; end
  class InstagramFileError < StandardError; end
  class InstagramSubmitError < StandardError; end
  class InstagramCameraError < StandardError; end
  class InstagramNoPostError < StandardError; end
  class InstagramExpandError < StandardError; end
  class InstagramCaptionError < StandardError; end

  class InstagramLoginError < StandardError; end
  class Instagram2FAActiveError < StandardError; end
  class InstagramSuspiciousError < StandardError; end

  class InstagramSecurityCodeSendError < StandardError; end
  class InstagramSecurityCodeInputError < StandardError; end
  class InstagramSecurityCodeSubmitError < StandardError; end
  class InstagramSecurityCodeReceiveError < StandardError; end
end
