module LayoutHelper
  def page_title(title=nil)
    @page_title ||= []
    @page_title.push(title) unless title.nil?
    @page_title.reverse.join(" - ")
  end
end
