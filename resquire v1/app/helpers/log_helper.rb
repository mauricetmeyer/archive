module LogHelper
  ##
  # Methods
  def ok!(name, options={})
    event!(:ok, name, options)
  end

  def warn!(name, options={})
    event!(:warn, name, options)
  end

  def error!(name, options={})
    event!(:error, name, options)
  end

  def debug!(name, options={})
    event!(:debug, name, options)
  end


  private
    def log!(level, name, options={})
    end
end
