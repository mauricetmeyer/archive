require 'redcarpet'
require 'redcarpet/render_strip'

module MarkdownHelper
  DEFAULT_FILTERS = [
    HTML::Pipeline::MarkdownFilter,
    HTML::Pipeline::AutolinkFilter,
    HTML::Pipeline::SanitizationFilter,
    HTML::Pipeline::TableOfContentsFilter,
    HTML::Pipeline::SyntaxHighlightFilter
  ]

  def stripped(text)
    markdown = Redcarpet::Markdown.new(Redcarpet::Render::StripDown)
    markdown.render(text)
  end

  def markdown(text)
    @default_pipeline ||= HTML::Pipeline.new(DEFAULT_FILTERS, {
      gfm:         true,
      anchor_icon: ''
    })
    @default_pipeline.call(text)[:output].to_s.html_safe
  end

  def markdown_mtime(name)
    f = "#{name}.markdown"
    File.mtime(f)
  end

  def markdown_content(name)
    f = "#{name}.markdown"
    Rails.cache.fetch("#{f}#{markdown_mtime(name)}") do
      markdown(File.read(f))
    end
  end
end