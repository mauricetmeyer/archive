module ReferralHelper
  def set_referral_cookie
    if params[:refcode]
      cookies[:refcode] = {
        :value     => params[:refcode],
        :expires   => 1.month.from_now,
        :http_only => true
      }
    end
  end

  def referral_id
    @referral_id ||= cookies[:refcode] || params[:refcode]
  end
end
