require 'json/ext'

module RequestHelper
  def ok!(data = nil, errors = nil)
    response!(200, data, errors)
  end

  def error!(data = nil, errors = nil)
    response!(500, data, errors)
  end

  def restricted!(data = nil, errors = nil)
    response!(403, data, errors)
  end

  def bad_request!(data = nil, errors = nil)
    response!(402, data, errors)
  end

  def not_found!(data = nil, errors = nil)
    response!(404, data, errors)
  end


  private
    def response!(code, data = nil, errors = nil)
      render plain: ({
        #
        # Meta
        meta: {
          status: code,
          errors: errors
        },

        #
        # Data
        data: data
      }.to_json).prepend('])}for(;;);'), :status => code, :content_type => Mime[:json]
    end
end
