module ResourceHelper
  def resource(name)
    @resources       ||= {}
    @resources[name] ||= File.read("#{Rails.root}/config/resources/#{name}.md")
  end
end
