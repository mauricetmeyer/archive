class Posts::CleanJob < ApplicationJob
  def perform(post)
    post.remove_image!
    post.destroy
  end


  ##
  # Just don't care
  rescue_from(ActiveRecord::RecordNotFound) do |e|
  end
end
