class Posts::UploadJob < ApplicationJob
  include InstagramHelper

  def perform(post)
    ##
    # Check if it has been posted or cancelled already
    if post.cancelled?
      Posts::CleanJob.perform_later(post)
      return
    end

    ##
    # Why isn't it pending?
    return unless post.queued?
    post.process

    ##
    # Let's do this!
    user = post.user

    ##
    # Chrome
    opts    = { 'chromeOptions' => { 'args' => SWITCHES }}
    caps    = Selenium::WebDriver::Remote::Capabilities.chrome(opts)
    browser = Watir::Browser.new(:remote,
      :url                  => "#{Settings.watir.host}:#{Settings.watir.port}/wd/hub",
      :desired_capabilities => caps
    )

    ##
    # Firefox
    # prof = Selenium::WebDriver::Firefox::Profile.new
    # prof['browser.privatebrowsing.dont_prompt_on_enter'] = true
    # prof['browser.privatebrowsing.autostart']            = true
    # prof['general.useragent.override']                   = AGENT
    # prof['dom.file.createInChild']                       = true
    # caps    = Selenium::WebDriver::Remote::Capabilities.firefox(firefox_profile: prof)
    # browser = Watir::Browser.new(:remote,
    #   :url                  => "#{Settings.watir.host}:#{Settings.watir.port}/wd/hub",
    #   :desired_capabilities => caps
    # )

    if browser.nil?
      post.failure
      return
    end

    begin
      ##
      # File uploads
      browser.driver.file_detector = lambda do |args|
        # args => ["/path/to/file"]
        str = args.first.to_s
        str if File.exist?(str)
      end

      ##
      # Login
      browser.goto 'instagram.com/accounts/login/'
      browser.text_field(:name => "username").send_keys "#{user.insta_login}"
      browser.text_field(:name => "password").send_keys "#{user.insta_password}"
      browser.button(:text => "Log in").click
      sleep(5)

      ##
      # Check log-in
      raise InstagramLoginError unless browser.element(:class => 'logged-in').exists?
      #puts "Logged In"

      ##
      # Work around "Save login info" 
      browser.goto 'instagram.com'


      ##
      # Post
      if true 
        ##
        # File upload
        camera = browser.div(:xpath => CAMERA_PATH)
        raise InstagramCameraError unless camera.exists?

        camera.click
        file = browser.file_field(:xpath => FILE_INPUT_PATH)
        raise InstagramFileError unless file.exists?
        file.send_keys("#{post.image.path}")
        puts "Selected File"

        sleep(3)

        ##
        # Check if it went through
        raise InstagramPostError unless browser.h1(:text => 'New Post').exists?

        ##
        # Remove crop
        expander = browser.button(:xpath => EXPANDER_PATH)
        expander.click if expander.exists?
        puts "Removed Crop"

        ##
        # Go on
        forward = browser.button(:text => 'Next')
        raise InstagramNextError unless forward.exists?
        forward.click
        puts "Adding Caption"
        sleep(3)


        ##
        # Caption
        caption = browser.textarea(:xpath => CAPTION_INPUT_PATH)
        raise InstagramCaptionError unless caption.exists?
        if post.caption
          #caption.set("#{post.caption}") 
          script = "arguments[0].value = '#{post.caption}';"
          caption.click
          caption.send_keys(post.caption)
        end

        ##
        # Branded?
        if user.is?(:branded)
          if post.caption
            caption.send_keys :return
            caption.send_keys :return
          end

          caption.send_keys "#{Settings.branding}"
        end

        ##
        # Post
        forward = browser.button(:text => 'Share')
        raise InstagramNextError unless forward.exists?
        forward.click

        ##
        # Wait 'n' stuff
        sleep(5)

        ##
        # Can the user use custom tags?
        if user.can?(:use_tags) && post.tags
          ##
          # Goto Post
          browser.goto "instagram.com/#{user.insta_login}"
          posts_el = browser.div(:xpath => POSTS_PATH)
          posts    = posts_el.as
          raise InstagramNoPostError unless posts.length > 0
          link = posts.first.href
          browser.goto link
          sleep(2)
          #puts "Adding Tags"

          ##
          # Add tags
          comment = browser.textarea(:xpath => COMMENT_INPUT_PATH)
          unless comment.exists?
            comment = browser.input(:xpath => COMMENT_INPUT2_PATH)
          end
          raise InstagramCommentError unless comment.exists?
          comment.send_keys "#{post.tags.strip}"
          comment.send_keys :enter
          sleep(2)
        end
      ##
      # Story
      else
      end

      ##
      # Teardown
      post.post
      browser.close
      user.events << Events::PostPublish.new(subject: post)
      ##
      # NOTE (Maurice):
      # We'll keep the jobs so users can actually see a history
      # and so that we can add stats to them later on.
      #Posts::CleanJob.perform_later(post)

      ##
      # Decrement post count
      user.counter.decrement('posts')
    rescue Exception => e
      puts e.message
      post.failure
      user.events << Events::PostFail.new(subject: post)
      browser.close
    end
  end


  ##
  # Just don't care
  rescue_from(ActiveRecord::RecordNotFound) do |e|
  end
end
