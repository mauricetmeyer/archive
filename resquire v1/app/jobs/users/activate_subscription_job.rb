class Users::ActivateSubscriptionJob < ApplicationJob
  def perform(subscription_id)
    subscription            = Stripe::Subscription.retrieve(subscription_id)
    user                    = User.find_by_stripe_id(subscription.customer)
    user.subscribed_at      = DateTime.now
    user.subscribed_ends_at = nil
    user.activate_pro
    user.save!
  end
end
