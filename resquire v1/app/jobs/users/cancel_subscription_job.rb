class Users::CancelSubscriptionJob < ApplicationJob
  def perform(subscription_id)
    subscription            = Stripe::Subscription.retrieve(subscription_id)
    user                    = User.find_by_stripe_id(subscription.customer)
    user.subscribed_ends_at = Time.at(subscription.current_period_end).to_datetime
    user.cancel_pro
    user.save!
  end
end
