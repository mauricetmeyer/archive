class Users::CheckJob < ApplicationJob
  include InstagramHelper

  def perform(user, params)
    user.reset_instagram
    user.update(insta_login: nil, insta_password: nil)

    ##
    # Chrome
    opts    = { 'chromeOptions' => { 'args' => SWITCHES }}
    caps    = Selenium::WebDriver::Remote::Capabilities.chrome(opts)
    browser = Watir::Browser.new(:remote,
      :url                  => "#{Settings.watir.host}:#{Settings.watir.port}/wd/hub",
      :desired_capabilities => caps
    )

    ##
    # Firefox
    # prof = Selenium::WebDriver::Firefox::Profile.new
    # prof['browser.privatebrowsing.dont_prompt_on_enter'] = true
    # prof['browser.privatebrowsing.autostart']            = true
    # prof['general.useragent.override']                   = AGENT
    # prof['dom.file.createInChild']                       = true
    # caps    = Selenium::WebDriver::Remote::Capabilities.firefox(firefox_profile: prof)
    # browser = Watir::Browser.new(:remote,
    #   :url                  => "#{Settings.watir.host}:#{Settings.watir.port}/wd/hub",
    #   :desired_capabilities => caps
    # )

    if browser.nil?
      return false
    end

    begin
      ##
      # Login
      browser.goto 'instagram.com/accounts/login/'
      browser.text_field(:name => "username").send_keys "#{params[:insta_login]}"
      browser.text_field(:name => "password").send_keys "#{params[:insta_password]}"
      browser.button(:text => "Log in").click
      sleep(5)

      ##
      # Check log-in
      unless browser.element(:class => 'logged-in').exists?
        ##
        # 2FA?
        if browser.text_field(:name => "verificationCode").exists?
          user.fail_2fa_instagram
          raise Instagram2FAActiveError 
        end


        ##
        # Unusal behaviour?
        # 
        # NOTE (Maurice):
        # This is an alternative solution.
        #if browser.h2(:text => "We Detected An Unusual Login Attempt").exists?
        #if browser.p(:text => "Suspicious Login Attempt").exists?
        #  button = browser.button(:class => %w(_qv64e _gexxb _4tgw8 _njrw0), :text => "Send Security Code")
        #  raise InstagramSecurityCodeSendError unless button.exists?
        #  button.click
        #  sleep(5)

        #  input = browser.text_field(:name => "securityCode")
        #  raise InstagramSecurityCodeInputError unless input.exists?

        #  key        = false
        #  time       = 5.minutes
        #  time_stop  = DateTime.now + time
        #  until (key = REDIS.exists(redis_key)) || (DateTime.now >= time_stop)  do
        #    sleep 1
        #  end

        #  ##
        #  # Key not received
        #  raise InstagramSecurityCodeReceiveError unless key

        #  ##
        #  # Send keys and submit
        #  code = REDIS.get(redis_key)
        #  input.send_keys("#{code}")
        #  REDIS.del(redis_key)
        #  button = browser.button(:class => %w(_qv64e _gexxb _4tgw8 _njrw0), :text => "Submit")
        #  raise InstagramSecurityCodeSubmitError unless button.exists?
        #  button.click

        #  ##
        #  # We probably have to login again :/
        #  # NOTE (Maurice):
        #  # Investigate more to see if we always have to login again
        #  unless browser.navs.length == 2
        #    ##
        #    # Tell the user to try again
        #  end
        #end
        if browser.p(:text => "Suspicious Login Attempt").exists?
          user.fail_suspicious_instagram
          raise InstagramSuspiciousLoginError 
        end

        ##
        # Just a default login error
        user.fail_instagram
        raise InstagramLoginError
      end

      ##
      # Save user image
      user.update(params)
      user.activate_instagram
      browser.goto "instagram.com/#{user.insta_login}"
      image = browser.image(alt: 'Change profile photo')
      if image.exists?; user.remote_avatar_url = image.src
      else; user.remove_avatar
      end
      user.save!

      ##
      # Teardown
      browser.close
    rescue Exception => e
      puts e.message
      browser.close
    end
  end


  ##
  # Just don't care
  rescue_from(ActiveRecord::RecordNotFound) do |e|
  end
end
