class ApplicationMailer < ActionMailer::Base
  layout 'mailer'
  default from: 'no-reply@lavireo.com'
end
