module Emails
  module Posts
    def posts_failed
    end

    def posts_published
    end

    def posts_scheduled
    end

    def posts_cancelled
    end
  end
end
