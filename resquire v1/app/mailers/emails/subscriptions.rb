module Emails
  module Subscriptions
    def subscriptions_remind
    end

    def subscriptions_cancelled
    end

    def subscriptions_activated
    end
  end
end
