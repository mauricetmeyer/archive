class Mailer < ApplicationMailer
  include Emails::Posts
  include Emails::Subscriptions
end
