require 'policy'

class Ability
  class << self
    def allowed?(user, action, subject=nil, opts={})
      policy = get_policy(user, subject)
      policy.allowed?(action)
    end

    def get_policy(user, subject)
      Policy.policy_for(user, subject)
    end
  end
end
