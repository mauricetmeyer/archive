class Account < ApplicationRecord
  include Countable
  include Encryptable

  ##
  # State
  state_machine :insta_state, initial: :insta_pending do
    ##
    # States
    state :insta_active
    state :insta_failed
    state :insta_2fa_active
    state :insta_suspicious_login
    state :insta_pending

    ##
    # Methods
    event :reset_instagram do
      transition any => :insta_pending
    end

    event :activate_instagram do
      transition any => :insta_active
    end

    event :fail_instagram do
      transition :insta_pending => :insta_failed
    end

    event :fail_2fa_instagram do
      transition :insta_pending => :insta_2fa_active
    end

    event :fail_suspicious_instagram do
      transition :insta_pending => :insta_suspicious_login
    end
  end

  ##
  # Relations
  has_many   :memberships, dependent: :destroy, class_name: 'User::Membership'
  has_many   :members,     dependent: :destroy, through: :memberships
  has_many   :clearances,  dependent: :destroy, class_name: 'Account::Clearance'
  belongs_to :owner,                            class_name: 'User'


  ##
  # Encrypted
  attr_encrypted :insta_login,    key: :encryption_key
  attr_encrypted :insta_password, key: :encryption_key
end
