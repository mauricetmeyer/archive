class Account::Clearance < ApplicationRecord
  ##
  # State
  state_machine :state, initial: :open do
    ##
    # States
    state :open
    state :pending
    state :rejected
    state :approved

    ##
    # Methods
    event :submit do
      transition :open => :pending
    end

    event :reject do
      transition :pending => :rejected
    end

    event :approve do
      transition :pending => :approved
    end
  end

  ##
  # Relations
  belongs_to :account
  belongs_to :author, class_name: 'User' 

  ##
  # Validations
  validates :author,  presence: true
  validates :amount,  presence: true
  validates :account, presence: true

  validates :due_at,  presence: true
  validates :post_at, presence: true
end