class Account::Membership < ApplicationRecord
  ##
  # State
  state_machine :state, initial: :pending do
    ##
    # States
    state :active
    state :pending
    state :rejected

    ##
    # Methods
    event :reject do
      transition :pending => :rejected
    end

    event :accept do
      transition :pending => :active
    end
  end

  ##
  # Relations
  belongs_to :user
  belongs_to :account

  ##
  # Validations
  validates :user,    presence: true
  validates :account, presence: true

  ##
  # Methods
  def id_prefix
    'mem'
  end
end
