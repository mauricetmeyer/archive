require 'serve/random'

class ApplicationRecord < ActiveRecord::Base
  include Serve::Random
  self.abstract_class = true
end
