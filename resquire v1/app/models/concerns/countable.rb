require 'serve'

module Countable
  extend ActiveSupport::Concern

  def counter
    @counter ||= Serve::Counter.new(id, self.class.name.gsub(/::/, '_').downcase)
  end
end
