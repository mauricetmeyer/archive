module Domainable
  extend ActiveSupport::Concern

  included do
    before_save :save_domain
  end

  private
    def save_domain
      format = title.downcase.gsub(/[^0-9a-z ]/i, '')
      self.domain = format.gsub(' ', '-')
    end
end
