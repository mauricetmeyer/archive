module Encryptable
  extend ActiveSupport::Concern

  def encryption_key
    ENV['ENCRYPTION_KEY']
  end
end
