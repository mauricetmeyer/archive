class User < ApplicationRecord
  include Countable
  include Encryptable

  acts_as_paranoid
  has_secure_password

  ##
  # State
  state_machine :insta_state, initial: :insta_pending do
    ##
    # States
    state :insta_active
    state :insta_failed
    state :insta_2fa_active
    state :insta_suspicious_login
    state :insta_pending

    ##
    # Methods
    event :reset_instagram do
      transition any => :insta_pending
    end

    event :activate_instagram do
      transition any => :insta_active
    end

    event :fail_instagram do
      transition :insta_pending => :insta_failed
    end

    event :fail_2fa_instagram do
      transition :insta_pending => :insta_2fa_active
    end

    event :fail_suspicious_instagram do
      transition :insta_pending => :insta_suspicious_login
    end
  end

  state_machine :pro_state, initial: :pro_pending do
    ##
    # States
    state :pro_active
    state :pro_failed
    state :pro_pending
    state :pro_cancelled

    ##
    # Methods
    event :fail_pro do
      transition :pro_pending => :pro_failed
    end

    event :reset_pro do
      transition any => :pro_pending
    end

    event :cancel_pro do
      transition any => :pro_cancelled
    end

    event :activate_pro do
      transition :pro_pending => :pro_active
    end
  end


  ##
  # Events
  before_validation :prepare
  before_validation :gen_referral_id


  ##
  # Relations
  has_many :posts,       dependent: :destroy, class_name: 'User::Post'
  has_many :events,      dependent: :destroy, class_name: 'User::Event'
  has_many :sessions,    dependent: :destroy, class_name: 'User::Session'
  has_many :referrals,   dependent: :destroy, class_name: 'User::Referral', foreign_key: :referrer_id
  #has_many :memberships, dependent: :destroy, class_name: 'Account::Membership'
  #has_many :accounts,    through: :memberships


  ##
  # Encryptions
  attr_encrypted :insta_login,    key: :encryption_key
  attr_encrypted :insta_password, key: :encryption_key


  ##
  # Validations
  validates :email, presence: true, uniqueness: true, format: { with: /\A[A-Za-z0-9._%+-]+@[A-Za-z0-9\.-]+\.([a-zA-Z]{2,6})\Z/ }
  validate  :valid_content_type

  ##
  # Attachements
  mount_uploader :avatar, AvatarUploader

  ##
  # Methods
  def id_prefix
    'acc'
  end

  def pro
    (pro_active? || (pro_cancelled? && DateTime.now < subscribed_ends_at))
  end

  def plan
    plan = 'free'
    plan = 'pro' if pro
    Settings.plans[plan]
  end

  def can?(action, subject=nil, opts={})
    Ability.allowed?(self, action, subject.nil? ? self : subject, opts)
  end
  alias_method :is?, :can?


  def gen_reset_token
    update(:reset_token => SecureRandom.urlsafe_base64)
  end

  def gen_activate_token
    update(:activate_token => SecureRandom.urlsafe_base64)
  end


  private
  def prepare
    self.email = email.downcase if self.email_changed?
  end

  def gen_referral_id
    self.referral_id = SecureRandom.hex(6) unless self.referral_id
  end

  def valid_content_type
    errors.add(:avatar, "is an invalid file type") unless %w(image/jpeg image/png).include? avatar.sanitized_file.content_type if avatar.present?
  end
end
