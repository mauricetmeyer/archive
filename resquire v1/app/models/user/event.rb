class User::Event < ApplicationRecord
  ##
  # Relations
  belongs_to :user
  belongs_to :session, class_name: 'User::Session'
  belongs_to :subject, polymorphic: true

  ##
  # Validations
  validates :user, presence: true

  ##
  # Methods
  def id_prefix
    'act'
  end
end
