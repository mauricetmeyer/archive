class User::Post < ApplicationRecord
  include Countable

  ##
  # State
  state_machine :state, initial: :queued do
    ##
    # States
    state :failed
    state :posted
    state :queued
    state :pending
    state :cancelled

    ##
    # Methods
    event :process do
      transition [:queued] => :pending
    end

    event :failure do
      transition :pending => :failed
    end

    event :post do
      transition :pending => :posted
    end

    event :retry do
      transition :failed => :queued
    end

    event :cancel do
      transition [:queued, :failed] => :cancelled
    end

    ##
    # Callbacks
    after_transition :on => :post do |p|
      p.posted_at = DateTime.now
      p.save
    end
  end

  ##
  # Scopes
  scope :posted,    -> { where(state: :posted) }
  scope :pending,   -> { where(state: :pending) }
  scope :cancelled, -> { where(state: :cancelled) }

  ##
  # Uploader
  mount_uploader :image, PostUploader

  ##
  # Seralize
  serialize :tags
  serialize :caption

  ##
  # Relations
  has_many   :events, class_name: 'User::Event', as: :subject
  belongs_to :user

  ##
  # Validations
  validates :user,         presence: true
  validates :state,        presence: true
  validates :image,        presence: true
  validates :tags,         length: { maximum:  2200 } # Instagram max
  validates :caption,      length: { maximum:  2200 } # Instagram max
  validates :scheduled_at, presence: true
  validate  :valid_content_type

  ##
  # Methods
  def id_prefix
    'pst'
  end


  private
  def valid_content_type
    errors.add(:image, "is an invalid file type") unless %w(image/jpeg image/png image/tiff).include? image.sanitized_file.content_type if image.present?
  end
end
