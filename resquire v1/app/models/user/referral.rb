class User::Referral < ApplicationRecord
  ##
  # State
  state_machine :state, initial: :pending do
    ##
    # States
    state :pending
    state :completed

    ##
    # Methods
    event :complete do
      transition any => :completed
    end

    ##
    # Transitions
    after_transition on: :activate do |ref|
    end
  end


  ##
  # Relations
  belongs_to :user
  belongs_to :referrer, class_name: 'User'


  ##
  # Validations
  validates :user,     presence: true
  validates :referrer, presence: true


  ##
  # Methods
  def id_prefix
    'ref'
  end
end
