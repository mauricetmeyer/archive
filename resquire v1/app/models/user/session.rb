class User::Session < ApplicationRecord
  ##
  # Relations
  belongs_to :user


  ##
  # Validations
  validates :user, presence: true


  ##
  # Methods
  def id_prefix
    'ses'
  end

  def self.authenticate(id)
    ses = where(:id => id, :revoked_at => nil).first
  end
  
  def revoked?
    (revoked_at != NULL)
  end

  def access(request)
    self.ip          = request.ip
    self.accessed_at = Time.now
    self.user_agent  = request.user_agent
    self.save!
  end

  def active?
    (accessed_at >= 2.weeks.ago)
  end

  def online?
    (accessed_at >= 2.minutes.ago)
  end
end
