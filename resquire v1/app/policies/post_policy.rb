class PostPolicy < ApplicationPolicy
  ability :retry do
  end

  ability :cancel do
  end

  ability :publish do
  end
end
