class User::PostPolicy < ApplicationPolicy
  ability :cancel do
    true
  end

  ability :publish do
    true
  end
end
