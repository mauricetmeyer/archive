class UserPolicy < ApplicationPolicy
  ability :branded do
    @user.plan['branded']
  end


  ability :use_tags do
    @user.plan['can_tag'] || @user.staff?
  end

  ability :use_retry do
    @user.plan['can_retry'] || @user.staff?
  end


  ability :create_post do
    @user.insta_login && @user.insta_password
  end

  ability :at_post_limit do
    plan     = @user.plan
    at_limit = plan['post_max'] == 0 ? false : @user.counter.value('posts') >= plan['post_max']
    at_limit && !@user.staff?
  end
end
