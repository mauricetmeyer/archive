class ApplicationSerializer < ActiveModel::Serializer
  attributes :id, :type, :created_at, :updated_at

	##
	# Methods
  def id
    return unless object.respond_to?(:id)
    object.id
  end

  def type
    return unless object.respond_to?(:type)
    object.type
  end

  def created_at
    return unless object.respond_to?(:created_at)
    object.created_at.to_i
  end

  def updated_at
    return unless object.respond_to?(:updated_at)
    object.updated_at.to_i
  end
end
