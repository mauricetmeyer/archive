class EventSerializer < ApplicationSerializer
  ##
  # String
  attributes :title
  attributes :state

  ##
  # Datetime
  attributes :seen_at
  attributes :created_at
end
