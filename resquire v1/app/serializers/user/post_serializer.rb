class User::PostSerializer < ApplicationSerializer
  ##
  # Image
  attribute :image

  ##
  # String
  attribute :tags
  attribute :state
  attribute :caption

  ##
  # Datetime
  attribute :posted_at
  attribute :scheduled_at
end
