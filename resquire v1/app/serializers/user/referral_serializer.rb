class User::ReferralSerializer < ApplicationSerializer
  ##
  # String
  attribute :state
  attribute :email

  ##
  # Datetime
  attribute :updated_at
  attribute :created_at


  ##
  # Methods
  def email
    object.user ? object.user.email : 'User deleted'
  end
end
