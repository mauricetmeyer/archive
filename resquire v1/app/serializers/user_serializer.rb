class UserSerializer < ApplicationSerializer
  ##
  # Text
  attribute :tags

  ##
  # String
  attribute :name
  attribute :plan
  attribute :email
  attribute :avatar
  attribute :insta_login

  ##
  # Boolean
  attribute :pro
  attribute :staff
  attribute :can_post
  attribute :referral_id
  attribute :pro_cancelled


  ##
  # DateTime
  attribute :subscribed_ends_at


  ##
  # Methods
  def can_post
    object.insta_active?
  end

  def pro_cancelled
    object.pro_cancelled?
  end
end
