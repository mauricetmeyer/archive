class AvatarUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick

  ##
  # Storage
  storage :file
  def store_dir
    "media/#{mounted_as}/#{model.class.to_s.underscore}/#{model.id}"
  end

  ##
  # Versions
  version :thumb do
    process resize_to_fit: [50, 50]
  end

  ##
  # Filename
  def filename
    "_.#{file.extension}" if original_filename.present?
  end

  def default_url
    "/static/img/avatar.svg"
  end


  ##
  # Whitelist
  def extension_whitelist
    %w(jpg jpeg gif png)
  end
end
