class PostUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick

  ##
  # Storage
  storage :file
  def store_dir
    "media/#{mounted_as}/#{model.class.to_s.underscore}/#{model.id}"
  end

  ##
  # Prepare
  process :prepare

  ##
  # Versions
  version :thumb do
    process resize_to_limit: [400, nil]
  end

  ##
  # Filename
  def filename
    "_.jpg"
  end

  ##
  # Whitelist
  def extension_whitelist
    %w(jpg jpeg png tif tiff)
  end


  private
  def prepare(opts={})
    manipulate! do |img|
      img.strip

      ##
      # Change to jpg
      img.format("jpg") do |c|
        c.colorspace  "sRGB"
      end
      
      ##
      # Resize image
      img.resize "1080^>"
      img = yield(img) if block_given?
      img
    end
  end
end
