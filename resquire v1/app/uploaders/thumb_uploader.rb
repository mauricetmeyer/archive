class ThumbUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick

  ##
  # Storage
  storage :file
  def store_dir
    "media/#{mounted_as}/#{model.class.to_s.underscore}/#{model.id}"
  end

  ##
  # Prepare
  process :prepare

  ##
  # Filename
  def filename
    "_.jpg"
  end

  ##
  # Whitelist
  def extension_whitelist
    %w(jpg jpeg png tif tiff)
  end


  private
  def prepare(opts={})
    manipulate! do |img|
      cols, rows = img[:dimensions]

      img.strip

      ##
      # Change to jpg
      img.format("jpg") do |c|
        c.colorspace  "sRGB"
        if width != cols || height != rows
          scale_x = 1440 / cols.to_f
          scale_y = 600 / rows.to_f
          if scale_x >= scale_y
            cols = (scale_x * (cols + 0.5)).round
            rows = (scale_x * (rows + 0.5)).round
            c.resize "#{cols}"
          else
            cols = (scale_y * (cols + 0.5)).round
            rows = (scale_y * (rows + 0.5)).round
            c.resize "x#{rows}"
          end
        end

        c.gravity 'Center'
        c.extent "#{width}x#{height}" if cols != width || rows != height
      end

      ##
      # Resize image
      img = yield(img) if block_given?
      img
    end
  end
end
