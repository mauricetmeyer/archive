require File.expand_path('../boot', __FILE__)

# Frameworks
require "active_model/railtie"
require "active_record/railtie"
require "action_controller/railtie"
require "action_mailer/railtie"
require "action_view/railtie"
require "sprockets/railtie"

# Bundler
Bundler.require(*Rails.groups)

module Automator
  class Application < Rails::Application
    config.encoding = "utf-8"
    config.eager_load_paths.push(*%W(#{config.root}/lib))
    config.middleware.use Rack::Attack
  end
end
