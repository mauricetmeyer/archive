Rails.application.configure do
  config.log_level = :debug

  # Settings specified here will take precedence over those in config/application.rb

  # In the development environment your application's code is reloaded on
  # every request. This slows down response time but is perfect for development
  # since you don't have to restart the web server when you make code changes.
  config.cache_classes = false

  # Show full error reports and disable caching
  config.consider_all_requests_local       = true
  config.action_controller.perform_caching = false

  config.action_mailer.delivery_method = :smtp
  config.action_mailer.smtp_settings = {
    address: ENV["MAIL_ADDRESS"],
    domain:  ENV["MAIL_DOMAIN"],
    user_name: ENV["SMTP_USER"],
    password:  ENV["SMTP_PASSWORD"],
    authentication: 'login',
    openssl_verify_mode: 'none'
  }

  config.action_mailer.default_url_options = { :host => 'sso.lavireo.com' }

  # Don't care if the mailer can't send
  config.action_mailer.raise_delivery_errors = true

  # Print deprecation notices to the Rails logger
  config.active_support.deprecation = :log

  # Only use best-standards-support built into browsers
  config.action_dispatch.best_standards_support = :builtin
  config.eager_load = false
end
