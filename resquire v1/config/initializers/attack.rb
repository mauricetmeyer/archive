class Rack::Attack
  throttle('req/ip', :limit => 300, :period => 5.minutes) do |req|
    req.ip unless req.path.start_with?('/media') || req.path.start_with?('/static')
  end

  throttle('logins/ip', :limit => 5, :period => 20.seconds) do |req|
    req.ip if req.path == '/ctrl/login' && req.post?
  end

  safelist('allow from localhost') do |req|
    # Requests are allowed if the return value is truthy
    '127.0.0.1' == req.ip || '::1' == req.ip
  end
end
