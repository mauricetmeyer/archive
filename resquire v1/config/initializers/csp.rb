Rails.application.config.content_security_policy do |p|
  p.default_src :self
  p.img_src     :self, :data, 'www.google-analytics.com', 'www.facebook.com'
  p.font_src    :self, :data, 'fonts.gstatic.com'
  p.media_src   :self, :data
  p.object_src  :none
  p.script_src  :self, 'js.stripe.com', 'www.google-analytics.com', 'connect.facebook.net', 'fullstory.com'
  p.style_src   :self, :unsafe_inline, 'fonts.googleapis.com'
  p.frame_src   :self, 'js.stripe.com'
  p.connect_src :self, 'api.stripe.com', 'www.google-analytics.com', 'rs.fullstory.com'

  ##
  # Specify URI for violation reports
  # p.report_uri  "/csp-violation-report-endpoint"
end
