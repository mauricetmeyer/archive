require 'mail'

Sidekiq.configure_server do |config|
  config.redis = {
    url: Serve::Redis.url,
    namespace: Serve::Redis::SIDEKIQ_NAMESPACE
  }

  ##
  # Database pool should be at least `sidekiq_concurrency` + 2
  # For more info, see: https://github.com/mperham/sidekiq/blob/master/4.0-Upgrade.md
  config = ActiveRecord::Base.configurations[Rails.env] ||
                Rails.application.config.database_configuration[Rails.env]
  config['pool'] = Sidekiq.options[:concurrency] + 2
  ActiveRecord::Base.establish_connection(config)
  Rails.logger.debug("Connection Pool size for Sidekiq Server is now: #{ActiveRecord::Base.connection.pool.instance_variable_get('@size')}")

  ##
  # Avoid autoload issue such as 'Mail::Parsers::AddressStruct'
  # https://github.com/mikel/mail/issues/912#issuecomment-214850355
  Mail.eager_autoload!
end

Sidekiq.configure_client do |config|
  config.redis = {
    url: Serve::Redis.url,
    namespace: Serve::Redis::SIDEKIQ_NAMESPACE
  }
end
