# Cookie Policy 

Our Cookies Policy explains what cookies are, how we use cookies, how third-parties we may partner with may use cookies on the Service, your choices regarding cookies and further information about the specific cookies we use.


## What are Cookies? 

Cookies are small text files that are stored in a computer’s web browser memory. They help website providers with things like understanding how people use a website, remembering a User’s login details, and storing website preferences. This page explains how we use cookies and other similar technologies to help us ensure that our Services function properly, prevent fraud and other harm, and analyze and improve the Services in accordance with our [Privacy Policy](/privacy). Any capitalized term used and not otherwise defined below has the meaning assigned to it in the Privacy Policy.


## How We Use Cookies 

Cookies play an important role in helping us provide personal, effective and safe Services. We use cookies on our website. We change the cookies periodically as we improve or add to our Services, but we generally use cookies for the following purposes:

**To Operate Our Services.** Some cookies are essential to the operation of our website and Services. We use those cookies in a number of different ways, including:

- **Authentication.** We use cookies to remember Your login state so You don’t have to login as You navigate through our site and Your dashboard. For example, when You log into Your account, we use a cookie so that You don’t have to login again as You navigate throughout the website.

- **Fraud Prevention and Detection.** Cookies and similar technologies that we deploy through websites and the Services help us learn things about computers and web browsers used to access the Services. This information helps us monitor for and detect potentially harmful or illegal use of our Services.

- **Site Features and Services.** We use cookies to remember how You prefer to use our Services so that You don’t have to reconfigure Your settings each time You log into Your account.

**To Analyze and Improve Our Services.** Cookies help us understand how to make our website and Services work better for You. Cookies tell us how people reach our website and our Users’ websites and they give us insights into improvements or enhancements we need to make to our website and Services.

**For Better Advertising.** Cookies can help us provide more effective advertising on our website. For example, we might use a cookie to help prevent You from seeing the same advertisement multiple times or to measure how many times an advertisement is viewed or clicked on.


## How To Manage Cookies

Your web browser may allow You to change Your cookie preferences, including to delete and disable Resquire cookies. Please consult the help section of Your web browser or follow the links below to understand Your options, but please note that if You choose to disable the cookies, some features of our website or Services may not operate as intended.

- **Chrome:** https://support.google.com/chrome/answer/95647?hl=en
- **Explorer:** https://support.microsoft.com/en-us/products/windows?os=windows-10
- **Safari:** https://support.apple.com/kb/PH21411
- **Firefox:** https://support.mozilla.org/products/firefox/cookies
- **Opera:** http://www.opera.com/help/tutorials/security/cookies/


## Cookie Table

Cookies that we commonly use are listed below. This list is not exhaustive, but describes the main reasons we typically set cookies.

### Resquire cookies

| Cookie Name    | Purpose                                                  | Persistent |
| -----------    | -------------------------------------------------------- | ---------- |
| client_session | Provides a unique session identifier for users           | yes        |
| user_session   | Keeps users login session                                | yes        |
| cookie_ack     | Stores the cookie notice preferences                     | yes        |


### Third party cookies

| Cookie Name    | Purpose                                                  |
| -----------    | -------------------------------------------------------- |
| Google         | Used for analytics and service improvement               |
| Stripe         | Used for payments and fraud analysis                     |