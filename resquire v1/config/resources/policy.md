# Privacy Policy 
This Privacy Policy describes how and when we collect, use, and share your information across our websites, APIs, email notifications and applications that link to this Policy (collectively, the “Service”), and from our partners and other third parties. For example, you send us information when you use our Service on the web, or from an application such as Resquire for iOS or Resquire for Android. When using any of our services you consent to the collection, transfer, storage, disclosure, and use of your information as described in this Privacy Policy. This includes any information you choose to provide that is deemed sensitive under applicable law.

When this policy mentions “we” or “us,” it refers to the controller of your information under this policy. Despite this, you alone control and are responsible for the posting and other content you submit through the Service, as provided in the Terms of Service.
Irrespective of which country you live in, you authorize us to transfer, store, and use your information in the Germany, and any other country where we operate. In some of these countries, the privacy and data protection laws and rules regarding when government authorities may access data may vary from those in the country where you live.


## Personal Data We Collect
We collect and use your information below to provide, understand, and improve our Service.

### Basic Account Information:
If you choose to create a Resquire account, you must provide us with some personal information, such as your full name, email address and password. You can create and manage multiple Resquire accounts.

### Contact Information:
You may use your contact information, such as your email address to customize your account or enable certain account features, for example, for login verification. We may use your contact information to send you information about our Service, to market to you, to help prevent spam, fraud, or abuse. You may use your settings for email and mobile notifications to control notifications you receive from Resquire. You may also unsubscribe from a notification by following the instructions contained within the notification or the instructions on our website.

### Content:
Most of the information you provide us through Resquire is information you are asking us to make public. You may provide us with profile information. When you share information or content via the Service, you should think carefully about what you are making public.

### Location Information:
We may receive information about your location. We may also determine location by using other data from your device, such as precise location information from GPS, information about wireless networks or cell towers near your mobile device, or your IP address. We may use and store information about your location to provide features of our Service, or to improve the Service.

### Links:
We may keep track of how you interact with links across our Service, including our email notifications, third-party services, and client applications, by redirecting clicks or through other means. We do this to help improve our Service, and to be able to share aggregate click statistics such as how many times a particular link was clicked on.

### Cookies:
We use cookies and similar technologies to collect additional website usage data and to improve our Service. We may use both session cookies and persistent cookies to better understand how you interact with our Service, to monitor aggregate usage by our users and web traffic routing on our Service, and to customize and improve our Service. Although most web browsers automatically accept cookies, some browsers’ settings can be modified to decline cookies or alert you when a website is attempting to place a cookie on your computer. However, some services may not function properly if you disable cookies.

To learn more about the cookies that may be served through our Sites and how You can control our use of cookies and third-party analytics, please see our [Cookie Policy](/cookies).

### Using Our Service:
We receive information when you view content on or otherwise interact with our Service, even if you have not created an account (“Log Data”). For example, when you visit our websites, sign into our Service, interact with our email notifications, we may receive information about you. This Log Data may include your IP address, browser type, operating system, the referring web page, pages visited, location, your mobile carrier, device information (including device and application IDs), search terms, or cookie information. We also receive Log Data when you click on, view or interact with links on our Service. We use Log Data to monitor the use of the Site and of our Service, and for the Site’s technical administration. We keep Log Data as needed for the purposes described in this Privacy Policy. We will either delete Log Data or remove any common account identifiers, such as your username, full IP address or email address, after a maximum of 18 months, if not sooner. We do not associate your IP address with any other personally identifiable information to identify you personally, except in case of violation of the Terms of Service.

### Third-Parties and Affiliates:
We may receive information about you from third parties, such as partners, or our corporate affiliates. Our partners and affiliates may share information with us such as a browser cookie ID, mobile device ID, or cryptographic hash of an email address, as well as demographic or interest data and content viewed or actions taken on a website or app.

### Personalizing Across Your Devices: 
When you log into your account with a browser or device, we will associate that browser or device with your account for purposes such as authentication and personalization.


## How We Use Personal Data

We use the information we collect in various ways, including to:

- Provide, operate, and maintain our Services
- Improve, personalize, and expand our Services
- Understand and analyze how you use our Services
- Develop new products, services, features, and functionality
- Communicate with you, either directly or through one of our partners, including for customer service, to provide you with updates and other information relating to the Service, and for marketing and promotional purposes
- Process your transactions
- Send you text messages and push notifications
- Find and prevent fraud
- For compliance purposes, including enforcing our Terms of Service, or other legal rights, or as may be required by applicable laws and regulations or requested by any judicial process or governmental agency.


## How We Disclose Personal Data
We do not disclose your private personal information except in the limited circumstances described here.

### User Consent or Direction
We may share or disclose your information at your direction, such as when you authorize a third-party web client or application to access your account or when you direct us to share your feedback with a business.

### Advertising
We work with third-party advertising partners to show you ads that we think may interest you. These advertising partners may set and access their own cookies, pixel tags, and similar technologies on our Services, and they may otherwise collect or have access to information about you which they may collect over time and across different online services. Some of our advertising partners are members of the Network Advertising Initiative or the Digital Advertising Alliance. To learn more about these programs, or opt-out of personalized ads, visit the Digital Advertising Alliance’s Self-Regulatory program for Online Behavioral Advertising at www.aboutads.info, or the Network Advertising Initiative at www.networkadvertising.org.

### Service Providers
We engage service providers to perform functions and provide services to us in the United States, Ireland, and other countries. For example, we use a variety of third-party services to help provide our Service to help us understand and improve the use of our Service, such as Google Analytics. We may share your private personal information with such service providers subject to obligations consistent with this Privacy Policy and any other appropriate confidentiality and security measures, and on the condition that the third parties use your private personal data only on our behalf and pursuant to our instructions. We share your payment information, including your credit or debit card number, card expiration date, CVV code, and billing address with payment services providers to process payments; prevent, detect and investigate fraud or other prohibited activities; facilitate dispute resolution such as chargebacks or refunds; and for other purposes associated with the acceptance of credit or debit cards.

### Corporate transactions
In the event that we enter into, or intend to enter into, a transaction that alters the structure of our business, such as a reorganization, merger, sale, joint venture, assignment, transfer, change of control, or other disposition of all or any portion of our business, assets or stock, we may share Personal Data with third parties for the purpose of facilitating and completing the transaction.

### Compliance and harm prevention
Notwithstanding anything to the contrary in this Privacy Policy, we may preserve or disclose your information if we believe that it is reasonably necessary to comply with a law, regulation, legal process, or governmental request; to protect the safety of any person; to address fraud, security or technical issues; or to protect our or our users’ rights or property. However, nothing in this Privacy Policy is intended to limit any legal defenses or objections that you may have to a third party’s, including a government’s, request to disclose your information.


## Your Rights and Choices

Depending on your location and subject to applicable law, you may have the following rights with regard to the Personal Data we control about you:

- You have the right to **request confirmation** of whether we process Personal Data relating to you, and if so, to request a copy of that Personal Data.
- If you wish to **access, correct, update, or request deletion** of your personal information, you can do so at any time by emailing privacy@resquire.com.
- In addition, you can **object to the processing** of your personal information, ask us to **restrict the processing** of your personal information, or request portability of your personal information. Again, you can exercise these rights by emailing privacy@resquire.com.
- You have the right to opt-out of marketing communications we send you at any time. You can exercise this right by clicking on the "unsubscribe" or "opt-out" link in the marketing emails we send you. To opt-out of other forms of marketing, please contact us by emailing privacy@resquire.com.
- Similarly, if we have collected and process your personal information with your consent, then you can **withdraw your consent at** any time.  Withdrawing your consent will not affect the lawfulness of any processing we conducted prior to your withdrawal, nor will it affect the processing of your personal information conducted in reliance on lawful processing grounds other than consent.
- You have the **right to complain to a data protection authority** about our collection and use of your personal information. For more information, please contact your local data protection authority.

In order to exercise your data protection rights, you may contact us as described in the [Contact Us](#contact-us) section below. We will comply to your request to the extent required by applicable law. We will not be able to respond to a request if we no longer hold your Personal Data. 

For your protection, we may need to verify your identity before responding to your request, such as verifying that the email address from which you send the request matches your email address that we have on file. If we no longer need to process Personal Data about you in order to provide our Services or our Sites, we will not maintain, acquire or process additional information in order to identify you for the purpose of responding to your request.



## Accessing and Modifying Your Personal Information

If you are a registered user of our Service, we provide you with tools and account settings to access, correct, delete, or modify the personal information you provided to us and associated with your account. 

You can also permanently delete your Resquire account. If you follow the instructions here, we begin the process of deleting your account from our systems immediately, this process can take up to a week.


## Links To Other Websites.

The Services may provide the ability to connect to other websites. These websites may operate independently from us and may have their own privacy notices or policies, which we strongly suggest you review. If any linked website is not owned or controlled by us, we are not responsible for its content, any use of the website or the privacy practices of the operator of the website.


## Use by Minors

The Services are not directed to individuals under the age of thirteen (13), and we request that they not provide Personal Data through the Services. If you learn that a child has provided us with personal information in violation of this Privacy Policy, you can alert us at privacy@resquire.com.


## Changes to this Privacy Policy

We may change this Privacy Policy from time to time to reflect new services, changes in our Personal Data practices or relevant laws. Any changes are effective when we post the revised Privacy Policy on the Services. If we materially change the ways in which we use or share personal information previously collected from you through our Services, we will notify you through our Services, by email, or other communication.


## International Data Transfers

We may transfer personal information to countries other than the country in which the data was originally collected. These countries may not have the same data protection laws as the country in which you initially provided the information. When we transfer your personal information to other countries, we will protect that information as described in this Privacy Policy.

## Contact Us

If You have any questions or complaints about this Privacy Policy, please contact us [electronically](mailto:privacy@resquire.com).