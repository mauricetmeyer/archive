Automator::Application.routes.draw do
  ##
  # Peek
  mount Peek::Railtie => 'peek'


  ##
  # Ctrl
  namespace :ctrl do
    get  'plans',                   to: 'plans#index'
    post 'plans/cancel',            to: 'plans#cancel'
    post 'plans/upgrade',           to: 'plans#upgrade'

    post 'login',                   to: 'auth#login'
    post 'logout',                  to: 'auth#logout'
    post 'register',                to: 'auth#register'

    get  'posts',                   to: 'posts#index'
    post 'posts',                   to: 'posts#create'
    post 'posts/retry',             to: 'posts#retry'
    post 'posts/cancel',            to: 'posts#cancel'
    post 'posts/publish',           to: 'posts#publish'

    get  'settings',                to: 'settings#index'
    post 'settings',                to: 'settings#update'
    post 'settings/check',          to: 'settings#check'
    post 'settings/password',       to: 'settings#password'

    get  'referrals',               to: 'referrals#index'
    get  'referrals/stats',         to: 'referrals#stats'
  end


  ##
  # Webhook
  namespace :webhook do

    ##
    # Login
    post 'google',                  to: 'google#index'
    post 'twitter',                 to: 'twitter#index'
    post 'facebook',                to: 'facebook#index'

    ##
    # Payments
    post 'stripe',                  to: 'stripe#index'
    post 'braintree',               to: 'braintree#index'
  end

  get  'login',                     to: 'auth#index',             as: 'login'
  get  'register',                  to: 'auth#index',             as: 'register'
  get  'settings',                  to: 'dashboard#index',        as: 'settings'
  get  'settings/billing',          to: 'dashboard#index',        as: 'billing'
  get  'settings/activity',         to: 'dashboard#index',        as: 'activity'
  root 'dashboard#index'
end
