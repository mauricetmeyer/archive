class CreateAccounts < ActiveRecord::Migration[5.0]
  def change
    create_table :accounts, id: false do |t|
      ##
      # Primary Key
      t.primary_key :id, :string, limit: 20, default: nil

      ##
      # Text
      t.text     :tags

      ##
      # String
      t.string   :owner_id

      t.string   :name
      t.string   :avatar
      t.string   :country
      t.string   :insta_state
      t.string   :encrypted_insta_login
      t.string   :encrypted_insta_login_iv
      t.string   :encrypted_insta_password
      t.string   :encrypted_insta_password_iv

      ##
      # Datetime
      t.datetime :blocked_at
      t.datetime :deleted_at

      ##
      # Timestamps
      t.timestamps
    end

    ##
    # Indexes
    add_index :accounts, :id
  end
end
