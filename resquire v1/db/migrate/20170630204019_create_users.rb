class CreateUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :users, id: false do |t|
      ##
      # Primary Key
      t.primary_key :id, :string, limit: 20, default: nil

      ##
      # Text
      t.text     :tags

      ##
      # String
      t.string   :name
      t.string   :plan, default: 'free'
      t.string   :email, limit: 191
      t.string   :avatar
      t.string   :country
      t.string   :stripe_id
      t.string   :reset_token
      t.string   :two_factor_secret, limit: 6
      t.string   :activate_token
      t.string   :password_digest
      t.string   :pro_state
      t.string   :insta_state
      t.string   :insta_login
      t.string   :insta_password

      ##
      # Boolean
      t.boolean  :staff
      t.boolean  :two_factor

      ##
      # Datetime
      t.datetime :blocked_at
      t.datetime :deleted_at
      t.datetime :activated_at
      t.datetime :subscribed_at
      t.datetime :subscribed_ends_at

      ##
      # Timestamps
      t.timestamps
    end

    ##
    # Indexes
    add_index :users, :email
  end
end
