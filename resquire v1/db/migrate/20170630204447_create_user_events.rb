class CreateUserEvents < ActiveRecord::Migration[5.0]
  def change
    create_table :user_events, id: false do |t|
      ##
      # Primary Key
      t.primary_key :id, :string, limit: 20, default: nil

      ##
      # String
      t.string :type
      t.string :user_id, limit: 191
      t.string :session_id
      t.string :subject_id
      t.string :subject_type

      ##
      # Timestamps
      t.timestamps
    end

    ##
    # Indexes
    add_index :user_events, :user_id
  end
end
