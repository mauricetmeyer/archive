class CreateUserSessions < ActiveRecord::Migration[5.0]
  def change
    create_table :user_sessions, id: false do |t|
      ##
      # Primary Key
      t.primary_key :id, :string, limit: 20, default: nil

      ##
      # String
      t.string   :ip, limit: 191
      t.string   :key, limit: 191
      t.string   :user_agent
      t.string   :user_id, limit: 191
      
      ##
      # Datetime
      t.datetime :revoked_at
      t.datetime :accessed_at

      ##
      # Timestamps
      t.timestamps
    end

    ##
    # Indexes
    add_index :user_sessions, :ip
    add_index :user_sessions, :key
    add_index :user_sessions, :user_id
  end
end
