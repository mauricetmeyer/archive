class CreateUserPosts < ActiveRecord::Migration[5.0]
  def change
    create_table :user_posts, id: false do |t|
      ##
      # Primary Key
      t.primary_key :id, :string, limit: 20, default: nil

      ##
      # Text
      t.text     :tags
      t.text     :caption

      ##
      # String
      t.string   :state
      t.string   :image
      t.string   :user_id, limit: 191

      ##
      # Datetime
      t.datetime :scheduled_at

      ##
      # Timestamps
      t.timestamps
    end

    ##
    # Indexes
    add_index :user_posts, :user_id
    add_index :user_posts, :scheduled_at
  end
end
