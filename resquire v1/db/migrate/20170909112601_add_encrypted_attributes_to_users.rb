class AddEncryptedAttributesToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :encrypted_insta_login,       :string
    add_column :users, :encrypted_insta_login_iv,    :string
    add_column :users, :encrypted_insta_password,    :string
    add_column :users, :encrypted_insta_password_iv, :string
    User.all.each do |user|
      user.insta_login    = user.read_attribute('insta_login')
      user.insta_password = user.read_attribute('insta_password')
      user.save
    end
  end
end
