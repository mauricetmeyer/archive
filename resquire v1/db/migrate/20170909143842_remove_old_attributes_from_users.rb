class RemoveOldAttributesFromUsers < ActiveRecord::Migration[5.0]
  def change
    remove_column :users, :insta_login,    :string
    remove_column :users, :insta_password, :string
  end
end
