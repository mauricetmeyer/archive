class AddPostedAtToUserPosts < ActiveRecord::Migration[5.2]
  def change
    add_column :user_posts, :posted_at, :datetime
    User::Post.update_all("posted_at=scheduled_at")
  end
end
