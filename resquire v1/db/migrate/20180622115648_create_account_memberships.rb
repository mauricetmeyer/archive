class CreateAccountMemberships < ActiveRecord::Migration[5.2]
  def change
    create_table :account_memberships, id: false do |t|
      ##
      # Primary Key
      t.primary_key :id, :string, limit: 20, default: nil

      ##
      # String
      t.string   :state
      t.string   :user_id,    limit: 20
      t.string   :account_id, limit: 20

      ##
      # Integer
      t.integer  :level

      ##
      # Datetime
      t.datetime :rejected_at
      t.datetime :accepted_at

      ##
      # Timestamps
      t.timestamps
    end

    ##
    # Indexes
    add_index :account_memberships, :account_id
    add_index :account_memberships, :user_id
  end
end
