class CreateAccountClearances < ActiveRecord::Migration[5.2]
  def change
    create_table :account_clearances, id: false do |t|
      ##
      # Primary Key
      t.primary_key :id, :string, limit: 20, default: nil

      ##
      # String
      t.string   :key
      t.string   :state
      t.string   :title
      t.string   :account_id, limit: 20

      ##
      # Datetime
      t.datetime :due_at
      t.datetime :approved_at

      ##
      # Timestamps
      t.timestamps
    end

    ##
    # Indexes
    add_index :account_clearances, :account_id
    add_index :account_clearances, :due_at
  end
end
