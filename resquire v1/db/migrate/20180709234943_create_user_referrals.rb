class CreateUserReferrals < ActiveRecord::Migration[5.2]
  def change
    create_table :user_referrals, id: false do |t|
      ##
      # Primary Key
      t.primary_key :id, :string, limit: 20, default: nil

      ##
      # String
      t.string   :state
      t.string   :user_id,     limit: 20
      t.string   :referrer_id, limit: 20

      ##
      # Datetime
      t.datetime :activated_at

      ##
      # Timestamps
      t.timestamps
    end

    ##
    # Indexes
    add_index :user_referrals, :user_id
    add_index :user_referrals, :referrer_id
  end
end
