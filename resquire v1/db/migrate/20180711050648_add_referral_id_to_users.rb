class AddReferralIdToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :referral_id, :string, limit: 12
  end
end
