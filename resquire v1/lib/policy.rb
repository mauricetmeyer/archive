require 'policy/cache'
require 'policy/ability'
require 'policy/base'

module Policy
  class << self
    def policy_for(user, subject, opts = {})
      cache = opts[:cache] || {}
      key = Cache.policy_key(user, subject)
      cache[key] ||= class_for(subject).new(user, subject, opts)
    end

    def class_for(subject)
      subject = find_delegate(subject)
      subject.class.ancestors.each do |klass|
        next unless klass.name

        begin
          policy_class = "#{klass.name}Policy".constantize
          return policy_class if policy_class < Base
        rescue NameError
          nil
        end
      end

      raise "no policy for #{subject.class.name}"
    end

    private

    def find_delegate(subject)
      seen = Set.new

      while subject.respond_to?(:policy_delegate)
        raise ArgumentError, "circular delegations" if seen.include?(subject.object_id)
        seen << subject.object_id
        subject = subject.policy_delegate
      end

      subject
    end
  end
end
