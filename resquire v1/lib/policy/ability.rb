module Policy
  class Ability
    attr_reader :name
    def initialize(name, &compute)
      @name    = name
      @compute = compute
    end

    def compute(context)
      !!context.instance_eval(&@compute)
    end
  end

  class ManifestAbility
    def initialize(ability, context)
      @ability = ability
      @context = context
    end

    # true or false whether this rule passes.
    # `context` is a policy - an instance of
    # Policy::Base.
    def pass?
      @context.cache(cache_key) { @ability.compute(@context) }
    end

    # Whether we've already computed this ability.
    def cached?
      @context.cached?(cache_key)
    end

    private
      def cache_key
        "/dp/ability/#{@ability.name}"
      end

      def user_key
        Cache.user_key(@context.user)
      end

      def subject_key
        Cache.subject_key(@context.subject)
      end
  end
end
