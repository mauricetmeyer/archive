module Policy
  class Base
    class << self
      # The `own_ability_map` vs `ability_map` distinction is used so that
      # the data structure is properly inherited - with subclasses recursively
      # merging their parent class.
      #
      # This pattern is also used for conditions.
      def abilities
        if self == Base
          own_abilities
        else
          superclass.abilities.merge(own_abilities)
        end
      end

      def own_abilities
        @own_abilities ||= {}
      end

      # Declares a ability. It gets stored in `own_abilities`, and generates
      # a query method based on the condition's name.
      def ability(name, opts = {}, &value)
        name                     = name.to_sym
        self.own_abilities[name] = Ability.new(name, &value)
      end
    end

    # A policy object contains a specific user and subject on which
    # to compute abilities. For this reason it's sometimes called
    # "context" within the framework.
    #
    # It also stores a reference to the cache, so it can be used
    # to cache computations by e.g. ManifestCondition.
    attr_reader :user, :subject, :cache
    def initialize(user, subject, opts = {})
      @user    = user
      @subject = subject
      @cache   = opts[:cache] || {}
    end

    # This is the main entry point for permission checks. It constructs
    # or looks up a Runner for the given ability and asks it if it passes.
    def allowed?(*abilities)
      abilities.all? { |a| runner(a).pass? }
    end

    # The inverse of #allowed?, used mainly in specs.
    def disallowed?(*abilities)
      abilities.all? { |a| !runner(a).pass? }
    end

    # computes the given ability and prints a helpful debugging output
    # showing which 
    def debug(ability, *a)
      runner(ability).debug(*a)
    end

    def repr
      subject_repr =
        if @subject.respond_to?(:id)
          "#{@subject.class.name}/#{@subject.id}"
        else
          @subject.inspect
        end

      user_repr =
        if @user
          @user.to_reference
        else
          "<anonymous>"
        end

      "(#{user_repr} : #{subject_repr})"
    end

    def inspect
      "#<#{self.class.name} #{repr}>"
    end

    # returns a ManifestAbility for the given ability, capable of computing whether
    # the ability is allowed. These are cached on the policy (which itself
    # is cached on @cache), and caches its result. This is how we perform caching
    # at the ability level.
    def runner(ability)
      act = self.class.abilities[ability.to_sym]
      ManifestAbility.new(act, self)
    end

    def cache(key, &b)
      return @cache[key] if cached?(key)
      @cache[key] = yield
    end

    def cached?(key)
      !@cache[key].nil?
    end


    def policy_for(other_subject)
      Policy.policy_for(@user, other_subject, cache: @cache)
    end
  end
end
