module Serve
  class Counter
    attr_accessor :id
    attr_accessor :prefix

    #
    #
    def initialize(id, prefix)
      self.id     = id
      self.prefix = prefix
    end

    # Key
    def hash_key(key)
      "#{Serve::Redis::COUNT_NAMESPACE}:#{prefix}:#{id}/#{key}"
    end

    # Set
    def set(key, value)
      @value = nil
      Serve::Redis.with do |redis|
        redis.hset(hash_key(key), id, value)
      end
    end

    # Return value
    def value(key)
      @value = nil
      Serve::Redis.with do |redis|
        @value ||= redis.hget(hash_key(key), id).to_i
      end
    end

    # Clear
    def clear(key)
      @value = nil
      Serve::Redis.with do |redis|
        redis.hdel(hash_key(key), id)
      end
    end

    # Increment
    def increment(key)
      @value = nil
      Serve::Redis.with do |redis|
        redis.hincrby(hash_key(key), id, 1)
      end
    end

    # Decrement
    def decrement(key)
      @value = nil
      Serve::Redis.with do |redis|
        redis.hincrby(hash_key(key), id, -1)
      end
    end

    # Expires At
    def expires_at(key, time)
      @value = nil
      Serve::Redis.with do |redis|
        redis.expireat(hash_key(key), time);
      end
    end
  end
end
