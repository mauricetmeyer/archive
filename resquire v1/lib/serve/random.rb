module Serve
  module Random
    def self.included(model)
      model.primary_key = :id
      model.before_create do
        self.id = "#{id_prefix}_#{SecureRandom.hex(6)}"
      end
    end

    def id_prefix
      raise NotImplementedError
    end
  end
end
