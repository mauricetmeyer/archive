/**
 * server.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import * as mongoose              from 'mongoose';

import { MongoHelper, JobHelper } from '../src/helpers';
import * as config                from '../src/config';
import { Server }                 from '../src/core';
import {
  xss,
  csp,
  hsts,
  csrf,
  frames,
  nosniff,
  server,
  version,
  log,
//  recruit
} from '../src/warez';


/**
 * Make sure we exit on SIGTERM signal. */
process.on('SIGTERM', () => process.exit());

/**
 * Connect to database
 * and start the server */
const URI = MongoHelper.buildURI(config.database);
mongoose.connect(URI, { useFindAndModify: false, useNewUrlParser: true }).then(async () => {
  /**
   * Create server and
   * register middleware
   */
  const app = new Server();
  app.use('Log', log);
  app.use('Server', server('resquire'));
  app.use('Version', version(process.env['APP_VERSION'] || ''));
  app.use('CSRF', csrf({ secret: config.secrets.csrf }));
  app.use('XSS', xss);
  app.use('CSP', csp(config.csp));
  app.use('HSTS', hsts({ subdomains: true, preload: true, maxAge: 7776000 }));
  // app.use('Recruit', recruit({ name: 'Resquire', url: 'https://resquire.com/jobs' }));
  app.use('Frames', frames('deny'));
  app.use('NoSniff', nosniff);

  /**
   * Register routes */
  config.routes(app.router);

  /**
   * Setup job queue and start the server. */
  await JobHelper.connect(config.redisConf);
  await app.startup(5000);
}).catch((err: Error) => console.error(err));
