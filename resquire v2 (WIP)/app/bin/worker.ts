/**
 * worker.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import * as mongoose   from 'mongoose';

import { MongoHelper } from '../src/helpers';
import * as config     from '../src/config';
import { Queue }       from '../src/core';


/**
 * Make sure we exit on SIGTERM signal. */
process.on('SIGTERM', () => process.exit());

/**
 * Connect to database
 * and start the server */
const URI = MongoHelper.buildURI(config.database);
mongoose.connect(URI, { useFindAndModify: false, useNewUrlParser: true }).then(() => {
  const queue = new Queue('jobs', { connection: config.redisConf, isWorker: true });
  queue.onError((err: Error) => console.error(err));
  config.jobs(queue);
  return queue.process();
}).catch((err: Error) => console.error(err));
