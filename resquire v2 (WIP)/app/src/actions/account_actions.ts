/**
 * account_actions.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import * as _                                     from 'underscore';

import { Context }                                from '../core';
import { AccessHelper, ContextHelper, JobHelper } from '../helpers';
import { User }                                   from '../models/user';
import { Account }                                from '../models/account';

const ACCOUNT_PARAMS     = ['handle', 'platform'];
const ACCOUNT_PARAMS_OUT = ['_id', 'createdAt', 'modifiedAt', ...ACCOUNT_PARAMS];
const AccountActions = {
  async create (ctx: Context)
  {
    try
    {
      const user = await AccessHelper.getUser(ctx);
      if (!user) return ctx.json(403, { error: 'invalid_auth' });

      /**
       * First check if the user has the right
       * to access/edit the workspace. */
      if (user.membership.role !== 'admin')
        return ctx.json(403, { error: 'not_authorized' });

      /**
       * Get relevant data from the body */
      const data   = await ContextHelper.json(ctx);
      const params = _.pick(data, ...ACCOUNT_PARAMS);

      /**
       * Create the new account */
      const owner     = user;
      const workspace = user.membership.workspace;

      const account = new Account({ owner, workspace, ...params });
      await account.save();

      /**
       * Get all admins of the workspace
       * and send them this email. */
      const { handle, platform } = account;
      const receivers = await User.find({ 'membership.workspace': workspace._id }, { email: 1 });
      await JobHelper.schedule('mail_send', {
        receiver: receivers.map(r => r.email),
        data:     { owner, handle, platform },
        subject:  'Social account added',
        template: 'account_added'
      });

      /**
       * Return the requested account */
      ctx.json(200, { data: _.pick(account, ...ACCOUNT_PARAMS_OUT) });
    }
    catch (err)
    {
      ctx.json(500, { error: 'error' });
    }
  },

  async update (ctx: Context)
  {
    const user = await AccessHelper.getUser(ctx);
    if (!user) return ctx.json(403, { error: 'invalid_auth' });

    /**
     * First check if the user has the right
     * to access/edit the workspace. */
    if (user.membership.role !== 'admin')
      return ctx.json(403, { error: 'not_authorized' });

    /**
     * Get relevant data from the body */
    const data           = await ContextHelper.json(ctx);
    const params         = _.pick(data, ...ACCOUNT_PARAMS);
    const { account_id } = data;

    try {
      /**
       * Find and update account
       * with a single query. */
      params.modifiedAt = Date.now();
      const account = await Account.findByIdAndUpdate(account_id, params, {
        new: true
      });

      /**
       * Return the account or `Not Found`. */
      const response = account ? { data: account } : { error: 'not_found' };
      ctx.json(account ? 200 : 404, response);
    }
    catch (err)
    {
      ctx.json(500, { error: 'error' });
    }
  },

  async remove (ctx: Context)
  {
    const user = await AccessHelper.getUser(ctx);
    if (!user) return ctx.json(403, { error: 'invalid_auth' });

    /**
     * First check if the user has the right
     * to access/edit the workspace. */
    if (user.membership.role !== 'admin')
      return ctx.json(403, { error: 'not_authorized' });

    /**
     * Get relevant data from the body */
    const data           = await ContextHelper.json(ctx);
    const { account_id } = data;

    try {
      /**
       * Find and update account
       * with a single query. */
      const workspace = user.membership.workspace;
      const account   = await Account.findOneAndDelete({
        _id:       account_id,
        workspace: workspace.id
      });

      /**
       * Return the account or `Not Found`. */
      if (account)
      {
        /**
         * @TODO (Maurice):
         * Get all admins of the workspace
         * and send them this email. */
        const { handle, platform } = account;
        const receivers = await User.find({ 'membership.workspace': workspace._id }, { email: 1 });
        await JobHelper.schedule('mail_send', {
          receiver: receivers.map(r => r.email),
          data:     { user, handle, platform },
          subject:  'Social account removed',
          template: 'account_removed'
        });

        return ctx.json(200, {
          data: _.pick(account, ...ACCOUNT_PARAMS_OUT)
        });
      }

      ctx.json(404, { error: 'not_found' });
    }
    catch (err)
    {
      ctx.json(500, { error: 'error' });
    }
  }
};

export { AccountActions };
