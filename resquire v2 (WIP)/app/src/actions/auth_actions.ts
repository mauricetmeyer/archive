/**
 * auth_actions.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { Context }                      from '../core';
import { app }                          from '../config';
import { ContextHelper, SessionHelper } from '../helpers';

import { User }                         from '../models/user';


const AuthActions = {
  /**
   * POST ``/i/login`` */
  async login (ctx: Context)
  {
    try {
      /**
       * Let's just check if there is a session
       * already. */
      const oldSession = ctx.cookies.get('session');
      if (oldSession) return ctx.json(200, {});

      /**
       * Get relevant data from the body */
      const data = await ContextHelper.json(ctx);
      const { email, password } = data;

      /**
       * Check if parameters are given */
      if (!email || !password)
        return ctx.json(403, { error: 'missing_parameters' });

      /**
       * Get user and
       * check if the password matches */
      const user = await User.findOne({ email });
      if (!user) return ctx.json(403, { error: 'invalid_auth' });

      if (!await user.authenticate(password))
        return ctx.json(403, { error: 'invalid_auth' });

      /**
       * Create a new user session, save the user
       * and return. */
      const session = SessionHelper.create(ctx);
      user.sessions.push(session);
      await user.save();
      return ctx.json(200, {});
    }
    catch (err)
    {
      return ctx.json(500, { error: 'error' });
    }
  },

  /**
   * POST ``/i/logout`` */
  async logout (ctx: Context)
  {
    /**
     * Get `session` cookie,
     * if it exists we'll revoke the user session. */
    const key = ctx.cookies.get('session');
    if (key)
    {
      /**
       * Let's clear the cookie first */
      ctx.cookies.set('session', undefined, {
        secure: app.auth.secure_cookie,
        httpOnly: true,
        domain: app.domain
      });

      /**
       * Get user via the session key
       * and atomically remove the session */
      const user = await User.findOneAndUpdate({ 'sessions.key': key }, {
        $pull: {
          sessions: { key }
        }
      });

      /**
       * If we haven't found a user
       * we can assume that there was no session */
      if (!user) return ctx.json(403, {});

      /**
       * The only thing left to to is
       * saving the user. */
      await user.save();
    }

    return ctx.json(200, {});
  }
};

export { AuthActions };
