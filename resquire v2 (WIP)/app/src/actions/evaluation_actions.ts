/**
 * evaluation_actions.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import * as _       from 'underscore';
import { Context }  from '../core';
import { services } from '../config';
import {
  AccessHelper,
  ContextHelper,
  RequestHelper,
  StreamFile
} from '../helpers';


const EVALUATION_PARAMS = ['media', 'caption', ''];
const EvaluationActions = {
  /**
   * POST ``/i/process`` */
  async process (ctx: Context)
  {
    try {
      const user = await AccessHelper.getUser(ctx);
      if (!user) return ctx.json(403, { error: 'invalid_auth' });

      /**
       * Pipe the media streams to the evaluation microservice
       * and return the results. */
      const type     = ctx.getHeader('content-type') as string;
      const response = await RequestHelper.post(`http://${services.recog}`, {
        headers: { 'content-type': type },
        data: ctx.req
      });

      /**
       * Check and transform reponse data */
      const { status, body } = response;
      const { data, error }  = JSON.parse(body);

      const outStatus = status === 200 ? status : 400;
      const outData   = status === 200 ? { data } : { error };
      ctx.json(outStatus, outData);
    }
    catch (err)
    {
      ctx.json(500, { error: 'error' });
    }
  },

  /**
   * POST ``/i/evaluate`` */
  async evaluate (ctx: Context)
  {
    const user = await AccessHelper.getUser(ctx);
    if (!user) return ctx.json(403, { error: 'invalid_auth' });

    /**
     * Get relevant data from the body */
    const data   = await ContextHelper.json(ctx);
    const accId  = data.account_id;
    const params = _.pick(data, ...EVALUATION_PARAMS);

    /**
     * Send data to eval microservice
     * for evaluation. */
    try {
      const response = await RequestHelper.post(`http://${services.eval}`, {
        headers: { 'Content-Type': 'application/json' },
        data: JSON.stringify(params)
      });

      /**
       * Check and transform reponse data */
      const { status, body } = response;
      const { data, error }  = JSON.parse(body);

      const outStatus = status === 200 ? status : 400;
      const outData   = status === 200 ? { data } : { error };
      ctx.json(outStatus, outData);
    }
    catch (err)
    {
      ctx.json(500, { error: 'error' });
    }
  }
};

export { EvaluationActions };
