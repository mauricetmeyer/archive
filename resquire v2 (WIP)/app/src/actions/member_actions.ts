/**
 * member.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import * as _                                                    from 'underscore';
import * as moment                                               from 'moment';

import { Context }                                               from '../core';
import { AccessHelper, ContextHelper, PaymentHelper, JobHelper, SessionHelper } from '../helpers';

import { User }                                                  from '../models/user';
import { Token }                                                 from '../models/token';


const MEMBERSHIP_PARAMS = ['role', 'email'];
const MemberActions = {
  /**
   * PUT ``/i/member`` */
  async create (ctx: Context)
  {
    try
    {
      const user = await AccessHelper.getUser(ctx);
      if (!user) return ctx.json(403, { error: 'invalid_auth' });

      /**
       * First check if the user has the right
       * to access/edit the workspace. */
      if (user.membership.role !== 'admin')
        return ctx.json(403, { error: 'not_authorized' });

      /**
       * Get relevant data from the body */
      const data   = await ContextHelper.json(ctx);
      const params = _.pick(data, ...MEMBERSHIP_PARAMS);

      /**
       * Prepare params that are used for
       * creating the new member. */
      const workspace       = user.membership.workspace;
      const { role, email } = params;

      if (workspace.isLocked)
        return ctx.json(400, { error: 'workspace_locked' });

      /**
       * Create a new bare user,
       * that is inactive and only
       * contains a membership and email. */
      const member = await User.create({
        email,
        membership: {
          role,
          workspace: workspace.id
        }
      });

      if (member)
      {
        /**
         * Send the member a nice invite email. */
        const expiresAt = moment().add(5, 'days').unix();
        const token     = await Token.generate(member, 'invite', expiresAt);
        await JobHelper.schedule('mail_send', {
          receiver: member.email,
          data:     { name: user.name, invite_token: token },
          subject:  `${user.name} invited you to join Resquire`,
          template: 'member_invited'
        });

        /**
         * The member was created.
         * All left to do now is updating the subscription,
         * to do that we first have to count all the remaining members
         * in the workspace and then send the update to stripe.
         *
         * @NOTE (Maurice):
         * This may need to be placed somewhere else,
         * maybe the right place for this is when the user
         * is activated.
         *
         * @TODO (Maurice):
         * We have to optimise the member count at some point,
         * this may require a rework of the membership system. */
        const count = await User.countDocuments({
          'membership.workspace': workspace.id
        });

        /**
         * Send it off. */
        const sId = workspace.subscription.subscriptionId;
        await PaymentHelper.updateSubscription(sId as string, { quantity: count });

        const { _id, name, email, membership } = member;
        return ctx.json(200, {
          data: { _id, name, email, role: membership.role }
        });
      }

      ctx.json(500, { error: 'error' });
    }
    catch (err)
    {
      ctx.json(500, { error: 'error' });
    }
  },

  /**
   * POST ``/i/member`` */
  async update (ctx: Context)
  {
    try
    {
      const user = await AccessHelper.getUser(ctx);
      if (!user) return ctx.json(403, { error: 'invalid_auth' });

      /**
       * First check if the user has the right
       * to access/edit the workspace. */
      if (user.membership.role !== 'admin')
        return ctx.json(403, { error: 'not_authorized' });

      /**
       * Get relevant data from the body */
      const data                = await ContextHelper.json(ctx);
      const { role, member_id } = data;

      /**
       * Get user from the database and update role. */
      const workspace = user.membership.workspace;
      const member    = await User.findOneAndUpdate({
        _id: member_id,
        'membership.workspace': workspace.id
      }, { $set: { 'membership.role': role } });

      /**
       * Return result */
      if (!member) return ctx.json(400, { error: 'not_a_member' });
      const { _id, name, email, membership } = member;
      return ctx.json(200, {
        data: { _id, name, email, role: membership.role }
      });
    }
    catch (err)
    {
      ctx.json(500, { error: 'error' });
    }
  },

  /**
   * DELETE ``/i/member`` */
  async remove (ctx: Context)
  {
    const user = await AccessHelper.getUser(ctx);
    if (!user) return ctx.json(403, { error: 'invalid_auth' });

    /**
     * First check if the user has the right
     * to access/edit the workspace. */
    if (user.membership.role !== 'admin')
      return ctx.json(403, { error: 'not_authorized' });

    /**
     * Get relevant data from the body */
    const data      = await ContextHelper.json(ctx);
    const member_id = data.member_id;
    if (!member_id) return ctx.json(400, { error: 'missing_member_id' });

    try
    {
      /**
       * Try deleting the member account. */
      const workspace = user.membership.workspace;
      const member    = await User.findOneAndDelete({
        _id: member_id,
        'membership.workspace': workspace.id
      });

      if (member)
      {
        /**
         * @NOTE (Maurice):
         * We have to discuss if we send and email
         * to the removed member or not.
         *
         * await JobHelper.schedule('mail_send', {
         *   options: {
         *     receiver: [],
         *     data:     { actor: user },
         *     subject:  'You have been removed from Resquire',
         *     template: 'member_removed'
         *   }
         * });
         */

        /**
         * The member was found and deleted.
         * All left to do now is updating the subscription,
         * to do that we first have to count all the remaining members
         * in the workspace and then send the update to stripe.
         *
         * @TODO (Maurice):
         * We have to optimise the member count at some point,
         * this may require a rework of the membership system. */
        const count = await User.countDocuments({
          'membership.workspace': workspace.id
        });

        /**
         * Send it off. */
        const sId = workspace.subscription.subscriptionId;
        await PaymentHelper.updateSubscription(sId, { quantity: count });
        return ctx.json(200, {
          data: { _id: member._id }
        });
      }

      /**
       * Return `not_a_member` error. */
      ctx.json(404, { error: 'not_a_member' });
    }
    catch(err)
    {
      ctx.json(500, { error: 'error' });
    }
  },


  /**
   * PUT ``/i/member/invite`` */
  async accept_invite (ctx: Context)
  {
    try
    {
      /**
       * Get relevant data from the body */
      const data      = await ContextHelper.json(ctx);
      const { token } = data;

      /**
       * Try getting the user by the invite token. */
      const member = await Token.get('invite', token);
      if (!member) return ctx.json(400, { error: 'invalid_token' });

      /**
       * Create a new user session, update the user and
       * set a nice little session cookie. */
      const { name, password } = data;
      const session            = SessionHelper.create(ctx);

      member.name     = name;
      member.password = password;
      member.sessions.push(session);
      await member.save();

      /**
       * @TODO (Maurice):
       * Should we send an email to the workspace owners to
       * let them know that their invite was accpeted?
       * await JobHelper.schedule('mail_send', {
       *   receiver: email,
       *   options: {
       *     data:     { member },
       *     subject:  `${member.name} accepted the invite to Resquire`,
       *     template: 'member_joined'
       *   }
       * });
       */

      ctx.json(200, {});
    }
    catch (err)
    {
      ctx.json(500, { error: 'error' });
    }
  },

  /**
   * POST ``/i/member/invite`` */
  async check_invite (ctx: Context)
  {
    try
    {
      /**
       * Get relevant data from the body */
      const data      = await ContextHelper.json(ctx);
      const { token } = data;

      /**
       * Try getting the user by the invite token. */
      const valid = await Token.valid('invite', token);
      if (!valid) return ctx.json(400, { error: 'invalid_token' });
      ctx.json(200, {});
    }
    catch (err)
    {
      ctx.json(500, { error: 'error' });
    }
  }
};

export { MemberActions };
