/**
 * members.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import * as _           from 'underscore';

import { Context }      from '../core';
import { AccessHelper } from '../helpers';

import { User }         from '../models/user';


const MembersActions = {
  /**
   * GET ``/i/members`` */
  async get (ctx: Context)
  {
    try
    {
      const user = await AccessHelper.getUser(ctx);
      if (!user) return ctx.json(403, { error: 'invalid_auth' });

      /**
       * First check if the user has the right
       * to access/edit the workspace. */
      if (user.membership.role !== 'admin')
        return ctx.json(403, { error: 'not_authorized' });

      /**
       * Get all the members of the workspace */
      const workspace = user.membership.workspace;
      const members   = await User.find({
        'membership.workspace': workspace.id
      });

      ctx.json(200, {
        data: members.map(({ _id, name, email, membership }) => {
          return { _id, name, email, role: membership.role };
        })
      });
    }
    catch (err)
    {
      ctx.json(500, { error: 'error' });
    }
  }
};

export { MembersActions };
