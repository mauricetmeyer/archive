/**
 * payment_actions.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import * as _                                                    from 'underscore';

import { Context }                                               from '../core';
import { AccessHelper, ContextHelper, PaymentHelper, JobHelper } from '../helpers';
import { User }                                                  from '../models/user';


const PLAN_MAP: { [key: string]: string | undefined } = {
  monthly: process.env.PLAN_MONTHLY,
  annual:  process.env.PLAN_ANNUALLY
};


const PAYMENT_PARAMS = ['plan', 'token'];
const PaymentActions = {
  /**
   * GET ``/i/payment`` */
  async get (ctx: Context)
  {
    const user = await AccessHelper.getUser(ctx);
    if (!user) return ctx.json(403, { error: 'invalid_auth' });

    /**
     * First check if the user has the right
     * to access/edit payment information. */
    if (user.membership.role !== 'admin')
      return ctx.json(403, { error: 'not_authorized' });

    /**
     * Let's get the subscription info from the
     * workspace that the user is a member off. */
    const workspace    = user.membership.workspace;
    const { customer } = workspace;

    try
    {
      /**
       * With the customerId and subscriptionId in our hands
       * we can now ask stripe for detailed infos. */
      const { members, subscription } =  workspace;
      const paymentInfo = await PaymentHelper.getCustomer(customer);

      /**
       * Successful, return the subscription details */
      ctx.json(200, {
        data: {
          plan: subscription.plan,
          members: members.length,
          isLocked: workspace.isLocked,
          ...paymentInfo
        }
      });
    }
    catch (err)
    {
      ctx.json(500, { error: 'error' });
    }
  },

  /**
   * POST ``/i/payment`` */
  async update (ctx: Context)
  {
    const user = await AccessHelper.getUser(ctx);
    if (!user) return ctx.json(403, { error: 'invalid_auth' });

    /**
     * First check if the user has the right
     * to access/edit payment information. */
    if (user.membership.role !== 'admin')
      return ctx.json(403, { error: 'not_authorized' });

    try
    {
      /**
       * Get all the relevant data from
       * the request. */
      const data      = await ContextHelper.json(ctx);
      const params    = _.pick(data, ...PAYMENT_PARAMS);
      const workspace = user.membership.workspace;

      /**
       * Let's update the payment info */
      const { subscriptionId } = workspace.subscription;
      const { plan, token }    = params;

      /**
       * Only take this route when the account is locked.
       * The user has to supply a new token that will be used with the
       * subscription we'll create right now. */
      if (workspace.isLocked || !subscriptionId)
      {
        /**
         * If the user didn't provide a new payment source
         * we can end it right here and let the user know. */
        if (!token)
        {
          return ctx.json(400, { error: 'missing_source' });
        }

        /**
         * Just warming up some data that we'll need
         * for updating the source and creating the
         * new subscription. */
        const { customer, subscription } = workspace;

        /**
         * Let's first update the customer source
         * and then we can work on creating the
         * new subscription. */
        await PaymentHelper.updateCustomer(customer, token);

        if (!subscriptionId)
        {
          /**
           * Create the new subscription now. */
          const usedPlan = plan || subscription.plan;
          const realPlan = PLAN_MAP[usedPlan];
          if (!realPlan)
          {
            /**
             * Don't know why but the user probably tried to
             * play around with the system. */
            return ctx.json(400, { error: 'invalid_plan' });
          }

          const subscriptionId = await PaymentHelper.createSubscription({
            customer,
            plan: realPlan
          });

          /**
           * Update workspace with the new subscription
           * we just created. */
          workspace.subscription.subscriptionId = subscriptionId;
          await workspace.save();
        }

        /**
         * @NOTE (Maurice):
         * Setting a new source will retry charging right away
         * to prevent any further downtime for the user. */
        return ctx.json(200, {});
      }

      /**
       * Only update the user's
       * default source when there is actually
       * a new token provided with the request. */
      if (token)
      {
        await PaymentHelper.updateCustomer(workspace.customer, token);
      }

      /**
       * Only update the user's subscription
       * plan when the plan was actually changed.
       * We also check if the plan was actually
       * changed. */
      if (plan && workspace.subscription.plan !== plan)
      {
        const realPlan = PLAN_MAP[plan];
        if (!realPlan)
        {
          /**
           * Don't know why but the user probably tried to
           * play around with the system. */
          return ctx.json(400, { error: 'invalid_plan' });
        }

        await PaymentHelper.updateSubscription(subscriptionId, {
          plan: realPlan
        });

        /**
         * Update subscription object for
         * the workspace. */
        workspace.subscription.plan = plan;
        await workspace.save();

        /**
         * Send an email and let the customer know! */
        const receivers = await User.find({ 'membership.workspace': workspace._id }, { email: 1 });
        await JobHelper.schedule('mail_send', {
          receiver: receivers.map(r => r.email),
          data:     { plan_new: plan },
          subject:  `Switched to the ${plan} plan`,
          template: 'plan_changed'
        });
      }

      /**
       * Successful, return the subscription details */
      ctx.json(200, {});
    }
    catch (err)
    {
      /**
       * Something went wrong when updating
       * the payment/plan details. */
      ctx.json(500, { error: 'error' });
    }
  },
};

export { PaymentActions };
