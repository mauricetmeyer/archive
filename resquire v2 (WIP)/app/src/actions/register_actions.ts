/**
 * register_actions.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import * as moment                                                     from 'moment';

import { Context }                                                     from '../core';
import { ContextHelper, ValidationHelper, WorkspaceHelper, JobHelper, SessionHelper } from '../helpers';
import { User }                                                        from '../models/user';
import { Token }                                                       from '../models/token';
import { MembershipRole }                                              from '../models/membership';


const RegisterActions = {
  /**
   * POST ``/i/register`` */
  async register (ctx: Context)
  {
    try
    {
      /**
       * Get relevant data from the body */
      const data = await ContextHelper.json(ctx);
      const { name, email, password } = data;

      /**
       * Check required parameters */
      if (!name || !email || !password)
        return ctx.json(400, { error: 'missing_parameters' });

      /**
       * Check if the email is valid */
      if (typeof email !== 'string' || !ValidationHelper.isEmail(email))
        return ctx.json(400, { error: 'email_invalid' });

      /**
       * Check if email is already in use */
      const exists = await User.findOne({ email });
      if (exists) return ctx.json(400, { error: 'email_in_use' });

      /**
       * Create a new workspace */
      const workspace = await WorkspaceHelper.create();
      if (!workspace) return ctx.json(500, { error: 'error' });

      /**
       * Create a new user */
      const session = SessionHelper.create(ctx);
      const user    = await User.create({
        name,
        email,
        password,
        sessions: [session],
        membership: {
          role:      MembershipRole.ADMIN,
          workspace: workspace._id
        }
      });

      try
      {
        /**
         * Now the only thing left is sending the welcome email.
         * If it doesn't work the user can always request a new
         * confirmation email. */
        const expiresAt = moment().add(2, 'day').unix();
        const token     = await Token.generate(user, 'confirm', expiresAt);
        if (token)
        {
          await JobHelper.schedule('mail_send', {
            receiver: email,
            data:     { name, token },
            subject:  'Welcome to Resquire',
            template: 'welcome'
          });
        }
      }
      catch (err) {}
      ctx.json(200, {});
    }
    catch (err)
    {
      /**
       * @TODO (Maurice):
       * This error should be logged so
       * that we can investigate it. */
      console.error(err);
      ctx.json(500, { error: 'error' });
    }
  }
};

export { RegisterActions };
