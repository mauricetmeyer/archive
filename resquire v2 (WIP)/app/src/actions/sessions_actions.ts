/**
 * sessions_actions.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import * as _                          from 'underscore';
import { Context }                     from '../core';
import { AccessHelper, ContextHelper } from '../helpers';
import { User } from '../models/user';


const SESSION_PARAMS  = ['_id', 'log', 'createdAt', 'revokedAt'];
const SessionsActions = {
  /**
   * GET ``/i/sessions`` */
  async list (ctx: Context)
  {
    const user = await AccessHelper.getUser(ctx);
    if (!user) return ctx.json(403, { error: 'invalid_auth' });

    const sorted = user.sessions.sort((a, b) => a.createdAt - b.createdAt);
    const data   = sorted.map(i => _.pick(i, ...SESSION_PARAMS));

    ctx.json(200, { data });
  },

  /**
   * POST ``/i/sessions/clear`` */
  async clear (ctx: Context)
  {
    const user = await AccessHelper.getUser(ctx);
    if (!user) return ctx.json(403, { error: 'invalid_auth' });

    /**
     * Revoke the requested session. */
    try {
      const now = Date.now();
      const key = ctx.cookies.get('session');

      user.sessions.forEach((i) => {
        if (i.key !== key) i.revokedAt = now;
      });

      await user.save();
      ctx.json(200, {});
    }
    catch (err)
    {
      ctx.json(500, { error: 'error' });
    }
  },

  /**
   * DELETE ``/i/sessions`` */
  async delete (ctx: Context)
  {
    const user = await AccessHelper.getUser(ctx);
    if (!user) return ctx.json(403, { error: 'invalid_auth' });

    /**
     * Get relevant data from the body */
    const data = await ContextHelper.json(ctx);
    const { session_id } = data;

    /**
     * Revoke the requested session. */
    try {
      await User.findOneAndUpdate({ 'sessions._id': session_id }, {
        $set: {
          'sessions.$.revokedAt': Date.now()
        }
      });

      ctx.json(200, {});
    }
    catch (err)
    {
      ctx.json(500, { error: 'error' });
    }
  }
};

export { SessionsActions };
