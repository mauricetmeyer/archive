/**
 * user.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import * as _                          from 'underscore';
import * as bcrypt                     from 'bcryptjs';

import { Context }                     from '../core';
import { AccessHelper, ContextHelper } from '../helpers';


const USER_PARAMS     = ['name', 'email'];
const USER_PARAMS_OUT = ['_id', 'createdAt', 'modifiedAt', ...USER_PARAMS];
const UserActions = {
  /**
   * GET ``/i/user`` */
  async get (ctx: Context)
  {
    let user = await AccessHelper.getUser(ctx);
    if (!user) return ctx.json(403, { error: 'invalid_auth' });

    const data       = _.pick(user, ...USER_PARAMS_OUT);
    const membership = _.pick(user.membership, 'role');
    ctx.json(200, { data: { ...data, membership } });
  },

  /**
   * POST ``/i/user`` */
  async update (ctx: Context)
  {
    const user = await AccessHelper.getUser(ctx);
    if (!user) return ctx.json(403, { error: 'invalid_auth' });

    /**
     * Get relevant data from the body */
    const data   = await ContextHelper.json(ctx);
    const params = _.pick(data, ...USER_PARAMS);
    const { password, password_old } = data;

    try {
      /**
       * We have to do some special thing
       * when the user wants to change his password. */
      if (password && password_old)
      {
        const matches = await bcrypt.compare(password_old, user.password);
        if (!matches) return ctx.json(403, { error: 'invalid_password' });
        user.password = password;
      }

      /**
       * Override user attributes with specified params.
       * and save the user. */
      _.extend(user, params);
      await user.save();

      /**
       * Return the account or `Not Found`. */
      ctx.json(200, { data: _.pick(user, { ...USER_PARAMS_OUT }) });
    }
    catch (err)
    {
      ctx.json(500, { error: 'error' });
    }
  }
};

export { UserActions };
