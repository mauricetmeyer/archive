/**
 * webhook_actions.ts
 *
 * Webhookor: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { Context }                      from '../core';
import { payments }                     from '../config';
import { PaymentHelper, ContextHelper, JobHelper } from '../helpers';
import { Event }                        from '../models/event';


const WebhooksActions = {
  /**
   * POST ``/i/webhooks/stripe`` */
  async stripe (ctx: Context)
  {
    try
    {
      /**
       * @NOTE (Maurice):
       * You can use the following code if you have
       * to replay a stripe webhook request.
       *
       * const event = await ContextHelper.json(ctx);
       *
       * Otherwise the following will verify the webhook payload
       * and fail when the signature doesn't match. */
      const { signature } = payments;
      const raw    = await ContextHelper.raw(ctx);
      const header = ctx.getHeader('stripe-signature');
      const stripe = PaymentHelper.DRIVER;
      const event  = stripe.webhooks.constructEvent(raw, header as string, signature || '');

      /**
       * We'll first make sure that the event
       * doesn't yet exists in our processed events. */
      const { id, type } = event;
      const query  = { type: 'webhooks.stripe', 'data.id': id };
      const exists = await Event.findOne(query);
      if (!!exists) return ctx.send(200);

      /**
       * User the webhook event */
      switch (type)
      {
        case 'invoice.created':
        case 'invoice.payment_failed':
        case 'invoice.payment_succeeded': {
          await JobHelper.schedule('stripe_invoice_event', {
            type:    type.split('.')[1],
            invoice: event.data.object
          });
          break;
        }

        case 'customer.source.deleted':
        case 'customer.source.expiring': {
          await JobHelper.schedule('stripe_source_event', {
            type:   type.split('.')[2],
            source: event.data.object
          });
          break;
        }

        case 'customer.subscription.updated':
        case 'customer.subscription.created':
        case 'customer.subscription.deleted':
        case 'customer.subscription.trial_will_end': {
          await JobHelper.schedule('stripe_subscription_event', {
            type:         type.split('.')[2],
            subscription: event.data.object
          });
          break;
        }
      }

      /**
       * Save the event
       * so that we won't process it a second time. */
      await Event.create({
        type: 'webhooks.stripe',
        data: { id, type }
      });

      /**
       * Send 200 as stripe expects it. */
      ctx.send(200, 'Ok');
    }
    catch (err)
    {
      ctx.send(400, 'Bad Request');
    }
  }
};

export { WebhooksActions };
