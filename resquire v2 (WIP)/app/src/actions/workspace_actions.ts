/**
 * workspace_actions.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import * as _                                         from 'underscore';

import { Context }                                    from '../core';
import { AccessHelper, ContextHelper, PaymentHelper, JobHelper } from '../helpers';
import { Account }                                    from '../models/account';


const WORKSPACE_PARAMS     = ['features', 'disabled', 'deactivated'];
const WORKSPACE_PARAMS_OUT = ['createdAt', 'modifiedAt', ...WORKSPACE_PARAMS];
const WorkspaceActions = {
  async get (ctx: Context)
  {
    try
    {
      const user = await AccessHelper.getUser(ctx);
      if (!user) return ctx.json(403, { error: 'invalid_auth' });

      /**
       * First get the workspace from the user. */
      const workspace = user.membership.workspace;
      if (!workspace)
      {
        /**
         * @TODO (Maurice):
         * This error should be logged so
         * that we can investigate it. */
        return ctx.json(500, { error: 'no_workspace' });
      }

      const base     = _.pick(workspace, ...WORKSPACE_PARAMS_OUT);
      const accounts = await Account.find({ workspace: workspace._id }, {
        handle: 1, platform: 1
      });

      ctx.json(200, { data: { ...base, accounts } });
    }
    catch (err)
    {
      console.log(err);
      ctx.json(500, { error: 'error' });
    }
  },

  async update (ctx: Context)
  {
    try
    {
      const user = await AccessHelper.getUser(ctx);
      if (!user) return ctx.json(403, { error: 'invalid_auth' });

      /**
       * First check if the user has the right
       * to access/edit the workspace. */
      if (user.membership.role !== 'admin')
        return ctx.json(403, { error: 'not_authorized' });

      /**
       * Get relevant data from the body */
      const data      = await ContextHelper.json(ctx);
      const params    = _.pick(data, ...WORKSPACE_PARAMS);
      const workspace = user.membership.workspace;
      if (!workspace)
      {
        /**
         * @TODO (Maurice):
         * This error should be logged so
         * that we can investigate it. */
        return ctx.json(500, { error: 'no_workspace' });
      }

      /**
       * Update workspace and return. */
      workspace.modifiedAt = Date.now();
      _.extend(workspace, params);

      await workspace.save();
      ctx.json(200, { data: _.pick(workspace, ...WORKSPACE_PARAMS_OUT) });
    }
    catch (err)
    {
      ctx.json(500, { error: 'error' });
    }
  },

  async remove (ctx: Context)
  {
    const user = await AccessHelper.getUser(ctx);
    if (!user) return ctx.json(403, { error: 'invalid_auth' });

    /**
     * First check if the user has the right
     * to access/edit the workspace. */
    if (user.membership.role !== 'admin')
      return ctx.json(403, { error: 'not_authorized' });

    /**
     * Get workspace associated with the user */
    const workspace = user.membership.workspace;
    if (!workspace)
    {
      /**
       * @TODO (Maurice):
       * This should be logged so
       * that we can investigate it. */

      /**
       * We don't need to return an error here
       * as the indent was to delete the workspace anyway. */
      return ctx.json(200, {});
    }

    try {
      /**
       * Delete the customer
       * on Stripe's side. */
      await PaymentHelper.deleteCustomer(workspace.customer);

      /**
       * Delete the workspace and all
       * associated users/accounts. */
      await JobHelper.schedule('workspace_purge', {
        workspaceId: workspace._id
      });

      ctx.json(200, {});
    }
    catch (err)
    {
      /**
       * @TODO (Maurice):
       * This error should be logged so
       * that we can investigate it. */
      ctx.json(500, { error: 'error' });
    }
  }
};

export { WorkspaceActions };
