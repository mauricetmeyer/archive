/**
 * account_modal.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { createElement, Component, MouseEvent, KeyboardEvent } from 'react';


import { i18n }                                                from '../i18n';
import { http }                                                from '../utils';
import { Endpoints, Platforms }                                from '../const';

import { Modal, ModalSize }                                    from './modal';
import { Input, InputSize }                                    from './input';
import { toast, ToastType }                                    from './toasts';
import { Section }                                             from './section';
import { Confirm }                                             from './confirm';


interface AccountModalProps
{
  isOpen:   boolean;

  account:  any;
  onClose:  ()             => void;
  onRemove: (account: any) => void;
  onChange: (account: any) => void;
}

interface AccountModalState
{
  handle:       string;

  isEnabled:    boolean;
  isSubmitting: boolean;
}


class AccountModal extends Component<AccountModalProps, AccountModalState>
{
  state: AccountModalState = {
    handle:       '',
    isEnabled:    false,
    isSubmitting: false,
  };


  render()
  {
    const { account, onChange, ...props } = this.props;
    const { isSubmitting }                = this.state;

    if (!account)
      return null;

    const key  = account.platform.toUpperCase();
    const info = Platforms[key];

    return (
      <Modal size={ModalSize.XSmall} {...props}>
        <Section label={i18n.messages.ACCOUNT_MANAGE as string} className="margin--bottom--xl">
          <div className="relative input--withOptions">
            <Input type="text" size={InputSize.Large} disabled={isSubmitting} defaultValue={account.handle} placeholder={i18n.messages.ACCOUNT_USERNAME.format({ platform: info.name }) as string} onKeyUp={this.handleKey.bind(this)} className="pad--right--xxxl" />
            <div className="input--options">
              <div className="center--vertical pad--topBottom--xxs">
                <Confirm text={i18n.messages.REMOVE as string} onConfirm={this.handleRemoveClick.bind(this)} />
              </div>
            </div>
          </div>
        </Section>
      </Modal>
    );
  }


  private handleKey (ev: KeyboardEvent<HTMLInputElement>)
  {
    if (ev.key === 'Enter')
    {
      ev.preventDefault();
      ev.stopPropagation();
      this.handleSubmit();
    }
    else
    {
      const handle = ev.target.value;
      void this.setState({ handle, isEnabled: handle.length > 0 });
    }
  }

  private handleSubmit ()
  {
    const { account }           = this.props;
    const { handle, isEnabled } = this.state;
    if (!isEnabled) return;

    http.post(Endpoints.ACCOUNT, { handle, account_id: account._id }).then(({ data, error }) => {
      this.setState({ isSubmitting: false });
      if (error)
      {
        toast({
          type: ToastType.Error,
          message: i18n.messages.ERROR as string
        });

        return;
      }

      this.props.onChange(data);

      toast({
        type: ToastType.Success,
        message: i18n.messages.ACCOUNT_UPDATED as string
      });
    });
  }

  private handleRemoveClick (ev: MouseEvent)
  {
    const { account } = this.props;

    ev.preventDefault();
    http.delete(Endpoints.ACCOUNT, { account_id: account._id }).then(({ error }) => {
      this.setState({ isSubmitting: false });
      if (error)
      {
        toast({
          type: ToastType.Error,
          message: i18n.messages.ERROR as string
        });

        return;
      }

      this.props.onRemove(account);
      toast({
        type: ToastType.Success,
        message: i18n.messages.ACCOUNT_REMOVED as string
      });
    });
  }
}

export { AccountModal };
