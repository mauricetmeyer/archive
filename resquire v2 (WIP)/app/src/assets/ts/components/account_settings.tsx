/**
 * account_settings.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { createElement, Fragment, Component, MouseEvent, ChangeEvent } from 'react';

import { i18n }                                                        from '../i18n';
import { http }                                                        from '../utils';
import { Endpoints }                                                   from '../const';

import { Input, InputSize }                                            from './input';
import { Section }                                                     from './section';
import { Confirm }                                                     from './confirm';
import { toast, ToastType } from './toasts';


interface AccountSettingsProps
{
  user:                 any;

  onUserChange:         (user: any) => void;
  onPasswordClick:      () => void;
  onSubscriptionClick:  () => void;
  onDeleteAccountClick: () => void;
}

interface AccountSettingsState
{
  name: string;
}

class AccountSettings extends Component<AccountSettingsProps, AccountSettingsState>
{
  state = {
    name: this.props.user.name
  };


  render ()
  {
    const { user } = this.props;
    const { name } = this.state;

    const isAdmin  = user.membership.role === 'admin';

    return (
      <Fragment>
        <Section label={i18n.messages.SETTINGS_PROFILE as string} className="margin--bottom--xxl">
          <div className="relative input--withOptions margin--bottom--xxs">
            <Input
              type="string"
              size={InputSize.Large} value={name}
              placeholder={i18n.messages.NAME as string}
              onChange={this.handleNameChange.bind(this)} />
            <div className="input--options">
              { user.name !== name &&
                <div className="center--vertical pad--topBottom--xxs">
                  <a
                    href="#"
                    className="c--blue fontSize--m"
                    onClick={this.handleNameSaveClick.bind(this)}>
                    {i18n.messages.SAVE}
                  </a>
                </div>
              }
            </div>
          </div>
          <div className="input input--l disabled">
            <span>{user.email}</span>
            <a
              tabIndex={-1}
              style={{ lineHeight: '18px' }}
              className="right fontSize--m c--blue pointer"
              onClick={this.handlePasswordClick.bind(this)}>
              {i18n.messages.CHANGE_PASSWORD}
            </a>
          </div>
        </Section>
        { isAdmin &&
          <Section label={i18n.messages.SETTINGS_ADMIN as string}>
            <div
              className="input input--l pointer margin--bottom--xxs"
              onClick={this.handleBillingClick.bind(this)}>
              <span>{i18n.messages.BILLING_MANAGE}</span>
            </div>
            <div className="input input--l">
              <Confirm
                text={i18n.messages.DELETE_ACCOUNT as string}
                onConfirm={this.handleDeleteAccount.bind(this)}
                className="fontSize--m"  style={{ lineHeight: '18px' }} />
            </div>
          </Section>
        }
      </Fragment>
    );
  }


  /**
   * **INTERNAL**
   * Handle name save click. */
  private handleNameChange (ev: ChangeEvent<HTMLInputElement>)
  {
    const { value } = ev.target;
    this.setState({ name: value });
  }

  /**
   * **INTERNAL**
   * Handle name save click. */
  private handleNameSaveClick (ev: MouseEvent)
  {
    ev.preventDefault();
    const { user } = this.props;
    const { name } = this.state;
    http.post(Endpoints.USER, { name }).then(({ error }) => {
      if (error)
      {
        toast({
          type: ToastType.Error,
          message: i18n.messages.ERROR as string
        });
        return;
      }
      const newUser = { ...user, name };
      this.props.onUserChange(newUser);

      toast({
        type: ToastType.Success,
        message: i18n.messages.NAME_CHANGED as string
      });
    });
  }


  /**
   * **INTERNAL**
   * Handle password field click. */
  private handleBillingClick (ev: MouseEvent)
  {
    ev.preventDefault();
    this.props.onSubscriptionClick();
  }

  /**
   * **INTERNAL**
   * Handle password field click. */
  private handlePasswordClick (ev: MouseEvent)
  {
    ev.preventDefault();
    this.props.onPasswordClick();
  }

  /**
   * **INTERNAL**
   * Handle account deletion. */
  private handleDeleteAccount (ev: MouseEvent)
  {
    ev.preventDefault();
    this.props.onDeleteAccountClick();
  }
}

export { AccountSettings };
