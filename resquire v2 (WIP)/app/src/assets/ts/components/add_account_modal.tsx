/**
 * add_account_modal.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { createElement, Component, MouseEvent, KeyboardEvent } from 'react';


import { i18n }                                                from '../i18n';
import { http }                                                from '../utils';
import { Endpoints, Platforms }                                from '../const';

import { Modal, ModalSize }                                    from './modal';
import { Input, InputSize }                                    from './input';
import { Button, ButtonSize, ButtonColor }                     from './button';
import { toast, ToastType }                                    from './toasts';
import { Section }                                             from './section';
import { Platform }                                            from './platform';


const RoleMap: { [key: string]: string } = {
  admin:  i18n.messages.OWNER as string,
  member: i18n.messages.MEMBER as string
};


interface AddAccountModalProps
{
  isOpen:         boolean;

  onClose:        () => void;
  onAccountAdded: (account: any) => void;
}

interface AddAccountModalState
{
  handle:            string;
  platform:          string;

  isError:           boolean;
  isEnabled:         boolean;
  isSubmitting:      boolean;
  isShowingDropdown: boolean;
}


class AddAccountModal extends Component<AddAccountModalProps, AddAccountModalState>
{
  state: AddAccountModalState = {
    handle:            '',
    platform:          'instagram',

    isError:           false,
    isEnabled:         false,
    isSubmitting:      false,
    isShowingDropdown: false
  };


  render()
  {
    const { platform, isSubmitting, isShowingDropdown } = this.state;

    const key  = platform.toUpperCase();
    const info = Platforms[key];

    return (
      <Modal size={ModalSize.XSmall} {...this.props}>
        <Section label={i18n.messages.ACCOUNTS_CONNECT as string} className="margin--bottom--xl">
          <div className="relative input--withOptions">
            <Input type="text" size={InputSize.Large} disabled={isSubmitting} placeholder={i18n.messages.ACCOUNT_USERNAME.format({ platform: info.name }) as string} onKeyUp={this.handleKey.bind(this)} className="pad--right--xxxl" />
            <div className="input--options">
              <div className="center--vertical pad--topBottom--xxs">
                <a href="#" onClick={this.handleDropdownClick.bind(this)} className="c--blue fontSize--m">
                  <span style={{ color: info.color }}>{info.name}</span>
                </a>
              </div>
              <Platform
                isOpen={isShowingDropdown}
                platform={platform}
                onPlatformClick={this.handlePlatformChange.bind(this)} />
            </div>
          </div>
        </Section>
      </Modal>
    );
  }


  private handleKey (ev: KeyboardEvent<HTMLInputElement>)
  {
    if (ev.key === 'Enter')
    {
      ev.preventDefault();
      ev.stopPropagation();
      this.handleSubmit();
    }
    else
    {
      const handle = ev.target.value;
      void this.setState({ handle, isEnabled: handle.length > 0 });
    }
  }

  private handleSubmit ()
  {
    const { handle, platform, isEnabled } = this.state;
    if (!isEnabled) return;

    this.setState({ isSubmitting: true });
    http.put(Endpoints.ACCOUNT, { handle, platform }).then(({ data, error }) => {
      this.setState({ isSubmitting: false });
      if (error)
      {
        toast({
          type: ToastType.Error,
          message: i18n.messages.ERROR as string
        });

        return;
      }

      this.props.onAccountAdded(data);

      toast({
        type: ToastType.Success,
        message: i18n.messages.ACCOUNTS_ADD_SUCCESS as string
      });
    });
  }

  private handlePlatformChange (platform: string, ev: MouseEvent)
  {
    ev.preventDefault();
    this.setState({ platform });
  }

  private handleDropdownClick (ev: MouseEvent)
  {
    ev.preventDefault();
    this.setState(prev => ({ isShowingDropdown: !prev.isShowingDropdown }));
  }
}

export { AddAccountModal };
