/**
 * avatar.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { createElement, Component } from 'react';
import * as classnames              from 'classnames';
import * as AVATAR_DEFAULT          from '../../img/avatar.png';

enum AvatarSize
{
  XSmall = 'avatar--xs',
  Small  = 'avatar--s',
  Medium = 'avatar--m',
  Large  = 'avatar--l',
}

interface AvatarProps
{
  alt:        string;
  src?:       string;
  size?:      AvatarSize;

  className?: string;
}

class Avatar extends Component<AvatarProps>
{
  static defaultProps = {
    size: AvatarSize.Small
  };


  render()
  {
    const { src, alt, size, className } = this.props;

    return (
      <div className={classnames('avatar', size, className)}>
        <img src={src || AVATAR_DEFAULT} alt={alt} />
      </div>
    );
  }
}

export { Avatar, AvatarSize };
