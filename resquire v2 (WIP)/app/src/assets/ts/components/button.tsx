/**
 * button.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { createElement, Component, MouseEvent } from 'react';
import * as classnames                          from 'classnames';


enum ButtonSize
{
  Small  = 'btn--s',
  Medium = 'btn--m',
  Large  = 'btn--l',
  XLarge = 'btn--xl',
}

enum ButtonColor
{
  Primary   = 'primary',
  Secondary = 'secondary',
  Clear     = 'clear'
}

interface ButtonProps
{
  type?:      'submit' | 'reset' | 'submit';

  size:       ButtonSize;
  color:      ButtonColor;
  onClick:    (ev: MouseEvent) => void;

  href?:      string;
  disabled?:  boolean;
  className?: string;
}


class Button extends Component<ButtonProps>
{
  constructor (props: ButtonProps)
  {
    super(props);
  }


  render()
  {
    const {
      type, size, color, className,
      children, disabled, onClick
    } = this.props;

    return (
      <button type={type} className={classnames('btn', size, color, className)} onClick={onClick} disabled={disabled}>
        {children}
      </button>
    );
  }
}

export { Button, ButtonSize, ButtonColor };
