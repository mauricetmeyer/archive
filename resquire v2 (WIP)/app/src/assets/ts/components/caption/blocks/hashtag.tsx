/**
 * hashtag.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { createElement, ReactElement } from 'react';
import * as classnames                 from 'classnames';

interface HashtagProps
{
  rating:  string;
  hashtag: string;
  children: any;
}

const Hashtag: (props: HashtagProps) => ReactElement = ({ rating, children }) =>
  <a href="#" className={classnames('hashtag', rating, 'good')}>{children}</a>;

export { Hashtag };
