/**
 * index.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { CompositeDecorator } from 'draft-js';
import { Hashtag, Mention }   from '../blocks';
import { TextUtils } from './utils';


const Decorators = new CompositeDecorator([
  {
    /**
     * Hashtags */
    component: Hashtag,
    strategy: (block, callback) => {
      const text    = block.getText();
      const results = TextUtils.hashtagIndices(text);

      results.forEach(index => {
        callback(index.indices[0], index.indices[1]);
      });
    }
  },
  {
    /**
     * Mentions */
    component: Mention,
    strategy: (block, callback) => {
      const text    = block.getText();
      const results = TextUtils.mentionIndices(text);

      results.forEach(index => {
        callback(index.indices[0], index.indices[1]);
      });
    }
  }
]);

export { Decorators };

