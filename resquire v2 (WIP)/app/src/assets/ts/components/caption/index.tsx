/**
 * index.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { createElement, Component }                   from 'react';
import * as classnames                                from 'classnames';
import { Editor, EditorState }                        from 'draft-js';
import { Decorators } from './decorators';


interface CaptionProps
{
  value?:       string;
  placeholder?: string;

  className?:   string;

  readonly?:    boolean;

  onChange:     (val: string) => void;
}

interface CaptionState
{
  value:       string;
  editorState: EditorState;
}


const STATE = EditorState.createEmpty();
class Caption extends Component<CaptionProps, CaptionState>
{
  state = {
    value:       '',
    editorState: EditorState.set(STATE, { decorator: Decorators })
  };


  /**
   * **INTERNAL**
   * Renders the editor
   *
   * @return {}
   */
  render()
  {
    const { editorState }                      = this.state;
    const { readonly, className, placeholder } = this.props;

    return (
      <div className={classnames('caption', className)}>
        <Editor stripPastedStyles readOnly={readonly} placeholder={placeholder} editorState={editorState} onChange={this.handleChange.bind(this)} />
        {this.renderSuggestions()}
      </div>
    );
  }

  /**
   * **INTERNAL**
   * Render suggestions picker
   * when it's active and needed.
   *
   * @return {ReactElement?}
   */
  private renderSuggestions ()
  {
    return null;
  }


  private handleChange (editorState: EditorState)
  {
    const value = editorState.getCurrentContent().getPlainText();
    this.setState({ editorState, value });
  }
}

export { Caption };
