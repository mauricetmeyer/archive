/**
 * input.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { createElement, Component, MouseEvent } from 'react';
import * as classnames                          from 'classnames';
import { i18n }                                 from '../i18n';


interface ConfirmProps
{
  text:       string;
  style?:     any;
  className?: string;

  onConfirm:  (ev: MouseEvent) => void;
}

interface ConfirmState
{
  isPreClicked: boolean;
}


class Confirm extends Component<ConfirmProps, ConfirmState>
{
  state = {
    isPreClicked: false
  };


  /**
   * **INTERNAL**
   * Renders the input element. */
  render()
  {
    const { text, className, onConfirm, ...props } = this.props;
    const { isPreClicked }                         = this.state;
    const content                                  = isPreClicked ? i18n.messages.SURE : text;
    const classes                                  = classnames({
      'c--red': !isPreClicked,
      'c--light': isPreClicked
    }, className);

    return (
      <a
        {...props}
        role="button"
        className={classes}
        onBlur={this.handleBlur.bind(this)}
        onClick={this.handleClick.bind(this)}>
        {content}
      </a>
    );
  }


  /**
   * Handle blur event. */
  private handleBlur ()
  {
    this.setState({ isPreClicked: false });
  }

  private handleClick (ev: MouseEvent)
  {
    ev.preventDefault();
    const { onConfirm }    = this.props;
    const { isPreClicked } = this.state;
    if (isPreClicked) onConfirm(ev);
    else this.setState({ isPreClicked: true });
  }
}

export { Confirm };
