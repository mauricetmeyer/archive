/**
 * datepicker.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { createElement, Component, MouseEvent } from 'react';
import * as moment                              from 'moment';

import { i18n }                                 from '../../i18n';

import { DatePickerMonth }                      from './datepicker_month';


const getMonths = (value: moment.Moment, amount: number) =>
{
  const month = value.clone();
  month.subtract(1, 'month');

  const months = [];
  for (let i = 0; i < amount; i++)
  {
    const curr = month.clone();
    month.add(1, 'month');
    months.push(curr);
  }

  return months;
};


enum DatePickerTransition
{
  NONE,
  PREV,
  NEXT
}


interface DatePickerProps
{
  value:    moment.Moment;
  onChange: (datetime: moment.Moment) => void;
}

interface DatePickerState
{
  month:      moment.Moment;
  months:     moment.Moment[];

  transition: DatePickerTransition;
}


class DatePicker extends Component<DatePickerProps, DatePickerState>
{
  state = {
    month:      this.props.value,
    months:     getMonths(this.props.value, 3),
    transition: DatePickerTransition.NONE
  };


  render ()
  {
    const { value }              = this.props;
    const { months, transition } = this.state;
    const days                   = this.getWeekDays();

    const isAnimating = transition !== DatePickerTransition.NONE;
    const isPrevAnim  = transition === DatePickerTransition.PREV;
    const isNextAnim  = transition === DatePickerTransition.NEXT;

    return (
      <div className="pad--xs">
        <div className="datepicker">
          <div className="datepicker--nav">
            <a href="#" onClick={this.handlePrevClick.bind(this)} className="arrowPrev">
              <i className="icon prev" />
            </a>
            <a href="#" onClick={this.handleNextClick.bind(this)} className="arrowNext">
              <i className="icon next" />
            </a>
          </div>
          <div className="datepicker--header">
            <ul>{ days.map((d, i) => <li key={i}>{d}</li>)}</ul>
          </div>
          <div className="datepicker--container">
            <div className="datepicker--container--wrap">
              { months.map((m, i) => {
                const isPrev   = isNextAnim && i === 1;
                const isActive = (!isAnimating && i === 1)
                              || isPrevAnim && i === 0
                              || isNextAnim && i === 2;

                return (
                  <DatePickerMonth
                    key={i}
                    month={m}
                    value={value}
                    prev={isPrev || !isAnimating && i === 0}
                    active={isActive}
                    animating={isAnimating && !(isNextAnim && i === 0)}
                    onDayClick={this.handleDayClick.bind(this)} />
                );
              })}
            </div>
          </div>
        </div>
      </div>
    );
  }


  private handleDayClick (day: moment.Moment)
  {
    const { value } = this.props;
    const newValue = day.clone().set({
      hours: value.hours(),
      minutes: value.minutes()
    });

    this.props.onChange(newValue);
  }

  private handlePrevClick (ev: MouseEvent)
  {
    ev.preventDefault();
    this.setState({ transition: DatePickerTransition.PREV });
    setTimeout(() => this.handleTransition(), 200);
  }

  private handleNextClick (ev: MouseEvent)
  {
    ev.preventDefault();
    this.setState({ transition: DatePickerTransition.NEXT });
    setTimeout(() => this.handleTransition(), 200);
  }

  private handleTransition ()
  {
    const { month, transition } = this.state;
    if (transition === DatePickerTransition.NONE) return;

    const newMonth = month.clone();
    switch (transition)
    {
      case DatePickerTransition.PREV:
        newMonth.subtract(1, 'month');
        break;

      case DatePickerTransition.NEXT:
        newMonth.add(1, 'month');
        break;
    }

    this.setState({
      transition: DatePickerTransition.NONE,
      months:     getMonths(newMonth, 3),
      month:      newMonth
    });
  }


  private getWeekDays ()
  {
    return [
      i18n.messages.DAYS_SHORT_SUN,
      i18n.messages.DAYS_SHORT_MON,
      i18n.messages.DAYS_SHORT_TUE,
      i18n.messages.DAYS_SHORT_WED,
      i18n.messages.DAYS_SHORT_THU,
      i18n.messages.DAYS_SHORT_FRI,
      i18n.messages.DAYS_SHORT_SAT,
    ];
  }
}

export { DatePicker };
