/**
 * datepicker_day.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { createElement, Component } from 'react';
import * as moment                  from 'moment';
import * as classnames              from 'classnames';


interface DatePickerDayProps
{
  day?:      moment.Moment;

  outside?:  boolean;
  selected?: boolean;

  onClick:   (day: moment.Moment) => void;
}


class DatePickerDay extends Component<DatePickerDayProps>
{
  render ()
  {
    const { day, outside, selected, onClick } = this.props;
    return (
      <td onClick={_ => day && onClick(day)} className={classnames('datepicker--day borderRadius--m', { outside, selected })}>
        <span>{day && day.format('D')}</span>
      </td>
    );
  }
}

export { DatePickerDay };
