/**
 * datepicker_month.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { createElement, Component } from 'react';
import * as moment                  from 'moment';
import * as classnames              from 'classnames';

import { DatePickerDay }            from './datepicker_day';
import { DatePickerWeek }           from './datepicker_week';


const isSameDay = (lhs: moment.Moment, rhs: moment.Moment) =>
     lhs.date() === rhs.date()
  && lhs.month() === rhs.month()
  && lhs.year() === rhs.year();


interface DatePickerMonthProps
{
  prev:       boolean;
  active:     boolean;
  animating:  boolean;

  value:      moment.Moment;
  month:      moment.Moment;

  onDayClick: (day: moment.Moment) => void;
}

interface DatePickerMonthState
{
  weeks?: Array<moment.Moment[]>;
}


class DatePickerMonth extends Component<DatePickerMonthProps, DatePickerMonthState>
{
  render ()
  {
    const { value, month, onDayClick, ...props } = this.props;
    const weeks = this.getMonthWeeks();

    return (
      <div className={classnames('datepicker--month', props)}>
        <div className="datepicker--month--title textAlign--m">
          {month.format('MMMM YYYY')}
        </div>
        <table>
          <tbody>
            { weeks.map((w, i) => (
              <DatePickerWeek key={i}>
                { w.map((d, i) =>
                  <DatePickerDay
                    key={i}
                    day={d || undefined}
                    outside={!d}
                    selected={(!!d && isSameDay(d, value))}
                    onClick={onDayClick} />
                )}
              </DatePickerWeek>
            ))}
          </tbody>
        </table>
      </div>
    );
  }


  private getMonthWeeks ()
  {
    const { month }    = this.props;
    const firstOfMonth = month.clone().startOf('month').hour(12);
    const lastOfMonth  = month.clone().endOf('month').hour(12);
    const total        = lastOfMonth.diff(firstOfMonth, 'days') + 1;

    const first     = firstOfMonth.day();
    const currDay   = firstOfMonth.clone();
    let   currWeek  = [];
    const currMonth = [];

    /**
     * Days of previous months */
    for (let i = 0; i < first; i++)
      currWeek.push(null);

    /**
     * Current month */
    let   curr = first;
    const last = curr + total;
    while (curr < last)
    {
      currWeek.push(currDay.clone());
      currDay.add(1, 'day');
      curr++;

      /**
       * Week is done. */
      if (currWeek.length === 7)
      {
        currMonth.push(currWeek);
        currWeek = [];
      }
    }

    /**
     * Days belonging to the next month */
    if (!!currWeek.length)
    {
      let remaining = 7 - currWeek.length;
      while (--remaining >= 0)
        currWeek.push(null);
      currMonth.push(currWeek);
    }

    return currMonth;
  }
}

export { DatePickerMonth };
