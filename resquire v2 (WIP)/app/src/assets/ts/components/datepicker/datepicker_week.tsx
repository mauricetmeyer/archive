/**
 * datepicker_week.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { createElement, ReactElement, PropsWithChildren } from 'react';


type DatePickerWeekProps = PropsWithChildren<{}>;
const DatePickerWeek: (_: DatePickerWeekProps) => ReactElement = ({ children }) =>
  <tr className="">{children}</tr>;

export { DatePickerWeek };
