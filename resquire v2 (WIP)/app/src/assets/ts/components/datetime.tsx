/**
 * datetime.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { createElement, Component }                        from 'react';
import * as moment                                         from 'moment';

import { i18n }                                            from '../i18n';

import { Popover, PopoverOriginV }                         from './popover';
import { DatePicker }                                      from './datepicker';
import { TimePicker }                                      from './timepicker';


interface DateTimeProps
{
  value?:       moment.Moment;
  placeholder?: string;
  className?:   string;

  onChange:     (datetime: moment.Moment) => void;
}

interface DateTimeState
{
  isTimePickerOpen: boolean;
  isDatePickerOpen: boolean;
}


class DateTime extends Component<DateTimeProps, DateTimeState>
{
  state = {
    isTimePickerOpen: false,
    isDatePickerOpen: false
  };


  render()
  {
    const { value = moment() }                   = this.props;
    const { isDatePickerOpen, isTimePickerOpen } = this.state;

    return (
      <div className="cf">
        <div className="col--6 relative">
          <div onClick={this.handleDateClick.bind(this)} className="fontWeight--500">
            {this.renderDate(value)}
          </div>
          <Popover
            open={isDatePickerOpen}
            onBlur={this.handleDateBlur.bind(this)}
            origin={{ vertical: PopoverOriginV.Bottom }}>
            <DatePicker value={value} onChange={this.handleDateTimeChange.bind(this)} />
          </Popover>
        </div>
        <div className="col--6 fontWeight--500 relative">
          <div onClick={this.handleTimeClick.bind(this)} className="fontWeight--500">
            {this.renderTime(value)}
          </div>
          <Popover
            open={isTimePickerOpen}
            onBlur={this.handleTimeBlur.bind(this)}
            origin={{ vertical: PopoverOriginV.Bottom }} full>
            <TimePicker value={value} onChange={this.handleDateTimeChange.bind(this)} />
          </Popover>
        </div>
      </div>
    );
  }


  private renderDate (datetime: moment.Moment)
  {
    const date      = datetime.clone();
    const date_now  = moment().startOf('day');
    const date_diff = date.startOf('day').diff(date_now, 'days', true);
    const date_key  = this.getRelativeFormat(date_diff);
    const date_fmt  = date_key ? i18n.messages[date_key]
                    : datetime.format('dddd, Do MMMM');

    return (<span>{date_fmt}</span>);
  }

  private renderTime (datetime: moment.Moment)
  {
    const fmt_time = datetime.format('hh:mm a');
    return (<span className="pad--left--s">{fmt_time}</span>);
  }


  private getRelativeFormat (diff: number)
  {
    if (diff <= -1 || diff >= 2) return;

    if (diff < 0)  return 'DATE_YESTERDAY';
    if (diff < 1)  return 'DATE_TODAY';
    if (diff < 2)  return 'DATE_TOMORROW';
  }


  private handleDateBlur ()
  {
    this.setState({ isDatePickerOpen: false });
  }

  private handleDateClick ()
  {
    this.setState({ isDatePickerOpen: true, isTimePickerOpen: false });
  }

  private handleTimeBlur ()
  {
    this.setState({ isTimePickerOpen: false });
  }

  private handleTimeClick ()
  {
    this.setState({ isDatePickerOpen: false, isTimePickerOpen: true });
  }

  private handleDateTimeChange (datetime: moment.Moment)
  {
    this.props.onChange(datetime);
  }
}

export { DateTime };
