/**
 * graph.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { createElement, Component, FocusEvent, ChangeEvent, KeyboardEvent } from 'react';
import * as classnames                                                      from 'classnames';
import { isNullOrUndefined } from 'util';


const GRAPH_HEIGHT          = 120;
const GRAPH_SEPERATOR_WIDTH = 2;
const GRAPH_SEPERATOR_COLOR = '#d5d8dd';


interface GraphProps
{
  colors: string[];
  values: number[][];
}

interface GraphState
{
  width:     number;
  isRunning: boolean;
}


class Graph extends Component<GraphProps, GraphState>
{
  state = {
    width:     0,
    isRunning: true
  };


  private el?:     HTMLDivElement;
  private ctx?:    CanvasRenderingContext2D;
  private canvas?: HTMLCanvasElement;


  componentDidMount ()
  {
    if (!this.el) return;
    this.setState({ width: this.el.offsetWidth });

    const call = () => {
      const { isRunning } = this.state;
      if (!isRunning) return;

      this.draw();
      window.requestAnimationFrame(call);
    };

    call();
  }

  componentDidUpdate (_: GraphProps, state: GraphState)
  {
    const { width } = this.state;
    if (state.width === width) return;
    this.handleResize();
  }


  /**
   * **INTERNAL**
   * Renders the input element. */
  render()
  {
    return (
      <div ref={r => r && (this.el = r)} className="graph">
        <canvas ref={r => {
          if (!r) return;
          this.canvas = r;
          this.ctx    = r.getContext('2d') || undefined;
        }} />
      </div>
    );
  }


  draw ()
  {
    const { width }  = this.state;
    const { values } = this.props;

    if (width <= 0)
      return;

    /**
     * Normalize values */
    const max = [0];
    for (let i = 0; i < values.length; i++)
    {
      const value = values[0];
      if (max[0] < value[0]) max[0] = value[0];
    }

    const height           = GRAPH_HEIGHT - 2;
    const maxValues        = max.map(mv => height / mv);
    const normalizedValues = values.map(v => {
      return v.map((rv, i) => Math.round(maxValues[i] * rv));
    });

    const lineWidth  = width - (GRAPH_SEPERATOR_WIDTH * 5);
    const lineOffset = lineWidth / (values.length - 1);

    normalizedValues.forEach((v, i) => {
      const offset = Math.round(i * lineOffset);
      this.drawSeperator(offset);
      this.drawValue(offset, v);
    });
  }

  drawValue (x: number, v: number[])
  {
    /**
     * Early return in case that
     * we have no context. */
    if (!this.ctx) return;

    const { ctx }    = this;
    const { colors } = this.props;

    const i       = 0;
    const val     = v[i];
    const yCenter = GRAPH_HEIGHT / 2;
    const yStart  = yCenter - val;
    const yEnd    = yCenter + val;

    ctx.beginPath();
    ctx.lineCap     = 'round';
    ctx.lineWidth   = 2;
    ctx.strokeStyle = colors[i];
    ctx.moveTo(x + 1, yStart);
    ctx.lineTo(x + 1, yEnd);
    ctx.stroke();
  }

  drawSeperator (x: number)
  {
    /**
     * Early return in case that
     * we have no context. */
    if (!this.ctx) return;

    const { ctx } = this;

    ctx.beginPath();
    ctx.lineCap     = 'round';
    ctx.lineWidth   = 2;
    ctx.strokeStyle = GRAPH_SEPERATOR_COLOR;
    ctx.moveTo(x + 1, 1);
    ctx.lineTo(x + 1, GRAPH_HEIGHT - 1);
    ctx.stroke();
  }


  private handleResize ()
  {
    const { width } = this.state;
    if (this.canvas)
    {
      this.canvas.width  = width;
      this.canvas.height = GRAPH_HEIGHT;
    }
  }
}

export { Graph };
