/**
 * input.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { createElement, Component, FocusEvent, ChangeEvent, KeyboardEvent } from 'react';
import * as classnames                                                      from 'classnames';


enum InputSize
{
  Small  = 'input--s',
  Medium = 'input--m',
  Large  = 'input--l',
  XLarge = 'input--xl',
}

interface InputProps
{
  size:         InputSize;
  disabled?:    boolean;

  type:         string;
  name?:        string;
  value?:       string;
  className?:   string;
  placeholder?: string;

  onBlur?:      (ev: FocusEvent)                    => void;
  onFocus?:     (ev: FocusEvent)                    => void;
  onError?:     (ev: ChangeEvent<HTMLInputElement>) => void;
  onChange?:    (ev: ChangeEvent<HTMLInputElement>) => void;

  onKeyUp?:     (ev: KeyboardEvent)                 => void;
  onKeyDown?:   (ev: KeyboardEvent)                 => void;

  validate?:    (val: string)     => boolean;
}

interface InputState
{
  isValid: boolean;
}


class Input extends Component<InputProps, InputState>
{
  state = {
    isValid: true,
    isDirty: false
  };


  /**
   * **INTERNAL**
   * Renders the input element. */
  render()
  {
    const {
      className,
      type, size,
      onChange, validate, ...props
    } = this.props;

    return (
      <input className={classnames('input', size, className)}
        {...props}
        type={type}
        name={name}
        onChange={validate ? this.handleChange : onChange} />
    );
  }


  private handleChange (ev: ChangeEvent)
  {
    const { validate, onChange } = this.props;
  }
}

export { Input, InputSize };
