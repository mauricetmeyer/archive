/**
 * measure.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { createElement, Component, ReactElement } from 'react';
import { is, synced }                             from '../utils';


type MeasureFn = (w: number, h: number) => ReactElement;
interface MeasureProps
{
  watch?:   boolean;
  children: ReactElement | MeasureFn;
  onChange: (w: number, h: number) => void;
}

interface MeasureState
{
  width:  number;
  height: number;
}


class Measure extends Component<MeasureProps, MeasureState>
{
  private container?: HTMLDivElement;


  state = {
    width:  0,
    height: 0
  };


  componentDidMount ()
  {
    this.handleResize();
    const { watch } = this.props;
    if (watch) document.addEventListener('resize', this.handleResize);
  }

  componentWillUnmount ()
  {
    const { watch } = this.props;
    if (watch) document.removeEventListener('resize', this.handleResize);
  }


  render ()
  {
    const { children }      = this.props;
    const { width, height } = this.state;

    return (
      <div ref={r => r && (this.container = r)}>
        { is.function(children) ? children(width, height) : children }
      </div>
    );
  }


  private handleResize = synced(() => {
    const { container } = this;
    if (!container) return;

    const width  = container.offsetWidth;
    const height = container.offsetHeight;
    this.setState({ width, height });
    this.props.onChange(width, height);
  });
}

export { Measure };
