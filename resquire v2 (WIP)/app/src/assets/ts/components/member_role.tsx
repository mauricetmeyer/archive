/**
 * members_modal.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { createElement, Component, MouseEvent, Fragment }    from 'react';
import * as classnames                             from 'classnames';

import { i18n }                                    from '../i18n';

import { Popover, PopoverOriginV, PopoverOriginH } from './popover';
import { Confirm }                                 from './confirm';


const RoleMap: { [key: string]: string } = {
  admin:  i18n.messages.OWNER as string,
  member: i18n.messages.MEMBER as string
};

const RoleDescription: { [key: string]: string } = {
  admin:  i18n.messages.OWNER_CAN as string,
  member: i18n.messages.MEMBER_CAN as string
};


interface MemberRoleProps
{
  role:           string;
  isNew?:         boolean;
  isOpen?:        boolean;

  onBlur?:        () => void;
  onRoleClick:    (role: string, ev: MouseEvent) => void;
  onRemoveClick?: (ev: MouseEvent) => void;
}

class MemberRole extends Component<MemberRoleProps>
{
  render()
  {
    const { role, isNew, isOpen, onBlur } = this.props;

    return (
      <div className="width--100 relative">
        <Popover open={isOpen} onBlur={onBlur} origin={{ vertical: PopoverOriginV.Bottom, horizontal: PopoverOriginH.Right }}>
          <ul className="pad--xs">
            { ['admin', 'member'].map(i =>
              <li key={i} className={classnames('pad--topBottom--xxs pad--leftRight--xs', { active: i === role })} onClick={this.handleRoleClick.bind(this, i)}>
                <h3 className={classnames('fontSize--m', { 'c--blue': i === role })}>
                  {RoleMap[i]}
                </h3>
                <p className="fontSize--s">
                  {RoleDescription[i]}
                </p>
              </li>
            )}
            { !isNew &&
              <Fragment>
                <li className="popover--seperator" />
                <li className="pad--topBottom--xxs pad--leftRight--xs">
                  <Confirm text={i18n.messages.REMOVE} onConfirm={this.handleRemoveClick.bind(this)} className="fontSize--m" />
                </li>
              </Fragment>
            }
          </ul>
        </Popover>
      </div>
    );
  }


  handleRoleClick (role: string, ev: MouseEvent)
  {
    ev.preventDefault();
    this.props.onRoleClick && this.props.onRoleClick(role, ev);
  }

  handleRemoveClick (ev: MouseEvent)
  {
    ev.preventDefault();
    this.props.onRemoveClick && this.props.onRemoveClick(ev);
  }
}

export { MemberRole };
