/**
 * members.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { Component } from 'react';
import { http }      from '../utils';
import { Endpoints } from '../const';


interface IMember
{
  _id:   string;
  role:  string;
  name:  string;
  email: string;
}

interface MembersProps
{
  onLoad: (user: IMember[]) => void;
}

interface MembersState
{
  members:   IMember[];

  isLoaded:  boolean;
  isErrored: boolean;
}


class Members extends Component<MembersProps, MembersState>
{
  componentDidMount ()
  {
    http.get(Endpoints.MEMBERS).then(({ data, error }) => {
      if (error) return this.setState({ isErrored: true });
      this.setState({ members: data, isLoaded: true });
      this.props.onLoad(data);
    });
  }

  render ()
  {
    return null;
  }
}

export { Members, IMember };
