/**
 * members_modal.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { createElement, Component, Fragment, MouseEvent, KeyboardEvent } from 'react';


import { i18n }                                           from '../i18n';
import { http }                                           from '../utils';
import { Endpoints }                                      from '../const';

import { Modal, ModalSize }                               from './modal';
import { Input, InputSize }                               from './input';
import { Section }                                        from './section';
import { Members, IMember }                               from './members';
import { MemberRole }                                     from './member_role';
import { toast, ToastType } from './toasts';


const RoleMap: { [key: string]: string } = {
  admin:  i18n.messages.OWNER as string,
  member: i18n.messages.MEMBER as string
};


interface MembersModalProps
{
  user:    any;
  isOpen?: boolean;
  onClose: () => void;
}

interface MembersModalState
{
  members:           IMember[];
  expanded?:         any;

  role:              string;
  email:             string;

  isLoaded:          boolean;
  isInviteEnabled:   boolean;
  isShowingDropdown: boolean;
}


class MembersModal extends Component<MembersModalProps, MembersModalState>
{
  state: MembersModalState = {
    role:              'member',
    email:             '',
    members:           [],
    isLoaded:          false,
    isInviteEnabled:   false,
    isShowingDropdown: false
  };


  render()
  {
    const { user, ...props } = this.props;
    const { isLoaded }       = this.state;

    return (
      <Modal size={ModalSize.XSmall} {...props}>
        <Members onLoad={this.handleMembersLoad.bind(this)} />
        { isLoaded ? this.renderSections() : <div className="fontSize--m textAlign--m">{i18n.messages.LOADING}</div> }
      </Modal>
    );
  }

  /**
   * **INTERNAL**
   * Render the member modal sections.
   *
   * @return {React.Element}
   */
  private renderSections ()
  {
    const { role, members, isShowingDropdown } = this.state;
    return (
      <Fragment>
        <Section label={i18n.messages.INVITE_MEMBERS as string} className="margin--bottom--xl">
          <div className="relative input--withOptions">
            <Input type="text" size={InputSize.Large} placeholder={i18n.messages.INVITE_MEMBER_ADDRESS as string} onKeyUp={this.handleInviteKey.bind(this)} className="pad--right--xxxl" />
            <div className="input--options">
              <div className="center--vertical pad--topBottom--xxs">
                <a href="#" onClick={this.handleInviteDropdownClick.bind(this)} className="c--blue fontSize--m">
                  {RoleMap[role]}
                </a>
              </div>
              <MemberRole
                isNew
                isOpen={isShowingDropdown}
                role={role}
                onBlur={this.handleInviteRoleBlur.bind(this)}
                onRoleClick={this.handleInviteRoleClick.bind(this)} />
            </div>
          </div>
        </Section>
        <Section label={i18n.messages.MEMBERS as string} className="margin--bottom--l">
          { members.map((m, i) => this.renderMemberEntry(m, i)) }
        </Section>
      </Fragment>
    );
  }

  /**
   * **INTERNAL**
   * Render the member entry.
   * 
   * @param  {object} member
   * @return {React.Element}
   */
  private renderMemberEntry (member: any, index: number)
  {
    const { user }     = this.props;
    const { expanded } = this.state;
    const isOpen = expanded && member._id === expanded._id;
    return (
      <div className="pad--topBottom--xxs inline" key={index}>
        <div className="inline--grow">
          <h3 className="fontSize--m">{member.name ? member.name : i18n.messages.INVITED}</h3>
          <p className="fontSize--s">{member.email}</p>
        </div>
        <div>
          <div className="center--vertical pad--topBottom--xxs">
            { user._id !== member._id ?
              <a href="#" onClick={this.handleDropdownClick.bind(this, member)} className="c--blue fontSize--m">
                {RoleMap[member.role]}
              </a>
            :
              <a href="#" className="c--grey fontSize--m">
                {RoleMap[member.role]}
              </a>
            }
          </div>
          { user._id !== member._id &&
            <MemberRole
              role={member.role}
              isOpen={isOpen}
              onBlur={this.handleRoleBlur.bind(this, member)}
              onRoleClick={this.handleRoleClick.bind(this, member)}
              onRemoveClick={this.handleRemoveClick.bind(this, member)} />
          }
        </div>
      </div>
    );
  }


  /**
   * **INTERNAL**
   * Handle members loaded. */
  private handleMembersLoad (members: any)
  {
    this.setState({
      members,
      isLoaded: true
    });
  }


  private handleInvite (email: string, role: string, ev?: MouseEvent)
  {
    http.put(Endpoints.MEMBER, { role, email }).then(({ data, error }) => {
      if (error)
      {
        toast({
          type: ToastType.Error,
          message: i18n.messages.ERROR as string
        });

        return;
      }

      this.addMember(data);
      toast({
        type: ToastType.Success,
        message: i18n.messages.MEMBER_INVITED.format({ email })
      });
    });
  }

  private handleInviteKey (ev: KeyboardEvent<HTMLInputElement>)
  {
    if (ev.key === 'Enter')
    {
      ev.preventDefault();
      ev.stopPropagation();
      const { role, email } = this.state;
      this.handleInvite(email, role);
    }
    else
    {
      const email = ev.target.value;
      void this.setState({
        email,
        isInviteEnabled: email.length > 0
      });
    }
  }

  private handleInviteRoleBlur ()
  {
    this.setState({ isShowingDropdown: false });
  }

  private handleInviteRoleClick (role: string, ev: MouseEvent)
  {
    ev.preventDefault();
    this.setState({ role });
  }

  private handleInviteDropdownClick (ev: MouseEvent)
  {
    ev.preventDefault();
    this.setState(prev => ({ isShowingDropdown: !prev.isShowingDropdown }));
  }


  private handleRoleBlur (member: any)
  {
    const { expanded } = this.state;
    if (expanded && member._id === expanded._id)
      this.setState({ expanded: null });
  }

  private handleRoleClick (member: any, role: string, ev: MouseEvent)
  {
    http.post(Endpoints.MEMBER, { role, member_id: member._id }).then(({ error }) => {
      if (error) return;
      this.changeMember(member, role);
    });
  }

  private handleRemoveClick (member: any, ev: MouseEvent)
  {
    http.delete(Endpoints.MEMBER, { member_id: member._id }).then(({ error }) => {
      if (error) return;
      this.removeMember(member);
    });
  }

  private handleDropdownClick (member: any, ev: MouseEvent)
  {
    ev.preventDefault();
    const { expanded } = this.state;
    if (expanded && member._id === expanded._id)
      this.setState({ expanded: null });
    else
      this.setState({ expanded: member });
  }


  private addMember (member: any)
  {
    const { members } = this.state;
    members.push(member);
    this.setState({ members });
  }

  private removeMember (member: any)
  {
    this.setState(prev => ({
      members: prev.members.filter(m => m._id !== member._id)
    }));
  }

  private changeMember (member: any, role: string)
  {
    this.setState(prev => ({
      members: prev.members.map(m => {
        if (m._id === member._id)
          m.role = role;
        return m;
      })
    }));
  }
}

export { MembersModal };
