/**
 * modal.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { createElement, Component, MouseEvent } from 'react';
import * as classnames              from 'classnames';
import { Portal }                   from './portal';
import { Transition }               from 'react-transition-group';


enum ModalSize
{
  XSmall  = 'modal--xs',
  Small   = 'modal--s',
  Medium  = 'modal--m',
  Large   = 'modal--l',
}

interface ModalProps
{
  size:       ModalSize;
  className?: string;

  isOpen?:    boolean;

  hasPrev?:   boolean;
  hasClose?:  boolean;

  onPrev?:    () => void;
  onClose?:   () => void;
}


class Modal extends Component<ModalProps>
{
  static defaultProps = {
    hasPrev:  false,
    hasClose: true
  };

  /**
   * **INTERNAL**
   * Render modal.
   *
   * @return {JSX.Element}
   */
  render()
  {
    const { size, children, isOpen, hasPrev, hasClose } = this.props;

    return (
      <Transition in={isOpen} timeout={200} mountOnEnter unmountOnExit>
        { state => (
          <Portal zIndex={100}>
            <div className={classnames('modal', { open: state === 'entered' })}>
              <div className="modal--actions">
                { hasPrev  && this.renderAction('back', 'Previous modal', this.handlePrevClick) }
                { hasClose && this.renderAction('close', 'Close modals', this.handleCloseClick) }
              </div>
              <div className="modal--wrapper">
                <div className="width--100">
                  <div className={classnames('modal--container', size, { open: state === 'entered' })}>
                    {children}
                  </div>
                </div>
              </div>
            </div>
          </Portal>
        )}
      </Transition>
    );
  }

  /**
   * **INTERNAL**
   * Render modal action
   *
   * @return {JSX.Element}
   */
  private renderAction (action: string, label: string, onClick: (ev: MouseEvent) => void)
  {
    return (
      <a role="button" className={classnames(action)} aria-label={label} onClick={onClick.bind(this)}>
        <i className={classnames('icon', action)} />
      </a>
    );
  }


  handlePrevClick(ev: MouseEvent)
  {
    const { onPrev } = this.props;
    ev.preventDefault();
    onPrev && onPrev();
  }

  handleCloseClick(ev: MouseEvent)
  {
    const { onClose } = this.props;
    ev.preventDefault();
    onClose && onClose();
  }
}

export { Modal, ModalSize };
