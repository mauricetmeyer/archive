/**
 * password_settings.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { createElement, Fragment, Component, ChangeEvent } from 'react';

import { i18n }                                            from '../i18n';
import { http }                                            from '../utils';
import { Endpoints }                                       from '../const';

import { Input, InputSize }                                from './input';
import { Button, ButtonSize, ButtonColor }                 from './button';
import { Section }                                         from './section';
import { toast, ToastType } from './toasts';


interface PasswordSettingsProps
{
  onChange: () => void;
}

interface PasswordSettingsState
{
  password?:     string;
  password_old?: string;

  isSubmitting:  boolean;
}


class PasswordSettings extends Component<PasswordSettingsProps, PasswordSettingsState>
{
  state: PasswordSettingsState = {
    isSubmitting: false
  };


  render()
  {
    const { isSubmitting } = this.state;

    return (
      <Fragment>
        <Section label={i18n.messages.CHANGE_PASSWORD as string} className="margin--bottom--l">
          <Input type="password" size={InputSize.Large} placeholder={i18n.messages.OLD_PASSWORD} className="margin--bottom--xxs" onChange={this.handlePasswordOldChange.bind(this)} />
          <Input type="password" size={InputSize.Large} placeholder={i18n.messages.NEW_PASSWORD} className="margin--bottom--xxs" onChange={this.handlePasswordChange.bind(this)} />
        </Section>
        <Button size={ButtonSize.Large} color={ButtonColor.Primary} disabled={isSubmitting} onClick={this.handleSubmit.bind(this)} className="width--100">
          {isSubmitting ? i18n.messages.UPDATING : i18n.messages.SAVE}
        </Button>
      </Fragment>
    );
  }


  /**
   * **INTERNAL**
   * Handle request submission. */
  private handleSubmit (ev: MouseEvent)
  {
    ev.preventDefault();
    const { password, password_old } = this.state;

    http.post(Endpoints.USER, { password, password_old }).then(({ error }) => {
      if (error)
      {
        toast({ type: ToastType.Error, message: i18n.messages.ERROR as string });
        return;
      }
      this.setState({ isSubmitting: false });
      toast({
        type: ToastType.Success,
        message: i18n.messages.PASSWORD_CHANGE_SUCCESS as string
      });
    });
  }

  /**
   * **INTERNAL**
   * Handle password value change.
   *
   * @param {ChangeEvent} ev
   */
  private handlePasswordChange (ev: ChangeEvent<HTMLInputElement>)
  {
    const { value } = ev.target;
    this.setState({ password: value });
  }

  /**
   * **INTERNAL**
   * Handle old password value change.
   *
   * @param {ChangeEvent} ev
   */
  private handlePasswordOldChange (ev: ChangeEvent<HTMLInputElement>)
  {
    const { value } = ev.target;
    this.setState({ password_old: value });
  }
}

export { PasswordSettings };
