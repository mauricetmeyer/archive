/**
 * platform.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { createElement, Component, MouseEvent }    from 'react';
import * as _                                      from 'underscore';
import * as classnames                             from 'classnames';

import { i18n }                                    from '../i18n';
import { Platforms }                               from '../const';

import { Popover, PopoverOriginV, PopoverOriginH } from './popover';


interface PlatformProps
{
  platform:        string;
  isOpen?:         boolean;

  onPlatformClick: (platform: string, ev: MouseEvent) => void;
}

class Platform extends Component<PlatformProps>
{
  render()
  {
    const { platform, isOpen } = this.props;
    const descriptions = {
      TWITTER:   i18n.messages.TWITTER_DESCRIPTION,
      INSTAGRAM: i18n.messages.INSTAGRAM_DESCRIPTION
    };

    return (
      <div className="width--100 relative">
        <Popover open={isOpen} origin={{ vertical: PopoverOriginV.Bottom, horizontal: PopoverOriginH.Right }}>
          <ul className="pad--xs">
            { _.map(Platforms, (val, idx) => {
              const key = idx.toLowerCase();
              return (
                <li key={idx} className={classnames('pad--topBottom--xxs pad--leftRight--xs', { active: key === platform })} onClick={val.disabled ? undefined  : this.handlePlatformClick.bind(this, key)}>
                  <div className="inline">
                    <div className="margin--right--xs">
                      <i className={classnames('icon', 'center--vertical', key)} />
                    </div>
                    <div className="inline--grow">
                      <h3 className={classnames('fontSize--m', { 'c--light': val.disabled })} style={val.disabled ? {} : { color: val.color }}>
                        {val.name}
                      </h3>
                      <p className="fontSize--s">
                        {val.disabled ? i18n.messages.UNAVAILABLE : descriptions[idx]}
                      </p>
                    </div>
                  </div>
                </li>
              );
            })}
          </ul>
        </Popover>
      </div>
    );
  }


  private handlePlatformClick (platform: string, ev: MouseEvent)
  {
    ev.preventDefault();
    this.props.onPlatformClick && this.props.onPlatformClick(platform, ev);
  }
}

export { Platform };
