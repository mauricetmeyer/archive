/**
 * popover.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { createElement, Component } from 'react';
import * as classnames              from 'classnames';

enum PopoverOriginV
{
  Top    = 'popover--top',
  Bottom = 'popover--bottom',
}

enum PopoverOriginH
{
  Left  = 'popover--align--left',
  Right = 'popover--align--right'
}

interface PopoverProps
{
  open?: boolean;
  full?: boolean;
  origin: {
    vertical?:   PopoverOriginV;
    horizontal?: PopoverOriginH;
  };

  onBlur?: () => void;
}


class Popover extends Component<PopoverProps>
{
  root?: HTMLDivElement;


  componentDidMount()
  {
    const { open } = this.props;
    if (open) document.addEventListener('click', this.handleDocumentClick);
  }

  componentWillUnmount()
  {
    document.removeEventListener('click', this.handleDocumentClick);
  }


  componentDidUpdate (prev: PopoverProps)
  {
    const { open } = this.props;

    /**
     * Only change the event handling when the open state changes. */
    if (prev.open === open) return;

    if (open) document.addEventListener('click', this.handleDocumentClick);
    else document.removeEventListener('click', this.handleDocumentClick);
  }


  render()
  {
    const { full, open, origin, children, ...props } = this.props;
    const { vertical, horizontal }                 = origin;

    return (
      <div
        {...props}
        tabIndex={-1}
        ref={r => r && (this.root = r)}
        className={classnames('popover', vertical, horizontal, { open, 'width--100': full })}>
        {children}
      </div>
    );
  }


  handleDocumentClick = (ev: MouseEvent) => {
    const { root } = this;
    if (!root || root === ev.target || root.contains(ev.target as Node))
      return;

    ev.preventDefault();
    ev.stopPropagation();
    this.props.onBlur && this.props.onBlur();
  }
}

export { Popover, PopoverOriginV, PopoverOriginH };
