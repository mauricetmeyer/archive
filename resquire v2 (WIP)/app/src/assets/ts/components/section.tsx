/**
 * section.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { createElement, Component } from 'react';
import * as classnames              from 'classnames';


interface SectionProps
{
  label:      string;
  className?: string;
}

class Section extends Component<SectionProps>
{
  render ()
  {
    const { label, children, className } = this.props;

    return (
      <div className={className}>
        <p className="margin--bottom--s c--grey fontSize--s">{label}</p>
        <div>{children}</div>
      </div>
    );
  }
}

export { Section };
