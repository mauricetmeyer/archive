/**
 * settings_modal.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { createElement, Component }        from 'react';

import { Modal, ModalSize }                from './modal';
import { AccountSettings }                 from './account_settings';
import { SubscriptionSettings }            from './subscription_settings';
import { PasswordSettings } from './password_settings';


interface SettingsModalProps
{
  user:    any;
  isOpen?: boolean;

  onClose:              () => void;
  onUserChange:         (user: any) => void;
  onDeleteAccountClick: () => void;
}

interface SettingsModalState
{
  showSettings:     boolean;
  showPassword:     boolean;
  showSubscription: boolean;
}


class SettingsModal extends Component<SettingsModalProps, SettingsModalState>
{
  state: SettingsModalState = {
    showSettings:     true,
    showPassword:     false,
    showSubscription: false
  };

  render()
  {
    const { user, onUserChange, ...props }                 = this.props;
    const { showSettings, showPassword, showSubscription } = this.state;

    return (
      <Modal
        {...props}
        size={ModalSize.XSmall}
        hasPrev={!showSettings}
        onPrev={this.handlePrevClick.bind(this)}>
        { showSettings &&
          <AccountSettings
            user={user}
            onUserChange={onUserChange}
            onPasswordClick={this.handlePasswordClick.bind(this)}
            onSubscriptionClick={this.handleSubscriptionClick.bind(this)}
            onDeleteAccountClick={this.props.onDeleteAccountClick} />
        }

        { showPassword &&
          <PasswordSettings onChange={this.handlePasswordChange.bind(this)} />
        }

        { showSubscription &&
          <SubscriptionSettings />
        }
      </Modal>
    );
  }


  private handlePrevClick ()
  {
    this.setState({
      showSettings:     true,
      showPassword:     false,
      showSubscription: false
    });
  }

  private handlePasswordClick ()
  {
    this.setState({
      showSettings: false,
      showPassword: true
    });
  }

  private handlePasswordChange ()
  {
    this.setState({
      showSettings: true,
      showPassword: false
    });
  }

  private handleSubscriptionClick ()
  {
    this.setState({
      showSettings:     false,
      showSubscription: true
    });
  }
}

export { SettingsModal };
