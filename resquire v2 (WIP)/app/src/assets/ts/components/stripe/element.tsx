/**
 * element.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { createElement, Component } from 'react';
import { StripeContext }            from './context';
import { IStripeElement }           from './interfaces';


const StripeElementsStyle = {
  classes: {
    empty:    'empty',
    focus:    'active',
    invalid:  'error',
    complete: 'complete',
  },
  style: {
    base: {
      color:         '#828a99',
      fontSize:      '16px',
      fontWeight:    '500',
      fontFamily:    '-apple-system, "Source Sans Pro", roboto, Helvetica Neue, Helvetica, Arial, sans-serif',
      fontSmoothing: 'antialiased'
    }
  }
};


interface StripeChangeEvent
{
  empty:    boolean;
  error:    boolean;
  complete: boolean;
}


enum StripeElementType
{
  CVC    = 'cardCvc',
  NUMBER = 'cardNumber',
  EXPIRY = 'cardExpiry'
}

interface StripeElementProps
{
  type:        StripeElementType;
  placeholder: string;
  className?:  string;

  onBlur?:     () => void;
  onFocus?:    () => void;
  onReady?:    () => void;
  onChange?:   (change: StripeChangeEvent) => void;
}


class StripeElement extends Component<StripeElementProps>
{
  static contextType = StripeContext;

  private el?:  any;
  private ref?: HTMLDivElement;


  componentDidMount ()
  {
    const {
      type, placeholder,
      onBlur, onFocus, onReady, onChange
    } = this.props;

    this.context.createElement(type, { ...StripeElementsStyle, placeholder }).then((el: IStripeElement) => {
      this.el = el;

      /**
       * Register events for
       * the element. */
      onBlur   && el.on('blur',   ()       => onBlur());
      onFocus  && el.on('focus',  ()       => onFocus());
      onReady  && el.on('ready',  ()       => onReady());
      onChange && el.on('change', (change: StripeChangeEvent) => onChange(change));

      /**
       * Mount the stripe iframe. */
      this.ref && el.mount(this.ref);

      /**
       * Register element */
      if (type === StripeElementType.NUMBER)
        this.context.registerElement(el);
    });
  }

  componentWillUnmount ()
  {
    const { type } = this.props;
    if (this.el)
    {
      const element = this.el;
      element.destroy();

      if (type === StripeElementType.NUMBER)
        this.context.unregisterElement(element);
    }
  }


  render()
  {
    const { className } = this.props;

    return (
      <div ref={r => r && (this.ref = r)} className={className} />
    );
  }
}

export { StripeElement, StripeElementType, StripeChangeEvent };
