/**
 * index.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { createElement }                                       from 'react';
import { Stripe, StripeCtx }                                   from './stripe';
import { StripeElement, StripeElementType, StripeChangeEvent } from './element';

const StripeCVC    = (props: any) => <StripeElement type={StripeElementType.CVC} {...props} />;
const StripeNumber = (props: any) => <StripeElement type={StripeElementType.NUMBER} {...props} />;
const StripeExpiry = (props: any) => <StripeElement type={StripeElementType.EXPIRY} {...props} />;

export { Stripe, StripeCVC, StripeNumber, StripeExpiry, StripeCtx, StripeChangeEvent };
