/**
 * interfaces.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


interface IStripeToken
{
  id: string;
}

interface IStripeResponse
{
  error?: any;
  token:  IStripeToken;
}

interface IStripeElement
{
  mount   (el: HTMLElement) : void;
  on      (type: string, handler: (...args: any[]) => void) : IStripeElement;

  destroy ()             : IStripeElement;
  update  (options: any) : IStripeElement;
}

interface IStripeElements
{
  create (type: string, options?: any) : Promise<IStripeElement>;
}

interface IStripeContext
{
  createToken (options?: any) : Promise<IStripeResponse>;

  createElement (type: string, options?: any) : Promise<IStripeElement>;
  registerElement (element: IStripeElement) : void;
  unregisterElement (element: IStripeElement) : void;
}

interface IStripe
{
  elements (options?: any) : IStripeElements;
  createToken (element?: IStripeElement, options?: any) : Promise<IStripeResponse>;
}

export { IStripe, IStripeContext, IStripeElement, IStripeElements, IStripeResponse };
