/**
 * stripe.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { createElement, Component }                 from 'react';
import { StripeProvider }                           from './provider';
import { IStripe, IStripeElement, IStripeResponse, IStripeElements } from './interfaces';
import { Script } from '../script';
import { Emitter } from '../../utils';


let StripeInstance: IStripe;
const StripeGetOrSet = (key: string) => {
  if (!StripeInstance) StripeInstance = window.Stripe(key);
  return StripeInstance;
}

interface StripeCtx
{
  createToken () : Promise<IStripeResponse>;
}

interface StripeProps
{
  token:  string;
  onLoad: (stripe: StripeCtx) => void;
}

interface StripeState
{
  element?: IStripeElement;
}

class Stripe extends Component<StripeProps, StripeState>
{
  emitter:   Emitter = new Emitter();
  stripe?:   IStripe;
  elements?: IStripeElements;


  componentDidMount ()
  {
    const { onLoad }  = this.props;
    const createToken = this.handleCreateToken.bind(this);
    onLoad({ createToken });
  }


  render ()
  {
    const { children } = this.props;

    return (
      <StripeProvider value={this.createInterface()}>
        <Script url="https://js.stripe.com/v3/" onLoad={this.handleLoad.bind(this)} />
        {children}
      </StripeProvider>
    );
  }


  /**
   * This will return the wrapped stripe
   * interface that can be used by children. */
  private createInterface ()
  {
    return {
      createElement:     this.handleCreateElement.bind(this),
      registerElement:   this.handleRegisterElement.bind(this),
      unregisterElement: this.handleUnregisterElement.bind(this)
    };
  }


  /**
   * **INTERNAL**
   * Helper function that calls the callback when stripe
   * is available.
   *
   * @param {function} fn
   */
  private getStripe () : Promise<IStripe>
  {
    return new Promise((res, rej) => {
      if (this.stripe) res(this.stripe);
      else this.emitter.on('did-load', () => res(this.stripe));
    });
  }

  /**
   * **INTERNAL**
   * Handle `stripe loaded` event.
   */
  private handleLoad ()
  {
    const { token } = this.props;
    this.stripe = StripeGetOrSet(token);
    this.emitter.emit('did-load');
  }

  /**
   * **INTERNAL**
   *
   * @param  {string}
   * @return {Promise<IStripeResponse>}
   */
  private async handleCreateToken () : Promise<IStripeResponse>
  {
    const stripe      = await this.getStripe();
    const { element } = this.state;
    if (!element) throw new Error('Missing StripeElement component detected!');
    return stripe.createToken(element);
  }

  /**
   * **INTERNAL**
   *
   * @param  {string} type
   * @param  {any}    options
   * @return {void}
   */
  private async handleCreateElement (type: string, options: any) : Promise<IStripeElement>
  {
    const stripe = await this.getStripe();
    if (!this.elements) this.elements = stripe.elements();
    return this.elements.create(type, options);
  }

  /**
   * **INTERNAL**
   *
   * @param  {}
   * @return {void}
   */
  private handleRegisterElement (element: IStripeElement) : void
  {
    this.setState({ element });
  }

  /**
   * **INTERNAL**
   *
   * @param  {}
   * @return {void}
   */
  private handleUnregisterElement (element: IStripeElement) : void
  {
    this.setState({ element: undefined });
  }
}

export { Stripe, StripeCtx };
