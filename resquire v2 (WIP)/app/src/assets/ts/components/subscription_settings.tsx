/**
 * subscription_settings.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { createElement, Component, MouseEvent } from 'react';

import { i18n }                                 from '../i18n';
import { Endpoints }                            from '../const';
import { Config, http }                         from '../utils';

import { Button, ButtonSize, ButtonColor }      from './button';
import { Section }                              from './section';
import {
  Stripe,
  StripeCtx,
  StripeCVC,
  StripeNumber,
  StripeExpiry,
  StripeChangeEvent
} from './stripe';
import { toast, ToastType } from './toasts';


const SUBSCRIPTION_MIN    = 3;
const SUBSCRIPTION_PRICE  = 5;
const SUBSCRIPTION_ANNUAL = 10;


interface SubscriptionSettingsProps {}
interface SubscriptionSettingsState
{
  stripe?:           StripeCtx;

  plan?:             string;
  members?:          number;

  source?:           any;
  subscription?:     any;

  isValid:           boolean;
  isValidCVC:        boolean;
  isValidNumber:     boolean;
  isValidExpiry:     boolean;
  isLoaded:          boolean;
  isLocked:          boolean;
  isSubmitting:      boolean;
  isCardUpdate:      boolean;
  isPlanUpdate:      boolean;

  isAnnualSelected:  boolean;
  isMonthlySelected: boolean;
}


class SubscriptionSettings extends Component<SubscriptionSettingsProps, SubscriptionSettingsState>
{
  state: SubscriptionSettingsState = {
    isValid:           false,
    isValidCVC:        false,
    isValidNumber:     false,
    isValidExpiry:     false,
    isLoaded:          false,
    isLocked:          false,
    isSubmitting:      false,
    isPlanUpdate:      false,
    isCardUpdate:      false,
    isAnnualSelected:  false,
    isMonthlySelected: false
  };


  componentDidMount ()
  {
    http.get(Endpoints.PAYMENT).then(({ data }) => {
      if (data.error) return;
      const isAnnualSelected  = data.plan === 'annual';
      const isMonthlySelected = !isAnnualSelected;
      this.setState({
        ...data,
        isAnnualSelected,
        isMonthlySelected,
        isLoaded: true
      });
    });
  }


  render()
  {
    const {} = this.props;
    const { isLoaded } = this.state;

    return isLoaded ? this.renderForm() : <div className="fontSize--m textAlign--m">{i18n.messages.LOADING}</div>;
  }


  private renderForm ()
  {
    const {
      source,
      isLocked,
      isCardUpdate
    } = this.state;

    const {
      UPGRADE_SELECT,
      UPGRADE_CURRENT,
      UPGRADE_CREDIT_CARD,
      UPGRADE_ADD_DETAILS,
      UPGRADE_CHANGE_DETAILS
    } = i18n.messages;

    const isActive    = !isLocked && !!source && !isCardUpdate;
    const plansLabel  = !!source ? UPGRADE_CURRENT : UPGRADE_SELECT;
    const sourceLabel = !source  ? UPGRADE_ADD_DETAILS
                      : isActive ? UPGRADE_CREDIT_CARD : UPGRADE_CHANGE_DETAILS;

    return (
      <form>
        <Section className="margin--bottom--xl" label={plansLabel as string}>
          {this.renderPlans()}
        </Section>
        <Section className="margin--bottom--l" label={sourceLabel as string}>
          {isActive && !isCardUpdate ? this.renderSource() : this.renderPaymentForm()}
        </Section>
        {this.renderActions()}
      </form>
    );
  }

  private renderPlans ()
  {
    const {
      members,
      isAnnualSelected,
      isMonthlySelected
    } = this.state;

    const seats = Math.max(SUBSCRIPTION_MIN, members || 0);

    return (
      <div className="inline">
        <div className="width--50">
          <label className="plan margin--right--xxxs monthly" onClick={this.handleMonthlyClick.bind(this)}>
            <input className="plan--trigger" type="radio" name="plan" value="monthly" defaultChecked={isMonthlySelected} />
            <div className="plan--content pad--leftRight--m pad--topBottom--s">
              <h3 className="margin--bottom--xxs fontSize--s">{i18n.messages.UPGRADE_MONTHLY}</h3>
              <p className="margin--bottom--xxs fontSize--xxl c--green">$15</p>
              <span className="c--grey">{`$${SUBSCRIPTION_PRICE} x ${seats} seats`}</span>
            </div>
          </label>
        </div>
        <div className="width--50">
          <label className="plan margin--left--xxxs annual" onClick={this.handleAnnualClick.bind(this)}>
            <input className="plan--trigger" type="radio" name="plan" value="annual" defaultChecked={isAnnualSelected} />
            <div className="plan--content pad--leftRight--m pad--topBottom--s">
              <h3 className="margin--bottom--xxs fontSize--s">{i18n.messages.UPGRADE_ANNUAL}</h3>
              <p className="margin--bottom--xxs fontSize--xxl c--green">$150</p>
              <span className="c--grey">{`$${SUBSCRIPTION_PRICE * SUBSCRIPTION_ANNUAL} x ${seats} seats`}</span>
            </div>
          </label>
        </div>
      </div>
    );
  }

  private renderSource ()
  {
    const { source } = this.state;

    return (
      <div className="input input--l pointer" onClick={this.handleCardClick.bind(this)}>
        <span>{`•••• •••• •••• ${source.last4}`}</span>
        <span className="margin--left--m">{`${source.exp_month} / ${source.exp_year}`}</span>
        <a className="right fontSize--m c--blue" style={{ lineHeight: '18px' }} tabIndex={-1}>{i18n.messages.EDIT}</a>
      </div>
    );
  }

  private renderActions ()
  {
    const { source, isValid, isLocked, isCardUpdate, isPlanUpdate } = this.state;
    const isActive = !isLocked && !!source && !isCardUpdate;
    const disabled = isCardUpdate || !isActive ? !isValid : !isPlanUpdate;

    return (
      <div>
        <Button size={ButtonSize.Large} color={ButtonColor.Primary} disabled={disabled} onClick={this.handleSubmit.bind(this)} className="width--100">
          {isCardUpdate ? i18n.messages.UPGRADE_CARD : i18n.messages.UPGRADE}
        </Button>
        { isCardUpdate &&
          <Button size={ButtonSize.Large} color={ButtonColor.Secondary} onClick={this.handleCancelClick.bind(this)} className="width--100 margin--top--xs">
            {i18n.messages.CANCEL}
          </Button>
        }
      </div>
    );
  }

  private renderPaymentForm ()
  {
    return (
      <Stripe token={Config.get('stripe') || ''} onLoad={this.handleStripeLoad.bind(this)}>
        <StripeNumber className="input input--l margin--bottom--xxs" placeholder={i18n.messages.UPGRADE_CC_NUMBER} onChange={this.handleNumberChange.bind(this)} />
        <div className="flex">
          <div className="margin--right--xxs" style={{ width: '60%' }}>
            <StripeExpiry className="input input--l" onChange={this.handleExpiryChange.bind(this)} />
          </div>
          <div className="flex--grow">
            <StripeCVC className="input input--l" onChange={this.handleCVCChange.bind(this)} />
          </div>
        </div>
      </Stripe>
    );
  }


  private async handleSubmit (ev: MouseEvent)
  {
    ev.preventDefault();
    const { stripe, source, isLocked, isCardUpdate, isAnnualSelected } = this.state;
    this.setState({ isSubmitting: true });
    try
    {
      const plan = isAnnualSelected ? 'annual' : 'monthly';
      if ((!source || isLocked || isCardUpdate) && stripe)
      {
        await stripe.createToken().then(({ token, error }) => {
          if (error) return;
          return http.post(Endpoints.PAYMENT, { plan, token: token.id });
        });
      }
      else
      {
        await http.post(Endpoints.PAYMENT, { plan });
      }

      toast({
        type: ToastType.Success,
        message: i18n.messages.UPGRADE_SUCCESS as string
      });
    }
    catch (err)
    {
      toast({
        type: ToastType.Error,
        message: i18n.messages.UPGRADE_ERROR as string
      });
    }

    this.setState({ isSubmitting: false });
  }

  private handleCardClick ()
  {
    this.setState({ isCardUpdate: true });
  }

  private handleCancelClick ()
  {
    this.setState({ isCardUpdate: false });
  }

  private handleAnnualClick ()
  {
    const { plan }     = this.state;
    const isPlanUpdate = plan !== 'annual';

    this.setState({
      isPlanUpdate,
      isAnnualSelected: true,
      isMonthlySelected: false
    });
  }

  private handleMonthlyClick ()
  {
    const { plan }     = this.state;
    const isPlanUpdate = plan !== 'monthly';

    this.setState({
      isPlanUpdate,
      isAnnualSelected: false,
      isMonthlySelected: true
    });
  }


  private handleStripeLoad (stripe: StripeCtx)
  {
    this.setState({ stripe });
  }


  private handleCVCChange (change: StripeChangeEvent)
  {
    const { isValidNumber, isValidExpiry } = this.state;
    const isValid = change.complete;
    this.setState({
      isValid: isValidNumber && isValidExpiry && isValid,
      isValidCVC: isValid
    });
  }

  private handleNumberChange (change: StripeChangeEvent)
  {
    const { isValidCVC, isValidExpiry } = this.state;
    const isValid = change.complete;
    this.setState({
      isValid: isValidCVC && isValidExpiry && isValid,
      isValidNumber: isValid
    });
  }

  private handleExpiryChange (change: StripeChangeEvent)
  {
    const { isValidCVC, isValidNumber } = this.state;
    const isValid = change.complete;
    this.setState({
      isValid: isValidCVC && isValidNumber && isValid,
      isValidExpiry: isValid
    });
  }
}

export { SubscriptionSettings };
