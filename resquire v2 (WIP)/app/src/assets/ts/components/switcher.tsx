/**
 * switcher.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { createElement, Component, MouseEvent, Fragment } from 'react';
import * as classnames                          from 'classnames';
import { Transition }                           from 'react-transition-group';

import { i18n }                                 from '../i18n';
import { Platforms }                            from '../const';

import { Avatar, AvatarSize }                   from './avatar';
import { Measure }                              from './measure';


interface SwitcherProps
{
  value?:     number;
  accounts?:  any[];

  onBlur?:       (ev: MouseEvent) => void;
  onClick?:      (ev: MouseEvent) => void;
  onChange?:     (value: number)  => void;
  onAddClick:    (ev: MouseEvent) => void;
  onOptionClick: (value: number)  => void;
}

interface SwitcherState
{
  value?: number;
  isOpen: boolean;
}


class Switcher extends Component<SwitcherProps, SwitcherState>
{
  state = {
    value:  this.props.value,
    isOpen: false,
  };


  render()
  {
    const { accounts } = this.props;

    return (
      <div className="switcher">
        {!!accounts ? this.renderSwitcher() : this.renderEmpty()}
      </div>
    );
  }

  private renderEmpty ()
  {
    const { onAddClick } = this.props;

    return (
      <div role="button" className="switcher--trigger" onClick={onAddClick}>
        <div className="pad--leftRight--xs inline">
          <div className="inline--grow fontSize--m pad--left--xxs">
            {i18n.messages.ADD_ACCOUNT}
          </div>
          <div><i className="icon add" /></div>
        </div>
      </div>
    )
  }

  private renderSwitcher ()
  {
    const { accounts }      = this.props;
    const { value, isOpen } = this.state;

    const isAtLimit  = false;
    const selected   = accounts[value || 0];

    return (
      <Fragment>
        <div className="flex flex--center">
          <div role="button" className="switcher--trigger" onClick={this.handleClick.bind(this)}>
            <div className="pad--leftRight--xs inline">
              <div className="inline--grow fontSize--m pad--left--xxs">{selected.handle}</div>
              <div><i className="icon dropdown" /></div>
            </div>
          </div>
        </div>
        <Transition in={isOpen} timeout={100} mountOnEnter unmountOnExit>
          { state =>
            <div className={classnames('switcher--options center', { open: state === 'entered' })}>
              <Measure onChange={this.handleSizeChange.bind(this)}>
                <ul className="pad--xs">
                  { accounts.map((v, i) => this.renderValue(v, i)) }
                  { !isAtLimit && this.renderAdd() }
                </ul>
              </Measure>
            </div>
          }
        </Transition>
      </Fragment>
    );
  }


  private renderAdd ()
  {
    const { onAddClick } = this.props;
    return (
      <li className="switcher--add pad--leftRight--m  pad--topBottom--xxs" onClick={onAddClick}>
        <div className="inline">
          <div>
            <i className="icon add" />
          </div>
          <div className="margin--left--xs inline--grow">
            <div className="fontSize--m center--vertical">{i18n.messages.ACCOUNTS_ADD}</div>
          </div>
        </div>
      </li>
    );
  }

  private renderValue (value: any, index: number)
  {
    const { handle, avatar_url } = value;

    return (
      <li key={index} className="switcher--value pad--leftRight--m  pad--topBottom--xxs" onClick={() => this.handleValueClick(index)}>
        <div className="inline">
          <div>
            <Avatar alt={handle} src={avatar_url} size={AvatarSize.XSmall} className="center--vertical" />
          </div>
          <div className="margin--left--xs inline inline--grow">
            <div className="inline--grow">
              <h3 className={classnames('fontSize--m center--vertical', { 'c--light': false })}>
                {handle}
              </h3>
            </div>
            <div>
              <div className="center--vertical pad--topBottom--xxs pad--left--l">
                <a href="#" className="fontSize--m switcher--value--options block" onClick={this.handleOptionClick.bind(this, index)}>
                  <i className="icon more" />
                </a>
              </div>
            </div>
          </div>
        </div>
      </li>
    );
  }


  private handleClick ()
  {
    this.setState({ isOpen: true });
  }

  private handleValueClick (value: number)
  {
    const { onChange } = this.props;
    this.setState((prev) => {
      if (prev.value !== value) onChange && onChange(value);
      return { value, isOpen: false };
    });
  }

  private handleOptionClick (value: number, ev: MouseEvent)
  {
    ev.preventDefault();
    const { onOptionClick } = this.props;
    this.setState((prev) => {
      if (prev.value !== value) onOptionClick && onOptionClick(value);
      return { value, isOpen: false };
    });
  }


  private handleSizeChange (width: number, height: number)
  {

  }
}

export { Switcher };
