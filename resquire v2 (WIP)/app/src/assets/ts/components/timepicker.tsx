/**
 * datetimepicker.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { createElement, Component } from 'react';
import * as moment                  from 'moment';
import * as classnames              from 'classnames';


const TIME_DEFAULTS: TimePickerTime[] = [];
for (let h = 0; h < 24; h++)
  TIME_DEFAULTS.push([h, 0], [h, 30]);


type TimePickerTime = [number, number];
interface TimePickerProps
{
  value:      moment.Moment;
  times?:     TimePickerTime[];
  display24?: boolean;

  onChange:   (datetime: moment.Moment) => void;
}

interface TimePickerState
{

}


class TimePicker extends Component<TimePickerProps, TimePickerState>
{
  static defaultProps = {
    times: TIME_DEFAULTS
  };


  render ()
  {
    const time = this.valueToTime();

    return (
      <ul className="pad--xs timepicker">
        { this.getOptions().map(({ value, label }, i) =>
          <li
            key={i}
            className={classnames('pad--topBottom--xxs pad--leftRight--xs', { active: this.getMatches(time, value) })}
            onClick={this.handleItemClick.bind(this, value)}>
            <h3 className="fontSize--m">{label}</h3>
          </li>
        )}
      </ul>
    );
  }


  private handleItemClick (item: TimePickerTime)
  {
    const { value }        = this.props;
    const [hours, minutes] = item;
    const newValue = value.clone().set({ hours, minutes });
    this.props.onChange(newValue);
  }


  private getOptions ()
  {
    const { times } = this.props;
    if (!times) return [];

    return times.map(t => ({
      value: t,
      label: this.secondsToHuman(t)
    }));
  }

  private getMatches (lhs: TimePickerTime, rhs: TimePickerTime)
  {
    return (lhs[0] === rhs[0] && lhs[1] === rhs[1]);
  }


  private valueToTime ()
  {
    const { value } = this.props;
    return [value.get('hours'), value.get('minutes')] as TimePickerTime;
  }

  private secondsToHuman (time: TimePickerTime)
  {
    const pad = (val: number) => val < 10 ? `0${val}` : val;
    const { display24 }    = this.props;
    const [hours, minutes] = time;

    if (display24) return `${pad(hours)}:${pad(minutes)}`;

    const am_pm = hours < 12 ? 'am' : 'pm';
    return `${pad(hours % 12 || 12)}:${pad(minutes)} ${am_pm}`;
  }
}

export { TimePicker };
