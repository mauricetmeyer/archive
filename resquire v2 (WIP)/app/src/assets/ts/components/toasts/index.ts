/**
 * index.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { ToastType, ToastProps } from './toast';
import { Toasts }                from './toasts';
import { ToastEmitter }          from './toastEmitter';


function toast (props: ToastProps)
{
  ToastEmitter.emit('toasts:add', props);
}

export { toast, Toasts, ToastType };
