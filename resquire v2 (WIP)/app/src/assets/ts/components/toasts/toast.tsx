/**
 * toast.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { createElement, Component, MouseEvent } from 'react';
import { Transition }                           from 'react-transition-group';
import * as classnames                          from 'classnames';

import { Measure }                              from '../measure';


enum ToastType
{
  Info    = 'info',
  Error   = 'error',
  Success = 'success'
}

interface ToastAction
{
  content: string;
  onClick: (ev: MouseEvent) => void;
}

interface ToastProps
{
  type:     ToastType;

  message:  string;
  content?: string;

  actions?: ToastAction[];
}

interface ToastPropsInt extends ToastProps
{
  timeout:     number;
  onDismissed: () => void;
}

interface ToastState
{
  isExpanded: boolean;
}

class Toast extends Component<ToastPropsInt, ToastState>
{
  state: ToastState = {
    isExpanded: false
  };

  timeout?: number;


  componentDidMount ()
  {
    const { timeout, onDismissed } = this.props;
    this.timeout = setTimeout(onDismissed, timeout);
  }


  render ()
  {
    const { type, message, content, actions } = this.props;
    const { isExpanded }                      = this.state;

    return (
      <div className={classnames('toast', 'margin--bottom--xxs', type)}>
        <div className="pad--leftRight--m pad--topBottom--xs textAlign--m">
          <div className="toast--message fontSize--m">{message}</div>
          { content && content.length &&
            <Transition in={isExpanded} timeout={200}>
              <Measure onChange={this.handleSizeChange}>
                <div className="toast--content">
                  {content}
                </div>
              </Measure>
            </Transition>
          }
          { actions && actions.length &&
            <div className="toast--actions">
              {actions.map(a => this.renderAction(a))}
            </div>
          }
        </div>
      </div>
    );
  }

  private renderAction (action: ToastAction)
  {
    const { content, ...props } = action;
    return (
      <a href="#" {...props}>{content}</a>
    );
  }


  private handleSizeChange (w: number, h: number)
  {

  }
}

export { Toast, ToastType, ToastProps };
