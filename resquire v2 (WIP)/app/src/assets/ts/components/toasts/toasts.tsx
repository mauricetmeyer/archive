/**
 * toasts.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { createElement, Component } from 'react';
import { Transition }               from 'react-transition-group';
import * as classnames              from 'classnames';

import { Toast, ToastProps }        from './toast';
import { ToastEmitter }             from './toastEmitter';

import { Portal }                   from '../portal';


interface ToastsProps
{
  timeout:      number;
  onDismissed?: () => void;
}

interface ToastState
{
  toasts: ToastsProps[];
}


class Toasts extends Component<ToastsProps, ToastState>
{
  state = {
    toasts: []
  };


  componentDidMount ()
  {
    ToastEmitter.on('toasts:add', (props: ToastProps) => {
      this.setState(prev => ({ toasts: prev.toasts.concat(props) }));
    });
  }

  componentWillUnmount ()
  {
    ToastEmitter.clear();
  }


  render ()
  {
    return (
      <Portal zIndex={1000}>
        <div className="toasts">
          <div className="wrap--xxs">
            { this.renderToasts() }
          </div>
        </div>
      </Portal>
    );
  }

  renderToasts ()
  {
    const { timeout } = this.props;
    const { toasts }  = this.state;

    return toasts.map((toast, idx) =>
      <Toast key={idx} {...toast} timeout={timeout} onDismissed={this.handleDismissed.bind(this, idx)} />
    );
  }


  handleDismissed (idx: number)
  {
    const { onDismissed } = this.props;
    this.setState(prev => ({ toasts: prev.toasts.filter((_, i) => i !== idx) }));
  }
}


export { Toasts };
