/**
 * actions.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { mirror } from '../utils';


const Actions = mirror(
  /**
   * Modal */
  'MODAL_OPEN',
  'MODAL_CLOSE',
  'MODAL_PREVIOUS',

  /**
   * Special modal */
  'ALERT_OPEN',
  'CONFIRM_OPEN',

  /**
   * Settings modal */
  'SETTINGS_MODAL_INIT',
  'SETTINGS_MODAL_OPEN',
  'SETTINGS_MODAL_CLOSE',

  /**
   * Subscription modal */
  'SUBSCRIPTION_MODAL_INIT',
  'SUBSCRIPTION_MODAL_OPEN',
  'SUBSCRIPTION_MODAL_CLOSE',


  /**
   * Auth */
  'LOGIN',
  'LOGIN_SUCCESS',
  'LOGIN_FAILED',
  'LOGOUT',
  'LOGOUT_SUCCESS',
  'LOGOUT_FAILED',
  'REGISTER',
  'REGISTER_SUCCESS',
  'REGISTER_FAILED',

  /**
   * User */
  'USER_UPDATE',
  'USER_UPDATE_SUCCESS',
  'USER_UPDATE_FAILED',

  /**
   * Member */
  'MEMBER_GET',
  'MEMBER_GET_SUCCESS',
  'MEMBER_GET_FAILED',
  'MEMBER_ADD',
  'MEMBER_ADD_SUCCESS',
  'MEMBER_ADD_FAILED',
  'MEMBER_UPDATE',
  'MEMBER_UPDATE_SUCCESS',
  'MEMBER_UPDATE_FAILED',
  'MEMBER_DELETE',
  'MEMBER_DELETE_SUCCESS',
  'MEMBER_DELETE_FAILED',

  /**
   * Payment */
  'PAYMENT_GET',
  'PAYMENT_GET_SUCCESS',
  'PAYMENT_GET_FAILED',
  'PAYMENT_UPDATE',
  'PAYMENT_UPDATE_SUCCESS',
  'PAYMENT_UPDATE_FAILED',

  /**
   * Workspace */
  'WORKSPACE_GET',
  'WORKSPACE_GET_SUCCESS',
  'WORKSPACE_GET_FAILED',
  'WORKSPACE_UPDATE',
  'WORKSPACE_UPDATE_SUCCESS',
  'WORKSPACE_UPDATE_FAILED',
  'WORKSPACE_DELETE',
  'WORKSPACE_DELETE_SUCCESS',
  'WORKSPACE_DELETE_FAILED'
);


export { Actions };
