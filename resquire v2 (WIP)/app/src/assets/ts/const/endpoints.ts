/**
 * actions.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


const Endpoints = {
  LOGIN:      '/i/login',
  LOGOUT:     '/i/logout',
  REGISTER:   '/i/register',
  USER:       '/i/user',
  INVITE:     '/i/member/invite',
  MEMBER:     '/i/member',
  MEMBERS:    '/i/members',
  ACCOUNT:    '/i/account',
  PAYMENT:    '/i/payment',
  WORKSPACE:  '/i/workspace',

  PROCESS:    '/i/process',
  EVALUATE:   '/i/evaluate',
  SUGGESTION: '/i/suggestion',
};

export { Endpoints };
