/**
 * i18n.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { getMessage }    from './parse';
import IntlMessageFormat from 'intl-messageformat';


interface I18NProps
{
  initial?:    string;

  getLocales:  () => I18NLocale[];
  getMessages: I18NMessagesFn;
}

interface I18NLocale
{
  code:    string;
  name:    string;
  enName:  string;
  enabled: boolean;
}


type I18NMessage    = IntlMessageFormat | string;
type I18NMessages   = { [key: string]: I18NMessage };

type I18NChangeFn   = (locale: string) => void;
type I18NMessagesFn = (locale: string) => { [key: string]: string };

const I18N_DEFAULT = 'en';
class I18N
{
  public readonly messages:    I18NMessages;

  private         chosen?:     string;
  private         requested?:  string;
  private         locales:     I18NLocale[];
  private         onChange?:   I18NChangeFn;
  private         getMessages: I18NMessagesFn;


  constructor (opts: I18NProps)
  {
    this.messages    = {};
    this.getMessages = opts.getMessages;

    /**
     * Load locales */
    this.locales = opts.getLocales();
    this.setLocale(opts.initial || this.getDefaultLocale());
  }

  getLocale ()
  {
    return this.chosen;
  }

  setLocale (locale: string)
  {
    /**
     * Early return */
    if (this.chosen === locale) return;

    const old   = this.chosen;
    this.chosen = locale;

    /**
     * Load messages */
    this.requested = locale;
    this.applyMessagesForLocale(locale);

    /**
     * Dispatch callback */
    if (this.onChange && old != null && this.chosen !== old) {
      this.onChange(this.chosen);
    }
  }

  getSystemLocale ()
  {
    /**
     * @TODO (Maurice):
     * Actually return the browsers real locale */
    return I18N_DEFAULT;
  }

  getDefaultLocale ()
  {
    const locale   = this.getSystemLocale() || I18N_DEFAULT;
    const locales  = this.locales.filter(l => l.enabled).map(l => l.code);
    const language = locale.split('-')[0];


    /**
     * Check if language is supported by us. */
    if (locales.indexOf(language) !== -1)
      return language;

    return I18N_DEFAULT;
  }


  getLocales ()
  {
    return this.locales;
  }

  getAvailableLocales ()
  {
    /**
     * Filter and transform */
    const res = this.locales
      .filter(l => l.enabled)
      .map(({ code, name, enName }) => {
        return {
          name:          enName,
          value:         code,
          localizedName: name
        };
      });

    /**
     * Sort before returning */
    res.sort((l, r) => {
      const lname = l.name.toLowerCase();
      const rname = r.name.toLowerCase();
      return lname < rname ? -1 : lname > rname ? 1 : 0;
    });

    return res;
  }


  private loadMessagesForLocale (locale: string) : { [key: string]: string }
  {
    let fallback = () => {
      return this.loadMessagesForLocale(I18N_DEFAULT);
    };

    /**
     * There is no fallback for the 
     * default locale */
    if (locale === I18N_DEFAULT) {
      fallback = () => {
        throw new Error('Error loading ' + I18N_DEFAULT);
      };
    }

    try
    {
      /**
       * Try loading the messages */
      return this.getMessages(locale);
    } catch (e) {
      /**
       * return fallback */
      return fallback();
    }
  }

  private applyMessagesForLocale (locale: string)
  {
    /**
     * Check if they are matching */
    if (this.requested !== locale) return;
    const messages = this.loadMessagesForLocale(locale);

    /**
     * Go over messages */
    for (const key in messages)
    {
      const message = messages[key];
      try {
        const resultMessage = getMessage(locale, message);
        resultMessage && (this.messages[key] = resultMessage);
      } catch (err) {}
    }
  }
}

export { I18N };
