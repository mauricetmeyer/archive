/**
 * index.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { I18N }    from './i18n';
import { Locales } from './locales';

const i18n = new I18N({
  getLocales ()
  {
    return Locales;
  },

  getMessages (code: string)
  {
    return require(`../locales/${code}.ts`);
  }
});

export { i18n };
