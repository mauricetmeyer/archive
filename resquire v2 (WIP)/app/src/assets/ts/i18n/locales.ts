/**
 * locales.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


const Locales = [
  {
    name:    'English, US',
    enName:  'English, US',
    code:    'en-US',
    enabled: true
  },
  {
    name:    'English, UK',
    enName:  'English, UK',
    code:    'en-GB',
    enabled: true
  },
  {
    name:    'Deutsch',
    enName:  'German',
    code:    'de',
    enabled: true
  },
  {
    name:    'Français',
    enName:  'French',
    code:    'fr',
    enabled: true
  }
];

export { Locales };
