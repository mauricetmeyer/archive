/**
 * parse.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import intl_format from 'intl-messageformat';


const FORMAT_RE = /\{.+?}/;
function getMessage(locale: string, message?: string) {
  if (message == null) return message;

  /**
   * Clean up the string */
  message = message.replace(/^\n+|\n+$/g, '');

  /**
   * Only replace if the string actually
   * contains some format modifiers */
  if (FORMAT_RE.test(message))
    return new intl_format(message, locale);

  return message;
}

export { getMessage };
