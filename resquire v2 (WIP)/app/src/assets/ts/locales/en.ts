/**
 * en.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


module.exports = {
  SURE:                            'Sure?',
  EDIT:                            'Edit',
  LIKES:                           'Likes',
  VIEWS:                           'Views',
  COMMENTS:                        'Comments',
  CHANGE_PASSWORD:                 'Change password',

  UPGRADE:                         'Upgrade',
  UPGRADE_CARD:                    'Update card details',
  UPGRADE_SELECT:                  'Select a plan',
  UPGRADE_MONTHLY:                 'Monthly',
  UPGRADE_ANNUAL:                  'Annual',
  UPGRADE_CURRENT:                 'Current plan',
  UPGRADE_ADD_DETAILS:             'Add your card details',
  UPGRADE_CHANGE_DETAILS:          'Update your card details',
  UPGRADE_CREDIT_CARD:             'Credit card',
  UPGRADE_CC_NUMBER:               'Card number',
  UPGRADE_NEXT_DATE:               'Next billing date: {date}',
  UPGRADE_SUCCESS:                 'Subscription updated!',
  UPGRADE_ERROR:                   'Uhh.. That didn\'t work. Ensure your card information is up to date and try again.',
  UPGRADE_PAYMENT_ERROR:           'Unfortunately our payment processor is unable to load right now. Please try again later',

  LOGIN:                           'Log in',
  LOGIN_TITLE:                     'Welcome Back!',
  LOGIN_BODY:                      'Sign into your account',

  INVITE_INVALID:                  'Uhh.. Seems like that invite has expired.',
  INVITE_ACCEPT:                   'Accept invite',
  INVITE_TITLE:                    'Welcome!',
  INVITE_BODY:                     'Setup your account',

  LOGOUT:                          'Logout',

  REGISTER_TITLE:                  'Sign up for free',
  REGISTER_BODY:                   'You will love it!',
  REGISTER:                        'Register',
  CONTINUE:                        'Continue',

  NAME:                            'Name',
  EMAIL:                           'Email',
  PASSWORD:                        'Password',
  CAPTION:                         'Caption',
  USER_IMAGE:                      'User Image',
  OLD_PASSWORD:                    'Old password',
  NEW_PASSWORD:                    'New password',

  AUTH_SWITCH_LOGIN:               'Already have an account?',
  AUTH_SWITCH_REGISTER:            'Need an account?',
  AUTH_FORGOT_PASSWORD:            'Forgot Your Password?',
  AUTH_ERROR:                      '',
  AUTH_ERROR_WRONG:                'Please double-check your credentials and try again.',
  AUTH_ERROR_LIMIT:                'Uhh.. Something seems a bit off.',

  UPLOAD:                          'Upload',
  UPLOAD_BODY:                     'Drag & Drop your photos and/or videos to start',
  UPLOAD_DROP:                     'Drop your files to upload',
  UPLOAD_UPLOADING:                'Uploading',
  UPLOAD_ERROR_LIMIT_TITLE:        'That file is massive!',
  UPLOAD_ERROR_LIMIT_HELP:         'Max file size is {maxSize} please.',

  PROCESSING:                      'Processing',
  PROCESSING_ERROR:                'Failed',

  PASSWORD_CHANGE_SUCCESS:         'Password has been changed!',
  PASSWORD_CHANGE:                 'Change password',
  DELETE_ACCOUNT:                  'Delete Account',
  DELETE_ACCOUNT_BODY:             'This will instantly delete your Resquire account and all associated data.<br><br>If you have any active subscription it will be cancelled immediately and you will not be billed going forward.<br><br><strong>This cannot be undone.</strong>',

  ACCOUNT_PROMPT_TITLE:            'Well, this looks empty!',
  ACCOUNT_PROMPT_BODY:             'Get started by connecting a social media account.',
  ACCOUNT_PROMPT_ACTION:           'Add account',

  ACCOUNTS:                        'Accounts',
  ACCOUNTS_USAGE:                  '{used}/{cap} social accounts connected',
  ACCOUNTS_ACCOUNT:                'Account',
  ACCOUNTS_CONNECT:                'Connect a new account',
  ACCOUNTS_ADD:                    'Add account',
  ACCOUNTS_ADD_SUCCESS:            'Social media account added!',

  ACCOUNT_MANAGE:                  'Manage account',
  ACCOUNT_REMOVED:                 'Account removed!',
  ACCOUNT_PLATFORM:                'Platform',
  ACCOUNT_UPDATED:                 'Account updated!',
  ACCOUNT_USERNAME:                '{platform} username',
  ACCOUNT_SCANNING:                'Currently scanning',
  ACCOUNT_LAST_SCAN:               'Last scanned: {when}',

  OWNER:                           'Owner',
  MEMBER:                          'Member',
  OWNER_CAN:                       'Can access billing & team settings',
  MEMBER_CAN:                      'Can evaluate posts',
  MEMBER_INVITED:                  'Invited {email}!',

  INVITED:                         'Invited...',
  INVITE_MEMBER_ADDRESS:           'Enter email address',
  INVITE_MEMBERS:                  'Invite people',
  MEMBERS:                         'People',
  REMOVE:                          'Remove',

  SETTINGS:                        'Settings',
  SETTINGS_ADMIN:                  'Workspace',
  SETTINGS_PROFILE:                'Profile',
  SETTINGS_BILLING:                'Subscription',
  MEMBERS_MANAGE:                  'Manage members',
  BILLING_MANAGE:                  'Manage subscription',
  SESSIONS_TITLE:                  'Sessions',
  SESSIONS_BODY:                   'These are devices that have logged into your account. Revoke any sessions that you do not recognize.',
  SESSIONS_ACCESSED:               'Last accessed {when}',
  SESSIONS_REVOKE_ERROR:           'Failed to revoke session!',

  NAME_CHANGED:                    'Name change saved!',

  CANCEL:                          'Cancel',
  CANCEL_ERROR:                    'We were unable to cancel your subscription. Please try again or reach out to our support team if this problem persists.',

  CREDIT_CARD_ERROR_NUMBER:        'Something is wrong with your credit card number',
  CREDIT_CARD_ERROR_EXPIRATION:    'Something is wrong with your expiration date',
  CREDIT_CARD_ERROR_SECURITY_CODE: 'Something is wrong with your security code',
  CREDIT_CARD_ERROR_ZIP_CODE:      'Something is wrong with your postal code',

  DATEPICKER_SAVE:                 'Save Selected',

  SAVE:                            'Save',
  UPDATING:                        'Updating...',
  LOADING:                         'Getting some info...',

  ERROR:                           'Oops, something went wrong...',
  ERROR_BROWSER_TITLE:             'Well, that is some accient technology',
  ERROR_BROWSER_HELP:              'It looks like you\'re using an unsupported browser.',

  UNAVAILABLE:                     'Currently unavailable',

  TWITTER_DESCRIPTION:             'The home of tweets.',
  INSTAGRAM_DESCRIPTION:           'Share your photos & videos.',


  DAYS_SUN:                        'Sunday',
  DAYS_MON:                        'Monday',
  DAYS_TUE:                        'Tuesday',
  DAYS_WED:                        'Wednesday',
  DAYS_THU:                        'Thursday',
  DAYS_FRI:                        'Friday',
  DAYS_SAT:                        'Saturday',

  DAYS_SHORT_SUN:                  'Sun',
  DAYS_SHORT_MON:                  'Mon',
  DAYS_SHORT_TUE:                  'Tue',
  DAYS_SHORT_WED:                  'Wed',
  DAYS_SHORT_THU:                  'Thu',
  DAYS_SHORT_FRI:                  'Fri',
  DAYS_SHORT_SAT:                  'Sat',

  DATE_TODAY:                      'Today',
  DATE_TOMORROW:                   'Tomorrow',
  DATE_YESTERDAY:                  'Yesterday',
};
