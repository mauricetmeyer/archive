/**
 * main.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import '../img/favicon.png';
import '../img/apple-touch-icon.png';
import '../scss/master.scss';


import { createElement }        from 'react';
import { render }               from 'react-dom';
import { createBrowserHistory } from 'history';

import { App }                  from './views/app';


const root = document.getElementById('wrap') || document.body;
render(<App history={createBrowserHistory()} />, root);
