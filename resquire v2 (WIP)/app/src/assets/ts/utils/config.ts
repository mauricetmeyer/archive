/**
 * config.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


const Config  = new Map<string, string>();
const element = document.getElementById('preload');

if (element)
{
  const raw  = element.textContent || '';
  const data = JSON.parse(raw.replace(/&quot;/g, '"'));
  for (const key in data)
    Config.set(key, data[key]);
}

export { Config };
