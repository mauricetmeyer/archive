/**
 * dispatcher.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { Disposable } from './disposable';


type DispatcherFn<Payload> = (type: string, data?: Payload) => void;
class Dispatcher<Payload>
{
  private lastID:      number;
  private dispatching: boolean;

  private payload?:    Payload;

  private isHandled: Set<string>;
  private isPending: Set<string>;
  private callbacks: Map<string, DispatcherFn<Payload>>;


  /**
   * Create dispatcher.
   *
   * @return {Dispatcher}
   */
  constructor ()
  {
    this.lastID      = 0;
    this.dispatching = false;
    this.isHandled   = new Set();
    this.isPending   = new Set();
    this.callbacks   = new Map();
  }


  /**
   * Dispatch payload
   *
   * @param {string}  type
   * @param {Payload} data
   */
  dispatch (type: string, data?: Payload)
  {
    this.startDispatch(data);
    try {
      for (const id in this.callbacks) {
        if (this.isPending.has(id))
          continue;

        this.isPending.add(id);
        const fn = this.callbacks.get(id);
        if (fn) fn(type, this.payload);
        this.isHandled.add(id);
      }
    } finally {
      this.stopDispatch();
    }
  }


  /**
   * Register new handler
   *
   * @param {}
   */
  attach (fn: DispatcherFn<Payload>) : Disposable
  {
    const id = `${this.lastID++}`;
    this.callbacks.set(id, fn);

    return new Disposable(() => {
      this.callbacks.delete(id);
    });
  }


  /**
   * **INTERNAL**
   * Stop dispatch
   */
  private stopDispatch ()
  {
    delete this.payload;
    this.dispatching = false;
  }

  /**
   * **INTERNAL**
   * Start dispatch
   */
  private startDispatch (data?: Payload)
  {
    /**
     * Clear sets that store the state
     * of each handler. */
    this.isPending.clear();
    this.isHandled.clear();

    /**
     * Save payload and
     * set the state to dispatching. */
    this.payload     = data;
    this.dispatching = true;
  }
}

const dispatcher = new Dispatcher<any>();
export { Dispatcher, dispatcher };
