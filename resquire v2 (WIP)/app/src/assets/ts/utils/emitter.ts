/**
 * emitter.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


type EmitterFn = (...args: any[]) => void;
class Emitter
{
  private handlers: { [key: string]: EmitterFn[] };


  /**
   * Create a new emitter.
   *
   * @return {Emitter}
   */
  constructor ()
  {
    /**
     * Clear will initialize the initial state. */
    this.handlers = {};
  }


  /**
   * Attach to event
   *
   * @param  {string}   event
   * @param  {function} handler
   * @return {Emitter}
   */
  on (event: string, handler: EmitterFn)
  {
    const handlers = this.handlers[event];
    if (handlers)
    {
      /**
       * We already got a few handlers for
       * this event so we'll just push into the array. */
      this.handlers[event].push(handler);
    }
    else
    {
      /**
       * Otherwise we'll just initialize the handlers
       * with the new handle. */
      this.handlers[event] = [handler];
    }

    return this;
  }

  /**
   * Dettach from event
   *
   * @param  {string}   event
   * @param  {function} handler
   * @return {Emitter}
   */
  off (event: string, handler: EmitterFn)
  {
    const handlers = this.handlers[event];
    if (handlers)
    {
      const idx = handlers.indexOf(handler);
      if (-1 !== idx) handlers.splice(idx, 1);
    }

    return this;
  }

  /**
   * Emit an event
   *
   * @param {string} event
   * @param {any[]}  args
   */
  emit (event: string, ...args: any[])
  {
    const handlers = this.handlers[event];
    if (handlers)
    {
      const copy = handlers.slice();
      for (let i = 0; i < copy.length; i++)
        copy[i](...args);
    }
  }

  /**
   * Clear all registered handlers.
   *
   * @param {string} event
   */
  clear (event?: string)
  {
    /**
     * If we got an event we'll clear only the handlers
     * responding to the event. */
    if (event)
    {
      this.handlers[event] = [];
      return;
    }

    /**
     * Otherwise just set a new handlers object. */
    this.handlers = {};
  }
}

export { Emitter };
