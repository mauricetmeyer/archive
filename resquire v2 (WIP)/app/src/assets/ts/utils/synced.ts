/**
 * synced.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


function synced (fn: Function)
{
  let lastId:   number | null = null;
  let lastArgs: any[]         = [];

  return (...args: any[]) => {
    lastArgs = args;
    if (lastId) return;
    lastId = requestAnimationFrame(() => {
      lastId = null;
      fn(...lastArgs);
    });
  };
}

export { synced };
