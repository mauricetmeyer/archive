/**
 * app.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { createElement, Component }  from 'react';
import { History }                   from 'history';
import { Router, Route, Switch }     from 'react-router-dom';

import { Main }                      from './main';
import { Login, Invite, Register }   from './auth';
import { Toasts } from '../components/toasts';


interface AppProps
{
  history: History;
}


class App extends Component<AppProps>
{
  constructor (props: AppProps)
  {
    super(props);
  }


  render()
  {
    const { history } = this.props;
    return (
      <div>
        <Toasts timeout={3e3} />
        <Router history={history}>
          <Switch>
            <Route path="/" component={Main} exact />
            <Route path="/login" component={Login} />
            <Route path="/register" component={Register} />
            <Route path="/invite/:token" component={Invite} />
          </Switch>
        </Router>
      </div>
    );
  }
}

export { App };
