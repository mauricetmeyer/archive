/**
 * auth.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { createElement, Component } from 'react';

class Auth extends Component
{
  render()
  {
    const { children } = this.props;
    return (
      <div className="height--100">
        <div className="center center--vertical wrap--xxs">
          {children}
        </div>
      </div>
    );
  }
}

export { Auth };
