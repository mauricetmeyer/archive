/**
 * login.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { createElement, Component, FormEvent, ChangeEvent } from 'react';
import { RouteComponentProps }                              from 'react-router-dom';

import { i18n }               from '../../i18n';
import { http }               from '../../utils';
import { Endpoints }          from '../../const';

import { Auth }               from './auth';
import { Input, InputSize }   from '../../components/input';
import { Button, ButtonSize } from '../../components/button';


interface InviteState
{
  name?:        string;
  password?:    string;

  isValid:      boolean;
  isLoaded:     boolean;
  isSubmitting: boolean;
}

interface InviteParams
{
  token: string;
}


class Invite extends Component<RouteComponentProps<InviteParams>, InviteState>
{
  state: InviteState = {
    isValid:      false,
    isLoaded:     false,
    isSubmitting: false
  };


  componentDidMount ()
  {
    const { token } = this.props.match.params;
    http.post(Endpoints.INVITE, { token }).then(({ error }) => {
      this.setState({ isLoaded: true, isValid: !error });
    });
  }


  render()
  {
    const { isValid, isLoaded } = this.state;

    return (
      <Auth>
        { isLoaded && isValid ?
          this.renderForm()
        :
          <div className="fontSize--m textAlign--m">
            {isLoaded && i18n.messages.INVITE_INVALID}
          </div>
        }
      </Auth>
    );
  }

  private renderForm ()
  {
    const { isSubmitting } = this.state;

    return (
      <form className="margin--bottom--m" onSubmit={this.handleSubmit.bind(this)}>
        <div className="textAlign--m">
          <h1 className="margin--bottom--xs fontSize--xxxl fontWeight--600 slideIn--up">{i18n.messages.INVITE_TITLE}</h1>
          <h3 className="margin--bottom--l c--grey slideIn--up animDelay--1">{i18n.messages.INVITE_BODY}</h3>
        </div>
        <div className="slideIn--up animDelay--2">
          <Input size={InputSize.Large} onChange={this.handleNameChange.bind(this)} type="text" name="name" placeholder={i18n.messages.NAME} className="margin--bottom--xxs" />
          <Input size={InputSize.Large} onChange={this.handlePasswordChange.bind(this)} type="password" name="password" placeholder={i18n.messages.PASSWORD} className="margin--bottom--m" />
          <Button size={ButtonSize.Large} type="submit" name="name" color="primary" className="width--100" disabled={isSubmitting}>{i18n.messages.INVITE_ACCEPT}</Button>
        </div>
      </form>
    );
  }


  /**
   * **INTERNAL**
   * Handle form submit.
   *
   * @param {FormEvent} ev
   */
  private handleSubmit (ev: FormEvent)
  {
    ev.preventDefault();
    this.setState({ isSubmitting: true });

    const { match, history } = this.props;
    const { name, password } = this.state;
    const { token }          = match.params;

    http.put(Endpoints.INVITE, { token, name, password }).then(({ error }) => {
      this.setState({ isSubmitting: false });
      if (!error) history.push('/');
    });
  }

  /**
   * **INTERNAL**
   * Handle name value change.
   *
   * @param {ChangeEvent} ev
   */
  private handleNameChange (ev: ChangeEvent<HTMLInputElement>)
  {
    const { value } = ev.target;
    this.setState({ name: value });
  }

  /**
   * **INTERNAL**
   * Handle password value change.
   *
   * @param {ChangeEvent} ev
   */
  private handlePasswordChange (ev: ChangeEvent<HTMLInputElement>)
  {
    const { value } = ev.target;
    this.setState({ password: value });
  }
}

export { Invite };
