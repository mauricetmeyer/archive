/**
 * login.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { createElement, Component, FormEvent, ChangeEvent } from 'react';
import { Link, RouteComponentProps }                        from 'react-router-dom';

import { i18n }               from '../../i18n';
import { http }               from '../../utils';
import { Endpoints }          from '../../const';

import { Auth }               from './auth';
import { Input, InputSize }   from '../../components/input';
import { Button, ButtonSize } from '../../components/button';
import { toast, ToastType } from '../../components/toasts';


interface LoginState
{
  email?:       string;
  password?:    string;

  isSubmitting: boolean;
}


class Login extends Component<RouteComponentProps, LoginState>
{
  state: LoginState = {
    isSubmitting: false
  };

  render()
  {
    const { isSubmitting } = this.state;

    return (
      <Auth>
        <form className="margin--bottom--m" onSubmit={this.handleSubmit.bind(this)}>
          <div className="textAlign--m">
            <h1 className="margin--bottom--xs fontSize--xxxl fontWeight--600 slideIn--up">{i18n.messages.LOGIN_TITLE}</h1>
            <h3 className="margin--bottom--l c--grey slideIn--up animDelay--1">{i18n.messages.LOGIN_BODY}</h3>
          </div>
          <div className="slideIn--up animDelay--2">
            <Input size={InputSize.Large} onChange={this.handleEmailChange.bind(this)} type="text" name="email" placeholder={i18n.messages.EMAIL} className="margin--bottom--xxs" />
            <Input size={InputSize.Large} onChange={this.handlePasswordChange.bind(this)} type="password" name="password" placeholder={i18n.messages.PASSWORD} className="margin--bottom--m" />
            <Button size={ButtonSize.Large} type="submit" name="email" color="primary" className="width--100" disabled={isSubmitting}>{i18n.messages.LOGIN}</Button>
          </div>
        </form>
        <div className="textAlign--m slideIn--up animDelay--3">
          <Link className="switch_btn fontSize--s" to="/register">{i18n.messages.AUTH_SWITCH_REGISTER}</Link>
        </div>
      </Auth>
    );
  }


  /**
   * **INTERNAL**
   * Handle form submit.
   *
   * @param {FormEvent} ev
   */
  private handleSubmit (ev: FormEvent)
  {
    ev.preventDefault();

    const { history }         = this.props;
    const { email, password } = this.state;
    this.setState({ isSubmitting: true });
    http.post(Endpoints.LOGIN, { email, password }).then(({ error }) => {
      this.setState({ isSubmitting: false });
      if (error)
      {
        toast({
          type: ToastType.Error,
          message: i18n.messages.ERROR as string
        });

        return;
      }

      history.push('/');
    });
  }

  /**
   * **INTERNAL**
   * Handle email value change.
   *
   * @param {ChangeEvent} ev
   */
  private handleEmailChange (ev: ChangeEvent<HTMLInputElement>)
  {
    const { value } = ev.target;
    this.setState({ email: value });
  }

  /**
   * **INTERNAL**
   * Handle password value change.
   *
   * @param {ChangeEvent} ev
   */
  private handlePasswordChange (ev: ChangeEvent<HTMLInputElement>)
  {
    const { value } = ev.target;
    this.setState({ password: value });
  }
}

export { Login };
