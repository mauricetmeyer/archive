/**
 * carousel.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { createElement, Component, MouseEvent, Fragment } from 'react';
import { Transition }                           from 'react-transition-group';
import * as _                                   from 'underscore';
import * as classnames                          from 'classnames';

import { Measure, Button, ButtonSize, ButtonColor } from '../../components';

import { Media }                    from './media';
import { CarouselItem }             from './carousel_item';
import { Dropzone }                 from './dropzone';
import { i18n } from '../../i18n';


interface CarouselProps
{
  media:         Media[];
  className?:    string;

  onMediaAdd:    (media: Media) => void;
  onMediaRemove: (media: Media) => void;
}

interface CarouselState
{
  size:       number;
  offset:     number;
  current:    number;

  isDragging: boolean;
}


class Carousel extends Component<CarouselProps, CarouselState>
{
  state = {
    size:       0,
    offset:     0,
    current:    0,

    isDragging: false,
  };


  private input?: HTMLInputElement;


  private prev ()
  {
    const { current } = this.state;

    Math.max(0, current - 1);
  }

  private next ()
  {
    const { media }   = this.props;
    const { current } = this.state;

    Math.min(media.length - 1, current + 1);
  }

  private show (idx: number)
  {
    this.setState(prev => ({ current: idx, offset: idx * prev.size }));
  }


  render ()
  {
    const { offset }           = this.state;
    const { media, className } = this.props;

    return (
      <div className={classnames('carousel', className)}>
        <div className="slideIn--up carousel--items">
          <Dropzone
            onFiles={this.handleFiles.bind(this)}
            onDragEnter={this.handleDragEnter.bind(this)}
            onDragLeave={this.handleDragLeave.bind(this)}>
            <Measure onChange={this.handleSizeChange.bind(this)} >
                <div
                  className="flex carousel--items--wrap"
                  style={{ transform: `translateX(${-offset}px)` }}>
                  { !media.length
                    ? this.renderEmpty()
                    : media.map((m, i) => <CarouselItem key={i} media={m} />)
                  }
                </div>
            </Measure>
          </Dropzone>
        </div>
        <Transition in={media.length > 1} timeout={300} mountOnEnter unmountOnExit>
          <div className="carousel--thumbnails flex margin--top--s">
            { media.map((m, i) =>
              <div
                key={i}
                role="button"
                className="carousel--thumbnail margin--right--s"
                onClick={this.handleThumbnailClick.bind(this, m)}>
                <img
                  alt="Thumbnail"
                  src={m.thumbnailSrc}
                  className="borderRadius--l" />
              </div>
            )}
          </div>
        </Transition>
      </div>
    );
  }

  private renderEmpty ()
  {
    const { isDragging } = this.state;

    return (
      <div className="carousel--placeholder textAlign--m width--100">
        <div className="center--vertical">
          { !isDragging ?
            <Fragment>
              <div style={{ display: "none", position: "absolute" }}>
                <input type="file" style={{ position: "absolute", opacity: 0 }} multiple ref={r => r && (this.input = r)} onChange={this.handleChange.bind(this)} />
              </div>
              <p className="margin--bottom--l pad--leftRight--xl fontSize--l slideIn--up">{i18n.messages.UPLOAD_BODY}</p>
              <Button onClick={this.handleClick.bind(this)} size={ButtonSize.Large} color={ButtonColor.Primary}>{i18n.messages.UPLOAD}</Button>
            </Fragment>
          :
            <div className="pad--leftRight--xl slideIn--up">
              <p className="fontSize--l c--grey">{i18n.messages.UPLOAD_DROP}</p>
            </div>
          }
        </div>
      </div>
    );
  }


  private handleSizeChange (w: number)
  {
    this.setState(prev => ({
      size:   w,
      offset: prev.current * w
    }));
  }

  private handleThumbnailClick (media: Media)
  {
    const idx = _.indexOf(this.props.media, media);
    if (-1 === idx) return;
    this.show(idx);
  }


  private handleClick (ev: MouseEvent)
  {
    ev.preventDefault();
    if (this.input) this.input.click();
  }

  private handleChange ()
  {
    /**
     * Make sure that the input is actually
     * rendered and accessible. */
    if (this.input)
    {
      const { files } = this.input;
      if (files && files.length)
        this.handleFiles(files);
    }
  }

  private handleFiles (files: FileList)
  {
    this.setState({ isDragging: false });
    const transformed: Media[] = [];
    for (let i = 0; i < files.length; i++)
    {
      const media = new Media(files[i]);
      media.load().then(() => this.props.onMediaAdd(media));
    }
  }

  private handleDragEnter ()
  {
    this.setState({ isDragging: true });
  }

  private handleDragLeave ()
  {
    this.setState({ isDragging: false });
  }


  private handleReposition ()
  {
    // this.items.wrap.$el.css({ transform: `translateX(${-this.offset}px)` });
    // window.requestAnimationFrame(this.__reposition.bind(this));
  }
}

export { Carousel };
