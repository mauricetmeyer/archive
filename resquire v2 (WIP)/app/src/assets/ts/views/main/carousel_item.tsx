/**
 * carousel_item.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { createElement, Component, Fragment } from 'react';

import { i18n }                     from '../../i18n';

import { Media }                    from './media';
import { Player }                   from './player';
import { CarouselNote }             from './carousel_note';


interface CarouselItemProps
{
  media: Media;
}

interface CarouselItemState
{
  isLoaded:  boolean;
  isErrored: boolean;
}

class CarouselItem extends Component<CarouselItemProps, CarouselItemState>
{
  state = {
    isLoaded:  false,
    isErrored: false
  };


  componentDidMount ()
  {
    const { media } = this.props;
    media.process()
      .then(({ data, error }) => {
        if (error) this.handleError();
        this.handleLoad(data);
      })
      .catch(() => this.handleError());
  }


  render()
  {
    return (
      <div className="carousel--item">
        {this.renderMedia()}
        {this.renderNotes()}
      </div>
    );
  }


  private renderMedia ()
  {
    const { media } = this.props;

    return (
      <div className="carousel--item--post">
        { media.type === 'image' ? <img src={media.thumbnailSrc} alt="Post" />
                                 : <Player media={media} /> }
      </div>
    );
  }

  private renderNotes ()
  {
    /**
     * Get media prop and display
     * information depending on the state. */
    const { media }               = this.props;
    const { isLoaded, isErrored } = this.state;

    return (
      <div className="absolute--bottom width--100">
        <div className="pad--s cf">
          { media.type === 'video' &&
            <CarouselNote>
              {this.secondsToHuman(media.duration as number)}
            </CarouselNote>
          }

          { !isLoaded &&
            <CarouselNote bg={isErrored ? 'bg--red' : undefined}>
              { !isErrored ?
                <Fragment>
                  <div className="loader loader--s margin--right--xxs"></div>
                  <span>{ i18n.messages.PROCESSING }</span>
                </Fragment>
              : i18n.messages.PROCESSING_ERROR }
            </CarouselNote>
          }
        </div>
      </div>
    );
  }


  private handleLoad (data: any)
  {
    this.setState({ isLoaded: true });
  }

  private handleError ()
  {
    this.setState({ isErrored: true });
  }


  private secondsToHuman (secs: number)
  {
    const { hours, minutes, seconds } = this.secondsToTimes(secs);

    const hoursFmt   = hours < 10   ? `0${hours}`   : hours;
    const minutesFmt = minutes < 10 ? `0${minutes}` : minutes;
    const secondsFmt = seconds < 10 ? `0${seconds}` : seconds;

    const human = `${minutesFmt}:${secondsFmt}`;

    return !!hours ? `${hoursFmt}:${human}` : human;
  }

  private secondsToTimes (secs: number)
  {
    const minutes = secs / 60;
    const hours   = minutes / 60;
    return {
      hours:   Math.round(hours),
      minutes: Math.round(minutes % 60),
      seconds: Math.round(secs % 60)
    };
  }
}

export { CarouselItem };
