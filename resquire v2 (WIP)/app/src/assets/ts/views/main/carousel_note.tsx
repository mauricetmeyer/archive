/**
 * carousel_note.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { createElement, ReactElement, PropsWithChildren } from 'react';
import * as classnames                                    from 'classnames';


type CarouselNoteProps = PropsWithChildren<{ bg?: string }>;
const CarouselNote: (props: CarouselNoteProps) => ReactElement = ({ bg, children }) => (
  <div className={classnames(bg ? bg : 'bg--black', 'borderRadius--m margin--left--xs right')}>
    <div className="pad--leftRight--xs pad--topBottom--xxs c--white fontSize--s fontWeight--600">
      {children}
    </div>
  </div>
);

export { CarouselNote };
