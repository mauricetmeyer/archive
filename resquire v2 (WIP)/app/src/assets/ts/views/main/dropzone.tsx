/**
 * dropzone.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { createElement, Component, DragEvent } from 'react';
import { Media } from './media';


interface DropzoneProps
{
  accept?:      string | string[];
  multiple?:    boolean;

  onFiles:      (files: FileList) => void;
  onDragEnter?: () => void;
  onDragLeave?: () => void;
}

class Dropzone extends Component<DropzoneProps>
{
  counter = 0;


  render ()
  {
    const { children, onFiles, ...props } = this.props;

    return (
      <div {...props}
        onDrop={this.handleDrop.bind(this)}
        onDragEnd={e => e.preventDefault()}
        onDragOver={e => e.preventDefault()}
        onDragStart={e => e.preventDefault()}
        onDragEnter={this.handleDragEnter.bind(this)}
        onDragLeave={this.handleDragLeave.bind(this)}>
        {children}
      </div>
    );
  }


  private handleDrop (ev: DragEvent)
  {
    const { onFiles } = this.props;
    this.counter = 0;

    ev.preventDefault();
    ev.stopPropagation();

    const transfer = ev.dataTransfer;
    if (!transfer) return;

    const { files } = transfer;

    /**
     * @NOTE (Maurice):
     * This would allow us to work with folders,
     * but I'll leave it out for now.
     *
     * if (items && items[0].webkitGetAsEntry)
     * {
     *   if (!this.__checkFileLimit(items.length))
     *     return;
     *
     *   const len = items.length;
     *   for (let i = 0; i < len; i++)
     *   {
     *     const item = items[i].webkitGetAsEntry();
     *     if (item.isDirectory)
     *     {
     *     }
     *
     *     item.file(f => {});
     *   }
     * }
     * else
     */
    if (files && files.length)
      onFiles(files);
  }

  private handleDragEnter ()
  {
    const { onDragEnter } = this.props;

    ++this.counter;
    if (this.counter === 1)
      onDragEnter && onDragEnter();
  }

  private handleDragLeave ()
  {
    const { onDragLeave } = this.props;

    --this.counter;
    if (this.counter === 0)
      onDragLeave && onDragLeave();
  }
}

export { Dropzone };
