/**
 * dropzone.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { createElement, Component, MouseEvent, createRef } from 'react';

import { i18n }                                            from '../../i18n';
import { Button, ButtonSize, ButtonColor }                 from '../../components/button';


interface DropzoneProps
{
  accept?:   string | string[];
  multiple?: boolean;

  onFiles: (files: FileList) => void;
}

interface DropzoneState
{
  isDragging: boolean;
}


class Dropzone extends Component<DropzoneProps, DropzoneState>
{
  public state = {
    isDragging: false
  };

  private fileInput = createRef<HTMLInputElement>();


  render ()
  {
    const { isDragging } = this.state;

    return (
      <div className="textAlign--m">
        <div className="wrap--xs width--100">
          { isDragging ? this.renderDrop() : this.renderInfo() }
        </div>
      </div>
    );
  }

  private renderInfo ()
  {
    const { multiple } = this.props;

    return (
      <div>
        <div style={{ display: "none", position: "absolute" }}>
          <input type="file" style={{ position: "absolute", opacity: 0 }} multiple={multiple} ref={this.fileInput} onChange={this.handleChange.bind(this)} />
        </div>
        <p className="margin--bottom--l pad--leftRight--xl fontSize--l c--grey slideIn--up">{i18n.messages.UPLOAD_BODY}</p>
        <Button onClick={this.handleClick.bind(this)} size={ButtonSize.Large} color={ButtonColor.Primary}>{i18n.messages.UPLOAD}</Button>
      </div>
    );
  }

  private renderDrop ()
  {
    return (
      <div>
        <p className="pad--leftRight--xl fontSize--l c--grey">{i18n.messages.UPLOAD_DROP}</p>
      </div>
    );
  }


  componentDidMount ()
  {
    window.addEventListener('drop',      this.handleDrop.bind(this));
    window.addEventListener('dragenter', this.handleDragEnter.bind(this));
    window.addEventListener('dragleave', this.handleDragLeave.bind(this));
  }

  componentWillUnmount ()
  {
    window.removeEventListener('drop',      this.handleDrop.bind(this));
    window.removeEventListener('dragenter', this.handleDragEnter.bind(this));
    window.removeEventListener('dragleave', this.handleDragLeave.bind(this));
  }


  private handleDrop (ev: DragEvent)
  {
    const { onFiles } = this.props;

    ev.preventDefault();
    ev.stopPropagation();

    const transfer = ev.dataTransfer;
    if (!transfer) return;

    const { files } = transfer;

    /**
     * @NOTE (Maurice):
     * This would allow us to work with folders,
     * but I'll leave it out for now.
     *
     * if (items && items[0].webkitGetAsEntry)
     * {
     *   if (!this.__checkFileLimit(items.length))
     *     return;
     *
     *   const len = items.length;
     *   for (let i = 0; i < len; i++)
     *   {
     *     const item = items[i].webkitGetAsEntry();
     *     if (item.isDirectory)
     *     {
     *     }
     *
     *     item.file(f => {});
     *   }
     * }
     * else
     */
    if (files && files.length)
      onFiles(files);
  }

  private handleClick (ev: MouseEvent)
  {
    ev.preventDefault();
    if (this.fileInput.current)
      this.fileInput.current.click();
  }

  private handleChange ()
  {
    const { onFiles } = this.props;

    /**
     * Make sure that the input is actually
     * rendered and accessible. */
    const input = this.fileInput.current;
    if (input)
    {
      const { files } = input;
      if (files && files.length)
        onFiles(files);
    }
  }


  private handleDragEnter ()
  {
    this.setState({ isDragging: true });
  }

  private handleDragLeave ()
  {
    this.setState({ isDragging: false });
  }
}

export { Dropzone };
