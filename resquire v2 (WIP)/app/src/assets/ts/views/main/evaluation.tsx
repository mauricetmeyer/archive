/**
 * evaluation.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { createElement, Component, Fragment } from 'react';
import * as moment                            from 'moment';
import * as _                                 from 'underscore';

import { i18n }                               from '../../i18n';
import { Graph, Caption, DateTime }           from '../../components';

import { Media }                              from './media';
import { Carousel }                           from './carousel';


interface EvaluationProps
{
}

interface EvaluationState
{
  media:       Media[];
  caption:     string;
  datetime:    moment.Moment;

  isEvaluated: boolean;
}


class Evaluation extends Component<EvaluationProps, EvaluationState>
{
  constructor (props: EvaluationProps)
  {
    super(props);

    /**
     * Set time to use for datetime. */
    const tomorrow  = moment().add(1, 'd');
    const remainder = 30 - (tomorrow.minute() % 30);
    const datetime  = tomorrow.add(remainder, 'minutes');

    this.state = {
      datetime,
      media:       [],
      caption:     '',

      isEvaluated: true
    };
  }


  render ()
  {
    const { media, isEvaluated } = this.state;

    return (
      <div className="wrap--xs width--100 pad--top--l slideIn--top">
        <Carousel
          media={media}
          className="margin--bottom--m"
          onMediaAdd={this.handleMediaAdd.bind(this)}
          onMediaRemove={this.handleMediaRemove.bind(this)} />
        { media.length > 0 && this.renderInputs() }
        { isEvaluated && this.renderResults() }
      </div>
    );
  }

  renderInputs ()
  {
    const { caption, datetime } = this.state;

    return (
      <Fragment>
        <Caption className="margin--bottom--xs fontSize--m c--dark" value={caption} placeholder={i18n.messages.CAPTION as string} onChange={this.handleCaptionChange.bind(this)} />
        <div className="cf pad--top--xs border--top border--dashed">
          <DateTime value={datetime} onChange={this.handleDateTimeChange.bind(this)} />
        </div>
      </Fragment>
    );
  }

  renderResults ()
  {
    const testStyle = {
      height:       '6px',
      borderRadius: '3px',
      width:        '55%'
    };

    const values     = 21;
    const testValues = [];
    for (let i = 0; i < values; i++)
      testValues.push([Math.random() * 120]);

    console.log(testValues);

    return (
      <div className="pad--topBottom--l bg--grey_tint borderRadius--l margin--top--xl">
        <div className="cf margin--bottom--l">
          <div className="col--6">
            <div className="pad--leftRight--l">
              <h2 className="fontSize--xxl c--black">263</h2>
              <p className="fontSize--s c--grey">{i18n.messages.LIKES}</p>
            </div>
          </div>
          <div className="col--6">
            <div className="pad--leftRight--l">
              <h2 className="fontSize--xxl c--black">23</h2>
              <p className="fontSize--s c--grey">{i18n.messages.COMMENTS}</p>
            </div>
          </div>
        </div>
        <div className="pad--leftRight--l">
          <div className="inline margin--bottom--m">
            <div className="fontSize--s c--grey">3am</div>
            <div className="fontSize--s c--grey">6am</div>
            <div className="fontSize--s c--grey">9am</div>
            <div className="fontSize--s c--grey">12pm</div>
            <div className="fontSize--s c--grey">3pm</div>
            <div className="fontSize--s c--grey">6pm</div>
            <div className="fontSize--s c--black">9pm</div>
          </div>
          <Graph colors={['#5968ED']} values={testValues} />
        </div>
      </div>
    );
  }


  private handleMediaAdd (media: Media)
  {
    this.setState(prev => ({ media: prev.media.concat(media) }));
  }

  private handleMediaRemove (media: Media)
  {
    this.setState(prev => ({ media: _.without(prev.media, media) }));
  }


  private handleCaptionChange (caption: string)
  {
    this.setState({ caption });
  }

  private handleDateTimeChange (datetime: moment.Moment)
  {
    this.setState({ datetime });
  }
}

export { Evaluation };
