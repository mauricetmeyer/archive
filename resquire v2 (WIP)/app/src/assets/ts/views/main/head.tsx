/**
 * head.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { createElement, Component, MouseEvent }    from 'react';
import { Link }                                    from 'react-router-dom';
import * as classnames                             from 'classnames';

import { i18n }                                    from '../../i18n';
import { Popover, PopoverOriginV, PopoverOriginH } from '../../components';
import { Switcher }                                from '../../components/switcher';


interface HeadProps
{
  user:              any;
  accounts:          any[];

  onLogoutClick:        (ev: MouseEvent) => void;
  onMembersClick:       (ev: MouseEvent) => void;
  onSettingsClick:      (ev: MouseEvent) => void;
  onAddAccountClick:    (ev: MouseEvent) => void;
  onAccountOptionClick: (index: number)  => void;
}

interface HeadState
{
  isPopoverOpen: boolean;
}


class Head extends Component<HeadProps, HeadState>
{
  state = {
    isPopoverOpen: false
  };


  render()
  {
    const { user, accounts } = this.props;
    const { isPopoverOpen }  = this.state;

    const isAdmin            = user.membership.role === 'admin';

    return (
      <div className="head pad--m relative zindex--50 slideIn--down">
        <div className="inline">
          <div className="inline--grow inline">
            <div>
              <Link to="/" className="logo colored center--vertical" />
            </div>
            <div className="margin--left--xs inline--grow">
              { !!accounts.length &&
                <Switcher
                  accounts={accounts}
                  onAddClick={this.handleAddClick.bind(this)}
                  onOptionClick={this.props.onAccountOptionClick} />
              }
            </div>
          </div>
          <div>
            <div className="margin--left--xs relative">
              <a className={classnames('options', { active: isPopoverOpen })} onClick={this.handleMoreClick.bind(this)}>
                <i className="icon more" />
              </a>
              <Popover origin={{vertical: PopoverOriginV.Bottom, horizontal: PopoverOriginH.Right}} open={isPopoverOpen} onBlur={this.handlePopoverBlur.bind(this)} >
                <ul className="pad--xs">
                  <li onClick={this.handleSettingsClick.bind(this)} className="pad--topBottom--xxs pad--leftRight--xs">
                    <span className="fontSize--m">{i18n.messages.SETTINGS}</span>
                  </li>
                  { isAdmin &&
                    <li onClick={this.handleMembersClick.bind(this)} className="pad--topBottom--xxs pad--leftRight--xs">
                      <span className="fontSize--m">{i18n.messages.MEMBERS}</span>
                    </li>
                  }
                  <li onClick={this.handleLogoutClick.bind(this)} className="pad--topBottom--xxs pad--leftRight--xs">
                    <span className="fontSize--m c--red">{i18n.messages.LOGOUT}</span>
                  </li>
                </ul>
              </Popover>
            </div>
          </div>
        </div>
      </div>
    );
  }


  /**
   * **INTERNAL**
   * Handle user click. */
  private handleAddClick (ev: MouseEvent)
  {
    ev.preventDefault();
    this.props.onAddAccountClick(ev);
  }

  /**
   * **INTERNAL**
   * Handle user click. */
  private handleMoreClick ()
  {
    /**
     * Just change the state. */
    this.setState(prev => ({ isPopoverOpen: !prev.isPopoverOpen }));
  }

  /**
   * **INTERNAL**
   * Handle logout click. */
  private handleLogoutClick (ev: MouseEvent)
  {
    ev.preventDefault();
    this.props.onLogoutClick(ev);
  }

  /**
   * **INTERNAL**
   * Handle members click. */
  private handleMembersClick (ev: MouseEvent)
  {
    ev.preventDefault();
    this.props.onMembersClick(ev);
  }

  /**
   * **INTERNAL**
   * Handle settings click. */
  private handleSettingsClick (ev: MouseEvent)
  {
    ev.preventDefault();
    this.props.onSettingsClick(ev);
  }

  /**
   * **INTERNAL**
   * Handle settings click. */
  private handlePopoverBlur ()
  {
    this.setState({ isPopoverOpen: false });
  }
}

export { Head };
