/**
 * main.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { createElement, Component }        from 'react';
import { RouteComponentProps }             from 'react-router';

import { i18n }                            from '../../i18n';
import { http }                            from '../../utils';
import { Endpoints }                       from '../../const';

import { MembersModal }                    from '../../components/members_modal';
import { SettingsModal, AddAccountModal, Button, ButtonSize, ButtonColor }  from '../../components';

import { Head }                            from './head';
import { User, IUser }                     from './user';
import { Workspace, IWorkspace, IAccount } from './workspace';
import { Evaluation }                      from './evaluation';
import { AccountModal }                    from '../../components/account_modal';


interface MainState
{
  user?:               IUser;
  workspace?:          IWorkspace;

  isUserLoaded:        boolean;
  isWorkspaceLoaded:   boolean;

  isShowMembers:       boolean;
  isShowSettings:      boolean;
  isShowAddAccount:    boolean;
  isShowManageAccount: number;
}

class Main extends Component<RouteComponentProps, MainState>
{
  state: MainState = {
    isUserLoaded:        false,
    isWorkspaceLoaded:   false,

    isShowMembers:       false,
    isShowSettings:      false,
    isShowAddAccount:    false,
    isShowManageAccount: -1
  };


  render()
  {
    const { isUserLoaded, isWorkspaceLoaded } = this.state;

    return (
      <div>
        <User onLoad={this.handleUserLoad.bind(this)} />
        <Workspace onLoad={this.handleWorkspaceLoad.bind(this)} />
        { (isUserLoaded && isWorkspaceLoaded) ? this.renderApp() :
          <div className="width--100 height--100 absolute">
            <div className="center--vertical">
              <div className="flex flex--center">
                <div className="loader loader--l loader--dark" />
              </div>
            </div>
          </div>
        }
      </div>
    );
  }

  private renderApp ()
  {
    const {
      user,
      workspace,

      isShowMembers,
      isShowSettings,
      isShowAddAccount,
      isShowManageAccount
    } = this.state;

    const isAdmin    = user && user.membership.role === 'admin';
    const hasAccount = workspace && !!workspace.accounts.length;

    return (
      <div>
        <Head
          user={user}
          accounts={workspace ? workspace.accounts : []}
          onLogoutClick={this.handleLogoutClick.bind(this)}
          onMembersClick={this.handleMembersClick.bind(this)}
          onSettingsClick={this.handleSettingsClick.bind(this)}
          onAddAccountClick={this.handleAddAccountClick.bind(this)}
          onAccountOptionClick={this.handleAccountOptionClick.bind(this)} />

        { hasAccount ? <Evaluation /> : this.renderEmpty() }

        <MembersModal
          user={user}
          isOpen={isShowMembers}
          onClose={this.handleMembersClose.bind(this)} />

        <SettingsModal
          user={user}
          isOpen={isShowSettings}
          onClose={this.handleSettingsClose.bind(this)}
          onUserChange={this.handleUserChange.bind(this)}
          onDeleteAccountClick={this.handleDeleteAccountClick.bind(this)} />

        { isAdmin && workspace && workspace.accounts &&
          <AddAccountModal
            isOpen={isShowAddAccount}
            onClose={this.handleAddAccountClose.bind(this)}
            onAccountAdded={this.handleAccountAdded.bind(this)} />
        }

        { isAdmin && workspace && workspace.accounts &&
          <AccountModal
            isOpen={isShowManageAccount !== -1}
            account={workspace.accounts[isShowManageAccount]}
            onChange={this.handleAccountChanged.bind(this)}
            onRemove={this.handleAccountRemoved.bind(this)}
            onClose={this.handleAccountOptionClose.bind(this)} />
        }
      </div>
    );
  }

  private renderEmpty ()
  {
    return (
      <div className="wrap--xxs width--100 pad--top--l slideIn--top">
        <div className="textAlign--m">
          <h3 className="fontSize--l margin--bottom--xxs">
            {i18n.messages.ACCOUNT_PROMPT_TITLE}
          </h3>
          <p className="fontSize--m c--grey margin--bottom--m">
            {i18n.messages.ACCOUNT_PROMPT_BODY}
          </p>
          <Button size={ButtonSize.Medium} color={ButtonColor.Primary} onClick={this.handleAddAccountClick.bind(this)}>
            {i18n.messages.ACCOUNT_PROMPT_ACTION}
          </Button>
        </div>
      </div>
    );
  }


  private handleUserLoad (user: IUser)
  {
    this.setState({ user, isUserLoaded: true });
  }

  private handleUserChange (user: IUser)
  {
    this.setState({ user });
  }


  private handleAccountAdded (account: IAccount)
  {
    const { workspace } = this.state;
    if (workspace)
    {
      workspace.accounts.push(account);
      this.setState({ workspace });
    }
  }

  private handleAccountRemoved (account: IAccount)
  {
    const { workspace } = this.state;
    if (workspace)
    {
      workspace.accounts = workspace.accounts.filter(a => a._id !== account._id);
      this.setState({ workspace });
    }
  }

  private handleAccountChanged (account: IAccount)
  {
    const { workspace } = this.state;
    if (workspace)
    {
      workspace.accounts = workspace.accounts.map(a => {
        if (a._id === account._id) return account;
        return a;
      });

      this.setState({ workspace });
    }
  }


  private handleWorkspaceLoad (workspace: IWorkspace)
  {
    this.setState({ workspace, isWorkspaceLoaded: true });
  }


  private handleMembersClose ()
  {
    this.setState({ isShowMembers: false });
  }

  private handleMembersClick ()
  {
    this.setState({ isShowMembers: true });
  }


  private handleAddAccountClose ()
  {
    this.setState({ isShowAddAccount: false });
  }

  private handleAddAccountClick ()
  {
    this.setState({ isShowAddAccount: true });
  }

  private handleAccountOptionClick (idx: number)
  {
    this.setState({ isShowManageAccount: idx });
  }

  private handleAccountOptionClose ()
  {
    this.setState({ isShowManageAccount: -1 });
  }


  private handleLogoutClick ()
  {
    const { history } = this.props;
    http.post(Endpoints.LOGOUT).then(({ error }) => {
      if (error) return;
      history.push('/login');
    });
  }


  private handleSettingsClose ()
  {
    this.setState({ isShowSettings: false });
  }

  private handleSettingsClick ()
  {
    this.setState({ isShowSettings: true });
  }

  private handleDeleteAccountClick ()
  {
    const { history } = this.props;
    http.delete(Endpoints.WORKSPACE).then(({ error }) => {
      if (error) return;
      history.push('/login');
    });
  }
}

export { Main };
