/**
 * media.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { http }      from '../../utils';
import { Endpoints } from '../../const';


type MediaType = 'video' | 'image';
class Media
{
  public readonly file:           File;
  public readonly mime:           string;
  public readonly type:           MediaType;

  private         _width:         number;
  private         _height:        number;
  private         _duration?:     number;

  private         _objectSrc?:    string;
  private         _thumbnailSrc?: string;


  constructor (file: File)
  {
    this.file    = file;
    this.mime    = file.type;
    this.type    = file.type.split('/', 1).toString().toLowerCase() as MediaType;
    this._width  = 0;
    this._height = 0;
  }


  /**
   * Create ``blob:`` URL
   * that can be used as a source.
   *
   * @return {string}
   */
  createUrl ()
  {
    /**
     * We will only create one
     * when there isn't one already. */
    if (!this._objectSrc)
    {
      this._objectSrc = URL.createObjectURL(this.file);
    }

    return this._objectSrc;
  }

  /**
   * Revoke ``blob:`` URL
   * so that it frees the used memory. */
  revokeUrl ()
  {
    /**
     * Only cleanup if there is one already being used */
    if (!this._objectSrc)
      return;

    URL.revokeObjectURL(this._objectSrc);
  }


  /**
   * Read media and get a thumbnail
   * from the file.
   *
   * @return {Promise}
   */
  load ()
  {
    return new Promise((res, rej) => {
      // @ts-ignore
      if (!window.FileReader) /*  */
        return;

      switch (this.type)
      {
        /**
         * Handle image files. */
        case 'image': {
          /**
           * Create a FileReader */
          const reader = new FileReader();

          /**
           * Register some callbacks. */
          reader.onload = () => {
            const src  = reader.result as string;
            const node = new Image();

            node.onload = () => {
              this._width        = node.naturalWidth;
              this._height       = node.naturalHeight;
              this._thumbnailSrc = src;
              res();
            };

            node.onerror = (_, err) => rej(err);
            node.src = src;
          };

          reader.onerror = () => rej('Failed to read media file.');
          reader.readAsDataURL(this.file);
          break;
        }

        /**
         * Handle video files.
         *
         * @NOTE (Maurice):
         * Later on we can extend this one to also
         * take audio files. */
        case 'video': {
          const node    = document.createElement(this.type);
          const canPlay = node.canPlayType(this.mime);
          if (canPlay !== '')
          {
            /**
             * Create sourceFile */
            const src = this.createUrl();

            /**
             * Offset time as the first frame could be blank. */
            node.muted       = true;
            node.currentTime = 1;

            /**
             * Register some callbacks. */
            node.onerror      = (_, err) => rej(err);
            node.onloadeddata = () => {
              /**
               * Save some infos from the video source. */
              this._width    = node.videoWidth;
              this._height   = node.videoHeight;
              this._duration = node.duration;

              /**
               * Create canvas */
              const canvas  = document.createElement('canvas');
              const context = canvas.getContext('2d');
              if (context)
              {
                /**
                 * Set canvas size. */
                canvas.width  = node.videoWidth;
                canvas.height = node.videoHeight;

                /**
                 * Draw image and fill the media. */
                context.drawImage(node, 0, 0, canvas.width, canvas.height);
                this._thumbnailSrc = canvas.toDataURL();

                /**
                 * Revoke ObjectURL to prevent
                 * a memory leak. */
                // this.revokeUrl();

                /**
                 * resolve with Media. */
                return res();
              }

              /**
               * Reject with an error. */
              rej('Failed to create a canvas context.');
            };

            /**
             * Assign src to the media element */
            node.src = src;
          }
          break;
        }

        /**
         * Why would you upload anything that
         * is not a media file?! */
        default:
          rej('Unsupported media type.');
          break;
      }
    });
  }

  /**
   * Let the recog service process the media file.
   *
   * @return {Promise}
   */
  process ()
  {
    return http.post(Endpoints.PROCESS, this.file).then(({ data, error }) => {
      /**
       * If an error happens we'll report it and stop right there. */
      if (error)
      {
        throw 'Failed to process the file!';
      }

      return data;
    });
  }


  get width ()
  {
    return this._width;
  }

  get height ()
  {
    return this._height;
  }

  get duration ()
  {
    return this._duration;
  }


  get thumbnailSrc ()
  {
    return this._thumbnailSrc;
  }
}

export { Media };
