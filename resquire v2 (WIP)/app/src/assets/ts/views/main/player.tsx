/**
 * player.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { createElement, Component, MouseEvent } from 'react';
import { Media }                                from './media';


interface PlayerProps
{
  media: Media;
}


class Player extends Component<PlayerProps>
{
  private el?: HTMLVideoElement;


  render()
  {
    const { media } = this.props;

    return (
      <div className="player">
        <video
          loop
          muted
          playsInline
          src={media.createUrl()}
          ref={r => r && (this.el = r)}
          poster={media.thumbnailSrc} />
        <div
          className="player--controls"
          onClick={this.handleClick.bind(this)}
          onMouseOut={this.handleMouseOut.bind(this)}
          onMouseOver={this.handleMouseOver.bind(this)} />
      </div>
    );
  }


  stop ()
  {
    if (this.el)
      this.el.pause();
  }

  play ()
  {
    if (this.el)
      this.el.play();
  }

  pause ()
  {
    if (this.el)
      this.el.pause();
  }

  toggle ()
  {
    if (this.el)
      this.el.paused ? this.play() : this.pause();
  }


  mute ()
  {
    if (this.el)
      this.el.muted = true;
  }

  unmute ()
  {
    if (this.el)
      this.el.muted = false;
  }

  toggleAudio ()
  {
    if (this.el)
      this.el.muted ? this.unmute() : this.mute();
  }

  componentWillUnmount ()
  {
    const { media } = this.props;
    media.revokeUrl();
  }


  private handleClick (ev: MouseEvent)
  {
    ev.preventDefault();
    this.toggleAudio();
  }

  private handleMouseOut (ev: MouseEvent)
  {
    ev.preventDefault();
    this.pause();
  }

  private handleMouseOver (ev: MouseEvent)
  {
    ev.preventDefault();
    this.play();
  }
}

export { Player };
