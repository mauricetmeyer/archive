/**
 * preview.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { createElement, Component } from 'react';
import { Media }                    from './media';
import { Player } from './player';


interface PreviewProps
{
  media: Media;
}

interface PreviewState
{

}


class Preview extends Component<PreviewProps, PreviewState>
{
  render()
  {
    return (
      <div className="preview">
        {this.renderMedia()}
        {this.renderNotes()}
      </div>
    );
  }


  private renderMedia ()
  {
    const { media } = this.props;

    return (
      <div>
        { media.type === 'image' ? <img src={media.thumbnailSrc} alt="Post" />
                                 : <Player media={media} /> }
      </div>
    );
  }

  private renderNotes ()
  {
    /**
     * Get media prop and display
     * information depending on the state. */
    const { media } = this.props;

    return (
      <div className="absolute--bottom cf width--100">
        <div className="pad--s right">
          <div className="bg--black borderRadius--m">
            <p className="pad--leftRight--xs pad--topBottom--xxs c--white fontSize--s fontWeight--600">{}</p>
          </div>
        </div>
      </div>
    );
  }
}

export { Preview };
