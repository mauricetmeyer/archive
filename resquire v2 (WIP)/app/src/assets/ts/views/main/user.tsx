/**
 * user.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { createElement, Component } from 'react';
import { http }                     from '../../utils';
import { Endpoints }                from '../../const';


interface IUser
{
  handle:   string;
  platform: string;
  membership: {
    role: string;
  };
}

interface UserProps
{
  onLoad: (user: IUser) => void;
}

interface UserState
{
  user:      IUser;

  isLoaded:  boolean;
  isErrored: boolean;
}


class User extends Component<UserProps, UserState>
{
  componentDidMount ()
  {
    http.get(Endpoints.USER).then(({ data, error }) => {
      if (error) return this.setState({ isErrored: true });
      this.setState({ user: data, isLoaded: true });
      this.props.onLoad(data);
    });
  }

  render ()
  {
    return null;
  }
}

export { User, IUser };
