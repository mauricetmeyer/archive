/**
 * workspace.tsx
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { createElement, Component } from 'react';
import { http }                     from '../../utils';
import { Endpoints }                from '../../const';


interface IAccount
{
  _id:      string;
  handle:   string;
  platform: string;
}

interface IWorkspace
{
  features: string;
  accounts: IAccount[];
}

interface WorkspaceProps
{
  onLoad: (workspace: IWorkspace) => void;
}

interface WorkspaceState
{
  workspace: IWorkspace;

  isLoaded:  boolean;
  isErrored: boolean;
}


class Workspace extends Component<WorkspaceProps, WorkspaceState>
{
  componentDidMount ()
  {
    http.get(Endpoints.WORKSPACE).then(({ data, error }) => {
      if (error) return this.setState({ isErrored: true });
      this.setState({ workspace: data, isLoaded: true });
      this.props.onLoad(data);
    });
  }

  render ()
  {
    return null;
  }
}

export { Workspace, IWorkspace, IAccount };
