/**
 * csp.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


const csp = {
  baseUri:    "'self'",
  defaultSrc: ["'none'"],
  connectSrc: ["'self'", 'blob:', 'www.google-analytics.com', 'api.stripe.com'],
  scriptSrc:  ["'self'", 'js.stripe.com', 'api.stripe.com'],
  styleSrc:   ["'self'", "'unsafe-inline'"],
  frameSrc:   ['js.stripe.com'],
  mediaSrc:   ["'self'", 'blob:'],
  imgSrc:     ["'self'", 'data:', 'www.google-analytics.com', 'q.stripe.com']
};

export { csp };
