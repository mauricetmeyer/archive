/**
 * jobs.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import {
  MailJobs,
  UserJobs,
  StripeJobs,
  WorkspaceJobs
} from '../jobs';
import { Queue } from '../core';


const jobs = (a: Queue) => {
  a.define('mail_send',                 MailJobs.send);
  a.define('user_remove_unconfirmed',   UserJobs.removeNotConfirmed);

  a.define('stripe_charge_event',       StripeJobs.chargeEvent);
  a.define('stripe_invoice_event',      StripeJobs.invoiceEvent);
  a.define('stripe_subscription_event', StripeJobs.subscriptionEvent);

  a.define('workspace_purge',           WorkspaceJobs.purge);
};

export { jobs };
