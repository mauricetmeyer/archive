/**
 * mail.ts
 *
 * Author: Simon Schwarz
 * E-Mail: black-simon@hotmail.de
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


const mail = {
  host:   process.env.EMAIL_HOST,
  user:   process.env.EMAIL_USER,
  pass:   process.env.EMAIL_PASSWORD,
  sender: process.env.EMAIL_SENDER,
};

export { mail };
