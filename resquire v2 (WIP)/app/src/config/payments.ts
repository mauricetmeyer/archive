/**
 * payments.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


const payments = {
  secret:    process.env.STRIPE_SECRET,
  public:    process.env.STRIPE_PUBLIC,
  signature: process.env.STRIPE_SIGNATURE
};

export { payments };
