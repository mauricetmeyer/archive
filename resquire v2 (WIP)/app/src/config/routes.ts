/**
 * routes.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import {
  AuthActions,
  UserActions,
  MemberActions,
  MembersActions,
  PaymentActions,
  AccountActions,
  RegisterActions,
  SessionsActions,
  WebhooksActions,
  WorkspaceActions,
  EvaluationActions
} from '../actions';
import { Router, Context }          from '../core';
import { PageHelper, AccessHelper } from '../helpers';


const routes = (r: Router) => {
  /**
   * Boilerplate routes */
  r.get('/',      PageHelper.requireUser);
  r.get('/login', async (ctx: Context) => {
    const user = await AccessHelper.getUser(ctx);
    if (user) return ctx.redirect(302, '/');
    PageHelper.displayOk(ctx);
  });
  r.get('/invite/:token', async (ctx: Context) => {
    const user = await AccessHelper.getUser(ctx);
    if (user) return ctx.redirect(302, '/');
    PageHelper.displayOk(ctx);
  });
  r.get('/register', async (ctx: Context) => {
    const user = await AccessHelper.getUser(ctx);
    if (user) return ctx.redirect(302, '/');
    PageHelper.displayOk(ctx);
  });

  r.namespace('/accounts', (r: Router) => {
    r.get('/',       PageHelper.requireUser);
    r.get('/:id',    PageHelper.requireUser);
    r.get('connect', PageHelper.requireUser);
  });


  /**
   * Internal API namespace */
  r.namespace('/i', (r: Router) => {
    r.get('members',         MembersActions.get);

    r.post('login',          AuthActions.login);
    r.post('logout',         AuthActions.logout);
    r.post('register',       RegisterActions.register);

    r.post('process',        EvaluationActions.process);
    r.post('evaluate',       EvaluationActions.evaluate);

    r.namespace('user', (r: Router) => {
      r.get(UserActions.get);
      r.post(UserActions.update);
    });

    r.namespace('member', (r: Router) => {
      r.put(MemberActions.create);
      r.post(MemberActions.update);
      r.delete(MemberActions.remove);

      r.put('invite', MemberActions.accept_invite);
      r.post('invite', MemberActions.check_invite);
    });

    r.namespace('account', (r: Router) => {
      r.put(AccountActions.create);
      r.post(AccountActions.update);
      r.delete(AccountActions.remove);
    });

    r.namespace('payment', (r: Router) => {
      r.get(PaymentActions.get);
      r.post(PaymentActions.update);
    });

    r.namespace('sessions', (r: Router) => {
      r.get(SessionsActions.list);
      r.delete(SessionsActions.delete);
      r.delete('clear', SessionsActions.delete);
    });

    r.namespace('webhooks', (r: Router) => {
      r.post('stripe', WebhooksActions.stripe);
    });

    r.namespace('workspace', (r: Router) => {
      r.get(WorkspaceActions.get);
      r.post(WorkspaceActions.update);
      r.delete(WorkspaceActions.remove);
    });
  });

  /**
   * Wildcard (404 Not Found) */
  r.get('*', async (ctx) => {
    ctx.send(404, '404 Not Found');
  });
};

export { routes };
