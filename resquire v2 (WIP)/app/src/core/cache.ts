/**
 * cache.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { RedisClient, createClient } from 'redis';


type CacheOpts = {
  host?: string;
  port?: number;
  ttl?:  number;
};


class Cache
{
  private ttl:    number;

  private host:   string;
  private port:   number;

  private handle: RedisClient;


  constructor (opts: CacheOpts)
  {
    opts = opts || {};

    this.ttl    = opts.ttl  || 8000;
    this.host   = opts.host || 'localhost';
    this.port   = opts.port || 6379;
    this.handle = createClient({
      host: this.host,
      port: this.port,
    });
  }


  /**
   * Has value?
   *
   * @param  {string} key
   * @return {Promise}
   */
  public async has (key: string) : Promise<boolean>
  {
    key = this.generateKey(key);
    return new Promise((res) => {
      this.handle.hget(key, key, () => res);
    });
  }

  /**
   * Get value
   *
   * @param  {string} key
   * @return {Promise}
   */
  public async get (key: string) : Promise<any>
  {
    key = this.generateKey(key);
    return new Promise((res) => {
      this.handle.mget(() => res);
    });
  }

  /**
   * Set value
   *
   * @param  {string} key
   * @param  {any}    val
   * @return {Promise}
   */
  public async set (key: string, val: any) : Promise<any>
  {
    key = this.generateKey(key);
    return new Promise((res) => {
      this.handle.mset(() => res);
      this.handle.set(key, val, val, () => res);
    });
  }

  /**
   * Clear cache
   *
   * @return {Promise}
   */
  public async clear () : Promise<any>
  {
    return new Promise((res) => {
      this.handle.flushdb(() => res);
    });
  }


  /**
   * Increment key
   *
   * @return {Promise}
   */
  private increment (key: string, amount: number = 1) : Promise<void>
  {
    key = this.generateKey(key);
    return new Promise((res) => {
      this.handle.incrby(key, amount, () => {
        this.handle.expire(key, this.ttl, () => res);
      });
    });
  }

  /**
   * Decrement key
   *
   * @return {Promise}
   */
  private decrement (key: string, amount: number = 1) : Promise<void>
  {
    key = this.generateKey(key);
    return new Promise((res) => {
      this.handle.decrby(key, amount, () => {
        this.handle.expire(key, this.ttl, () => res);
      });
    });
  }


  private generateKey (name: string) : string
  {
    return name;
  }
}

export { Cache };
