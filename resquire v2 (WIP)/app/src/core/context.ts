/**
 * context.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */

import * as Cookies                        from 'cookies';
import { Emitter }                         from 'event-kit';
import { IncomingMessage, ServerResponse } from 'http';
import { contentType }                     from 'mime-types';
import qs                                  from 'querystring';
import { Stream }                          from 'stream';
import { parse }                           from 'url';


const STATUS_EMPTY = [204, 205, 304];

class Context
{
  public  req:          IncomingMessage;
  public  res:          ServerResponse;

  public  cookies:      Cookies;
  public  emitter:      Emitter;

  private typeSet?:     boolean;
  private statusSet?:   boolean;

  private urlCache?:    URL;
  private bodyCache?:   any;

  private queryCache?:  { [name: string]: any };
  private paramsCache?: { [name: string]: any };

  constructor (req: IncomingMessage, res: ServerResponse)
  {
    this.req     = req;
    this.res     = res;
    this.emitter = new Emitter();
    this.cookies = new Cookies(req, res, {
      
    });
  }

  public pipe (dest: NodeJS.WritableStream)
  {
    this.req.pipe(dest);
  }

  public send (status: number, body?: string | Stream | Buffer)
  {
    /**
     * Set status */
    this.status = status;

    /**
     * Strip some headers */
    if (204 === this.res.statusCode
     || 304 === this.res.statusCode)
    {
      this.removeHeader('content-type');
      this.removeHeader('content-length');
      this.removeHeader('transfer-encoding');
    }

    /**
     * Send it! */
    this.body = body;
  }

  public json (status: number, data: { [key: string]: any })
  {
    const body = JSON.stringify(data);
    this.type  = 'json';
    return this.send(status, body);
  }

  public redirect (status: number, path: string)
  {
    this.setHeader('location', path);
    this.status = status;
  }


  /**
   * End the response. */
  public end (val: any)
  {
    if (!this.body) this.length = 0;
    this.res.end(val);
  }

  /**
   * Get request URL. */
  get url()
  {
    return this.req.url;
  }

  /**
   * Get WHATWG parsed URL.
   * Lazily memoized. */
  get URL()
  {
    if (!this.urlCache)
    {
      const protocol = this.protocol;
      const host = this.host;
      const url = this.req.url || '';

      try
      {
        this.urlCache = new URL(`${protocol}://${host}${url}`);
      // tslint:disable-next-line:no-empty
      } catch (err) {}
    }

    return this.urlCache;
  }

  /**
   * Parse the "Host" header field host
   * and support X-Forwarded-Host when a
   * proxy is enabled. */
  get host()
  {
    const proxy = this.getHeader('x-forwarded-host');
    const host = proxy || this.getHeader('host');
    if (!host || typeof host !== 'string') return '';
    return host.split(/\s*,\s*/)[0];
  }

  /**
   * Get request pathname. */
  get path()
  {
    if (!this.req.url) return '';
    return parse(this.req.url).pathname;
  }

  /**
   * Get full request URL. */
  get href()
  {
    if (!this.req.url) return;
    if (/^https?:\/\//i.test(this.req.url)) return this.req.url;
    return this.origin + this.req.url;
  }

  /**
   * Get origin of URL. */
  get origin()
  {
    return `${this.protocol}://${this.host}`;
  }

  /**
   * Parse the "Host" header field hostname
   * and support X-Forwarded-Host when a
   * proxy is enabled. */
  get hostname()
  {
    const host = this.host;
    if ('[' === host[0]) return this.URL ? this.URL.hostname : ''; // IPv6
    return host.split(':')[0];
  }

  /**
   * Get request method. */
  get method()
  {
    return this.req.method || 'GET';
  }

  /**
   * Get request parameters includes json body data and query. */
  get params()
  {
    if (!this.paramsCache)
    {
      const params      = this.query || {};
      this.paramsCache = { ...params/*, ...this.req.body*/ };
    }

    return this.paramsCache;
  }

  /**
   * Get parsed query-string. */
  get query()
  {
    const str = this.querystring;
    const c = this.queryCache = this.queryCache || {};
    return c[str] || (c[str] = qs.parse(str));
  }

  /**
   * Get query string.
   *
   * @return {String}
   * @api public
   */

  get querystring()
  {
    if (!this.req.url) return '';
    return parse(this.req.url).query || '';
  }

  /**
   * Return the request socket. */
  get socket()
  {
    return this.req.socket;
  }

  /**
   * Return the protocol string "http" or "https"
   * when requested with TLS. When the proxy setting
   * is enabled the "X-Forwarded-Proto" header
   * field will be trusted. */
  get protocol()
  {
    const proto = this.getHeader('x-forwarded-proto') || 'http';
    if (typeof proto !== 'string') return 'http';
    return proto.split(/\s*,\s*/)[0];
  }

  /**
   * Check if the connection is made over https */
  get secure()
  {
    return 'https' === this.protocol;
  }

  get ip(): string
  {
    return this.ips[0] || this.req.socket.remoteAddress || '';
  }

  /**
   * For example if the value were "client, proxy1, proxy2"
   * you would receive the array `["client", "proxy1", "proxy2"]`
   * where "proxy2" is the furthest down-stream. */
  get ips()
  {
    const val = this.getHeader('x-forwarded-for');
    return val && typeof val === 'string' ? val.split(/\s*,\s*/) : [];
  }

  /**
   * Return the request mime type void of
   * parameters such as "charset". */
  get type()
  {
    const type = this.getHeader('content-type');
    if (!type || typeof type !== 'string') return '';
    return type.split(';')[0];
  }

  /**
   * Set response type */
  set type(type)
  {
    const tmp = contentType(type);
    if (tmp)
    {
      this.setHeader('content-type', tmp);
      this.typeSet = true;
    }
    else this.removeHeader('content-type');
  }

  /**
   * Get body. */
  get body()
  {
    return this.bodyCache;
  }

  /**
   * Set response body. */
  set body(val: string | Stream | Buffer | undefined)
  {
    this.bodyCache = val;
    if (!val)
    {
      if (!STATUS_EMPTY.includes(this.status)) {
        this.status = 204;
      }

      this.removeHeader('content-type');
      this.removeHeader('content-length');
      this.removeHeader('transfer-encoding');
      return;
    }

    /**
     * Set status if it wasn't set yet */
    if (!this.statusSet) this.status = 200;

    /**
     * Handle stream */
    if (val instanceof Stream) {
      if (!this.typeSet) this.type = 'application/octet-stream';
      return;
    }

    /**
     * Buffer */
    if (val instanceof Buffer && !this.typeSet) {
      this.type = 'application/octet-stream';
    }

    /**
     * Everything */
    if (!this.typeSet) this.type = 'text/plain';
    this.length = Buffer.byteLength(val);
  }

  /**
   * Get response status code. */
  get status()
  {
    return this.res.statusCode;
  }

  /**
   * Set response status code. */
  set status(code: number)
  {
    if (this.headerSent) return;

    this.statusSet     = true;
    this.res.statusCode = code;
  }

  /**
   * Return request header. */
  public getHeader(field: string): string | number | string[]
  {
    return this.req.headers[field.toLowerCase()] || '';
  }

  /**
   * Set response header `field` to `val`, or pass
   * an object of header fields. */
  public setHeader(field: string, val: string | number | string[]): void
  {
    if (this.headerSent) return;
    if (Array.isArray(val)) val = val.map(String);
    else val = String(val);
    this.res.setHeader(field, val);
  }

  /**
   * Remove header. */
  public removeHeader(field: string): void
  {
    if (this.headerSent) return;
    this.res.removeHeader(field);
  }

  /**
   * Flush any set headers, and begin the body */
  public flushHeaders()
  {
    this.res.flushHeaders();
  }

  /**
   * Checks if the request is writable.
   * Tests for the existence of the socket
   * as node sometimes does not set it. */
  get writable()
  {
    if (this.res.finished) return false;

    const socket = this.req.socket;
    if (!socket) return true;
    return socket.writable;
  }

  /**
   * Set Content-Length field to `n`. */
  set length(n: number)
  {
    this.setHeader('content-length', n);
  }

  /**
   * Return parsed response Content-Length when present. */
  get length(): number
  {
    const len = this.getHeader('content-length');
    const body = this.body;

    if (null == len)
    {
      if (!body) return -1;
      if ('string' === typeof body) return Buffer.byteLength(body);
      if (Buffer.isBuffer(body)) return body.length;
      // if (isJSON(body)) return Buffer.byteLength(JSON.stringify(body));
      return -1;
    }

    return ~~len;
  }

  /**
   * Check if a header has been written to the socket. */
  get headerSent(): boolean
  {
    return this.res.headersSent;
  }
}

export { Context };
