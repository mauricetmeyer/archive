/**
 * job.ts
 *
 * Author: Simon Schwarz
 * E-Job: black-simon@hotmail.de
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import * as Bee  from 'bee-queue';
import { Queue } from './queue';


class Job<T = any>
{
  public readonly data:  T;
  public readonly type:  string;
  public readonly queue: Queue;

  private handle: Bee.Job;


  /**
   * Get a new job
   *
   * @param  {}
   * @return {Job<T>}
   */
  constructor (queue: Queue, job: Bee.Job)
  {
    const { type, data } = job.data;
    this.type   = type;
    this.data   = data;
    this.queue  = queue;
    this.handle = job;
  }


  /**
   * Broadcast progress
   *
   * @param  {number} progress
   * @return {Promise<void>}
   */
  async progress(progress: number)
  {
    this.handle.reportProgress(progress);
  }
}

export { Job };
