/**
 * queue.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { DateTime } from 'luxon';
import * as Bee     from 'bee-queue';

import { Job }      from './job';


interface IntJobDesc
{
  type: string;
  data: any;
}

interface JobOpts
{
  at?: DateTime;
}

type QueueFn<T = void> = (job: Job<any>) => Promise<T>;

interface QueueConnectionOpts
{
  port:     number;
  host:     string;
  password: string;
}

interface QueueOpts
{
  connection?:  QueueConnectionOpts;
  concurrency?: number;
  isWorker?:    boolean;
}

class Queue
{
  name:    string;
  threads: number;
  handles: { [key: string]: QueueFn };

  handle: Bee;


  /**
   * Create a new queue instance
   *
   * @param  {QueueOpts}
   * @return {Queue}
   */
  constructor (name: string, opts: QueueOpts = {})
  {
    this.name    = name;
    this.threads = opts.concurrency || 1;
    this.handles = {};

    this.handle = new Bee(name, {
      redis:           opts.connection,
      isWorker:        opts.isWorker || false,
      getEvents:       false,
      removeOnSuccess: true
    });
  }


  /**
   * Attach a new ready handler
   *
   * @param  {Function} fn
   * @return {Disposable}
   */
  onReady (fn: () => void)
  {
    this.handle.on('ready', fn);
  }

  /**
   * Attach a new error handler
   *
   * @param  {Function} fn
   * @return {Disposable}
   */
  onError (fn: (err: Error) => void)
  {
    this.handle.on('error', fn);
  }

  /**
   * Attach a new job error handler
   *
   * @param  {Function} fn
   * @return {Disposable}
   */
  onJobError (fn: (job: Bee.Job, err: Error) => void)
  {
    this.handle.on('failed', fn);
  }


  /**
   * Define a new job
   *
   * @param {string}   type
   * @param {Function} fn
   */
  define (type: string, fn: QueueFn)
  {
    this.handles[type] = fn;
  }

  /**
   * Start processing jobs.
   *
   * @return {Promise<void>}
   */
  async process ()
  {
    return this.handle.process(this.threads, async (job: Bee.Job) => {
      const { type } = job.data as IntJobDesc;
      console.log(`[JOB] ${type}:${job.id} - Started`);

      /**
       * Check if handle exists, otherwise
       * we'll stop right here. */
      const handle = this.handles[type];
      if (!handle) throw new Error(`No handler defined for: ${type}`);

      /**
       * Construct job object
       * and call the handler. */
      const sandbox = new Job(this, job);
      try {
        await handle(sandbox);
        console.log(`[JOB] ${type}:${job.id} - Done`);
      }
      catch (err)
      {
        console.log(`[JOB] ${type}:${job.id} - Failed`);
        console.error(err);
      }
    });
  }


  /**
   * Enqueue job.
   *
   * @param  {string}  type
   * @param  {object}  data
   * @param  {JobOpts} opts
   * @return {Promise}
   */
  async enqueue<T> (type: string, data: T, opts: JobOpts = {})
  {
    const { at } = opts;
    const job = this.handle.createJob({ type, data });
    if (at) job.delayUntil(at.toMillis());
    return job.save();
  }

  /**
   * Enqueue job to run in {secs} seconds.
   *
   * @param  {string}  type
   * @param  {integer} secs
   * @param  {object}  data
   * @param  {JobOpts} opts
   * @return {Promise}
   */
  async enqueueIn<T> (type: string, secs: number, data: T, opts: JobOpts = {})
  {
    opts = { ...opts, at: DateTime.local().plus({ seconds: secs }) };
    return this.enqueue(type, data, opts);
  }

  /**
   * Push a new job to run at {timestamp}.
   *
   * @param  {string}   type
   * @param  {DateTime} when
   * @param  {object}   data
   * @param  {JobOpts}  opts
   * @return {Promise}
   */
  async enqueueAt<T> (type: string, when: DateTime, data: T, opts: JobOpts = {})
  {
    opts = { ...opts, at: when };
    return this.enqueue(type, data, opts);
  }


  /**
   * Get number of current enqueued jobs
   *
   * @return {Promise}
   */
  async length ()
  {
    return 0;
  }


  /**
   * Clear queue
   *
   * @return {Promise<void>}
   */
  async clear ()
  {
    return this.handle.destroy();
  }


  /**
   * Shutdown queue
   *
   * @return {Promise<void>}
   */
  async shutdown ()
  {
    return this.handle.close();
  }
}

export { Job, Queue, QueueConnectionOpts };
