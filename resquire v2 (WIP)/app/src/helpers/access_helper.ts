/**
 * access_helper.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { Context }      from '../core';
import { app }          from '../config';
import { User, IUser }  from '../models/user';
import { IUserSession } from '../models/user_session';


const AccessHelper = {
  async getUser (ctx: Context) : Promise<IUser | undefined>
  {
    const key = ctx.cookies.get('session');
    if (!key) return;

    /**
     * Get the user */
    const user = await User.findOne({
      sessions: {
        $elemMatch: { key, revokedAt: null }
      }
    }).populate('membership.workspace');

    /**
     * Create a new user session log entry */
    if (user)
    {
      const ip        = ctx.ip;
      const agent     = ctx.getHeader('user-agent');
      const timestamp = Date.now();
      const entry     = { ip, agent, timestamp, location: '' };

      for (let i = 0; i < user.sessions.length; i++)
      {
        const session = user.sessions[i];
        if (session.key === key)
        {
          user.sessions[i].log.push(entry);
          await user.save();
          break;
        }
      }

      return user;
    }

    /**
     * Clear session cookies as it is still set
     * but no valid session was found. */
    ctx.cookies.set('session', undefined, {
      domain: app.domain,
      secure: app.auth.secure_cookie,
      httpOnly: true,
    });
  },

  async getSession (ctx: Context) : Promise<IUserSession | undefined>
  {
    const key  = ctx.cookies.get('session');
    const user = await this.getUser(ctx);
    if (!user) return;

    for (let i = 0, len = user.sessions.length; i < len; i++)
    {
      const session = user.sessions[i];
      if (session.key === key) return session;
    }
  }
};

export { AccessHelper };
