/**
 * context_helper.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import * as Busboy  from 'busboy';
import * as rawBody from 'raw-body';
import { Context }  from '../core';


interface StreamFile
{
  name:   string;
  type:   string;
  stream: NodeJS.ReadableStream;
}


async function raw (ctx: Context) : Promise<Buffer>;
async function raw (ctx: Context, encoding: 'utf-8' | 'base64') : Promise<string>;
async function raw (ctx: Context, encoding?: 'utf-8' | 'base64') : Promise<Buffer | string>
{
  return await rawBody(ctx.req, { encoding });
}


const ContextHelper = {
  raw,

  async data (ctx: Context) : Promise<any>
  {
    /**
     * We return a new promise
     * as we have to convert the event based
     * calling to a promise. */
    return new Promise((res, _) => {
      /**
       * Initialize a new busboy instance */
      const data:  { [key: string]: any }        = {};
      const files: { [key: string]: StreamFile } = {};
      const handle = new Busboy({ headers: ctx.req.headers });
      handle.on('file', (field, stream, name, _, type) => {
        /**
         * Add current StreamFile
         * to the files object. */
        const res: StreamFile = { name, type, stream };
        data[field] ? data[field].push(res) : data[field] = [res];
      });

      handle.on('field', (field, value) => {
        /**
         * Add field to the data. */
        data[field] = value;
      });

      handle.on('finish', () => {
        /**
         * Resolve promise with
         * data + files. */
        data.files = files;
        res(data);
      });

      /**
       * Pipe the request into
       * out busboy instance to get
       * the form data. */
      ctx.pipe(handle);
    });
  },

  async json (ctx: Context) : Promise<any>
  {
    const type = ctx.type;
    if (type !== 'application/json')
      throw new Error('Invalid request format');

    const body = await this.raw(ctx, 'utf-8');
    const json = JSON.parse(Buffer.isBuffer(body) ? body.toString('utf-8') : body);
    return json;
  }
};

export { ContextHelper, StreamFile };
