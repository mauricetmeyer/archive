/**
 * file_helper.ts
 *
 * Author: Simon Schwarz
 * E-File: black-simon@hotmail.de
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import * as fs from 'fs';


const FileHelper = {
  async read(path: string): Promise<string | undefined>
  {
    return new Promise(async (res) => {
      fs.readFile(path, 'utf-8', (err, data) => {
        if (err) res();
        res(data);
      });
    });
  },

  async exists(path: string): Promise<fs.Stats | false>
  {
    return new Promise((res) => {
      fs.stat(path, (err, stats) => {
        if (err) return res(false);
        res(stats);
      });
    });
  }
};

export { FileHelper };
