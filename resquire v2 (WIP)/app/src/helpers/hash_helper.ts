/**
 * hash_helper.ts
 *
 * Author: Maurice T. Meyer
 * E-Hash: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import * as crypto from 'crypto';


const HashHelper = {
  async sha1 (source: string)
  {
    return crypto.createHash('sha1').update(source, 'utf8').digest('hex');
  },

  async sha256 (source: string)
  {
    return crypto.createHash('sha256').update(source, 'utf8').digest('hex');
  },

  async sha512 (source: string)
  {
    return crypto.createHash('sha512').update(source, 'utf8').digest('hex');
  }
};

export { HashHelper };
