/**
 * mail_helper.ts
 *
 * Author: Simon Schwarz
 * E-Mail: black-simon@hotmail.de
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import * as path       from 'path';
import * as handlebars from 'handlebars';
import * as nodemailer from 'nodemailer';

import { mail }        from '../config';
import { FileHelper }  from '../helpers';


interface MailOpts
{
  cc?:      Array<string>;
  bcc?:     Array<string>;

  subject?: string;
  template: string;

  data?:    any;
}

interface MailTemplate
{
  text?: handlebars.TemplateDelegate;
  html?: handlebars.TemplateDelegate;
}

const MailDir       = path.join(path.dirname(__dirname), 'emails');
const MailTemplates = new Map<string, MailTemplate>();
const MailTransport = nodemailer.createTransport({
  pool: true,
  host: mail.host,
  port: 587,
  auth: {
    user: mail.user,
    pass: mail.pass,
  }
});

const MailHelper = {
  async sendMail(receiver: string, options: MailOpts): Promise<void>
  {
    const { data, subject, template } = options;

    /**
     * Try loading the template from the cache first
     * otherwise we'll have to read from the disk. */
    let templates = MailTemplates.get(template);
    if (!templates)
    {
      /**
       * Try reading the files from the disk. */
      const dir     = path.join(MailDir, template);
      const textRaw = await FileHelper.read(`${dir}.txt`);
      const htmlRaw = await FileHelper.read(`${dir}.html`);

      /**
       * If text and html don't exist
       * we'll error our. */
      if (!textRaw && !htmlRaw)
        throw new Error(`No templates for ${template}`);

      /**
       * Compile the templates to handlebar templates. */
      const text = textRaw ? handlebars.compile(textRaw) : undefined;
      const html = htmlRaw ? handlebars.compile(htmlRaw) : undefined;

      /**
       * If everything went well we'll cache it in the MailTemplates Map
       * and push it into the templates variable. */
      templates = { text, html };
      MailTemplates.set(template, templates);
    }

    try
    {
      /**
       * Prepare templates */
      const { text, html } = templates;

      /**
       * Try sending the mail using the SMTP connection. */
      return await MailTransport.sendMail({
        subject,
        text: text ? text(data) : undefined,
        html: html ? html(data) : undefined,
        from: mail.sender,
        to:   receiver,
      });
    }
    catch (err)
    {
      return err;
    }
  }
};

export { MailHelper };
