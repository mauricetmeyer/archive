/**
 * mongo_helper.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


interface MongoURIOpts
{
  db?:   string;
  host?: string;
  port?: number;

  user?: string;
  pass?: string;
}

const MongoHelper = {
  buildURI (opts?: MongoURIOpts)
  {
    opts = opts || {};

    const db   = opts.db   || 'scrum';
    const port = opts.port || 27017;
    const host = opts.host || '127.0.0.1';
    const auth = opts.user && opts.pass
               ? `${opts.user}:${opts.pass}@` : '';

    return `mongodb://${auth}${host}:${port}/${db}`;
  }
};

export { MongoHelper, MongoURIOpts };
