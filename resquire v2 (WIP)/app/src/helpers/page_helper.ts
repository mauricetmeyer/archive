/**
 * page_helper.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import hb, { compile } from 'handlebars';
import { Context } from '../core';
import { AccessHelper } from './access_helper';

/**
 * JSON helper */
hb.registerHelper('json', ctx => JSON.stringify(ctx));


const view = `
<!DOCTYPE hmtl>
<!--

  Un projet par Laviréo

-->
<html>
<head>

  <!-- Title -->
  <title>Resquire</title>

  <!-- Meta -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <link rel="shortcut icon" href="/static/favicon.png">
  <link rel="apple-touch-icon" href="/static/apple-touch-icon.png">

  <meta name="description" content="Resquire...">
  <meta name="twitter:description" content="Intuitive AI powered social media marketing tool. Know your future social value, today.">

  <!-- Data -->
  <script type="application/json" id="preload">{{{ json config }}}</script>

  <!-- Css -->
  <link href="/static/master.css" rel="stylesheet" type="text/css"/>

</head>
<body>

  <!-- NoScript -->
  <noscript>
    <div class="height--100">
      <div class="center center--vertical wrap--xs textAlign--m">
        <div class="margin--bottom--m">
          <h1 class="fontSize--xxl">No love for Javascript?</h1>
          <p class="fontSize--l c--grey">Resquire requires your browser to have Javascript enabled. <a href="http://enable-javascript.com" target="_blank" class="fontSize--l c--purple">Learn more</a></p>
        </div>
        <div class="footer">
          <p>&copy; Laviréo</p>
        </div>
      </div>
    </div>
  </noscript>

  <!-- Wrap -->
  <div id="wrap"></div>

  <!-- Boot -->
  <script type="text/javascript" src="/static/app.js"></script>

</body>
</html>`;

const template   = compile(view);
const PageHelper = {
  async displayOk (ctx: Context)
  {
    const content = template({
      config: {
        csrf:   '',
        stripe: process.env.STRIPE_PUBLIC
      }
    });

    ctx.type = 'html';
    ctx.send(200, content);
  },

  async displayNotFound (ctx: Context)
  {
    const content = template({
      csrf:   '',
      data:   {},
      config: {
        stripe: {
          token: ''
        }
      }
    });

    ctx.type = 'html';
    ctx.send(404, content);
  },


  async requireUser (ctx: Context)
  {
    const user = await AccessHelper.getUser(ctx);
    if (!user) return ctx.redirect(302, '/login');
    return PageHelper.displayOk(ctx);
  },

  async requireStaff (ctx: Context)
  {
    const user = await AccessHelper.getUser(ctx);
    if (!user || !user.staff) return PageHelper.displayNotFound(ctx);
    return PageHelper.displayOk(ctx);
  }
};


export { PageHelper };
