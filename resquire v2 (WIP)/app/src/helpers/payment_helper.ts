/**
 * payment_helper.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import * as _       from 'underscore';
import * as Stripe  from 'stripe';

import { payments } from '../config';


interface ISource
{
  brand:       string;
  country:     string;

  status:      string;
  funding:     string;

  last4:       string;
  fingerprint: string;

  exp_year:    number;
  exp_month:   number;
}

interface ICustomer
{
  prefix?:       string;

  source?:       ISource;
  subscription:  ISubscription;
}


interface ISubscription
{
  status:        string;

  startAt:       number;
  endAt?:        number;
  canceledAt?:   number;

  trialEndAt?:   number;
  trialStartAt?: number;

  periodEndAt:   number;
  periodStartAt: number;
}

interface ISubscriptionBaseOpts
{
  quantity?:  number;
  trial_end?: number;
}

interface ISubscriptionCreateOpts extends ISubscriptionBaseOpts
{
  plan:     string;
  customer: string;
}

interface ISubscriptionUpdateOpts extends ISubscriptionBaseOpts
{
  plan?: string;
}


const PARAMS = [
  'brand', 'country',
  'status', 'funding',
  'last4', 'fingerprint',
  'exp_year', 'exp_month'
];


const PaymentDriver = new Stripe(payments.secret || '');
const PaymentHelper = {
  DRIVER: PaymentDriver,


  /**
   * Get customer details
   *
   * @param  {string} customerId
   * @return {Promise<ICustomer>}
   */
  async getCustomer (cId: string) : Promise<ICustomer>
  {
    const customer = await PaymentDriver.customers.retrieve(cId);
    const { sources, subscriptions, default_source } = customer;

    let source: ISource | undefined = undefined;
    if (sources && sources.data.length > 0)
    {
      const defaultSrc = _.find(sources.data, s => s.id === default_source);
      if (defaultSrc) source = _.pick(defaultSrc, ...PARAMS) as ISource;
    }

    const {
      status,
      start,
      ended_at,
      canceled_at,
      trial_end,
      trial_start,
      current_period_end,
      current_period_start
    } = subscriptions.data[0];

    return {
      source,
      subscription: {
        status,

        startAt:       start,
        endAt:         ended_at    || undefined,
        canceledAt:    canceled_at || undefined,

        trialEndAt:    trial_end   || undefined,
        trialStartAt:  trial_start || undefined,

        periodEndAt:   current_period_end,
        periodStartAt: current_period_start
      }
    };
  },

  /**
   * Create a new customer
   *
   * @return {Promise<string>}
   */
  async createCustomer () : Promise<string>
  {
    const customer = await PaymentDriver.customers.create({});
    return customer.id;
  },

  /**
   * Update an existing customer
   *
   * @param  {string} cId
   * @param  {string} source
   * @return {Promise<void>}
   */
  async updateCustomer (cId: string, source: string) : Promise<void>
  {
    await PaymentDriver.customers.update(cId, { source });
  },

  /**
   * Delete an existing customer
   *
   * @param  {string} cId
   * @return {Promise<void>}
   */
  async deleteCustomer (cId: string) : Promise<void>
  {
    await PaymentDriver.customers.del(cId);
  },


  /**
   * Get subscription details.
   *
   * @param {string} subscriptionId
   * @return {Promise<ISubscription>}
   */
  async getSubscription (sId: string) : Promise<ISubscription>
  {
    const subscription = await PaymentDriver.subscriptions.retrieve(sId);
    const {
      status,
      start,
      ended_at,
      canceled_at,
      trial_end,
      trial_start,
      current_period_end,
      current_period_start
    } = subscription;

    return {
      status,

      startAt:       start,
      endAt:         ended_at    || undefined,
      canceledAt:    canceled_at || undefined,

      trialEndAt:    trial_end   || undefined,
      trialStartAt:  trial_start || undefined,

      periodEndAt:   current_period_end,
      periodStartAt: current_period_start
    };
  },

  /**
   * Create a new subscription
   *
   * @param  {ISubscriptionOpts} opts
   * @return {Promise<string>}
   */
  async createSubscription (opts: ISubscriptionCreateOpts) : Promise<string>
  {
    const { plan, ...params } = opts;
    const subscription = await PaymentDriver.subscriptions.create({
      items: [{ plan }],
      ...params
    });

    return subscription.id;
  },

  /**
   * Update an existing subscription
   *
   * @param  {string}            sId
   * @param  {ISubscriptionOpts} opts
   * @return {Promise<void>}
   */
  async updateSubscription (sId: string, opts: ISubscriptionUpdateOpts) : Promise<void>
  {
    const subscription = await PaymentDriver.subscriptions.retrieve(sId);
    subscription.items.data[0].id;
    await PaymentDriver.subscriptions.update(sId, opts);
  },

  /**
   * Cancel an existing subscription
   *
   * @param  {string} sId
   * @return {Promise<void>}
   */
  async cancelSubscription (sId: string) : Promise<void>
  {
    await PaymentDriver.subscriptions.update(sId, {
      cancel_at_period_end: true
    });
  }
};


export { PaymentHelper };
