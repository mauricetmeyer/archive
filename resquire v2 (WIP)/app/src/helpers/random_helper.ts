/**
 * random_helper.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


const RANDOM_BASE62 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
const RANDOM_BASE36 = 'abcdefghijklmnopqrstuvwxyz0123456789';
const RANDOM_CREATE = (chars: string) => {
  const charLen = Buffer.byteLength(chars);
  return (len: number) => {
    let salt = '';
    for (let i = 0; i < len; i++)
      salt += chars[Math.floor(charLen * Math.random())];
    return salt;
  };
};

const RandomHelper = {
  create:      RANDOM_CREATE(RANDOM_BASE62),
  createLower: RANDOM_CREATE(RANDOM_BASE36),

  createUpper (len: number) : string
  {
    return RandomHelper.createLower(len).toUpperCase();
  }
};

export { RandomHelper };
