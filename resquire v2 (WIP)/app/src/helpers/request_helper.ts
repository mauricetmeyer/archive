/**
 * request_helper.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import request from 'request';


interface RequestOpts
{
  data?: string | NodeJS.ReadableStream;
  headers?: { [key: string]: string | number };
}

interface RequestResponse
{
  body:   string;
  status: number;
}


const RequestHelper = {
  get (url: string, opts: RequestOpts) : Promise<RequestResponse>
  {
    return new Promise((res, rej) => {
      const { headers } = opts;
      request.get({ url, headers }, (err, response, body) => {
        if (err) return rej(err);
        res({ body, status: response.statusCode });
      });
    });
  },

  put (url: string, opts: RequestOpts) : Promise<RequestResponse>
  {
    return new Promise((res, rej) => {
      const { data, headers } = opts;
      request.put({ url, headers, body: data }, (err, response, body) => {
        if (err) return rej(err);
        res({ body, status: response.statusCode });
      });
    });
  },

  post (url: string, opts: RequestOpts) : Promise<RequestResponse>
  {
    return new Promise((res, rej) => {
      const { data, headers } = opts;
      request.post({ url, headers, body: data }, (err, response, body) => {
        if (err) return rej(err);
        res({ body, status: response.statusCode });
      });
    });
  }
};

export { RequestHelper };
