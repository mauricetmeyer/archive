/**
 * session_helper.ts
 *
 * Author: Maurice T. Meyer
 * E-Hash: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import * as geoip       from 'geoip-lite';

import { Context }      from '../core';
import { app }          from '../config';
import { RandomHelper } from '../helpers';


const SessionHelper = {
  create (ctx: Context)
  {
    /**
     * Create a new user session */
    const ip       = ctx.ip;
    const agent    = ctx.getHeader('user-agent');
    const location = geoip.lookup(ip);

    const log      = [{ ip, agent, location }];
    const key      = RandomHelper.create(app.auth.session_key_length);
    const session  = {
      log,
      key,
    };

    /**
     * Set cookie */
    ctx.cookies.set('session', key, {
      maxAge: app.auth.expires_in * 60000,
      secure: app.auth.secure_cookie,
      httpOnly: true,
      domain: app.domain
    });

    return session;
  }
};

export { SessionHelper };
