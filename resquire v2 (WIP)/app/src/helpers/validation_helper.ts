/**
 * validation_helper.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


const NAME_REG  = /[a-z0-9- ]{3,64}/i;
const EMAIL_REG = /^[-!#$%&'*+\/0-9=?A-Z^_a-z{|}~](\.?[-!#$%&'*+\/0-9=?A-Z^_a-z`{|}~])*@[a-zA-Z0-9](-*\.?[a-zA-Z0-9])*\.[a-zA-Z](-?[a-zA-Z0-9])+$/;
const ValidationHelper = {
  isName (str: string) : boolean
  {
    return NAME_REG.test(str);
  },

  isEmail (str: string) : boolean
  {
    if (str.length <= 256 && EMAIL_REG.test(str))
    {
      const parts = str.split('@');
      if (parts[0].length < 65)
      {
        const domainParts = parts[1].split('.');
        if (domainParts.some(p => p.length > 255))
          return false;

        return true;
      }
    }

    return false;
  }
};

export { ValidationHelper };
