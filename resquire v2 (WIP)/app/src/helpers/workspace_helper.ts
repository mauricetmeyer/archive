/**
 * workspace_helper.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import * as moment               from 'moment';

import { PaymentHelper }         from '../helpers';
import { Workspace, IWorkspace } from '../models/workspace';


const WorkspaceHelper = {
  async create () : Promise<IWorkspace | undefined>
  {
    /**
     * We first create a new stripe customer. */
    try {
      const plan = process.env.PLAN_MONTHLY;
      if (!plan) throw new Error('Please set PLAN_MONTHLY');

      const trial_end    = moment().add(14, 'days').unix();
      const customer     = await PaymentHelper.createCustomer();
      const subscription = await PaymentHelper.createSubscription({
        plan, customer, trial_end
      });

      /**
       * Now create a new workspace. */
      return await Workspace.create({
        customer,
        subscription: {
          plan:           'monthly',
          planVersion:    1,
          trialEndsAt:    trial_end,
          subscriptionId: subscription
        }
      });
    }
    catch (err)
    {
      /**
       * @TODO (Maurice):
       * This error should be logged so
       * that we can investigate it. */
      console.error(err);
    }
  },

  async teardown (workspace: IWorkspace)
  {

  }
};

export { WorkspaceHelper };
