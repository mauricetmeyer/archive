/**
 * user_jobs.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { Job }        from '../core';
import { MailHelper } from '../helpers';


interface MailSendData
{
  receiver: string | string[];
  cc:       string[];
  bcc:      string[];

  data?:    any;
  subject:  string;
  template: string;
}


const MailJobs = {
  async send (job: Job<MailSendData>)
  {
    const { queue, type, data }    = job;
    const { receiver, ...options } = data;

    /**
     * We will schedule jobs for each receiver,
     * so we check if **receiver** is an array. */
    if (Array.isArray(receiver))
    {
      for (let i = 0, len = receiver.length; i < len; i++)
      {
        await queue.enqueue(type, {
          ...options,
          receiver: receiver[i]
        });
      }

      /**
       * We can return here as we don't have
       * to send any email with this job. */
      return;
    }

    /**
     * Send mail */
    await MailHelper.sendMail(receiver, options);
  }
};

export { MailJobs };
