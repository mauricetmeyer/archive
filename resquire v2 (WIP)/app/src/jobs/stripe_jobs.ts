/**
 * stripe_jobs.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { Job }                         from '../core';
import { FormatHelper, PaymentHelper } from '../helpers';

import { User }                        from '../models/user';
import { Workspace }                   from '../models/workspace';


interface StripeSourceData
{
  type:   string;
  source: any;
}

interface StripeInvoiceData
{
  type:    string;
  invoice: any;
}

interface StripeSubscriptionData
{
  type:         string;
  subscription: any;
}


const StripeJobs = {
  async sourceEvent (job: Job<StripeSourceData>)
  {
    const { queue, data }  = job;
    const { type, source } = data;

    /**
     * Get workspace by customer_id
     * and send a mail to the workspace admins. */
    const customer  = source.customer;
    const workspace = await Workspace.findOne({ customer });

    /**
     * We couldn't find a workspace by the
     * given customer ID, we need to decide what to do in this case! */
    if (!workspace)
      return;

    /**
     * Get all admins from the workspace
     * and transform them to an array of emails. */
    const admins = await User.find({ 'membership.workspace': workspace._id }, { email: 1 });
    const emails = admins.map(r => r.email);

    /**
     * Let's see what we can do with the
     * event we received. */
    switch (type)
    {
      /**
       * @NOTE (Maurice):
       * We aren't currently using the 'customer.source.deleted' webhook. */
      case 'deleted':
        break;

      /**
       * We send the customer an email letting them know that
       * their current payment method is about to expire. */
      case 'expiring': {
        /**
         * Get customer source that
         * was supposed to be charged for this invoice. */
        const { source } = await PaymentHelper.getCustomer(customer);
        const last4      = source ? source.last4 : 'NA';

        /**
         * Send an email the customers way. */
        return await queue.enqueue('mail_send', {
          data:     { last4 },
          subject:  'Your card is about to expire',
          template: 'payment_source_expiring',
          receiver: emails
        });
      }
    }
  },

  async invoiceEvent (job: Job<StripeInvoiceData>)
  {
    const { queue, data }   = job;
    const { type, invoice } = data;

    /**
     * Get workspace by customer_id
     * and send a mail to the workspace admins. */
    const customer  = invoice.customer;
    const workspace = await Workspace.findOne({ customer });

    /**
     * We couldn't find a workspace by the
     * given customer ID, we need to decide what to do in this case! */
    if (!workspace)
      return;

    /**
     * Get all admins from the workspace
     * and transform them to an array of emails. */
    const { plan } = workspace.subscription;
    const admins   = await User.find({ 'membership.workspace': workspace._id }, { email: 1 });
    const emails   = admins.map(r => r.email);

    /**
     * Let's see what we can do with the
     * event we received. */
    switch (type)
    {
      case 'created':
        break;

      /**
       * In case the customer chose the annual plan we'll send them a heads up
       * to let them know about the upcoming charge. */
      case 'upcoming':
        if (plan === 'annual')
        {
          return await queue.enqueue('mail_send', {
            data:     {},
            subject:  'Your annual subscription is about to be renewed',
            template: 'invoice_upcoming',
            receiver: emails
          });
        }

        break;

      /**
       * We'll sent the workspace admins an email
       * stating that we couldn't charge the currently used card. */
      case 'payment_failed':
        return await queue.enqueue('mail_send', {
          data:     {},
          subject:  'We couldn\'t bill your account',
          template: 'invoice_failed',
          receiver: emails
        });

      /**
       * On success we send out an email to the admins to let
       * them know about the billing. */
      case 'payment_succeeded': {
        /**
         * If the account is locked we'll unlock it and
         * let the admins know. */
        const wasLocked = workspace.isLocked;
        if (workspace.isLocked)
        {
          workspace.isLocked = false;
          await workspace.save();
        }

        /**
         * Get customer source that
         * was supposed to be charged for this invoice. */
        const { source } = await PaymentHelper.getCustomer(customer);
        const last4      = source ? source.last4 : 'NA';

        /**
         * Send invoice receipt.
         *
         * @NOTE (Maurice):
         * Data that can be sent:
         * - amount         (required)
         * - amount_total   (required)
         * - amount_tax     (optional)
         * - tax_origin     (when amount_tax): Germany
         * - tax_percentage (when amount_tax): 19%
         * */
        const amount_raw   = invoice.amount_paid;
        const amount       = amount_raw;
        const amount_tax   = undefined;
        const amount_total = amount_raw;
        return await queue.enqueue('mail_send', {
          data: {
            last4,
            wasLocked,

            plan:         workspace.subscription.plan,

            amount:       FormatHelper.currency(amount),
            amount_tax:   FormatHelper.currency(amount_tax),
            amount_total: FormatHelper.currency(amount_total),
          },
          subject:  'Here\'s your receipt for Resquire',
          template: 'invoice',
          receiver: emails
        });
      }
    }
  },

  async subscriptionEvent (job: Job<StripeSubscriptionData>)
  {
    const { queue, data }        = job;
    const { type, subscription } = data;

    /**
     * Get workspace by customer_id
     * and send a mail to the workspace admins. */
    const customer  = subscription.customer;
    const workspace = await Workspace.findOne({ customer });

    /**
     * We couldn't find a workspace by the
     * given customer ID, we need to decide what to do in this case! */
    if (!workspace)
      return;

    /**
     * Get all admins from the workspace
     * and transform them to an array of emails. */
    const admins     = await User.find({ 'membership.workspace': workspace._id }, { email: 1 });
    const emails     = admins.map(r => r.email);

    /**
     * Let's see what we can do with the
     * event we received. */
    switch (type)
    {
      case 'updated':
        /**
         * We should check the status of the subscription and
         * act depending on it. */
        const { status } = subscription;
        switch (status)
        {
          /**
           * Canceling can happen on two occasions.
           * Once when the user cancels his subscription or
           * when the subscription is unpaid, we handle both cases. */
          case 'unpaid':
          case 'canceled':
            break;

          /**
           * The subscription will first be marked as incomplete when the
           * initial payments fails and will move into incomplete_expired
           * 23 hours after that.
           *
           * @TODO (Maurice):
           * Decide what to do when this state is reached. */
          case 'incomplete':
          case 'incomplete_expired':
            break;

          /**
           * This will happen when the customer couldn't be billed.
           * It'll later change to unpaid/canceled when invoices after this
           * fail too. */
          case 'past_due':
            break;
        }

        return;

      case 'deleted': {
        /**
         * We'll lock the workspace and remove the subscription
         * as the subscription wasn't payed!. */
        workspace.isLocked                    = true;
        workspace.subscription.subscriptionId = undefined;

        await workspace.save();
        return await queue.enqueue('mail_send', {
          data:     {},
          subject:  'Your workspace is now locked',
          template: 'workspace_locked',
          receiver: emails
        });
      }

      case 'trial_will_end':
        /**
         * We should probably send out an email
         * to the admins to notify them that
         * their trial is about to end. */
        return await queue.enqueue('mail_send', {
          data:     {},
          subject:  'Your trial is ending soon',
          template: 'trial_ending',
          receiver: emails
        });
    }
  }
};

export { StripeJobs };
