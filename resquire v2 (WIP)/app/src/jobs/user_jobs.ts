/**
 * user_jobs.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { DateTime } from 'luxon';

import { Job }      from '../core';
import { User }     from '../models/user';


const UserJobs = {
  async removeNotConfirmed (job: Job<void>)
  {
    const deadline = DateTime.local().minus({ days: 5 }).toMillis();
    await User.deleteMany({
      createdAt:   { $lte: deadline },
      activatedAt: { $ne: null }
    });
  }
};

export { UserJobs };
