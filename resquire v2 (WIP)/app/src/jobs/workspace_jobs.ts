/**
 * workspace_jobs.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { Job }       from '../core';
import { User }      from '../models/user';
import { Account }   from '../models/account';
import { Workspace } from '../models/workspace';


interface WorkspaceData
{
  workspaceId: string;
}


const WorkspaceJobs = {
  async purge (job: Job<WorkspaceData>)
  {
    const { queue, data } = job;
    const { workspaceId } = data;

    /**
     * Get workspace by customer_id
     * and send a mail to the workspace admins. */
    const workspace = await Workspace.findById(workspaceId);
    if (!workspace)
      return;

    /**
     * Delete all members and accounts. */
    await User.deleteMany({ 'membership.workspace': workspace._id });
    await Account.deleteMany({ workspace: workspace._id });
  }
};

export { WorkspaceJobs };
