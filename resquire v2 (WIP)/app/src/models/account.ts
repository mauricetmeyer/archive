/**
 * account.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { Document, Schema, model } from 'mongoose';

import { IUser }                   from './user';
import { IWorkspace }              from './workspace';


interface IAccount extends Document
{
  workspace:    IWorkspace;
  owner:        IUser;

  handle:       string;
  platform:     string;
  scrapeHandle: string;

  createdAt:    number;
  activatedAt:  number;
}


const Account = model<IAccount>('Account', new Schema<IAccount>({
  workspace:    { type: Schema.Types.ObjectId, ref: 'Workspace', required: true },
  owner:        { type: Schema.Types.ObjectId, ref: 'User',      required: true },

  handle:       { type: String, required: true, index: true },
  platform:     { type: String, required: true, index: true },
  scrapeHandle: { type: String },

  createdAt:    { type: Number, required: true, default: Date.now },
  modifiedAt:   { type: Number },
  activatedAt:  { type: Number }
}));

export { IAccount, Account };
