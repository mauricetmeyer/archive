/**
 * event.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { Document, Schema, model } from 'mongoose';


interface IEvent extends Document
{
  type:      string;
  data:      any;

  createdAt: number;
}


const Event = model<IEvent>('Event', new Schema({
  type:      { type: String, required: true, index: true },
  data:      { type: Object },

  actor:     { type: Schema.Types.ObjectId, ref: 'User', index: true },
  target:    { type: Schema.Types.ObjectId, ref: 'User', index: true },

  createdAt: { type: Number, required: true, default: Date.now }
}));

export { IEvent, Event };
