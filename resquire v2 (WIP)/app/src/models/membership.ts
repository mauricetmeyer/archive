
/**
 * membership.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { Document, Schema } from 'mongoose';
import { IUser }            from './user';
import { IWorkspace }       from './workspace';


enum MembershipRole
{
  ADMIN  = 'admin',
  MEMBER = 'member'
}


interface IMembership extends Document
{
  workspace:  IWorkspace;

  invitedBy?: IUser;
  updatedBy?: IUser;

  role:       MembershipRole;

  isGuest:    boolean;
  isSystem:   boolean;

  createdAt:  number;
  modifiedAt: number;
  acceptedAt: number;
}


const Membership = new Schema<IMembership>({
  workspace:  { type: Schema.Types.ObjectId, ref: 'Workspace', required: true },

  invitedBy:  { type: Schema.Types.ObjectId, ref: 'User' },
  updatedBy:  { type: Schema.Types.ObjectId, ref: 'User' },

  role:       { type: String,  required: true, default: 'member', enum: ['admin', 'member'] },

  isGuest:    { type: Boolean, required: true, default: false },
  isSystem:   { type: Boolean, required: true, default: false },

  createdAt:  { type: Number, required: true, default: Date.now },
  modifiedAt: { type: Number },
  acceptedAt: { type: Number },
});

export { IMembership, Membership, MembershipRole };
