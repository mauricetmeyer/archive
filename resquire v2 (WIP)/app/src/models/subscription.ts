/**
 * subscription.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { Document, Schema } from 'mongoose';


interface ISubscription extends Document
{
  plan:            string;
  planVersion:     number;

  subscriptionId?: string;

  seats:           number;
  createdAt:       number;
  modifiedAt:      number;
  trialEndsdAt:    number;
}


const Subscription = new Schema<ISubscription>({
  plan:           { type: String, required: true, enum: ['monthly', 'annual'] },
  planVersion:    { type: Number, required: true },

  subscriptionId: { type: String, default: null },

  seats:          { type: Number, required: true, default: 3 },
  createdAt:      { type: Number, required: true, default: Date.now },
  modifiedAt:     { type: Number },
  trialEndsAt:    { type: Number, required: true },
});

export { Subscription, ISubscription };
