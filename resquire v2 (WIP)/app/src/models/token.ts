/**
 * token.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import * as moment                         from'moment';
import { Document, Schema, Model, model }  from 'mongoose';

import { IUser }                           from './user';
import { HashHelper, RandomHelper }        from '../helpers';


interface IToken extends Document
{
  key:        string;
  type:       string;

  target:     IUser;

  usedAt?:    number;
  expiresAt?: number;
  createdAt:  number;
}

interface ITokenModel extends Model<IToken> {
  get (type: string, key: string) : Promise<IUser>;
  valid (type: string, key: string) : Promise<boolean>;
  generate (user: IUser, type: string, expiresAt: number) : Promise<string>;
}


const schema = new Schema({
  key:       { type: String, required: true, index: true, unique: true },
  type:      { type: String, required: true, index: true },

  target:    { type: Schema.Types.ObjectId, ref: 'User' },

  expiresAt: { type: Number, required: true },
  createdAt: { type: Number, required: true, default: Date.now }
});

schema.statics.get = async function (type: string, key: string)
{
  key         = await HashHelper.sha256(key);
  const token = await this.findOneAndDelete({ key, type }).populate('target');
  if (token.expiresAt > moment().unix()) return token.target;
};

schema.statics.valid = async function (type: string, key: string)
{
  key         = await HashHelper.sha256(key);
  const token = await this.findOne({ key, type });
  if (token && token.expiresAt > moment().unix()) return true;
  return false;
};

schema.statics.generate = async function (user: IUser, type: string, expiresAt: number)
{
  const token = RandomHelper.create(64);
  const key   = await HashHelper.sha256(token);
  try {
    await this.create({
      key,
      type,
      expiresAt,
      target: user.id,
    });

    return token;
  }
  catch (err) {}
};

const Token = model<IToken, ITokenModel>('Token', schema);

export { ITokenModel, IToken, Token };
