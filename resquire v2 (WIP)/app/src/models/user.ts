/**
 * user.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import * as bcrypt                               from 'bcryptjs';
import { Document, Schema, Model, Types, model } from 'mongoose';

import { app }                                   from '../config';
import { ValidationHelper }                      from '../helpers';

import { IEvent, Event }                         from './event';
import { IUserSession, UserSession }             from './user_session';
import { IMembership, Membership }               from './membership';


interface IUser extends Document
{
  events:      IEvent[];
  sessions:    Types.DocumentArray<IUserSession>;
  membership:  IMembership;

  name:        string;
  email:       string;
  password:    string;

  staff:       boolean;
  disabled:    boolean;

  createdAt:   number;
  modifiedAt:  number;
  activatedAt: number;

  authenticate(password: string) : Promise<boolean>;
}

// , validate: ValidationHelper.isName
const UserSchema = new Schema<IUser>({
  events:       { type: [{ type: Schema.Types.ObjectId, ref: 'Event' }] },
  sessions:     { type: [UserSession] },
  membership:   { type: Membership, required: true },

  name:         { type: String },
  email:        { type: String, required: true, unique: true, index: true },
  password:     { type: String },

  staff:        { type: Boolean, default: false },
  disabled:     { type: Boolean, default: false },

  createdAt:    { type: Number, required: true, default: Date.now },
  modifiedAt:   { type: Number },
  activatedAt:  { type: Number }
});

UserSchema.pre('save', async function (nxt)
{
  const user = this as IUser;

  /**
   * We only check if the password has changed. */
  if (!this.isModified('password'))
    return nxt();

  try
  {
    /**
     * Let's update the password with the new encrypted one! */
    const saltLen = app.auth.password_salt_length;
    user.password = await bcrypt.hash(user.password, saltLen);
    nxt();
  }
  catch (err)
  {
    nxt(err);
  }
});

UserSchema.methods.authenticate = async function(password: string)
{
  return await bcrypt.compare(password, this.password);
};


const User: Model<IUser> = model<IUser>('User', UserSchema);

export { User, IUser };
