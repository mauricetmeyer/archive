/**
 * user_session.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { Document, Schema, Types }         from 'mongoose';
import { IUserSessionLog, UserSessionLog } from './user_session_log';


interface IUserSession extends Document
{
  log:       Types.DocumentArray<IUserSessionLog>;
  key:       string;

  sudoAt:    number;
  createdAt: number;
  revokedAt: number;
}


const UserSession = new Schema<IUserSession>({
  log:       { type: [UserSessionLog] },
  key:       { type: String, validate: /[a-zA-Z0-9]{22}/i, required: true, index: {
    unique: true,
    partialFilterExpression: { key: { $type: 'string' }}
  }},

  sudoAt:    { type: Number },
  createdAt: { type: Number, required: true, default: Date.now },
  revokedAt: { type: Number },
});

export { IUserSession, UserSession };
