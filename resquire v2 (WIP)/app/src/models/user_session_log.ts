/**
 * user_session_log.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { Document, Schema } from 'mongoose';


interface IUserSessionLog extends Document
{
  ip:        string;
  agent:     string;
  location:  string;
  timestamp: number;
}

const UserSessionLog = new Schema<IUserSessionLog>({
  ip:        { type: String },
  agent:     { type: String },
  location:  { type: String },
  timestamp: { type: Number, required: true, default: Date.now },
});


export { IUserSessionLog, UserSessionLog };
