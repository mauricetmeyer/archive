/**
 * workspace.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { Document, Schema, Model, model } from 'mongoose';

import { IUser }                          from './user';
import { IEvent }                         from './event';
import { IAccount }                       from './account';
import { ISubscription, Subscription }    from './subscription';


interface IWorkspace extends Document
{
  customer:     string;
  features:     Array<string>;

  events:       IEvent[];
  members:      IUser[];
  accounts:     IAccount[];
  subscription: ISubscription;

  isLocked:     boolean;
  isDisabled:   boolean;
  isSuspended:  boolean;

  createdAt:    number;
  modifiedAt:   number;
}


const Workspace: Model<IWorkspace> = model<IWorkspace>('Workspace', new Schema({
  customer:     { type: String, required: true, unique: true, index: true },
  features:     { type: [String] },

  events:       { type: [{ type: Schema.Types.ObjectId, ref: 'Event' }] },
  members:      { type: [{ type: Schema.Types.ObjectId, ref: 'User' }] },
  accounts:     { type: [{ type: Schema.Types.ObjectId, ref: 'Account' }] },
  subscription: { type: Subscription, required: true },

  isLocked:     { type: Boolean, default: false },
  isDisabled:   { type: Boolean, default: false },
  isSuspended:  { type: Boolean, default: false },

  createdAt:    { type: Number, required: true, default: Date.now },
  modifiedAt:   { type: Number },
}));

export { Workspace, IWorkspace };
