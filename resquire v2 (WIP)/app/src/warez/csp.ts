/**
 * csp.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { Context } from '../core';


type CSPOpts = { [name: string]: string | string[] };


function dashify (str: string)
{
  return str.replace(/([a-z])([A-Z])/g, '$1-$2')
            .toLowerCase();
}

function csp (opts?: CSPOpts)
{
  /**
   * @NOTE (Maurice):
   * In case that no rules are given
   * this ware will fallback to a
   * locked off CSP config. */
  const safeOpts = opts || { defaultSrc: ['none'] };


  /**
   * Transform opts into header string */
  const directive = Object.keys(safeOpts).reduce((prev, curr) => {
    const key = dashify(curr);
    let   val = safeOpts[curr];

    /**
     * No value, nothing to do for us! */
    if (!val) return prev;

    /**
     * We'll join an array
     * of values */
    if (Array.isArray(val))
      val = val.join(' ');

    return prev.concat(`${key} ${val}`);
  }, Array<string>()).join('; ');

  /**
   * @TODO (Maurice):
   * Apply some transforms for special values like:
   * 'none', 'self'. */

  /**
   * Sets CSP header
   * and continues to the next ware. */
  return async (ctx: Context, nxt: Function) => {
    ctx.setHeader('content-security-policy', directive);
    return nxt();
  };
}


export { csp, CSPOpts };
