/**
 * csrf.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import compare          from 'tsscmp';
import * as crypto      from 'crypto';
import { Context }      from '../core';
import { RandomHelper } from '../helpers';


const CSRF_LEN: number                                = 12;
const CSRF_IGNORE_METHODS: { [key: string]: boolean } = {
  GET:     true,
  HEAD:    true,
  OPTIONS: true
};

interface ICSRFOpts
{
  secret:  string;
  length?: number;
}


function createToken (secret: string, token?: string)
{
  token = token || RandomHelper.create(CSRF_LEN);
  return token + crypto.createHash('sha1').update(token + secret).digest('base64');
}

function verifyToken (secret: string, token?: string) : boolean
{
  /**
   * Early return in case that no token was given */
  if (!token) return false;
  return compare(token, createToken(secret, token.slice(0, CSRF_LEN)));
}

function csrf (opts: ICSRFOpts)
{
  if (!opts.length) opts.length = CSRF_LEN;

  const { secret } = opts;
  return async (ctx: Context, nxt: Function) => {
    ctx.setHeader('x-csrf', createToken(secret));

    /**
     * Ignore some request methods */
    if (!CSRF_IGNORE_METHODS[ctx.method])
      return nxt();

    /**
     * Verify token */
    // const token = ctx.getHeader('x-csrf') as string | undefined;
    // if (!verifyToken(secret, token))
    // {
    //   ctx.send(403, 'Invalid CSRF Token');
    //   return;
    // }

    return nxt();
  };
}

export { csrf };
