/**
 * hsts.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { Context } from '../core';


const HSTS_DEFAULT_MAX_AGE = 180 * 24 * 60 * 60;
interface IHSTSOpts
{
  maxAge?:     number;
  preload?:    boolean;
  subdomains?: boolean;
}

function hsts (opts?: IHSTSOpts)
{
  opts = opts || { maxAge: HSTS_DEFAULT_MAX_AGE };

  /**
   * Construct the header value */
  let val: string;
  val  = (opts.maxAge !== undefined) ? 'max-age=' + opts.maxAge : '';
  val += (val && opts.subdomains)    ? '; includeSubDomains' : '';
  val += (val && opts.preload)       ? '; preload' : '';

  /**
   * Sets Strict-Transport-Security header
   * and continues to the next ware. */
  return async (ctx: Context, nxt: Function) => {
    if (val) ctx.setHeader('strict-transport-security', val);
    return nxt();
  };
}


export { hsts };
