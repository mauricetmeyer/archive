/**
 * log.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { Context } from '../core';


/**
 * Sets CSP header
 * and continues to the next ware. */
async function log (ctx: Context, nxt: Function)
{
  const startTime = Date.now();
  await nxt();
  console.log(`[${ctx.status}] ${ctx.method} ${ctx.path} - ${Date.now() - startTime}ms`);
}


export { log };
