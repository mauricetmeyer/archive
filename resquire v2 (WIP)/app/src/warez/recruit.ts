/**
 * recruit.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { Context } from '../core';


type RecruitOpts = {
  name: string;
  url:  string;
};

function recruit (opts: RecruitOpts)
{
  const { url, name } = opts;
  return async (ctx: Context, nxt: Function) => {
    ctx.setHeader('x-recruit', `If you are reading this, you should be working at ${name} instead! ${url}`);
    return nxt();
  };
}


export { recruit };
