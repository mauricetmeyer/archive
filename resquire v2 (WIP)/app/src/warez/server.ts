/**
 * server.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { Context } from '../core';

function server (name?: string)
{
  /**
   * Sets Server header
   * and continues to the next ware. */
  return async (ctx: Context, nxt: Function) => {
    if (name) ctx.setHeader('server', name);
    return nxt();
  };
}

export { server };
