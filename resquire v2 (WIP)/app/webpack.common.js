const path    = require('path');
const html    = require('html-webpack-plugin');
const merge   = require('webpack-merge');
const webpack = require('webpack');

const inputDir  = 'src';
const outputDir = 'dist';
const staticDir = 'public/static';

const common = {
  module: {
    rules: [
      {
        test: /\.ts(x?)$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'babel-loader',
            options: {

            }
          },
          {
            loader: "ts-loader"
          }
        ]
      }
    ]
  },

  resolve: {
    extensions: ['.js', '.ts', '.tsx', '.json'],
    modules: [path.resolve(__dirname, 'node_modules/')]
  },

  node: {
    __dirname: false,
    __filename: false
  }

  // optimization: {
  //   splitChunks: {
  //     chunks: 'all'
  //   }
  // }
};


const runtime = merge({}, common, {
  output: {
    filename: '[name].js',
    path:     path.resolve(__dirname, outputDir)
  },

  entry: { main: path.resolve(__dirname, 'src/main') },
  target: 'node'
});

const frontend = merge({}, common, {
  output: {
    filename: '[name].js',
    path:     path.resolve(__dirname, staticDir)
  },

  entry: { app: path.resolve(__dirname, 'src/assets/ts/main') },
  target: 'web',
  module: {
    rules: [
      {
        test: /\.(jpe?g|png|gif|svg)$/,
        use: {
          loader: 'file-loader',
          options: {
            name: '[name].[ext]',
            publicPath: '/static',
          }
        }
      }
    ]
  }
});

module.exports = { runtime, frontend };
