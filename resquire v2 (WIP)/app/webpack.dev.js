const common = require('./webpack.common');
const merge  = require('webpack-merge');

const config = {
  mode:    'development',
  devtool: 'source-map'
};

const runtime  = merge({}, common.runtime, config);
const frontend = merge({}, common.frontend, config, {
  module: {
    rules: [
      {
        test: /\.(css|scss)$/,
        use: [
          'style-loader',
          'css-loader',
          'sass-loader'
        ]
      }
    ]
  }
});

module.exports = [
  //runtime,
  frontend
];