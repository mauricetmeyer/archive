const common   = require('./webpack.common');
const merge    = require('webpack-merge');
const minify   = require('babel-minify-webpack-plugin');
const extract  = require('mini-css-extract-plugin');
const analyzer = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

const config = {
  mode: 'production',
  optimization: {
    minimizer: [new minify()]
  }
};

const runtime  = merge({}, common.runtime, config);
const frontend = merge({}, common.frontend, config, {
  module: {
    rules: [
      {
        test: /\.(css|scss)$/,
        use: [
          extract.loader,
          'css-loader',
          'sass-loader'
        ]
      }
    ]
  },

  plugins: [
    new extract({ filename: 'master.css' })
  ]
});

module.exports = [
  //runtime,
  frontend
];