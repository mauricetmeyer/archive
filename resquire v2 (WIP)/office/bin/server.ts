/**
 * server.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import * as mongoose              from 'mongoose';

import { MongoHelper, JobHelper } from '../src/helpers';
import * as config                from '../src/config';
import { Server }                 from '../src/core';
import {
  log,
  server,
  version
} from '../src/warez';


/**
 * Make sure we exit on SIGTERM signal. */
process.on('SIGTERM', () => process.exit());

/**
 * Connect to database
 * and start the server */
const URI = MongoHelper.buildURI(config.database);
mongoose.connect(URI, { useFindAndModify: false, useNewUrlParser: true }).then(async () => {
  /**
   * Create server and
   * register middleware
   */
  const app = new Server();
  app.use('Log', log);
  app.use('Server', server('office'));
  app.use('Version', version(process.env['APP_VERSION'] || ''));

  /**
   * Register routes */
  config.routes(app.router);

  /**
   * Setup job queue and start the server. */
  await JobHelper.connect(config.redis);
  await app.startup(5000);
}).catch((err: Error) => console.error(err));
