/**
 * query_actions.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { Context }                    from '../core';
import { ContextHelper, StoreHelper } from '../helpers';


const QueryActions = {
  async get (ctx: Context)
  {
    try {
      /**
       * Get relevant data from the body */
      const data = await ContextHelper.json(ctx);
      if (!data) return ctx.json(400, { error: 'Bad Request' });

      /**
       * Query data with the provided parameters. */
      const { id, count, cursor } = data;
      const query   = { for: id, _id: { $gt: cursor } };
      const results = await StoreHelper.query('data').find(query).limit(count);

      const next = 0;
      ctx.json(200, { next, data: results });
    }
    catch (err)
    {
      console.error(err);
      ctx.json(500, { error: 'Internal Error' });
    }
  }
};

export { QueryActions };
