/**
 * watch_actions.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { Context }                    from '../core';
import { ContextHelper, StoreHelper } from '../helpers';
import { contentType } from 'mime-types';
import { SSL_OP_CRYPTOPRO_TLSEXT_BUG } from 'constants';


const WatchActions = {
  async add (ctx: Context)
  {
    try {
      /**
       * Get relevant data from the body */
      const data = await ContextHelper.json(ctx);
      if (!data) return ctx.json(400, { error: 'Bad Request' });

      /**
       * Construct item. */
      const { interval, platform, identifier } = data;
      const item = {
        platform,
        identifier,
        interval,

        data:         [],
        posts:        [],
        lastScrapeAt: null,
        nextScrapeAt: null
      };

      /**
       * Add item to the watchlist and
       * return resulting ObjectId. */
      const { insertedId } = await StoreHelper.query('watched').insertOne(item);
      ctx.json(200, { id: insertedId });
    }
    catch (err)
    {
      console.error(err);
      ctx.json(500, { error: 'Internal Error' });
    }
  },

  async update (ctx: Context)
  {
    try {
      /**
       * Get relevant data from the body */
      const data = await ContextHelper.json(ctx);
      if (!data) return ctx.json(400, { error: 'Bad Request' });

      /**
       * Find watched item by
       * id and update the interval. */
      const { id, interval } = data;
      await StoreHelper.query('watched').updateOne({ _id: id }, { interval });
      ctx.json(200, {});
    }
    catch (err)
    {
      console.error(err);
      ctx.json(500, { error: 'Internal Error' });
    }
  },

  async delete (ctx: Context)
  {
    try {
      /**
       * Get relevant data from the body */
      const data = await ContextHelper.json(ctx);
      if (!data) return ctx.json(400, { error: 'Bad Request' });

      /**
       * @TODO (Maurice):
       * Identify all associated data and remove it
       * from our datasets.
       *
       * Find watched item by
       * id and update the interval. */
      const { id } = data;
      ctx.json(200, {});
    }
    catch (err)
    {
      console.error(err);
      ctx.json(500, { error: 'Internal Error' });
    }
  }
};

export { WatchActions };
