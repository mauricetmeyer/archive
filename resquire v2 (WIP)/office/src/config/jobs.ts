/**
 * jobs.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import {
  TwitterJobs,
  InternalJobs,
  InstagramJobs
} from '../jobs';
import { Queue } from '../core';


const jobs = (a: Queue) => {
  /**
   * Internal */
  a.define('purge',                InternalJobs.purge);
  a.define('unstuck',              InternalJobs.unstuck);

  /**
   * Twitter */
  a.define('twitter:getTag',       TwitterJobs.getTag);
  a.define('twitter:getTweet',     TwitterJobs.getTweet);
  a.define('twitter:getProfile',   TwitterJobs.getProfile);

  /**
   * Instagram */
  a.define('instagram:getTag',     InstagramJobs.getTag);
  a.define('instagram:getPost',    InstagramJobs.getPost);
  a.define('instagram:getProfile', InstagramJobs.getProfile);
};

export { jobs };
