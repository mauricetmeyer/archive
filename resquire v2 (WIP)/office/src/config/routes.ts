/**
 * routes.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import {
  QueryActions,
  WatchActions,
  HealthActions,
} from '../actions';
import { Router } from '../core';


const routes = (r: Router) => {
  r.get('/health', HealthActions.check);

  r.get('/query', QueryActions.get);
  r.namespace('/watch', (r: Router) => {
    r.put(WatchActions.add);
    r.post(WatchActions.update);
    r.delete(WatchActions.delete);
  });

  /**
   * Wildcard (400 Bad Request) */
  r.get('*', async (ctx) => {
    ctx.json(400, { error: 'Bad Request' });
  });
};

export { routes };
