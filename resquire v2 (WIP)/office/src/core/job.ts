/**
 * job.ts
 *
 * Author: Simon Schwarz
 * E-Job: black-simon@hotmail.de
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import * as Bull from 'bull';
import { Queue } from './queue';
import { ILog }  from '../interfaces';


class Job<T = any>
{
  public readonly data:  T;
  public readonly type:  string;
  public readonly queue: Queue;

  private handle:   Bull.Job;
  private messages: ILog[];


  /**
   * Get a new job
   *
   * @param  {}
   * @return {Job<T>}
   */
  constructor (queue: Queue, job: Bull.Job)
  {
    const { type, data } = job.data;
    this.type     = type;
    this.data     = data;
    this.queue    = queue;
    this.handle   = job;
    this.messages = [];
  }


  /**
   * Log info to job
   *
   * @param  {string} msg
   * @return {Promise<void>}
   */
  info (msg: string)
  {
    this.log('info', msg);
  }

  /**
   * Log warning to job
   *
   * @param  {string} msg
   * @return {Promise<void>}
   */
  warn (msg: string)
  {
    this.log('warning', msg);
  }

  /**
   * Log error to job
   *
   * @param  {string} msg
   * @return {Promise<void>}
   */
  error (msg: string)
  {
    this.log('error', msg);
  }

  /**
   * Log debug info to job
   *
   * @param  {string} msg
   * @return {Promise<void>}
   */
  debug (msg: string)
  {
    this.log('debug', msg);
  }


  /**
   * Repeat the job at a later point in time
   * defined by **interval**.
   *
   * @param {number} interval
   */
  async repeat (interval: number)
  {

  }

  /**
   * Broadcast progress
   *
   * @param  {number} progress
   * @return {Promise<void>}
   */
  async progress(progress: number)
  {
    this.handle.progress(progress);
  }


  /**
   * Log to job
   *
   * @param  {string} type
   * @param  {string} message
   * @return {Promise<void>}
   */
  private log (type: string, message: string)
  {
    this.messages.push({
      type,
      message,
      at: Date.now()
    });
  }
}

export { Job };
