/**
 * matcher.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


// tslint:disable:ban-types
import { Disposable }         from 'event-kit';
import { exec, match, parse } from 'matchit';



type Route<T> = {
  params:  {},
  handler: T
};


class Matcher<T>
{
  private routes:   any[];
  private handlers: Map<string, T>;


  constructor ()
  {
    this.routes   = [];
    this.handlers = new Map();
  }


  /**
   * Add a new command
   *
   * @param  {string} cmd
   * @param  {T} fn
   * @return {Disposable}
   */
  public add (cmd: string, fn: T)
  {
    const parsed = parse(cmd);
    this.routes.push(parsed);
    this.handlers.set(cmd, fn);
    return new Disposable(() => {
      const idx = this.routes.indexOf(parsed);
      if (-1 === idx) return;
      this.routes.splice(idx, 1);
      this.handlers.delete(cmd);
    });
  }

  /**
   * Resolve cmd to route
   *
   * @param  {string} cmd
   * @return {T}
   */
  public resolve (cmd: string) : Route<T> | undefined
  {
    const arr = match(cmd, this.routes || []);
    if (arr.length === 0) return;

    return {
      params:  exec(cmd, arr) || {},
      handler: this.handlers.get(arr[0].old) as T
    };
  }
}

export { Matcher };
