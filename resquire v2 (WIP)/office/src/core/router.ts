/**
 * router.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { Disposable } from 'event-kit';
import { join }       from 'path';
import { Context }    from './context';
import { Matcher }    from './matcher';


type RouterMethod    = 'GET' | 'PUT' | 'POST' | 'PATCH' | 'DELETE';
type RouterAction    = (ctx: Context, nxt?: Function) => Promise<void>;
type RouterNamespace = (r: Router) => void;


class Router
{
  private name?:   string;
  private methods: { [key: string]: Matcher<RouterAction> };


  /**
   * Create a new router
   *
   * @param  {RouterMethods} optional methods
   * @return {Router}
   */
  public constructor ();
  public constructor (name: string, methods: { [key: string]: Matcher<RouterAction> });
  public constructor (name?: string, methods?: { [key: string]: Matcher<RouterAction> })
  {
    /**
     *  Initialize methods */
    this.name    = name;
    this.methods = methods || {};
  }


  /**
   * Find matching route
   * 
   * @param {} method
   * @param {} path
   */
  public find (method: string, path: string)
  {
    if (this.methods[method])
      return this.methods[method].resolve(path);
    return;
  }


  /**
   * Register ``GET`` route
   *
   * @param  {string?}      path
   * @param  {RouterAction} fn
   * @return {Disposable}
   */
  public get (fn: RouterAction) : Disposable;
  public get (path: string, fn: RouterAction) : Disposable;
  public get (path: string | RouterAction, fn?: RouterAction) : Disposable
  {
    return this.route('GET', path, fn);
  }

  /**
   * Register ``PUT`` route
   *
   * @param  {string}       path
   * @param  {RouterAction} fn
   * @return {Disposable}
   */
  public put (fn: RouterAction) : Disposable;
  public put (path: string, fn: RouterAction) : Disposable;
  public put (path: string | RouterAction, fn?: RouterAction) : Disposable
  {
    return this.route('POST', path, fn);
  }

  /**
   * Register ``POST`` route
   *
   * @param  {string}       path
   * @param  {RouterAction} fn
   * @return {Disposable}
   */
  public post (fn: RouterAction) : Disposable;
  public post (path: string, fn: RouterAction) : Disposable;
  public post (path: string | RouterAction, fn?: RouterAction) : Disposable
  {
    return this.route('POST', path, fn);
  }

  /**
   * Register ``PATCH`` route
   *
   * @param  {string}       path
   * @param  {RouterAction} fn
   * @return {Disposable}
   */
  public patch (fn: RouterAction) : Disposable;
  public patch (path: string, fn: RouterAction) : Disposable;
  public patch (path: string | RouterAction, fn?: RouterAction) : Disposable
  {
    return this.route('PATCH', path, fn);
  }

  /**
   * Register ``DELETE`` route
   *
   * @param  {string}       path
   * @param  {RouterAction} fn
   * @return {Disposable}
   */
  public delete (fn: RouterAction) : Disposable;
  public delete (path: string, fn: RouterAction) : Disposable;
  public delete (path: string | RouterAction, fn?: RouterAction) : Disposable
  {
    return this.route('DELETE', path, fn);
  }


  /**
   * Route namespace
   *
   * @param {string}          name
   * @param {RouterNamespace} fn
   */
  public namespace (name: string, fn: RouterNamespace) : void
  {
    name = this.name ? join(this.name, name) : name;
    const namespacedRouter = new Router(name, this.methods);
    fn(namespacedRouter);
  }



  /**
   * **INTERNAL**
   * Register route
   *
   * @param  {string}       method
   * @param  {string?}      path
   * @param  {RouterAction} fn
   * @return {Disposable}
   */
  private route (method: RouterMethod, pathOrFn: string | RouterAction, fn?: RouterAction) : Disposable
  {
    /**
     * Handle function overloading */
    let path;
    if (typeof pathOrFn === 'function')
    {
      fn   = pathOrFn;
      path = '';
    } else path = pathOrFn;

    /**
     * No function... ERROR!!! */
    if (!fn) throw new Error('RouterAction missing!');

    /**
     * Transform path when we are in a subrouter */
    path = this.name ? join(this.name, path) : path;

    /**
     * First time seeing this method? */
    if (!this.methods[method])
      this.methods[method] = new Matcher();
    return this.methods[method].add(path, fn);
  }
}

export { Router };
