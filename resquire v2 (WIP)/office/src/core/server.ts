/**
 * cache.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { createServer, Server as HTTPServer, IncomingMessage, ServerResponse } from 'http';
import { Stream }                                        from 'stream';
import { Disposable, Emitter }                           from 'event-kit';

import { Router }                                        from './router';
import { Context }                                       from './context';


type ServerWare   = { name: string, fn: ServerAction };
type ServerAction = (ctx: Context, nxt: Function) => Promise<void>;

class Server
{
  public  router:  Router;

  private wares:   ServerWare[];
  private emitter: Emitter;
  private server:  HTTPServer;


  constructor ()
  {
    this.wares   = [];
    this.router  = new Router();
    this.emitter = new Emitter();
    this.server  = createServer((req, res) => this.handle(req, res));
  }


  /**
   * Start the server
   *
   * @param  {Number} port
   * @return {Disposable}
   */
  public async startup (port: number)
  {
    /**
     * Start HTTP server. */
    this.server.listen(port);

    /**
     * Return disposable that can be used to stop
     * the server. */
    return new Disposable(() => {
      this.server.close();
      this.emitter.emit('on-close');
    });
  }


  /**
   * Register middleware
   *
   * @param  {string}   name
   * @param  {Function} fn
   * @return {Disposable}
   */
  public use (name: string, fn: ServerAction) : Disposable
  {
    const opts = { name, fn };
    this.wares.push(opts);
    return new Disposable(() => {
      const idx = this.wares.indexOf(opts);
      if (-1 !== idx) this.wares.splice(idx, 1);
    });
  }


  /**
   * Attach ``error`` handle
   *
   * @param  {Function} fn
   * @return {Disposable}
   */
  public onError (fn: (error: Error) => void)
  {
    return this.emitter.on('on-error', fn);
  }

  /**
   * Attach ``close`` handle
   *
   * @param  {Function} fn
   * @return {Disposable}
   */
  public onClose (fn: () => void)
  {
    return this.emitter.on('on-close', fn);
  }


  /**
   * **INTERNAL**
   * Handle req/res
   *
   * @param {Object} req
   * @param {Object} res
   */
  private handle (req: IncomingMessage, res: ServerResponse)
  {
    /**
     * Create context and wares chain */
    const ctx   = new Context(req, res);
    const arr   = this.wares.slice(0);
    const route = this.router.find(ctx.method, ctx.path || '/');
    const chain = async () => {
      let   idx  = 0;
      const len  = arr.length;
      const nxt  = (err: any) => err ? Promise.reject(err) : loop();
      const loop = async () => {
        if (idx < len)
        {
          const { fn, name } = arr[idx++];
          const fail         = (err: any) => Promise.reject(`[${name}]: ${err.message}`);
          return Promise.resolve(fn(ctx, nxt).catch(fail));
        }
      };

      return loop();
    };

    /**
     * If we found a matching route we'll
     * now is the time to execute it. */
    if (route)
    {
      arr.push({
        name: 'Router',
        fn:   route.handler
      });
    }

    /**
     * Execute chain */
    chain()
      .then(() => this.respond(ctx))
      .catch((err) => {
        ctx.status = 500;
        this.emitter.emit('on-error', err);
      });
  }


  /**
   * **INTERNAL**
   * Respond to HTTP request
   *
   * @param  {Context} ctx
   */
  private respond (ctx: Context)
  {
    const body = ctx.body;

    /**
     * In cases where the body is a stream
     * we will pipe it right to the response. */
    if (body instanceof Stream)
      return body.pipe(ctx.res);

    /**
     * END IT!!! */
    ctx.end(body);
  }
}

export { Server };
