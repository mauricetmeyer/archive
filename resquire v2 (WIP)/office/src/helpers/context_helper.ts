/**
 * context_helper.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import * as rawBody from 'raw-body';
import { Context }  from '../core';


async function raw (ctx: Context) : Promise<Buffer>;
async function raw (ctx: Context, encoding: 'utf-8' | 'base64') : Promise<string>;
async function raw (ctx: Context, encoding?: 'utf-8' | 'base64') : Promise<Buffer | string>
{
  return await rawBody(ctx.req, { encoding });
}


const ContextHelper = {
  raw,
  async json (ctx: Context) : Promise<any>
  {
    const type = ctx.type;
    if (type !== 'application/json')
      throw new Error('Invalid request format');

    const body = await this.raw(ctx, 'utf-8');
    const json = JSON.parse(Buffer.isBuffer(body) ? body.toString('utf-8') : body);
    return json;
  }
};

export { ContextHelper };
