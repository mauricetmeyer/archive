/**
 * job_helper.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { DateTime }                   from 'luxon';
import { Queue, QueueConnectionOpts } from '../core';


let   JobDriver: Queue;
const JobHelper = {
  async schedule<T> (name: string, data: T) : Promise<void>
  {
    await JobDriver.enqueue(name, data);
  },

  async scheduleIn<T>(name: string, time: number, data: T) : Promise<void>
  {
    await JobDriver.enqueueIn(name, time, data);
  },

  async scheduleAt<T>(name: string, time: DateTime, data: T) : Promise<void>
  {
    await JobDriver.enqueueAt(name, time, data);
  },


  /**
   * Setup connection.
   *
   * @param  {QueueConnectionOpts} connection
   * @return {Promise<void>}
   */
  async connect (connection: QueueConnectionOpts)
  {
    JobDriver = new Queue('jobs', { connection });
  }
};

export { JobHelper };
