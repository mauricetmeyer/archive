/**
 * request_helper.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import axios from 'axios';
import Agent from 'user-agents';


const RequestHelper = {
  async get<T = any> (url: string)
  {
    const agent  = new Agent({ deviceCategory: 'desktop' });
    const config = {
      headers: {
        'user-agent': agent.toString()
      }
    };

    return axios.get<T>(url, config);
  },

  async post<T = any> (url: string, data: any)
  {
    const agent  = new Agent({ deviceCategory: 'desktop' });
    const config = {
      headers: {
        'user-agent': agent.toString()
      }
    };

    return axios.post<T>(url, data, config);
  }
};

export { RequestHelper };
