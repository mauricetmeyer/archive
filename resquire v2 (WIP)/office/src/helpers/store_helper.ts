/**
 * store_helper.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { DateTime }        from 'luxon';
import { MongoClient, Db } from 'mongodb';


let   StoreDriver: Db;
const StoreHelper = {
  /**
   * Execute query on the specified table.
   *
   * @param  {string} table
   * @return {Collection}
   */
  query (table: string)
  {
    return StoreDriver.collection(table);
  },

  /**
   * Setup connection.
   *
   * @param  {QueueConnectionOpts} connection
   * @return {Promise<void>}
   */
  async connect (connectionUrl: string)
  {
    const connection = await MongoClient.connect(connectionUrl, { useNewUrlParser: true, native_parser: true });
    StoreDriver = connection.db();
  }
};

export { StoreHelper };
