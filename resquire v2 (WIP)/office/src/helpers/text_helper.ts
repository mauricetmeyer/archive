/**
 * text_helper.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */



const REG_HASHTAG = /((?:^|[^a-zA-Z0-9_!#$%&*@]:?))(#)([a-zA-Z\u00E0-\u00FC0-9_]{1,20})/gi;
const REG_MENTION = /((?:^|[^a-zA-Z0-9_!#$%&*@]:?))(@)([a-zA-Z0-9_]{1,20})/g;

interface TextIndex
{
  value:   string;
  indices: [number, number];
}

const TextHelper = {
  /**
   * Get all hashtags from the string.
   *
   * @param  {string} content
   * @return {string[]}
   */
  hashtags (content: string) : string[]
  {
    return this.hashtagIndices(content).map(val => val.value);
  },

  /**
   * Get all mentions from the string.
   *
   * @param  {string} content
   * @return {string[]}
   */
  mentions (content: string) : string[]
  {
    return this.mentionIndices(content).map(val => val.value);
  },


  /**
   * Get hashtag indices from the string
   *
   * @param  {string} content
   * @return {TextIndex[]}
   */
  hashtagIndices (content: string) : TextIndex[]
  {
    if (!content || !content.match(/#/))
      return [];

    const indices: TextIndex[] = [];
    content.replace(REG_HASHTAG, (match, before, _, value, offset, full) => {
      const after = full.slice(offset + match.length);
      if (!after.match(/^(?:#|:\/\/)/))
      {
        const start = offset + before.length;
        const end   = start + value.length + 1;
        indices.push({ value, indices: [start, end] });
      }
    });

    return indices;
  },

  /**
   * Get mention indices from the string
   *
   * @param  {string} content
   * @return {TextIndex[]}
   */
  mentionIndices (content: string) : TextIndex[]
  {
    if (!content || !content.match(/@/))
      return [];

    const indices: TextIndex[] = [];
    content.replace(REG_MENTION, (match, before, _, value, offset, full) => {
      const after = full.slice(offset + match.length);
      if (!after.match(/^(?:@|:\/\/)/))
      {
        const start = offset + before.length;
        const end   = start + value.length + 1;
        indices.push({ value, indices: [start, end] });
      }
    });

    return indices;
  }
};

export { TextHelper };
