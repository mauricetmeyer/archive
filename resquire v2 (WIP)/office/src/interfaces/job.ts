/**
 * job.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { ILog } from './log';


interface IJob
{
  type:       string;
  status:     string;
  identifier: number;

  log:        ILog[];

  runAt:      number;
  doneAt:     number;
  queuedAt:   number;
}

export { IJob };
