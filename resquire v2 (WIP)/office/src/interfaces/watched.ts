/**
 * watched.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


interface IWatched
{
  platform:       string;
  identifier:     string;

  data:           string[];
  jobs:           string[];

  interval:       number;
  periodEndAt?:   number;
  periodStartAt?: number;

  lastScrapeAt?:  number;
  nextScrapeAt?:  number;
}

export { IWatched };
