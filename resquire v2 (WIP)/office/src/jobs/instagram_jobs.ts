/**
 * instagram_jobs.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { Job }                                    from '../core';
import { Endpoints }                              from '../config';
import { StoreHelper, RequestHelper, TextHelper } from '../helpers';

import { JobData }                                from './data';


function toWatched (identifier: string, platform: string, type: string)
{
  return {
    type,
    platform,
    identifier,
    interval,

    jobs:         [],
    data:         [],
    lastScrapeAt: null,
    nextScrapeAt: null
  };
}


const InstagramUrl  = Endpoints.INSTAGRAM;
const InstagramJobs = {
  async getTag (job: Job<JobData>)
  {
    const { data } = job;
    const { id }   = data;

    /**
     * Get item from the database first.
     * if there is none we'll stop there. */
    const watched = await StoreHelper.query('watcher').findOne({ _id: id });
    if (!watched) return;

    const { identifier } = watched;
    const res = await RequestHelper.get(`${InstagramUrl}/explore/tags/${identifier}`);
    if (res.status !== 200)
    {
      /**
       * If the request returned a none 200 status code we'll log
       * that to the database, so that we can investigate the cause. */
      await job.error(`Request returned with '${res.status} ${res.statusText}'.`);
      return;
    }
  },

  async getPost (job: Job<JobData>)
  {
    const { data } = job;
    const { id }   = data;

    /**
     * Get item from the database first.
     * if there is none we'll stop there. */
    const watched = await StoreHelper.query('watched').findOne({ _id: id });
    if (!watched) return;

    const { identifier } = watched;
    const res = await RequestHelper.get(`${InstagramUrl}/p/${identifier}`);
    if (res.status !== 200)
    {
      /**
       * If the request returned a none 200 status code we'll log
       * that to the database, so that we can investigate the cause. */
      await job.error(`Request returned with '${res.status} ${res.statusText}'.`);
      return;
    }
  },

  async getProfile (job: Job<JobData>)
  {
    const { data } = job;
    const { id }   = data;

    /**
     * Get item from the database first.
     * if there is none we'll stop there. */
    const watched = await StoreHelper.query('watcher').findOne({ _id: id });
    if (!watched) return;

    const { identifier } = watched;
    const res = await RequestHelper.get(`${InstagramUrl}/${identifier}`);
    if (res.status !== 200)
    {
      /**
       * If the request returned a none 200 status code we'll log
       * that to the database, so that we can investigate the cause. */
      await job.error(`Request returned with '${res.status} ${res.statusText}'.`);
      return;
    }
  }
};

export { InstagramJobs };
