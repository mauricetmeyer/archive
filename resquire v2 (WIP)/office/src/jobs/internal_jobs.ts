/**
 * internal_jobs.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


import { Job }         from '../core';
import { StoreHelper } from '../helpers';

import { JobData }     from './data';


const InternalJobs = {
  async purge (job: Job<JobData>)
  {
    throw new Error('Not implemented!');
    StoreHelper.query('data');
    StoreHelper.query('watched');
  },

  async unstuck (job: Job<JobData>)
  {
    throw new Error('Not implemented!');
    StoreHelper.query('data');
    StoreHelper.query('watched');
  }
};

export { InternalJobs };
