/**
 * user-agents.d.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


declare module 'user-agents' {
  export interface UserAgentData
  {
    vendor:         string;
    platform:       string;
    userAgent:      string;

    appName:        string;
    deviceCategory: string;

    screenWidth:    number;
    screenHeight:   number;

    viewportWidth:  number;
    viewportHeight: number;
  }

  export default class UserAgent
  {
    constructor (filters: any);

    data: UserAgentData;

    random () : UserAgent;
    toString () : string;
  }
}