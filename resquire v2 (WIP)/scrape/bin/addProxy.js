/**
 * addProxy.js
 * 
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 * 
 * (c) Laviréo
 */


const Source         = require('proxy-lists');

const list           = require('../lib/list');
const database       = require('../lib/database');

const db      = new database;
const proxies = new list(db, 'proxies');

db.connect(`mongodb://localhost`, 'instagram')
  .then(async () => {
    //const args = process.argv.slice(2);
    const getter = Source.getProxies({
      filterMode:       'strict',
      countries:        ['de', 'fr', 'nl', 'pl', 'at', 'dk', 'lu', 'ch', 'se'],
      protocols:        ['https'],
      anonymityLevels:  ['anonymous', 'elite'],
      //sourcesBlackList: ['bitproxies', 'kingproxies', 'proxies24']
    });

    getter.on('data', (entries) => {
      for (let i = 0; i < entries.length; i++)
      {
        const proxy = entries[i]; 
        const host  = `${proxy.ipAddress}:${proxy.port}`;
        proxies.add({ host }).catch((err) => console.log(`Already contains: ${host}`));
        console.log(host);
      }
    });

    getter.on('error', (err) => {});
    getter.on('end', () => console.log('DONE!!!'));
  })
  .catch((err) => console.error(err));
