/**
 * enqueueMissing.js
 * 
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 * 
 * (c) Laviréo
 */


const progress = require('cli-progress');

const list     = require('../lib/list');
const queue    = require('../lib/queue');
const database = require('../lib/database');


const URL_BASE = 'https://www.instagram.com';
const URL_TAG = `${URL_BASE}/explore/tags`;
const URL_POST = `${URL_BASE}/p`;

const q             = new queue({ name: 'jobs', isWorker: false, getEvents: false });
const db            = new database;
const data          = new list(db, 'data');
const links         = new list(db, 'links');
const visited_tags  = new list(db, 'v_tags');
const visited_posts = new list(db, 'v_posts');
const visited_users = new list(db, 'v_users');


async function checkTag(url)
{
  const tag      = url.substring(URL_TAG.length + 1);
  const contains = await visited_tags.has({ tag });
  return !!contains;
}

async function checkPost(url)
{
  const shortcode = url.substring(URL_POST.length + 1);
  const contains  = await visited_posts.has({ shortcode });
  return !!contains;
}

async function checkUser(url)
{
  const username = url.substring(URL_BASE.length + 1);
  const contains = await visited_users.has({ username });
  return !!contains;
}

(async function ()
{
  await db.connect(`mongodb://localhost`, 'instagram');

  const bar       = new progress.Bar({}, progress.Presets.legacy);
  const index     = process.argv[2];
  const rawQueue  = q.handle;
  const jobCounts = await rawQueue.checkHealth();

  const key       = rawQueue.toKey('failed');
  console.log(key);
  rawQueue.client.smembers(key, (err, results) => {
    bar.start(results.length, 0);
    if (err) {
      return (err);
    }

    
    
    (async function ()
    {
      for (let i = 0; i < results.length; i++)
      {
        const job = await rawQueue.getJob(results[i]);
        await job.retry();
        bar.update(i);
      }

      bar.stop();
    })().catch((err) => {
      console.error(err);
    });
  });

//  bar.start(jobCounts.newestJob, 0);
  //for (let i = index; i <= jobCounts.newestJob; i++)
  //{
  //  let   error = null;
  //  const job   = await rawQueue.getJob(i).catch(err => error = err);
  //  if (error)
  //  {
  //    console.log(error);
  //    //continue;
  //  }

  //  if (!job) continue;

  //  if (['created', 'succeeded'].includes(job.status))
  //    continue;
  //  
  //  if (job.status === 'failed')
  //  {
  //    await job.retry();
  //    bar.update(i);
  //    continue;
  //  }

  //  break;
  //}
})().catch((err) => {
  console.error(err);
});
