/**
 * enqueue.js
 * 
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 * 
 * (c) Laviréo
 */


const queue = require('../lib/queue');
const q     = new queue({ name: 'jobs', delay: 2000 });
(async function ()
{
  q.handle.checkStalledJobs(5000, (err, numStalled) => {
    console.log('Checked stalled jobs', numStalled);
  });

  const args = process.argv.slice(2);
  for (let i = 0; i < args.length; i++)
  {
    console.log(`Adding ${args[i]}`);
    await q.enqueue({ url: args[i] });
  }
  
})().catch((err) => {
  console.error(err);
});;
