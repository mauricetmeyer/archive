/**
 * scrape.js
 * 
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 * 
 * (c) Laviréo
 */


const fs = require('fs-plus');
const log = require('file-stream-rotator');
const url = require('url');
const path = require('path');
const zlib = require('zlib');
const http = require('http');
const https = require('https');
const cheer = require('cheerio');
const mkdirp = require('mkdirp');
const driver = require('puppeteer');
const parse  = require('argparse').ArgumentParser;

const random = require('../lib/util').random;
const list = require('../lib/list');
const queue = require('../lib/queue');
const scraper = require('../lib/scraper');
const database = require('../lib/database');


/**
 * Constants */
mkdirp('tmp');
const POSTS_MIN = 50;
const POSTS_MAX = 200;
const TAG_DELAY = 1000 * 60 * 60 * 3;
const URL_BASE = 'https://www.instagram.com/';
const URL_TAG = `${URL_BASE}explore/tags/`;
const URL_POST = `${URL_BASE}p/`;
const USER_AGENT = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_5) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.0 Safari/605.1.15";

const parser = new parse({
  version:     '1.0',
  description: 'Neuron Scraper'
});

parser.addArgument(['-p', '--proxy'], { help: 'Proxy Server' });
parser.addArgument(['-m', '--master'], { help: 'Master Server' });


/**
 * Text helpers */
var J  = /((?:^|[^a-zA-Z0-9_!#$%&*@]:?))(#)([a-zA-Z0-9_]{1,20})(\/[a-zA-Z][a-zA-Z0-9_-]{0,24})?/gi;
var it = /((?:^|[^a-zA-Z0-9_!#$%&*@]:?))(@)([a-zA-Z0-9_]{1,20})(\/[a-zA-Z][a-zA-Z0-9_-]{0,24})?/g;

function hashtagIndices(text) {
  if (!text || !text.match(/#/))
    return [];

  var indices = [];
  return text.replace(J, function (t, e, n, match, unused, offset, full) {
    if (!full.slice(offset + t.length).match(/^(?:#|:\/\/)/)) {
      var start = offset + e.length;
      var end = start + match.length + 1;
      indices.push({
        hashtag: match,
        indices: [start, end]
      })
    }
  }), indices
}

function mentionIndices(text) {
  if (!text || !text.match(/@/))
    return [];

  var indices = [];
  return text.replace(it, function (t, i, n, match, unused, offset, full) {
    if (!full.slice(offset + t.length).match(/^(?:@|:\/\/)/)) {
      var start = offset + i.length;
      var end = start + match.length + 1;

      indices.push({
        mention: match,
        indices: [start, end]
      })
    }
  }), indices
}

function getHashtags(text) {
  return hashtagIndices(text).map(function (val) {
    return val.hashtag
  });
}

function getMentions(text) {
  return mentionIndices(text).map(function (val) {
    return val.mention
  });
}


// createRequest(URL_START).then(($) => {
//   const res = $('script').filter((i, el) => {
//     if (!el.children[0]) return false;
//     const data = el.children[0].data;
//     return data.startsWith('window._sharedData');
//   }).map((i, el) => el.children[0].data);

//   const raw  = res[0].split('window._sharedData = ')[1];
//   const data = raw.substr(0, raw.length - 1);
//   console.log(JSON.parse(data));
// })



/**
 * Scraping helpers */
const scrapeTag = async (job, page, url) => {
  console.log("----- TAG -----");
  const amount = random(POSTS_MIN, POSTS_MAX);
  const res = await page.evaluate(() => window._sharedData.entry_data.TagPage[0].graphql.hashtag);

  /**
   * Make data accessible */
  const tag = res.name;
  const posts = res.edge_hashtag_to_media;
  const top_posts = res.edge_hashtag_to_top_posts;
  const posts_num = res.edge_hashtag_to_media.count;

  const visit = await visited_tags.has({ tag });
  if (visit && visit.when + TAG_DELAY > Date.now())
    return;

  console.log(`TAG: ${tag}`);
  await visited_tags.add({ tag });

  const first_posts = posts.edges;
  for (let i = 0; i < first_posts.length; i++) {
    const node = first_posts[i].node;
    const shortcode = node.shortcode;

    console.log(`LINK: tag ${tag} ${shortcode}`);
    await links.add({
      type: 'tag',
      from: {
        type: 'tag',
        url
      },
      to: {
        type: 'post',
        url: `${URL_POST}${shortcode}`
      }
    })

    if (!await visited_posts.has({ shortcode }))
      await q.enqueue({ url: `${URL_POST}${shortcode}` });
  }
};

const scrapePost = async (job, page, url) => {
  console.log("----- POST -----");
  const res = await page.evaluate(() => window._sharedData.entry_data.PostPage[0].graphql.shortcode_media);

  /**
   * Make data accessible */
  const id = res.id;
  const likes = res.edge_media_preview_like;
  const owner = res.owner;
  const comments = res.edge_media_to_comment;
  const captions = res.edge_media_to_caption;
  const shortcode = res.shortcode;
  const dimensions = res.dimensions;

  const likes_num = likes.count || 0;
  const captions_num = captions.count || 1;
  const comments_num = comments.count || 0;

  if (await visited_posts.has({ shortcode }))
  {
    console.log("Post has already been scraped");
    return;
  }

  console.log(`POST: ${shortcode}`);
  await data.add({
    url,
    type: 'post',
    raw: res
  });

  await visited_posts.add({ shortcode });

  /**
   * Scrape the owner too if we haven't seen him yet as it could
   * be that we came from the tag page. */
  const username = owner.username;
  if (!await visited_users.has({
      username
    }))
    q.enqueue({
      url: `${URL_BASE}${username}`
    });

  /**
   * Scan post caption for mentions
   * and tags. */
  const first_captions = captions.edges;
  for (let i = 0; i < first_captions.length; i++) {
    const node     = first_captions[i].node;
    const text     = node.text;
    const tags     = getHashtags(text);
    const mentions = getMentions(text);

    for (let i = 0; i < tags.length; i++) {
      console.log(`LINK: caption_tag ${shortcode} ${tags[i]}`);
      await links.add({
        type: 'caption',
        from: {
          type: 'post',
          url
        },
        to: {
          type: 'tag',
          url: `${URL_TAG}${tags[i]}`
        }
      });

      await q.enqueue({ url: `${URL_TAG}${tags[i]}` });
    }

    for (let i = 0; i < mentions.length; i++) {
      console.log(`LINK: caption_mention ${shortcode} ${mentions[i]}`);
      await links.add({
        type: 'caption',
        from: {
          type: 'post',
          url
        },
        to: {
          type: 'user',
          url: `${URL_BASE}${mentions[i]}`
        }
      });

      if (!await visited_users.has({ username: mentions[i] }))
        await q.enqueue({ url: `${URL_BASE}${mentions[i]}` });
    }
  }


  /**
   * Scan each comment for tags and mentions,
   * if there are any we scrape them too. */
  const first_likes = likes.edges;
  for (let i = 0; i < first_likes.length; i++) {
    const node = first_likes[i].node;
    const username = node.username;

    console.log(`LINK: like ${shortcode} ${username}`);
    await links.add({
      type: 'like',
      from: {
        type: 'post',
        url
      },
      to: {
        type: 'user',
        url: `${URL_BASE}${username}`
      }
    });

    if (!await visited_users.has({ username }))
      await q.enqueue({ url: `${URL_BASE}${username}` });
  }

  /**
   * Scan each comment for tags and mentions,
   * if there are any we scrape them too. */
  const first_comments = comments.edges;
  for (let i = 0; i < first_comments.length; i++) {
    const node = first_comments[i].node;
    const text = node.text;
    const username = node.owner.username;

    console.log(`LINK: comment ${shortcode} ${username}`);
    await links.add({
      type: 'comment',
      from: {
        type: 'post',
        url
      },
      to: {
        type: 'user',
        url: `${URL_BASE}${username}`
      }
    });

    if (!await visited_users.has({ username }))
      await q.enqueue({ url: `${URL_BASE}${username}` });

    /**
     * Extract tags and mentions
     * from the comment text */
    const tags = getHashtags(text);
    const mentions = getMentions(text);

    for (let i = 0; i < tags.length; i++) {
      console.log(`LINK: comment_tag ${shortcode} ${tags[i]}`);
      await links.add({
        type: 'comment',
        from: {
          type: 'post',
          url
        },
        to: {
          type: 'tag',
          url: `${URL_TAG}${tags[i]}`
        }
      });

      await q.enqueue({ url: `${URL_TAG}${tags[i]}` });
    }

    for (let i = 0; i < mentions.length; i++) {
      console.log(`LINK: comment_mention ${shortcode} ${mentions[i]}`);
      await links.add({
        type: 'comment',
        from: {
          type: 'post',
          url
        },
        to: {
          type: 'user',
          url: `${URL_BASE}${mentions[i]}`
        }
      });

      if (!await visited_users.has({ username: mentions[i] }))
        await q.enqueue({
          url: `${URL_BASE}${mentions[i]}`
        });
    }
  }
}

const scrapeUser = async (job, page, url) => {
  console.log("----- USER -----");
  const res = await page.evaluate(() => window._sharedData.entry_data.ProfilePage[0].graphql.user);

  /**
   * Make data accessible */
  const id = res.id;
  const name = res.full_name;
  const posts = res.edge_owner_to_timeline_media;
  const username = res.username;
  const biography = res.biography;


  const posts_num = posts.count;
  const follows_num = res.edge_follow.count;
  const followers_num = res.edge_followed_by.count;

  const is_private = res.is_private;
  const is_verified = res.is_verified;

  const first_posts = posts.edges;

  if (await visited_users.has({ username }))
    return;

  console.log(`USER: ${username}`);
  await data.add({
    url,
    type: 'user',
    raw: res
  });
  await visited_users.add({
    username
  });

  /**
   * Queue each post for scraping */
  for (let i = 0; i < first_posts.length; i++) {
    const node = first_posts[i].node;
    const shortcode = node.shortcode;

    console.log(`LINK: profile ${username} ${shortcode}`);
    await links.add({
      type: 'profile',
      from: {
        type: 'user',
        url
      },
      to: {
        type: 'post',
        url: `${URL_POST}${shortcode}`
      }
    });

    if (!await visited_posts.has({ shortcode }))
      q.enqueue({ url: `${URL_POST}${shortcode}` });
  }
}

const scrape = async (job, page, url) => {

  /**
   * Decide on how to process the data */
  if (url.startsWith(URL_TAG))
    return scrapeTag(job, page, url);
  else if (url.startsWith(URL_POST))
    return scrapePost(job, page, url);
  else if (url.startsWith(URL_BASE))
    return scrapeUser(job, page, url);

  await q.enqueue({ url });
  return 'no_wait';
};

/**
 * Let's get to the action */
const db            = new database;
const args          = parser.parseArgs();
const proxy         = args.proxy;
const master        = args.master || "localhost";

const data          = new list(db, 'data');
const links         = new list(db, 'links');
const proxies       = new list(db, 'proxies');
const visited_tags  = new list(db, 'v_tags');
const visited_posts = new list(db, 'v_posts');
const visited_users = new list(db, 'v_users');

let q;

db.connect(`mongodb://${master}`, 'instagram')
  .then(async () => {
    /**
     * Helper function to reload
     * the scraper in case an
     * error happens, or
     * that we want to switch
     * the proxy server. */
    const reload = async () => {
      q           = new queue({ name: 'jobs', delay: 2000 });
      const query = proxies.query();
      //const proxy = (await query.findOneAndDelete({})).value;
      const runner = new scraper(scrape, {
        proxy,
        //proxy: proxy ? proxy.host : undefined,
        queue: q,

        //proxy: '195.228.173.245:21231',
        //proxy: '93.171.179.92:33903',
        //proxy: '5.135.37.21:32431',

        agent:     USER_AGENT,
        delay_min: 500,
        delay_max: 2300,
        shouldProcess: url => {
          return url.startsWith(URL_POST) || !url.startsWith(URL_TAG)
        }
      });

      /**
       * NO PROXY?! FUUUUUCK!!! */
      //if (!proxy) throw new Error("I'm out of Proxies...");

      /**
       * We'll remove the once it
       * becomes unusable. */
      runner.onError((err) => {
        console.log("Restarting...");
        reload();
      });

      /**
       * Start the scraper
       * with a new proxy. */
      await runner.start();
      console.log('Running!');
      //console.log(`Using proxy: ${proxy.host}`);
    }

    await reload();
  })
  .catch((err) => console.error(err));
