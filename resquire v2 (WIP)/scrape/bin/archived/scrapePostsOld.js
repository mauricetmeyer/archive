/**
 * scrape.js
 * 
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 * 
 * (c) Laviréo
 */


const mkdirp = require('mkdirp');
const Agent  = require('user-agents');
const parse  = require('argparse').ArgumentParser;

const random = require('../lib/util').random;
const list = require('../lib/list');
const queue = require('../lib/queue');
const scraper = require('../lib/scraper');
const database = require('../lib/database');


/**
 * Constants */
mkdirp('tmp');
const POSTS_MIN = 50;
const POSTS_MAX = 200;
const TAG_DELAY = 1000 * 60 * 60 * 3;
const URL_BASE  = 'https://www.instagram.com/';
const URL_TAG   = `${URL_BASE}explore/tags/`;
const URL_POST  = `${URL_BASE}p/`;

const parser = new parse({
  version:     '1.0',
  description: 'Neuron Scraper'
});

parser.addArgument(['-p', '--proxy'], { help: 'Proxy Server' });
parser.addArgument(['-m', '--master'], { help: 'Master Server' });


/**
 * Text helpers */
var J  = /((?:^|[^a-zA-Z0-9_!#$%&*@]:?))(#)([a-zA-Z0-9_]{1,20})(\/[a-zA-Z][a-zA-Z0-9_-]{0,24})?/gi;
var it = /((?:^|[^a-zA-Z0-9_!#$%&*@]:?))(@)([a-zA-Z0-9_]{1,20})(\/[a-zA-Z][a-zA-Z0-9_-]{0,24})?/g;

function hashtagIndices(text) {
  if (!text || !text.match(/#/))
    return [];

  var indices = [];
  return text.replace(J, function (t, e, n, match, unused, offset, full) {
    if (!full.slice(offset + t.length).match(/^(?:#|:\/\/)/)) {
      var start = offset + e.length;
      var end = start + match.length + 1;
      indices.push({
        hashtag: match,
        indices: [start, end]
      })
    }
  }), indices
}

function mentionIndices(text) {
  if (!text || !text.match(/@/))
    return [];

  var indices = [];
  return text.replace(it, function (t, i, n, match, unused, offset, full) {
    if (!full.slice(offset + t.length).match(/^(?:@|:\/\/)/)) {
      var start = offset + i.length;
      var end = start + match.length + 1;

      indices.push({
        mention: match,
        indices: [start, end]
      })
    }
  }), indices
}

function getHashtags(text) {
  return hashtagIndices(text).map(function (val) {
    return val.hashtag
  });
}

function getMentions(text) {
  return mentionIndices(text).map(function (val) {
    return val.mention
  });
}


function getDataFromPage (page)
{
  const res  = page('script').filter((i, el) => {
    if (!el.children[0])
      return false;
    const data = el.children[0].data;
    return data.startsWith('window._sharedData');
  }).map((i, el) => el.children[0].data);
    
  const raw     = res[0].split('window._sharedData = ')[1];
  const rawData = raw.substr(0, raw.length - 1);
  const data    = JSON.parse(rawData);

  return data.entry_data;
}



/**
 * Scraping helpers */
const scrapePost = async (job, page, url) => {
  const res = getDataFromPage(page).PostPage[0].graphql.shortcode_media;

  /**
   * Make data accessible */
  const id = res.id;
  const likes = res.edge_media_preview_like;
  const owner = res.owner;
  const comments = res.edge_media_to_comment;
  const captions = res.edge_media_to_caption;
  const shortcode = res.shortcode;
  const dimensions = res.dimensions;

  const likes_num = likes.count || 0;
  const captions_num = captions.count || 1;
  const comments_num = comments.count || 0;

  if (await visited_posts.has({ shortcode }))
  {
    console.log(`SKIP: Post already scraped #${shortcode}`);
    return;
  }

  console.log(`POST: ${shortcode}`);
  await data.add({
    url,
    type: 'post',
    raw: res
  });

  await visited_posts.add({ shortcode });

  /**
   * Scrape the owner too if we haven't seen him yet as it could
   * be that we came from the tag page. */
  const username = owner.username;
  if (!await visited_users.has({ username }))
    queue_profiles.enqueue({ q: `${username}` });

  /**
   * Scan post caption for mentions
   * and tags. */
  const first_captions = captions.edges;
  for (let i = 0; i < first_captions.length; i++) {
    const node     = first_captions[i].node;
    const text     = node.text;
    const tags     = getHashtags(text);
    const mentions = getMentions(text);

    for (let i = 0; i < tags.length; i++) {
      console.log(`LINK: caption_tag ${shortcode} ${tags[i]}`);
      await links.add({
        type: 'caption',
        from: {
          type: 'post',
          url
        },
        to: {
          type: 'tag',
          url: `${URL_TAG}${tags[i]}`
        }
      });

      await queue_tags.enqueue({ q: `${tags[i]}` });
    }

    for (let i = 0; i < mentions.length; i++) {
      console.log(`LINK: caption_mention ${shortcode} ${mentions[i]}`);
      await links.add({
        type: 'caption',
        from: {
          type: 'post',
          url
        },
        to: {
          type: 'user',
          url: `${URL_BASE}${mentions[i]}`
        }
      });

      if (!await visited_users.has({ username: mentions[i] }))
        await queue_profiles.enqueue({ q: `${mentions[i]}` });
    }
  }


  /**
   * Scan each comment for tags and mentions,
   * if there are any we scrape them too. */
  const first_likes = likes.edges;
  for (let i = 0; i < first_likes.length; i++) {
    const node = first_likes[i].node;
    const username = node.username;

    console.log(`LINK: like ${shortcode} ${username}`);
    await links.add({
      type: 'like',
      from: {
        type: 'post',
        url
      },
      to: {
        type: 'user',
        url: `${URL_BASE}${username}`
      }
    });

    if (!await visited_users.has({ username }))
      await queue_profiles.enqueue({ q: `${username}` });
  }

  /**
   * Scan each comment for tags and mentions,
   * if there are any we scrape them too. */
  const first_comments = comments.edges;
  for (let i = 0; i < first_comments.length; i++) {
    const node = first_comments[i].node;
    const text = node.text;
    const username = node.owner.username;

    console.log(`LINK: comment ${shortcode} ${username}`);
    await links.add({
      type: 'comment',
      from: {
        type: 'post',
        url
      },
      to: {
        type: 'user',
        url: `${URL_BASE}${username}`
      }
    });

    if (!await visited_users.has({ username }))
      await queue_profiles.enqueue({ q: `${username}` });

    /**
     * Extract tags and mentions
     * from the comment text */
    const tags = getHashtags(text);
    const mentions = getMentions(text);

    for (let i = 0; i < tags.length; i++) {
      console.log(`LINK: comment_tag ${shortcode} ${tags[i]}`);
      await links.add({
        type: 'comment',
        from: {
          type: 'post',
          url
        },
        to: {
          type: 'tag',
          url: `${URL_TAG}${tags[i]}`
        }
      });

      await queue_tags.enqueue({ q: `${tags[i]}` });
    }

    for (let i = 0; i < mentions.length; i++) {
      console.log(`LINK: comment_mention ${shortcode} ${mentions[i]}`);
      await links.add({
        type: 'comment',
        from: {
          type: 'post',
          url
        },
        to: {
          type: 'user',
          url: `${URL_BASE}${mentions[i]}`
        }
      });

      if (!await visited_users.has({ username: mentions[i] }))
        await queue_profiles.enqueue({ q: `${mentions[i]}` });
    }
  }
}


/**
 * Let's get to the action */
const db             = new database;
const args           = parser.parseArgs();
const proxy          = args.proxy;
const master         = args.master || "localhost";

const BASE_CONF = {
  redis: { host: master },
  getEvents: false,
  isWorker: false,
};

const queue_main = new queue({
  redis: { host: master },
  name: 'posts',
  removeOnSuccess: true
});

const queue_tags     = new queue({ ...BASE_CONF, name: 'tags' });
const queue_profiles = new queue({ ...BASE_CONF, name: 'profiles' });

const data           = new list(db, 'data');
const links          = new list(db, 'links');
const visited_tags   = new list(db, 'v_tags');
const visited_posts  = new list(db, 'v_posts');
const visited_users  = new list(db, 'v_users');

db.connect(`mongodb://${master}`, 'instagram')
  .then(async () => {
    /**
     * Helper function to reload
     * the scraper in case an
     * error happens, or
     * that we want to switch
     * the proxy server. */
    const ua     = new Agent();
    const agent  = ua.toString();

    const runner = new scraper(scrapePost, {
      proxy,
      queue: queue_main,

      agent,
      delay_min: 500,
      delay_max: 2300,
      transformQuery: url => URL_POST + url,
    });

    /**
     * We'll remove the once it
     * becomes unusable. */
    runner.onError((err) => {
      console.error(err);
    });

    /**
     * Start the scraper
     * with a new proxy. */
    await runner.start();
    console.log('Running!');
    console.log(`Using user-agent: ${agent}`);
  })
  .catch((err) => console.error(err));
