/**
 * transition.js
 * 
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 * 
 * (c) Laviréo
 */


const progress  = require('cli-progress');
const queue     = require('../lib/queue');


const URL_BASE  = 'https://www.instagram.com';
const URL_TAG   = `${URL_BASE}/explore/tags`;
const URL_POST  = `${URL_BASE}/p`;
const BASE_CONF = {
  isWorker: false,
  getEvents: false
};


const q        = new queue({ ...BASE_CONF, name: 'jobs', redis: { port: 6380 } });
const tags     = new queue({ ...BASE_CONF, name: 'tags' });
const posts    = new queue({ ...BASE_CONF, name: 'posts' });
const profiles = new queue({ ...BASE_CONF, name: 'profiles' });


async function enqueue (queue, short)
{
  await queue.enqueue({ q: short });
}


(async function ()
{
  const bar       = new progress.Bar({}, progress.Presets.legacy);
  const rawQueue  = q.handle;
  const jobCounts = await rawQueue.checkHealth();

  //const key       = rawQueue.toKey('failed');
  //console.log(key);
  //rawQueue.client.smembers(key, (err, results) => {
  //  bar.start(results.length, 0);
  //  if (err) {
  //    return (err);
  //  }

  //  
  //  
  //  (async function ()
  //  {
  //    for (let i = 0; i < results.length; i++)
  //    {
  //      const job = await rawQueue.getJob(results[i]);
  //      await job.retry();
  //      bar.update(i);
  //    }

  //    bar.stop();
  //  })().catch((err) => {
  //    console.error(err);
  //  });
  //});

  const start = 3500000;
  bar.start(jobCounts.newestJob, start);
  for (let i = start; i <= jobCounts.newestJob; i++)
  {
    let   error = null;
    const job   = await rawQueue.getJob(i).catch(err => error = err);
    if (error)
    {
      console.log(error);
      //continue;
    }

    if (!job) continue;

    if (['succeeded', 'failed'].includes(job.status))
    {
      bar.update(i);
      continue;
    }
    
    //await job.remove();
    const data = job.data;
    const uri  = data.url;

    /**
     * Decide whhat to do with the job */
    try {
      if (uri.startsWith(URL_TAG))
      {
        const short = uri.substr(URL_TAG.length + 1);
        await enqueue(tags, short); 
      }
      else if (uri.startsWith(URL_POST))
      {
        const short = uri.substr(URL_POST.length + 1);
        await enqueue(posts, short); 
      }
      else if (uri.startsWith(URL_BASE))
      {
        const short = uri.substr(URL_BASE.length + 1);
        await enqueue(profiles, short); 
      }
    }
    catch (err)
    {
      break;
    }

    bar.update(i);
    await job.remove();
  }

  bar.stop();
})().catch((err) => {
  console.error(err);
});
