/**
 * count.js
 * 
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 * 
 * (c) Laviréo
 */


const queue = require('../lib/queue');


const BASE_CONF = {
  redis: { host: 'hub.local' },
  isWorker: false,
  getEvents: false
};


const tags  = new queue({ ...BASE_CONF, name: 'tags' });
const posts = new queue({ ...BASE_CONF, name: 'posts' });
const users = new queue({ ...BASE_CONF, name: 'users' });


(async function ()
{
  const tagCounts     = await tags.handle.checkHealth();
  const postCounts    = await posts.handle.checkHealth();
  const profileCounts = await users.handle.checkHealth();

  console.log(`Tags: ${tagCounts.waiting}, Posts: ${postCounts.waiting}, Profiles ${profileCounts.waiting}`);
})().catch((err) => {
  console.error(err);
});
