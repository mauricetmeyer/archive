/**
 * addProxy.js
 * 
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 * 
 * (c) Laviréo
 */


const fs           = require('fs');
const url          = require('url');
const path         = require('path');
const axios        = require('axios');
const { pipeline } = require('stream');
const agent        = require('user-agents');
const mkdirp       = require('mkdirp');
const parse        = require('argparse').ArgumentParser;
const progress     = require('cli-progress');
const proxyagent   = require('https-proxy-agent');

const util         = require('../lib/util');
const queue        = require('../lib/queue');

const DELAY_MIN = 220;
const DELAY_MAX = 650;

const parser = new parse({
  version:     '1.0',
  description: 'Neuron Scraper - Image downloader'
});

parser.addArgument(['-m', '--master'],  { help: 'Master Server' });
parser.addArgument(['-o', '--output'],  { help: 'Output directory' });
parser.addArgument(['-p', '--proxy'],   { help: 'Proxy URL' });
parser.addArgument(['-w', '--workers'], { help: 'Worker count' });

const args                            = parser.parseArgs();
const { proxy, output, master = 'localhost', workers = '1' } = args;

if (!master || !output)
{
  console.error("Missing arguments!");
  process.exit(1);
}


const pump = (src, dest) => {
  return new Promise((res, rej) => {
    pipeline(src, dest, err => (err ? rej(err) : res()));
  });
}

const ensure = async function (dir)
{
  return new Promise ((res, rej) => {
    mkdirp(dir, err => err ? rej() : res());
  });
};

const request = async function (uri, opts)
{
  const {
    agent,
    proxy,
    headers = {}
  } = opts || {};

  /**
   * Send it! */
  return axios.get(uri, {
    httpsAgent: proxy,
    responseType: 'stream',
    validateStatus: null,
    headers: {
      'user-agent': agent,
      ...headers
    }
  });
}

//const ua         = new agent();
//const userAgent  = ua.toString();
const proxyAgent = proxy ? new proxyagent(proxy) : undefined;
const images     = new queue({
  redis: { host: master },
  name:       'images',
  getEvents:  false,
  sendEvents: false,
  storeJobs:  false,
  removeOnSuccess: true
});

ensure(output).then(() => {
  const workerCnt = parseInt(workers);
  return images.process(workerCnt, async ({ data }) => {
    const short = data.s;
    const query = `https://instagram.com/p/${short}/media?size=l`;
  
    try {
      const ua  = new agent();
      const req = await request(query, { proxy: proxyAgent, agent: ua.toString() });
      switch (req.status)
      {
        /**
         * Save the file to the output
         * destination. */
        case 200: {
          const imagePath   = path.join(output, `${short}.jpg`);
          const imageStream = fs.createWriteStream(imagePath);
          await pump(req.data, imageStream);
          imageStream.close();
          console.log(`[INFO] ${short}: Downloaded`);
          break;
        }

        /**
         * In case of a 404 we only log the warning
         * but let the queue remove the job. */
        case 404:
          console.warn(`[WARN] ${short}: Doesn't exist anymore`);
          return;

        default: 
          console.error(`[ERROR] ${short}: Failed to download`);
          return Promise.reject();
      }
    }
    catch (err)
    {
      return Promise.reject(err);
    }

    //const delay = util.random(DELAY_MIN, DELAY_MAX);
    //if (delay) await util.sleep(delay);
  });
});

images.onError((err) => {
  console.error(err);
})
