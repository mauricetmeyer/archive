/**
 * enqueueFailed.js
 * 
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 * 
 * (c) Laviréo
 */


const progress = require('cli-progress');
const queue    = require('../lib/queue');

const BASE_CONF = {
  redis: { host: 'hub.local' },
  getEvents: false,
  isWorker: false
};

const q  = new queue({ ...BASE_CONF, name: 'images' });


(async function ()
{
  const bar      = new progress.Bar({}, progress.Presets.legacy);
  const rawQueue = q.handle;

  q.onReady(() => {
    const key       = rawQueue.toKey('failed');
    rawQueue.client.smembers(key, (err, results) => {
      bar.start(results.length, 0);
      if (err) return (err);

      (async function ()
      {
        for (let i = 0; i < results.length; i++)
        {
          const job = await rawQueue.getJob(results[i]);
          await job.retry();
          bar.update(i);
        }

        bar.stop();
      })().catch((err) => {
        console.error(err);
      });
    });
  });
})().catch((err) => {
  console.error(err);
});
