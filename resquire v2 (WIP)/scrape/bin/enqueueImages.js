/**
 * addProxy.js
 * 
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 * 
 * (c) Laviréo
 */


const parse    = require('argparse').ArgumentParser;
const progress = require('cli-progress');

const list     = require('../lib/list');
const queue    = require('../lib/queue');
const database = require('../lib/database');;

const parser = new parse({
  version:     '1.0',
  description: 'Neuron Scraper - Integrity'
});

parser.addArgument(['-m', '--master'], { help: 'Master Server' });

const args   = parser.parseArgs();
const master = args.master || "localhost";

if (!master)
{
  console.error("Missing arguments!");
  process.exit(1);
}


const db     = new database;
const data   = new list(db, 'data');
const images = new queue({
  redis: { host: master },
  name:       'images',
  isWorker:   false,
  getEvents:  false,
  sendEvents: false,
  storeJobs:  false
});


db.connect(`mongodb://${master}`, 'instagram')
  .then(async () => {
    /**
     * Query list */
    const q     = data.query();
    const res   = q.find({ type: 'user' }, { projection: { 'raw.edge_owner_to_timeline_media.edges.node.shortcode': 1 } });
    //res.skip(362333);
    //res.limit(20);
    res.batchSize(1000);
    const bar   = new progress.Bar({}, progress.Presets.legacy);
    const total = await res.count();

    /**
     * Start */
    bar.start(total, 0);
    let i = 0;
    let el;
    while (el = await res.next())
    {
      const data  = el.raw;
      const media = data.edge_owner_to_timeline_media.edges.map(a => a.node);
      for (let k = 0; k < media.length; k++)
      {
        const { shortcode } = media[k];
        await images.enqueue({ s: shortcode });
      }

      bar.update(++i);
    }

    bar.stop();
    process.exit(0);
  })
  .catch((err) => console.error(err));
