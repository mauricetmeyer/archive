/**
 * train.js
 * 
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 * 
 * (c) Laviréo
 */


const fs                    = require('fs-plus');
const csv                   = require('csv');
const path                  = require('path');
const mkdirp                = require('mkdirp');
const emojir                = require('emoji-regex');
const ld                    = require('languagedetect');

const { ArgumentParser }    = require('argparse');
const progress              = require('cli-progress');

const List                  = require('../lib/list');
const Database              = require('../lib/database');


/**
 * Constants */
const POST_TYPE_MAP  = {
  GraphImage:   0, // 'image'
  GraphVideo:   1, // 'video',
  GraphSidecar: 2, // 'carousel',
};


/**
 * Helpers */
const asyncMap = async (arr, fn) => {
  const res = [];
  for (let i = 0; i < arr.length; i++)
    res.push(await fn(arr[i], i, arr));
  return res;
}

const asyncEach = async (arr, fn) => {
  for (let i = 0; i < arr.length; i++)
    await fn(arr[i], i, arr);
}

const bool2int = val => val ? 1 : 0;


/**
 * Arguments */
const parser = new ArgumentParser({
  version:     '1.0',
  description: 'Neuron data exporter'
});

parser.addArgument(['-l', '--limit'],  { help: 'Data limit' });
parser.addArgument(['-m', '--master'], { help: 'Master Server' });
parser.addArgument(['-o', '--output'], { help: 'Data output path' });


/**
 * Extract data from raw data. */
const transformPost = async (data) => {
  const {
    __typename,

    location,
    shortcode,
    dimensions,

    video_view_count,
    taken_at_timestamp,
    comments_disabled,
    accessibility_caption,

    edge_liked_by,
    edge_media_to_comment,
    edge_media_to_caption
  } = data;

  const type                 = POST_TYPE_MAP[__typename];
  const { width, height }    = dimensions;
  const caption              = edge_media_to_caption.edges[0] ? edge_media_to_caption.edges[0].node.text : undefined;
  const timestamp            = taken_at_timestamp * 1000;

  const views_num            = video_view_count || 0;
  const likes_num            = edge_liked_by         ? edge_liked_by.count         : 0;
  const comments_num         = edge_media_to_comment ? edge_media_to_comment.count : 0;

  const is_ad                = bool2int(false);
  const is_sponsored         = bool2int(false);
  const has_comments_enabled = bool2int(!comments_disabled);

  return {
    type,
    width,
    height,
    caption,
    location,
    timestamp,
    shortcode,

    views_num,
    likes_num,
    comments_num,

    is_ad,
    is_sponsored,
    has_comments_enabled
  };
};

const transform = async (data) => {
  /**
   * Inputs */
  const {
    id,
    username,
    biography,
    full_name,
    is_private,
    is_verified,
    is_joined_recently = false,
    is_business_account,
    business_category_name,

    edge_follow,
    edge_followed_by,
    edge_owner_to_timeline_media
  } = data;

  /**
   * Posts */
  const posts      = edge_owner_to_timeline_media.edges.map(p => p.node);
  const posts_num  = edge_owner_to_timeline_media.count;
  const posts_proc = await asyncMap(posts, transformPost);

  /**
   * Profile */
  const follows_num   = edge_follow      ? edge_follow.count      : 0;
  const followers_num = edge_followed_by ? edge_followed_by.count : 0;

  const profile = {
    id,
    username,
    full_name,
    biography,
    category: business_category_name,
  
    posts_num,
    follows_num,
    followers_num,

    is_new:            bool2int(is_joined_recently),
    is_private:        bool2int(is_private),
    is_verified:       bool2int(is_verified),
    is_business:       bool2int(is_business_account),
  };

  /**
   * Return the prepared profile and posts data. */
  return { profile, posts: posts_proc };
};


/**
 * Training */
const db   = new Database();
const data = new List(db, 'data');
const args = parser.parseArgs();
const {
  limit,
  output,
  master = 'localhost',
} = args;

const dir          = output;
const posts_file   = fs.createWriteStream(path.join(output, 'posts.csv'));
const users_file   = fs.createWriteStream(path.join(output, 'users.csv'));
const posts_writer = csv.stringify();
const users_writer = csv.stringify();
posts_writer.pipe(posts_file);
users_writer.pipe(users_file);


db.connect(`mongodb://${master}`, 'instagram')
  .then(async () => {
    /**
     * Preparing! */
    const limitParsed  = parseInt(limit);

    const query = data.query();
    const users = query.find({ type: 'user' });
    users.batchSize(10000);
    if (limitParsed) users.limit(limitParsed);

    /**
     * Execute! */
    const total = limitParsed || await users.count();

    for (let i = 0, el = await users.next(); el !== null; el = await users.next(), ++i)
    {
      const { id, is_private } = el.raw;
      if (is_private)
      {
        console.log(`${i} - [${id}]: Skipping private account!`);
        continue;
      }

      const { posts, profile } = await transform(el.raw);
      await asyncEach(posts, async post => {
        console.log(`${i} - [${post.shortcode}]: Added!`);
      });
    }

    posts_file.close();
    users_file.close();
    process.exit(0);
  })
  .catch((err) => console.error(err));
