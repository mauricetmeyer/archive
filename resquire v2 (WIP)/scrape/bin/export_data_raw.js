/**
 * export_data_raw.js
 * 
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 * 
 * (c) Laviréo
 */


const fs                    = require('fs');
const csv                   = require('csv');
const path                  = require('path');

const { ArgumentParser }    = require('argparse');
const progress              = require('cli-progress');

const List                  = require('../lib/list');
const Database              = require('../lib/database');


/**
 * Constants */
const POST_TYPE_MAP  = {
  GraphImage:   0, // 'image'
  GraphVideo:   1, // 'video',
  GraphSidecar: 2, // 'carousel',
};


/**
 * Helpers */
const asyncMap = async (arr, fn) => {
  const res = [];
  for (let i = 0; i < arr.length; i++)
    res.push(await fn(arr[i], i, arr));
  return res;
}

const asyncEach = async (arr, fn) => {
  for (let i = 0; i < arr.length; i++)
    await fn(arr[i], i, arr);
}

const bool2int = val => val ? 1 : 0;


/**
 * Arguments */
const parser = new ArgumentParser({
  version:     '1.0',
  description: 'Neuron data exporter'
});

parser.addArgument(['-l', '--limit'],  { help: 'Data limit' });
parser.addArgument(['-m', '--master'], { help: 'Master Server' });
parser.addArgument(['-o', '--output'], { help: 'Data output path' });


/**
 * Extract data from raw data. */
const transformPost = async (data) => {
  const {
    __typename,

    location,
    shortcode,
    dimensions,

    video_view_count,
    taken_at_timestamp,
    comments_disabled,
    accessibility_caption,

    edge_liked_by,
    edge_media_to_comment,
    edge_media_to_caption
  } = data;

  const type                 = POST_TYPE_MAP[__typename];
  const { width, height }    = dimensions;
  const caption              = edge_media_to_caption.edges[0] ? edge_media_to_caption.edges[0].node.text : undefined;
  const timestamp            = taken_at_timestamp * 1000;

  const views_num            = video_view_count || 0;
  const likes_num            = edge_liked_by         ? edge_liked_by.count         : 0;
  const comments_num         = edge_media_to_comment ? edge_media_to_comment.count : 0;

  const is_ad                = bool2int(false);
  const is_sponsored         = bool2int(false);
  const has_comments_enabled = bool2int(!comments_disabled);

  return {
    type,
    width,
    height,
    //caption,
    location,
    timestamp,
    shortcode,

    views_num,
    likes_num,
    comments_num,

    is_ad,
    is_sponsored,
    has_comments_enabled
  };
};

const transform = async (data) => {
  /**
   * Inputs */
  const {
    id,
    username,
    biography,
    full_name,
    is_private,
    is_verified,
    external_url,
    is_joined_recently = false,
    is_business_account,

    business_email,
    business_phone_number,
    business_category_name,

    edge_follow,
    edge_followed_by,
    edge_owner_to_timeline_media
  } = data;

  /**
   * Posts */
  const posts      = edge_owner_to_timeline_media.edges.map(p => p.node);
  const posts_num  = edge_owner_to_timeline_media.count;
  const posts_proc = await asyncMap(posts, transformPost);

  /**
   * Profile */
  const follows_num   = edge_follow      ? edge_follow.count      : 0;
  const followers_num = edge_followed_by ? edge_followed_by.count : 0;

  const profile = {
    id,
    url:   external_url,
    email: business_email,
    phone: business_phone_number,
    username,
    full_name,
    //biography,
    category: business_category_name,
  
    posts_num,
    follows_num,
    followers_num,

    is_new:            bool2int(is_joined_recently),
    //is_private:        bool2int(is_private),
    is_verified:       bool2int(is_verified),
    //is_business:       bool2int(is_business_account),
  };

  /**
   * Return the prepared profile and posts data. */
  return { profile, posts: posts_proc };
};


/**
 * Training */
const db   = new Database();
const data = new List(db, 'data');
const args = parser.parseArgs();
const {
  limit,
  output,
  master = 'localhost',
} = args;

if (!output)
{
  console.error('Set output path!');
  process.exit(1);
}

db.connect(`mongodb://${master}`, 'instagram')
  .then(async () => {
    console.log(`Connected to ${master}...`);

    /**
     * Preparing! */
    const limitParsed  = parseInt(limit);

    let cnt = 0;
    const out   = [['Id', 'Website', 'Email', 'Phone', 'Username', 'Name', 'Category', 'Num. posts', 'Num. following', 'Num. followers', 'New account', 'Is verified', 'Avg. likes', 'Avg. Comments'].join(';')];
    const query = data.query();
    const users = query.find({ type: 'user', 'raw.is_business_account': true });
    users.batchSize(1000);
    //if (limitParsed) users.limit(limitParsed);

    /**
     * Execute! */
    console.log('Starting now!');
    //const total = limitParsed || await users.count();
    //console.log(`Limit: ${limitParsed}`);
    for (let i = 0, el = await users.next(); el !== null; el = await users.next(), ++i)
    {
      //console.log(el.raw);
      const { posts, profile }                  = await transform(el.raw);
      const { id, email, username, is_private } = profile;
      if (is_private || !email)
      {
        console.log(`[${i}] ${username}: Skipped`);
        continue;
      }

      let post_likes    = 0;
      let post_comments = 0;
      posts.forEach(({ likes_num, comments_num }) => {
        post_likes    += likes_num    || 0;
        post_comments += comments_num || 0;
      });

      const post_cnt          = posts.length;
      const post_likes_avg    = post_likes / post_cnt;
      const post_comments_avg = post_comments / post_cnt;

      // await asyncEach(posts, async post => {
      //   console.log(`${i} - [${post.shortcode}]: Added!`);
      // });
      ++cnt;
      console.log(`[${i}] ${username}: Included`);
      const data = { ...profile, likesAvg: post_likes_avg.toFixed(2), commentsAvg: post_comments_avg.toFixed(2) };
      const dataArr = Object.values(data);
      out.push(dataArr.join(';'));
      if (limitParsed && cnt >= limitParsed) break;
    }

    console.log('done');
    console.log(`Found ${cnt} accounts with emails!`);
    fs.writeFileSync(output, out.join('\n'));
    process.exit(0);
  })
  .catch((err) => console.error(err));
