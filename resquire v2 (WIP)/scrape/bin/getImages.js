/**
 * addProxy.js
 * 
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 * 
 * (c) Laviréo
 */


const fs        = require('fs');
const url       = require('url');
const path      = require('path');
const mkdirp    = require('mkdirp');
const request   = require('request');
const progress  = require('cli-progress');
const promisify = require('util').promisify;

const list     = require('../lib/list');
const util     = require('../lib/util');
const database = require('../lib/database');

const db   = new database;
const data = new list(db, 'data');

const root = process.argv[2];
if (!root) return -1;

const scrape = async function (el, parentShort)
{
  const type    = el.__typename;
  const short   = parentShort || el.shortcode;
  const isVideo = type === 'GraphVideo';

  /**
   * Process carousel */
  if (type === 'GraphSidecar')
  {
    for (let i = 0; i < el.edge_sidecar_to_children.edges.length; i++)
      scrape(el.edge_sidecar_to_children.edges[i].node, short);
    return;
  }

  /**
   * Get data */
  const resources = el.display_resources;
  const relevant  = resources[resources.length - 1];
  const mediaUrl  = relevant.src;
  const videoUrl  = el.video_url;

  /**
   * Pull data */
  //console.log(mediaUrl);
  await download(mediaUrl, path.join(root, short));
  await util.sleep(500);
};

const download = async function (URI, dir)
{
  const pathname = url.parse(URI).pathname || '';
  const ext      = path.extname(pathname);
  const out      = dir + ext;
  //const raw = await req(url);
  //console.log(out);
  //throw "";

  return new Promise ((res, rej) => {
    request.head(URI, () => {
      request(URI).pipe(fs.createWriteStream(out))
        .on('error', rej)
        .on('close', res);
    });
  });
}

const ensureDir = async function (dir)
{
  return new Promise ((res, rej) => {
    mkdirp(dir, err => err ? rej() : res());
  });
};


db.connect(`mongodb://localhost`, 'instagram')
  .then(async () => {
    /**
     * Ensure post directory */
    await ensureDir(path.join(root));

    /**
     * Query list */
    const q     = data.query();
    const res   = q.find({ type: 'post' });
    //res.limit(20);
    const bar   = new progress.Bar({}, progress.Presets.legacy);
    const count = await res.count();
    let   curr  = 0;

    /**
     * Start */
    bar.start(count, curr);
    while (await res.hasNext())
    {
      const el = await res.next();
      await scrape(el.raw);

      ++curr;
      bar.update(curr);
    }

    bar.stop();
  })
  .catch((err) => console.error(err));
