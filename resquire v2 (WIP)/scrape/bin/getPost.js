/**
 * getPost.js
 * 
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 * 
 * (c) Laviréo
 */


const request = require('request');
const cheerio = require('cheerio');

const get = async function (uri, opts)
{
  opts            = opts || {};
  opts['url']     = uri;
  opts['method']  = 'GET';
  opts['headers'] = { "User-Agent" : "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_5) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.0 Safari/605.1.15"}; 

  return new Promise ((res, rej) => {
    request(opts, (e, r, body) => {
      if (e || r.statusCode !== 200)
        return rej();

      /**
       * Let cheerio handle the html */
      const ctx = cheerio.load(body);
      res(ctx);
    });
  });
}

const scrape = async function ()
{
  /**
   * Pull data */
  const page = await get("https://www.instagram.com/p/BsF5sVkn49A/", { proxy: 'http://159.224.73.208:23500' });
  const res  = page('script').filter((i, el) => {
    if (!el.children[0])
      return false;
    const data = el.children[0].data;
    return data.startsWith('window._sharedData');
  }).map((i, el) => el.children[0].data);
    
  const raw     = res[0].split('window._sharedData = ')[1];
  const rawData = raw.substr(0, raw.length - 1);
  const data    = JSON.parse(rawData);

  const post = data.entry_data.PostPage[0].graphql.shortcode_media;
  console.log(post);

  /**
   * Make data accessible */
  const id         = post.id;
  const likes      = post.edge_media_preview_like;
  const owner      = post.owner;
  const comments   = post.edge_media_to_comment;
  const captions   = post.edge_media_to_caption;
  const shortcode  = post.shortcode;
  const dimensions = post.dimensions;

  const likes_num    = likes.count || 0;
  const captions_num = captions.count || 1;
  const comments_num = comments.count || 0;

  console.log(shortcode);
};

scrape().then(() => {
  console.log('done');
});