/**
 * integrity.js
 * 
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 * 
 * (c) Laviréo
 */


const progress  = require('cli-progress');
const parse     = require('argparse').ArgumentParser;

const list      = require('../lib/list');
const queue     = require('../lib/queue');
const database  = require('../lib/database');

const added     = [];


const parser = new parse({
  version:     '1.0',
  description: 'Neuron Scraper - Integrity'
});

parser.addArgument(['-m', '--master'], { help: 'Master Server' });


const db       = new database;
const args     = parser.parseArgs();
const master   = args.master || "localhost";

const data     = new list(db, 'data');
const users    = new list(db, 'v_users');
const m_users  = new list(db, 'm_users');
const q_users  = new queue({
  redis:     { host: master },
  name:      'users',
  isWorker:  false,
  getEvents: false
});


db.connect(`mongodb://${master}`, 'instagram')
  .then(async () => {
    /**
     * Query list */
    const q       = data.query();
    const res     = q.find({ type: 'post' }, { 'raw.owner': 1 });
    //res.limit(10);
    res.batchSize(10000);
    const bar     = new progress.Bar({}, progress.Presets.legacy);
    const total   = await res.count();

    let   missing = 0;

    /**
     * Start */
    bar.start(total, 0);
    for (let i = 0, el = await res.next(); el !== null; el = await res.next(), ++i)
    {
      const username       = el.raw.owner.username;
      const alreadyScraped = await users.has({ username });
      if (!alreadyScraped && added.indexOf(username) === -1)
      {
        added.push(username);
        await q_users.enqueue({ q: `${username}` });
        ++missing;
      }

      bar.update(i);
    }

    bar.stop();

    const perc_total    = 100 / total;
    const perc_complete = perc_total * (total - missing);
    const perc_rounded  = perc_complete.toFixed(2);
    console.log(`Integrity is at ${perc_rounded}% (${total - missing}/${total}).\n${missing} missing profiles have been added to the queue.`);
    process.exit(0);
  })
  .catch((err) => console.error(err));
