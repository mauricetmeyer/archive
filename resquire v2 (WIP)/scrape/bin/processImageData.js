/**
 * train.ts
 *
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 *
 * This document is the property of Laviréo.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * Laviréo.
 *
 * (c) Laviréo. All rights reserved.
 */


const _          = require('underscore');
const fs         = require('fs');
const path       = require('path');
const parse      = require('argparse').ArgumentParser;
const progress   = require('cli-progress');
const LineReader = require('n-readlines');

const list       = require('../lib/list');
const database   = require('../lib/database');

/**
 * Make sure we exit on SIGTERM signal. */
process.on('SIGTERM', () => process.exit());

const parser = new parse({
  version:     '1.0',
  description: 'Neuron Scraper - Integrity'
});

parser.addArgument(['-s', '--server'],  { help: 'Database server' });
parser.addArgument(['-i', '--input'], { help: 'Input data' });
parser.addArgument(['-o', '--output'], { help: 'Output path' })

const write = async (path, data) => {
  return new Promise((res, rej) => {
    fs.writeFile(path, data, err => err ? rej(err) : res());
  });
};

const { server = 'localhost', input, output } = parser.parseArgs();
if (!input || !output)
{
  console.error(`Usage: processImageData.js [options]`);
  process.exit(1);
}

const db   = new database;
const data = new list(db, 'data');
db.connect(`mongodb://${server}`, 'instagram')
  .then(async () => {
    const dataStream  = new LineReader(input);
    const dataEntries = [];

    let i = 0;
    let entry;
    while (entry = dataStream.next())
    {
      const last                    = Date.now();
      const [entryKey, ...entryRaw] = entry.toString('utf-8').split(';');
      const entryData               = entryRaw.map(e => parseFloat(e));

      /**
       * Get profile by shortcode */
      const q     = data.query();
      const res   = await q.findOne({ type: 'user', 'raw.edge_owner_to_timeline_media.edges.node.shortcode': entryKey });
      if (!res)
      {
        console.log(`${++i} [${entryKey}]: Failed to find owning profile!`);
        continue;
      }

      const { username, biography, full_name, is_private, is_verified, ...misc } = res.raw;
      //console.log(misc);
      const posts = misc.edge_owner_to_timeline_media.edges.map(p => p.node);
      const post  = _.find(posts, p => p.shortcode === entryKey);
      const results = {
        user: {
          username,
          biography,
          full_name,

          is_new: misc.is_joined_recently || false,
          is_private,
          is_verified,
          is_business: misc.is_business_account,

          following: misc.edge_follow.count,
          followers: misc.edge_followed_by.count
        },

        post: {
          type:                  post.__typename,
          caption:               post.edge_media_to_caption.edges[0] ? post.edge_media_to_caption.edges[0].node.text : null,
          location:              post.location,
          timestamp:             post.taken_at_timestamp,
          shortcode:             post.shortcode,
          dimensions:            post.dimensions,
          accessibility_caption: post.accessibility_caption,

          tagged:                post.edge_media_to_tagged_user ? post.edge_media_to_tagged_user.edges.map(s => s.node) : null,
          sponsors:              post.edge_media_to_sponsor_user ? post.edge_media_to_sponsor_user.edges.map(s => s.node) : null,

          likes:                 post.edge_liked_by.count,
          views:                 post.video_view_count,
          comments:              post.edge_media_to_comment.count,

          is_video:              post.is_video,
          is_comments_disabled:  post.comments_disabled
        },

        image: entryData
      };

      const ms         = (Date.now() - last) | 0;
      const outputPath = path.join(output, `${entryKey}.json`);
      await write(outputPath, JSON.stringify(results));
      console.log(`${++i} [${entryKey}]: Written to '${outputPath}', took ${ms}ms`);
    }

    process.exit(0);
  });
