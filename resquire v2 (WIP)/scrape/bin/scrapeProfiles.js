/**
 * scrape.js
 * 
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 * 
 * (c) Laviréo
 */


const Agent    = require('user-agents');
const parse    = require('argparse').ArgumentParser;

const list     = require('../lib/list');
const queue    = require('../lib/queue');
const scraper  = require('../lib/scraper');
const database = require('../lib/database');


/**
 * Constants */
const URL_BASE  = 'https://www.instagram.com/';

const parser = new parse({
  version:     '1.0',
  description: 'Neuron Scraper'
});

parser.addArgument(['-p', '--proxy'], { help: 'Proxy Server' });
parser.addArgument(['-m', '--master'], { help: 'Master Server' });


function getDataFromPage (page)
{
  const res  = page('script').filter((i, el) => {
    if (!el.children[0])
      return false;
    const data = el.children[0].data;
    return data.startsWith('window._sharedData');
  }).map((i, el) => el.children[0].data);
    
  const raw     = res[0].split('window._sharedData = ')[1];
  const rawData = raw.substr(0, raw.length - 1);
  const data    = JSON.parse(rawData);

  return data.entry_data;
}



/**
 * Scraping helpers */
const scrapePost = async (job, page, url) => {
  const res = getDataFromPage(page).ProfilePage[0].graphql.user;

  /**
   * Make data accessible */
  const posts    = res.edge_owner_to_timeline_media.edges;
  const username = res.username;

  if (await visited_users.has({ username }))
  {
    console.log(`SKIP: Profile already scraped #${username}`);
    return;
  }

  console.log(`PROFILE: ${username}`);
  await data.add({
    url,
    type: 'user',
    raw: res
  });

  await visited_users.add({ username });
}


/**
 * Let's get to the action */
const db             = new database;
const args           = parser.parseArgs();
const proxy          = args.proxy;
const master         = args.master || "localhost";


const queue_main = new queue({
  redis: { host: master },
  name: 'users',
  removeOnSuccess: true
});

const data           = new list(db, 'data');
const links          = new list(db, 'links');

const queue_tags     = new list(db, 'q_tags');
const queue_posts    = new list(db, 'q_posts');
const queue_users    = new list(db, 'q_users');

const visited_tags   = new list(db, 'v_tags');
const visited_posts  = new list(db, 'v_posts');
const visited_users  = new list(db, 'v_users');


db.connect(`mongodb://${master}`, 'instagram')
  .then(async () => {
    /**
     * Helper function to reload
     * the scraper in case an
     * error happens, or
     * that we want to switch
     * the proxy server. */
    const ua     = new Agent();
    const agent  = ua.toString();

    const runner = new scraper(scrapePost, {
      proxy,
      queue: queue_main,

      agent,
      delay_min: 500,
      delay_max: 1100, //2300,
      transformQuery: url => URL_BASE + url,
    });

    /**
     * We'll remove the once it
     * becomes unusable. */
    runner.onError((err) => {
      console.error(err);
    });

    /**
     * Start the scraper
     * with a new proxy. */
    await runner.start();
    console.log('Running!');
    console.log(`Using user-agent: ${agent}`);
  })
  .catch((err) => console.error(err));
