/**
 * train.js
 * 
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 * 
 * (c) Laviréo
 */


const fs                    = require('fs-plus');
const tf                    = require('@tensorflow/tfjs-node');
const log                   = require('file-stream-rotator');
const path                  = require('path');
const mkdirp                = require('mkdirp');
const emojir                = require('emoji-regex');
const ld                    = require('languagedetect');

const { ArgumentParser }    = require('argparse');
const { SentimentAnalyzer } = require('node-nlp');
const progress              = require('cli-progress');

const List                  = require('../lib/list');
const Database              = require('../lib/database');
const Tokenizer             = require('../lib/tokenizer');


/**
 * Constants */
const WORDS          = new Array();
const EMOJIES        = new Array();
const HASHTAGS       = new Array();
const CATEGORIES     = new Array();

const REG_HASHTAG    = /((?:^|[^a-zA-Z0-9_!#$%&*@]:?))(#)([a-zA-Z\u00E0-\u00FC0-9_]{1,20})/gi;
const REG_MENTION    = /((?:^|[^a-zA-Z0-9_!#$%&*@]:?))(@)([a-zA-Z0-9_]{1,20})/g;

const RATE_OPTIMIZER       = 0.02;
const RATE_DROPOUT         = 0.02;
const RATE_DROPOUT_NUMERIC = 0.01;

const POST_TYPE_MAP  = {
  GraphImage:   0, // 'image'
  GraphVideo:   1, // 'video',
  GraphSidecar: 2, // 'carousel',
};


/**
 * Helpers */
const asyncMap = async (arr, fn) => {
  const res = [];
  for (let i = 0; i < arr.length; i++)
    res.push(await fn(arr[i], i, arr));
  return res;
}

const asyncEach = async (arr, fn) => {
  for (let i = 0; i < arr.length; i++)
    await fn(arr[i], i, arr);
}

const random      = (min, max) => Math.floor(Math.random() * (max - min + 1)) + min;
const bool2int    = val => val ? 1 : 0;
const stringToInt = (arr, val) => {
  const idx = arr.indexOf(val);
  if (idx !== -1) return idx;
  return arr.push(val) - 1;
}

const hashtagIndices = (content) => {
  if (!content || !content.match(/#/))
    return [];

  const indices = [];
  content.replace(REG_HASHTAG, (match, before, _, value, offset, full) => {
    const after = full.slice(offset + match.length);
    if (!after.match(/^(?:#|:\/\/)/))
    {
      const start = offset + before.length;
      const end   = start + value.length + 1;
      indices.push({ value, indices: [start, end] });
    }
  });

  return indices;
}

const mentionIndices = (content) => {
  if (!content || !content.match(/@/))
    return [];

  const indices = [];
  content.replace(REG_MENTION, (match, before, _, value, offset, full) => {
    const after = full.slice(offset + match.length);
    if (!after.match(/^(?:@|:\/\/)/))
    {
      const start = offset + before.length;
      const end   = start + value.length + 1;
      indices.push({ value, indices: [start, end] });
    }
  });

  return indices;
}


/**
 * Arguments */
const parser = new ArgumentParser({
  version:     '1.0',
  description: 'Neuron Trainer'
});

parser.addArgument(['-l', '--limit'],  { help: 'Data limit' });
parser.addArgument(['-e', '--epochs'], { help: 'Epoche count' });
parser.addArgument(['-m', '--master'], { help: 'Master Server' });
parser.addArgument(['-o', '--output'], { help: 'Model output path' });


/**
 * Model */
const words    = tf.input({ shape: [2],  name: 'words' });
const labels   = tf.input({ shape: [1],  name: 'labels' });
const emojies  = tf.input({ shape: [2],  name: 'emojies' });
const hashtags = tf.input({ shape: [2],  name: 'hashtags' });
const features = tf.input({ shape: [24], name: 'features' });


/**
 * Image labels relation branches. */
const labels_words    = tf.layers.concatenate().apply([labels, words]);
const labels_hashtags = tf.layers.concatenate().apply([labels, hashtags]);


/**
 * Create feature layers */
const features_numeric = tf.layers.dropout({
  name:  'numeric_features',
  rate:  RATE_DROPOUT_NUMERIC,
  units: 10
}).apply(features);

const features_transformed = tf.layers.concatenate({
  name: 'transformed_features'
}).apply([labels_words, labels_hashtags]);


/**
 * Output branch */
const outputs_inputs  = tf.layers.concatenate().apply([features_numeric, features_transformed]);
// const outputs_flatten = tf.layers.flatten({
//   name: 'flatten'
// }).apply(outputs_inputs);

const outputs_layer_1 = tf.layers.lstm({
  name: 'reduce',
  units: 16,
  returnSequences: true
}).apply(outputs_inputs);

const outputs_layer_2 = tf.layers.dropout({
  name:  'dropout',
  rate:  RATE_DROPOUT,
  units: 10
}).apply(outputs_layer_1);

const outputs_layer_3 = tf.layers.lstm({
  name:  'dense',
  units: 10
}).apply(outputs_layer_2);

const outputs = tf.layers.dense({
  name:  'output',
  units: 3
}).apply(outputs_layer_3);

const inputs = [flags, words, emojies, hashtags];
const model  = tf.model({ inputs, outputs });

model.summary();
model.compile({
  loss:      tf.losses.meanSquaredError,
  metrics:   ["accuracy"],
  optimizer: tf.train.adam(RATE_OPTIMIZER)
});

return;


/**
 * Neural Network helpers */
const tokenizer = new Tokenizer();
const sentiment = new SentimentAnalyzer({ language: 'en' });

const transformPost = async (data) => {
  const {
    __typename,

    location,
    dimensions,

    video_view_count,
    taken_at_timestamp,
    comments_disabled,
    accessibility_caption,

    edge_liked_by,
    edge_media_to_comment,
    edge_media_to_caption
  } = data;

  /**
   * Date/Time related */
   const date    = new Date(taken_at_timestamp * 1000);
   const time    = date.getHours();
   const month   = date.getMonth();
   const weekday = date.getDay();

  /**
   * Extra related */
  const is_ad                = bool2int(false);
  const is_sponsored         = bool2int(false);
  const has_location         = bool2int(!!location);
  const has_comments_enabled = bool2int(!comments_disabled);


  /**
   * Media related */
  const type             = POST_TYPE_MAP[__typename];
  const has_description  = accessibility_caption && accessibility_caption.includes('Image may contain:');
  const contains         = has_description ? accessibility_caption.replace(/Image may contain: /g, '').split(',') : [];

  //const videos           = media.filter(m => m.type === 'video');
  //const videos_num       = videos.length;
  //const videos_durations = videos.map(v => v.duration);

  //const images     = media.filter(m => m.type === 'image');
  //const images_num = images.length;

  const is_squared  = bool2int(dimensions.height === dimensions.width);
  const is_carousel = bool2int(type === POST_TYPE_MAP.GraphSidecar);

  //const has_videos  = bool2int(videos_num > 0);
  //const has_images  = bool2int(images_num > 0);


  /**
   * Caption related */
  const caption               = edge_media_to_caption.edges[0].node.text;
  const caption_len           = caption.length;
  const caption_sent          = await sentiment.getSentiment(caption);
  // const caption_shortened     = caption_len > CAPTION_LEN_SHORT;


  const emojies               = caption.match(emojir()) || [];
  const emojies_seq           = emojies.map(e => stringToInt(EMOJIES, e));
  const emojies_num           = emojies.length;
  const emojies_first_offset  = emojies_num ? caption.indexOf(emojies[0]) : -1;


  const hashtag_indices       = hashtagIndices(caption);
  const hashtags              = hashtag_indices.map(i => i.value);
  const hashtags_seq          = hashtags.map(h => stringToInt(HASHTAGS, h));
  const hashtags_num          = hashtags.length;
  const hashtags_first_offset = hashtags_num ? caption.indexOf(hashtags[0]) : -1;


  const mention_indices       = mentionIndices(caption);
  const mentions              = mention_indices.map(i => i.value);
  const mentions_num          = mentions.length;
  const mentions_first_offset = mentions_num ? caption.indexOf(mentions[0]) : -1;


  const words     = tokenizer.tokenize(caption);
  const words_seq = words.map(w => stringToInt(WORDS, w));
  const words_num = words.length;
  let   words_len = 0;
  for (let i = 0; i < words_num; i++)
    words_len += words[i].length;
  if (words_num) words_len /= words_num;


  /**
   * Activity */
  const views_num    = video_view_count || 0;
  const likes_num    = edge_liked_by         ? edge_liked_by.count         : 0;
  const comments_num = edge_media_to_comment ? edge_media_to_comment.count : 0;

  // console.log(
  //   type,
  //   time,
  //   month,
  //   weekday,
  //   is_ad,
  //   is_squared,
  //   is_sponsored,
  //   has_location,
  //   has_comments_enabled,
  //   caption_len,
  //   caption_sent,
  //   words,
  //   words_num,
  //   emojies,
  //   emojies_num,
  //   emojies_first_offset,
  //   hashtags,
  //   hashtags_num,
  //   hashtags_first_offset,
  //   mentions_num,
  //   mentions_first_offset,
  // );

  // return {
  //   /**
  //    * Features */
  //   features: {
  //     type:                  tf.scalar(type,                 "int32"),
  //     time:                  tf.scalar(time,                 "int32"),
  //     month:                 tf.scalar(month,                "int32"),
  //     weekday:               tf.scalar(weekday,              "int32"),

  //     is_ad:                 tf.scalar(is_ad,                "bool"),
  //     is_squared:            tf.scalar(is_squared,           "bool"),
  //     is_sponsored:          tf.scalar(is_sponsored,         "bool"),

  //     has_location:          tf.scalar(has_location,         "bool"),
  //     has_comments_enabled:  tf.scalar(has_comments_enabled, "bool"),

  //     caption_len:           tf.scalar(caption_len,          "int32"),
  //     caption_sentiment:     tf.scalar(caption_sent.score,   "float32"),

  //     //words:                 tf.tensor(words_seq),
  //     words_num:             tf.scalar(words_num, "int32"),

  //     //emojies:               tf.tensor(emojies_seq),
  //     emojies_num:           tf.scalar(emojies_num,          "int32"),
  //     emojies_first_offset:  tf.scalar(emojies_first_offset, "int32"),

  //     //hashtags:              tf.tensor(hashtags_seq),
  //     hashtags_num:          tf.scalar(hashtags_num,          "int32"),
  //     hashtags_first_offset: tf.scalar(hashtags_first_offset, "int32"),

  //     mentions_num:          tf.scalar(mentions_num,          "int32"),
  //     mentions_first_offset: tf.scalar(mentions_first_offset, "int32"),
  //   },


  //   /**
  //    * Labels */
  //   labels: {
  //     views_num:             tf.scalar(views_num,    "int32"),
  //     likes_num:             tf.scalar(likes_num,    "int32"),
  //     comments_num:          tf.scalar(comments_num, "int32")
  //   }
  // }
  console.log(views_num, likes_num, comments_num);

  return {
    /**
     * Features */
    features: [
      tf.scalar(type,                 "int32"),
      tf.scalar(time,                 "int32"),
      tf.scalar(month,                "int32"),
      tf.scalar(weekday,              "int32"),

      tf.scalar(is_ad,                "bool"),
      tf.scalar(is_squared,           "bool"),
      tf.scalar(is_sponsored,         "bool"),

      tf.scalar(has_location,         "bool"),
      tf.scalar(has_comments_enabled, "bool"),

      tf.scalar(caption_len,          "int32"),
      tf.scalar(caption_sent.score,   "float32"),

      //tf.tensor(words_seq),
      tf.scalar(words_num,            "int32"),

      //tf.tensor(emojies_seq),
      tf.scalar(emojies_num,          "int32"),
      tf.scalar(emojies_first_offset, "int32"),

      //tf.tensor(hashtags_seq),
      tf.scalar(hashtags_num,          "int32"),
      tf.scalar(hashtags_first_offset, "int32"),

      tf.scalar(mentions_num,          "int32"),
      tf.scalar(mentions_first_offset, "int32"),
    ],


    /**
     * Labels */
    labels: tf.tensor([views_num, likes_num, comments_num], [3, 1], "int32")
  }
};

const transform = async (data) => {
  /**
   * Inputs */
  const {
    is_private,
    is_verified,
    is_joined_recently = false,
    is_business_account,
    business_category_name,

    edge_follow,
    edge_followed_by,
    edge_owner_to_timeline_media
  } = data;

  /**
   * Posts */
  const posts      = edge_owner_to_timeline_media.edges.map(p => p.node);
  const posts_num  = edge_owner_to_timeline_media.count;
  const posts_proc = await asyncMap(posts, transformPost);

  /**
   * Profile */
  const follows_num   = edge_follow      ? edge_follow.count      : 0;
  const followers_num = edge_followed_by ? edge_followed_by.count : 0;
  // const profile = {
  //   is_new:        tf.scalar(bool2int(is_joined_recently),  "bool"),
  //   // is_private:    tf.scalar(bool2int(is_private),          "bool"),
  //   is_verified:   tf.scalar(bool2int(is_verified),         "bool"),
  //   is_business:   tf.scalar(bool2int(is_business_account), "bool"),

  //   posts_num:     tf.scalar(posts_num,     "int32"),
  //   follows_num:   tf.scalar(follows_num,   "int32"),
  //   followers_num: tf.scalar(followers_num, "int32"),
  // };

  const profile = [
    tf.scalar(bool2int(is_joined_recently),  "bool"),
    // is_private:    tf.scalar(bool2int(is_private),          "bool"),
    tf.scalar(bool2int(is_verified),         "bool"),
    tf.scalar(bool2int(is_business_account), "bool"),
  
    tf.scalar(posts_num,     "int32"),
    tf.scalar(follows_num,   "int32"),
    tf.scalar(followers_num, "int32"),
  ];

  /**
   * Return the prepared profile and posts data. */
  return { profile, posts: posts_proc };
};


/**
 * Training */
const db   = new Database();
const data = new List(db, 'data');
const args = parser.parseArgs();
const {
  limit,
  epochs = 1,
  master = 'localhost'
} = args;


db.connect(`mongodb://${master}`, 'instagram')
  .then(async () => {
    /**
     * Preparing! */
    console.log('Neuron Trainer v1.0');
    const limitParsed  = parseInt(limit);
    const epochsParsed = parseInt(epochs);

    const query = data.query();
    const users = query.find({ type: 'user' });
    users.batchSize(10000);
    if (limitParsed) users.limit(limitParsed);

    /**
     * Execute! */
    const total = limitParsed || await users.count();

    let lastAccuracy = 0;
    for (let i = 0, el = await users.next(); el !== null; el = await users.next(), ++i)
    {
      const { id, is_private } = el.raw;
      if (is_private)
      {
        console.log(`${i} - [${id}]: Skipping private account!`);
        continue;
      }

      const { posts, profile } = await transform(el.raw);
      await asyncEach(posts, async post => {
        const { features, labels } = post;
        const { history } = await model.fit([ ...profile, ...features ], labels, {
          epochs: epochsParsed,
          callbacks: {
            onEpochEnd: (it, log) => {},
            onTrainEnd: () => {}
          }
        });

        tf.dispose({ ...features, ...label });
        console.log(`${i} - [${id}]: ${history.loss[0]}`);
      });

      tf.dispose(profile);
    }

    console.log(`Done! Current accuracy at ${lastAccuracy}`);
    process.exit(0);
  })
  .catch((err) => console.error(err));
