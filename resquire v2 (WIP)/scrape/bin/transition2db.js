/**
 * transition2db.js
 * 
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 * 
 * (c) Laviréo
 */


const progress  = require('cli-progress');
const parse     = require('argparse').ArgumentParser;

const util      = require('../lib/util');
const List      = require('../lib/list');
const Queue     = require('../lib/queue');
const Database  = require('../lib/database');

/**
 * Argument handling */
const parser    = new parse({
  version:     '1.0',
  description: 'Transition2DB'
});

parser.addArgument(['-q', '--queue'],      { help: 'Queue key' });
parser.addArgument(['-m', '--master'],     { help: 'Host IP' });
parser.addArgument(['-c', '--collection'], { help: 'Database collection' })

const args   = parser.parseArgs();
const host   = args.master;
const q_name = args.queue;
const c_name = args.collection;
if (!q_name || q_name.length < 1
  ||!c_name || c_name.length < 1)
{
  throw new Error("queue and collection have to be defined!");
}

/**
 * Database and queue connections */
const db    = new Database();
const list  = new List(db, c_name);
const queue = new Queue({ redis: { host }, name: q_name, removeOnSuccess: true });
queue.onError((err) => {
  console.error(err);
});

console.log(`Transition: '${q_name} (redis)' --> '${c_name} (mongo)'`);

/**
 * ACTION!!! */
db.connect(`mongodb://${host}`, 'instagram')
  .then(async () => {
    const bar       = new progress.Bar({}, progress.Presets.legacy);
    const rawQueue  = queue.handle;
    const jobCounts = await rawQueue.checkHealth();

    let i = 0;
    bar.start(jobCounts.waiting, i);
    queue.process(20, async j => {
      bar.update(++i);

      const data = j.data;
      const q    = data.q;
      if (!q) throw new Error("Couldn't find query string!");

      /**
       * We only add the item
       * when it doesn't exists yet.
       * This is ensured by an index in the database. */
      await list.add({ q, queued_at: [] }).catch(err => console.error(err));
    });
  })
  .catch((err) => console.error(err));
