/**
 * cache.js
 * 
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 * 
 * (c) Laviréo
 */


module.exports = class Cache
{
  constructor (pool, opts)
  {
    if (!pool) throw new Error("Missing pool");

    opts = opts || {};    

    this.pool   = pool;
    this.name   = opts.name   || 'def';
    this.prefix = opts.prefix || 'nc';
    this.rname  = `${this.prefix}:${this.name}`;
  }


  /**
   * Check if key is present
   * 
   * @param  {string} key
   * @return {Promise}
   */
  async has (key)
  {
    return this.pool.get(c => c.hexists(this.rname, key));
  }

  /**
   * Get value by key
   * 
   * @param  {string} key
   * @return {Promise}
   */
  async get (key)
  {
    return this.pool.get(c => c.hget(this.rname, key));
  }

  /**
   * Remove value by key
   * 
   * @param  {string} key
   * @return {Promise}
   */
  async del (key)
  {
    return this.pool.get(c => c.hdel(this.rname, key));
  }

  /**
   * Set value by key
   * 
   * @param  {string} key
   * @param  {any}    value
   * @return {Promise}
   */
  async set (key, value)
  {
    return this.pool.get(c => c.hset(this.rname, key, value));
  }


  /**
   * Get list length
   * 
   * @return {Promise}
   */
  async len ()
  {
    return this.pool.get(c => c.hlen(this.rname));
  }

  /**
   * Clear cache
   *
   * @return {Promise}
   */
  async clear ()
  {
    return this.pool.get(c => c.del(this.rname));
  }
}