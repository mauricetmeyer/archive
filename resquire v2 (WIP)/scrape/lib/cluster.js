/**
 * cluster.js
 * 
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 * 
 * (c) Laviréo
 */


const os      = require('os');
const cluster = require('cluster');


module.exports = class Cluster
{
  constructor (fn, opts)
  {
    /**
     * Let's process the worker */
    if (cluster.isWorker)
    {

    }

    /**
     * Let's set the options */
    this.workers = opts.workers || 1;
    this.running = true;

    for (let i = 0; i < this.workers; i++)
      this.__spawn();
  }


  __kill (worker)
  {
    worker.process.kill();
    worker.disconnect();
    this.workers.delete(worker);
  }

  __spawn ()
  {
    const worker = cluster.fork();
    worker.on('message', msg => this.__handle(msg));
  }

  __listen ()
  {
    const shutdown = () => {
      this.running = false;
      for (var id in cluster.workers)
        this.__kill(cluster.workers[id]);
    };

    process.on('SIGINT', shutdown);
    process.on('SIGTERM', shutdown);
    cluster.on('exit', (worker, code, signal) => {
      if (this.running && !worker.exitedAfterDisconnect) {
        logger.log(`Respawning ${worker.id} after it exited`);
        spawn();
      }
    });
  }

  __handle (msg)
  {

  }
}