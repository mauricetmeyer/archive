/**
 * database.js
 * 
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 * 
 * (c) Laviréo
 */


const mongo = require('mongodb').MongoClient;


module.exports = class Scheduler
{
  constructor ()
  {
    this._db     = null;
    this._handle = null;
  }

  async connect (host, db)
  {
    this._db = db;
    const handle = await mongo.connect(`${host}/${db}`, { useNewUrlParser: true, native_parser: true });
    this._handle = handle.db();
  }


  query (table)
  {
    return this._handle.collection(table);
  }
}