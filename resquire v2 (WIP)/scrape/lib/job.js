/**
 * job.js
 * 
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 * 
 * (c) Laviréo
 */


module.exports = class Job
{
  constructor (pool, key, args)
  {
    this.key  = key;
    this.args = args;
    this.pool = pool;
  }


  /**
   * Get value by key
   * 
   * @param  {string} key
   * @return {Promise}
   */
  async get (key)
  {
    return this.pool.do(async (c) => {

    });
  }

  /**
   * Set value by key
   * 
   * @param  {string} key
   * @param  {any}    value
   * @return {Promise}
   */
  async set (key, value)
  {
    return this.pool.do(async (c) => {

    });
  }


  /**
   * Set progress for job.
   *
   * @param  {integer} val 
   * @return {Promise}
   */
  async progress (val)
  {
    if (!val) return this.get('progress');
    return this.set('progress', val);
  }
}