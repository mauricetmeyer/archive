/**
 * list.js
 * 
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 * 
 * (c) Laviréo
 */


module.exports = class List
{
  constructor (db, name)
  {
    this.db   = db;
    this.name = name || 'nl';
  }


  /**
   * Check if value is present
   * 
   * @param  {string|number}
   * @return {Promise}
   */
  async has (val)
  {
    return this.db.query(this.name).find(val).sort({ when: -1 }).limit(1).next();
  }

  /**
   * Add value to list
   * 
   * @param  {string|number}
   * @return {Promise}
   */
  async add (val)
  {
    val.when = Date.now();
    return this.db.query(this.name).insertOne(val);
  }


  /**
   * Get list length
   * 
   * @return {Promise}
   */
  async len ()
  {
    return this.db.query(this.name).count();
  }

  /**
   * Clear list
   * 
   * @return {Promise}
   */
  async clear ()
  {
    return this.db.query(this.name).drop();
  }


  /**
   * Get values from the list
   * 
   * @param  {Object} query
   * @return {Promise}
   */
  query ()
  {
    return this.db.query(this.name);
  }

  /**
   * Delete a values from the list
   * 
   * @param  {Object} query
   * @return {Promise}
   */
  async delete (query)
  {
    return this.db.query(this.name).delete(query);
  }
}