/**
 * lock.js
 * 
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 * 
 * (c) Laviréo
 */


module.exports = class Lock
{
  constructor (pool)
  {
    this.pool = pool;
  }


  /**
   * Set a lock
   *
   * @param  {string}  key
   * @param  {integer} ttl
   * @return {Promise}
   */
  async lock (key, ttl)
  {
    if (!key) return Promise.reject("Empty key");
    key = this.__makeKey(key);

    /**
     * Set key */
    ttl = ttl || 1000;
    await this.pool.do(c => c.set(key, '', 'PX', ttl, 'NX'));
    return key;
  }

  /**
   * Release lock
   *
   * @param  {string} key
   * @return {Promise}
   */
  async unlock (key)
  {
    if (!key) return Promise.reject("Empty key");
    key = this.__makeKey(key);
    return this.pool.do(c => c.del(key));
  }


  __makeKey (key)
  {
    return key + ':lock';
  }
}