/**
 * model.js
 * 
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 * 
 * (c) Laviréo
 */


const tf = require('@tensorflow/tfjs');
require('@tensorflow/tfjs-node');
tf.setBackend('tensorflow');


class ModelInvalidLayerError extends Error {
  constructor (type)
  {
    super(`Invalid layer type '${type}'`);
  }
}


module.exports = class Model
{
  /**
   * Construct model
   *
   * @param {string} name 
   */
  constructor (name, conf)
  {
    this.name   = name;
    this.handle = tf.sequential();

    /**
     * Create layers and compile */
    this.handle.compile();
  }


  /**
   * Save model
   *
   * @param {string} path
   */
  async save (path)
  {
    return this.handle.save(`file://${path}`);
  }

  /**
   * Load model
   *
   * @param {string} path
   */
  static async load (path)
  {
    return this.handle.load(`file://${path}`);
  }


  /**
   * Predict
   *
   * @param  {any[]} sets
   * @param  {any}   opts
   * @return {Promise}
   */
  async train (sets, opts)
  {
    this.handle.fit(ins, outs, {
      onTrainEnd()
    });
  }

  /**
   * Predict
   *
   * @param  {any} input
   * @param  {any} opts
   * @return {Promise}
   */
  predict (input, opts)
  {
    return this.handle.predict(input);
  }


  /**
   * Add layer */
  addLayer (type, opts)
  {
    const layer = tf.layers[type];
    if (!layer) throw new ModelInvalidLayerError(type);
    this.handle.add(layer(opts));
  }

  /**
   * Add dense layer
   * 
   * @param {any} opts
   */
  addDenseLayer (opts)
  {
    this.addLayer('dense', opts);
  }

  /**
   * Add dropout layer
   * 
   * @param {any} opts
   */
  addDropoutLayer (opts)
  {
    this.addLayer('dropout', opts);
  }
}