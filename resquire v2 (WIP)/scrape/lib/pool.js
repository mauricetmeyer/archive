/**
 * pool.js
 * 
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 * 
 * (c) Laviréo
 */


const Emitter      = require("event-kit").Emitter;
const TimedPromise = require("./timed_promise");
const util         = require('./util');


module.exports = class Pool
{
  /**
   * 
   * @param {any} fns 
   * @param {any} opts 
   */
  constructor (fns, opts)
  {
    /**
     * Require functions */
    if (!fns.create || !fns.destroy)
      throw new Error("Missing constructor functions");

    this.fns       = fns;
    this.num       = 0;

    this.all       = new Set;
    this.waiting   = [];
    this.available = [];
    this.emitter   = new Emitter;

    /**
     * Optional options */
    this.min       = opts.min       || 1;
    this.max       = opts.max       || 1;
    this.max_usage = opts.max_usage || 0;

    for (let i = 0; i < this.max; i++)
      this.create();
  }


  /**
   * Get connection from pool.
   * 
   * @param   {Function} fn
   * @returns {Promise}
   */
  async do (fn, timeout)
  {
    /**
     * Default timeout */
    timeout  = timeout || 0;
    let conn = null;

    try
    {
      /**
       * We do this promise thing here
       * to ensure that we actually work with a promise
       * in case a function forgets about it! */
      conn         = await this.__checkout(timeout);
      const prom   = Promise.resolve(fn(conn));
      const result = await prom;

      /**
       * Connection was released...
       * let's put it back */
      this.__checkin(conn);
      return result;
    }
    catch (err)
    {
      /**
       * Release connection and emit
       * an error event. */
      if (conn) this.__checkin(conn);
      this.emitter.emit('error', err);
      return;
    }
  }

  /**
   * Shutdown pool
   * 
   * @return {Promise}
   */
  async shutdown ()
  {

  }


  /**
   * Register event handler for
   * error events.
   * 
   * @param  {Function} fn
   * @return {Disposable}
   */
  onError (fn)
  {
    return this.emitter.on('error', fn);
  }

  /**
   * Register event handler for
   * reconnect events.
   * 
   * @param  {Function} fn
   * @return {Disposable}
   */
  onDidReconnect (fn)
  {
    return this.emitter.on('did-reconnect', fn);
  }

  /**
   * Register event handler for
   * disconnect events.
   * 
   * @param  {Function} fn
   * @return {Disposable}
   */
  onDidDisconnect (fn)
  {
    return this.emitter.on('did-disconnect', fn);
  }


  /**
   * Return connection
   *
   * @param {any} conn
   */
  async __checkin (conn)
  {
    this.__dispatchToWaiting(conn);
  }

  /**
   * Get connection
   *
   * @return {Promise}
   */
  async __checkout (timeout)
  {
    return TimedPromise((res, rej) => {
      this.waiting.push(res);
    }, timeout);
  }


  __dispatchToWaiting (resource)
  {
    const waiting = this.waiting.shift();
    if (waiting)
    {
      this.available.push(resource);
      return;
    }

    /**
     * Resolve waiting promise with resource */
    waiting(resource);
  }


  async create ()
  {
    try
    {
      const item = await this.fns.create();
      if (item)
      {
        this.all.add(item);
        this.available.push(item);
      }
    } catch (err) {}
  }
}