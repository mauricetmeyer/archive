/**
 * queue.js
 * 
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 * 
 * (c) Laviréo
 */


const queue = require("bee-queue");


module.exports = class Queue
{
  constructor (opts)
  {
    opts = opts || {};
    const { name, ...args } = opts;

    this.db     = null;
    this.name   = name || 'nq';
    this.handle = new queue(opts.name, args);
  }


  /**
   * Attach a new job handler
   *
   * @param  {Function} fn 
   * @return {Disposable}
   */
  onJob (fn)
  {
    this.handle.on('start', fn);
  }

  /**
   * Attach a new error handler
   *
   * @param  {Function} fn 
   * @return {Disposable}
   */
  onError (fn)
  {
    this.handle.on('fail', fn);
  }

  /**
   * Attach a new ready handler
   *
   * @param  {Function} fn 
   * @return {Disposable}
   */
  onReady (fn)
  {
    this.handle.on('ready', fn);
  }


  /**
   * Start processing jobs.
   *
   * @param  {Function} fn 
   * @return {Promise}
   */
  async process (fnOrAmount, fn)
  {
    return this.handle.process(fnOrAmount, fn);
  }


  /**
   * Enqueue job.
   * 
   * @param  {string} key
   * @param  {object} args
   * @param  {object} opts
   * @return {Promise}
   */
  async enqueue (args, opts)
  {
    opts = opts || {};
    const job = this.handle.createJob(args);
    if (opts.at) job.delayUntil(opts.at);
    return job.save();
  }

  /**
   * Enqueue job to run in {secs} seconds.
   * 
   * @param  {integer} secs
   * @param  {string}  key
   * @param  {object}  args
   * @param  {object}  opts
   * @return {Promise}
   */
  async enqueueIn (secs, args, opts)
  {
    const now = Date.now();
    return this.enqueue(args, { at: new Date(now + secs) });
  }

  /**
   * Push a new job to run at {timestamp}.
   * 
   * @param  {integer} timestamp
   * @param  {string}  key
   * @param  {object}  args
   * @param  {object}  opts
   * @return {Promise}
   */
  async enqueueAt (timestamp, args, opts)
  {
    return this.enqueue(args, { at: new Date(timestamp) });
  }


  /**
   * Get number of current enqueued jobs
   * 
   * @return {Promise}
   */
  async length ()
  {
    return 0;
  }


  /**
   * Clear queue
   * 
   * @return {Promise}
   */
  async clear ()
  {
    return this.handle.purge();
  }


  /**
   * Shutdown queue */
  async shutdown ()
  {
    return this.handle.close();
  }
}
