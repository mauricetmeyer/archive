/**
 * redis.js
 * 
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 * 
 * (c) Laviréo
 */


const promisify   = require("util").promisify;
const RedisClient = require("redis").RedisClient;
const cmds        = require("redis-commands").list;

const proto = RedisClient.prototype;
cmds.forEach((cmd) => {
  if (cmd !== 'multi') proto[cmd] = promisify(proto[cmd]);
});

module.exports = redis;