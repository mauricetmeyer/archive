/**
 * scraper.js
 * 
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 * 
 * (c) Laviréo
 */


const Emitter    = require('event-kit').Emitter;
//const driver     = require('puppeteer');
const cheer      = require('cheerio');
const axios      = require('axios');
const proxyagent = require('https-proxy-agent');

const util       = require('./util');
const random     = require('../lib/util').random;
const Pool       = require('./pool');
const Queue      = require('./queue');


const goto = async function (uri, opts)
{
  opts            = opts || {};
  const { agent, proxy, headers = {} } = opts;

  /**
   * Send it! */
  return axios.get(uri, {
    httpsAgent: proxy,
    headers: {
      'user-agent': agent,
      ...headers
    }
  });
}


module.exports = class Scraper
{
  constructor(fn, opts)
  {
    this.fn             = fn;
    this.shouldProcess  = opts.shouldProcess;
    this.transformQuery = opts.transformQuery;
    this.max            = opts.max   || 1;
    this.delay_min      = opts.delay_min || 0;
    this.delay_max      = opts.delay_max || 0;
    this.agent          = opts.agent;
    this.proxy          = opts.proxy ? new proxyagent(opts.proxy) : undefined;
    this.queue          = opts.queue || new Queue();
    this.emitter        = new Emitter;
    this.instance       = null;

    /**
     * Create page pool */
    // this.pool = new Pool({
    //   create: async () => {
    //     const page = await this.instance.newPage();
    //     if (this.agent) page.setUserAgent(this.agent);

    //     /**
    //      * Disable image loading */
    //     await page.setRequestInterception(true);
    //     page.on('request', req => {
    //       if (req.resourceType() === 'image')
    //         req.abort();
    //       else
    //         req.continue();
    //     });
    //   },

    //   destroy: (p) => p.close(),
    //   validate: (p) => Promise.resolve(true)
    // }, { max: this.max });
  }


  /**
   * Start scraper
   *
   * @return {Promise}
   */
  async start()
  {
    if (this.__started) return this;

    /**
     * Only start of there is no instance */
    // const args = ['--mute-audio'];
    // if (this.proxy) args.push(`--proxy-server=${this.proxy}`)
    // this.instance  = await driver.launch({ args });
    // const page = await this.instance.newPage();
    // if (this.agent) page.setUserAgent(this.agent);

    // /**
    //  * Disable image loading */
    // await page.setRequestInterception(true);
    // page.on('request', req => {
    //   if (req.resourceType() === 'image') req.abort();
    //   else req.continue();
    // });

    // this.page = page;

    /**
     * Process jobs */
    this.queue.process(async j => {
      return this.__request(j);
    });

    this.__started = true;
    return this;
  }

  /**
   * Shutdown scraper
   * 
   * @return {Promise}
   */
  async shutdown()
  {
    await this.queue.shutdown();
    this.emitter.dispose();
    this.instance.close();
    this.__started = false;
  }


  /**
   * Attach handler to
   * error events.
   * 
   * @param  {Function} fn
   * @return {Disposable}
   */
  onError(fn)
  {
    return this.emitter.on('error', fn);
  }

  /**
   * Attach handler to
   * disconnect events.
   * 
   * @param  {Function} fn
   * @return {Disposable}
   */
  onDisconnect(fn)
  {
    return this.emitter.on('error', fn);
  }


  async __request (job)
  {
    const args  = job.data;
    const proxy = this.proxy;
    const agent = this.agent;

    let   error = null;
    let   query = args.q;

    /**
     * Transform query */
    if (this.transformQuery)
      query = this.transformQuery(query);

    /**
     * Did we provide a ``shouldProcess`` method? */
    if (this.shouldProcess && !this.shouldProcess(query))
    {
      console.log('SKIP: ' + query);
      await this.queue.enqueue({ q: args.q });
      return;
    }

    let page = null;
    try
    {
      const req = await goto(query, { proxy, agent });
      if (req.status !== 200) return Promise.reject("Failed to scrape url: " + query);
      page = cheer.load(req.data);
      if (!page) return Promise.reject("Failed to parse url: " + query);
    }
    catch (error)
    {
      this.emitter.emit('error', error);
      return Promise.reject(error);
    }

    //const p   = this.page;
    //await p.goto(args.url).catch(err => error = err);
    // if (error)
    // {
    //   this.emitter.emit('error', error);
    //   await this.shutdown();
    //   return Promise.reject(error);
    // }

    /**
     * Call function to
     * start processing */
    const status = await this.fn(job, page, args.url).catch(err => error = err);
    if (error)
    {
      //this.emitter.emit('error', error);
      return Promise.reject(error);
    }

    if (status === 'no_wait') return;
    const delay = random(this.delay_min, this.delay_max);
    if (delay) await util.sleep(delay);
  }
}
