/**
 * timed_promise.js
 * 
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 * 
 * (c) Laviréo
 */


module.exports = class TimePromise extends Promise
{
  constructor (fn, timeout)
  {
    timeout = timeout || 0;
    super((res, rej) => {
      fn(res, rej);
      if (timeout > 0)
        setTimeout(() => rej(new Error(`Waited ${timeout} ms`)), timeout);
    });
  }
}