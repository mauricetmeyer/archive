/**
 * tokenizer.js
 * 
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 * 
 * (c) Laviréo
 */


module.exports = class Pool
{
  tokenize (text)
  {
    const replaced = this.replace(text);
    const tokens   = this.trim(replaced.split(/\W+/));
    return tokens;
  }

  replace (text)
  {
    let result = text.replace(/n't([ ,:;.!?]|$)/gi,   ' not ');
    result     = result.replace(/'s([ ,:;.!?]|$)/gi,  ' is ');
    result     = result.replace(/'re([ ,:;.!?]|$)/gi, ' are ');
    result     = result.replace(/'ve([ ,:;.!?]|$)/gi, ' have ');
    result     = result.replace(/'m([ ,:;.!?]|$)/gi,  ' am ');
    result     = result.replace(/'d([ ,:;.!?]|$)/gi,  ' had ');
    return result;
  }

  trim (tokens)
  {
    while (tokens[tokens.length - 1] === '')
      tokens.pop();

    while (tokens[0] === '')
      tokens.shift();

    return tokens;
  }
}