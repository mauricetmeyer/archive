/**
 * util.js
 * 
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 * 
 * (c) Laviréo
 */


module.exports = {
  /**
   * Sleep promise for {ms} ms
   * 
   * @param  {integer} ms
   * @return {Promise}
   */
  sleep: function(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  },

  /**
   * Return random integer between min/max
   * 
   * @param  {integer} min
   * @param  {integer} max
   * @return {integer}
   */
  random: function (min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min
  },

  /**
   * Shuffles array in place.
   *
   * @param  {Array} array
   * @return {Array}
   */
  shuffle: function (arr)
  {
    for (let i = arr.length - 1; i > 0; i--)
    {
      const rnd = Math.floor(Math.random() * (i + 1));
      const tmp = arr[i];
      arr[i]    = arr[rnd];
      arr[rnd]  = tmp;
    }

    return arr;
  }
}