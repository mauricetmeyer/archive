/**
 * worker.js
 * 
 * Author: Maurice T. Meyer
 * E-Mail: maurice@lavireo.com
 * 
 * (c) Laviréo
 */


const uuid    = require('uuid');
const Emitter = require("event-kit").Emitter;


module.exports = class Worker
{
  constructor (pool, key)
  {
    this.job     = null;
    this.key     = key;
    this.pool    = pool;
    this.uuid    = Buffer.alloc(16);
    this.down    = false;
    this.done    = false;
    this.rname   = `${key}:workers`;
    this.jrname  = `${key}:jobs`;
    this.lrname  = `${key}:processing`;
    this.emitter = new Emitter;

    /**
     * Generate UUID */
    uuid.v1(this.uuid);
  }


  /**
   * Start processing jobs
   * 
   * @return {Promise}
   */
  async start (fn)
  {
    /**
     * Register worker in redis
     * so that we can track it */
    await this.pool.do(c => c.hsetnx(this.rname, this.uuid));
    return this.__run(fn);
  }

  /**
   * Shutdown the worker process
   * 
   * @return {Promise}
   */
  async shutdown ()
  {
    this.done = true;
    await this.pool.do(c => c.hdel(this.rname, this.uuid));
    this.emitter.dispose();
  }


  /**
     * Worker loop */
  async __run (fn)
  {
    try
    {
      const job_id = await this.__fetch();
      if (this.down)
      {
        const msg = `Redis is online, ${Date.now() - this.down} sec downtime`;
        this.down = null;
      }

      if (job_id) await this.__process(job, fn);
      this.pool.do(c => c.lrem(this.jrname, 1, job_id));
    }
    catch (err)
    {

    }

    /**
     * Continue with the next item */
    if (!this.done) setImmediate(() => this.__run(fn));
  }

  async __fetch ()
  {
    try
    {
      return this.pool.do(c => c.brpoplpush(this.jrname, this.lrname, 0));
    }
    catch (err)
    {
      if (!this.down)
      {
        /**
         * Error fetching job */
        this.down = Date.now();
      }
    }
  }

  async __process (job, fn)
  {
    /**
     * We do this promise thing here
     * to ensure that we actually work with a promise
     * in case a function forgets about it! */
    return Promise.resolve(fn(job));
  }
}